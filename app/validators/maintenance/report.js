const { check } = require('express-validator/check');
const moment = require('moment');

module.exports = {
    rules: [
        check('offset').isInt({ min: 0 }),
        check('limit').isInt({ min: 1 }),
        check('propertyId').custom(value => {

            switch (value) {
                case null:
                case undefined:
                    return true;
            }

            return check('propertyId').isInt({ min: 1 });
        }),
        check('userId').custom(value => {

            switch (value) {
                case null:
                case undefined:
                    return true;
            }

            return check('userId').isInt({ min: 1 });
        }),
        check('startDate').custom(value => {
            switch (value) {
                case null:
                case undefined:
                    return true;
            }
            return moment(value).isValid();
        }),
        check('endDate').custom(value => {
            switch (value) {
                case null:
                case undefined:
                    return true;
            }
            return moment(value).isValid();

        }),
    ],
};
