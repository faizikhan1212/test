const { check } = require('express-validator/check');
const countSymbAfterComma = require('../../helpers/regex/count-symb-after-comma');

module.exports = {
    rules: [
        check('maintenanceId').isInt({ min: 1 }),
        check('budget').custom(value => {
            switch (value) {
                case null:
                case undefined: {
                    return true;
                }
            }
            return value >= 1 && value <= 1000000000000 && countSymbAfterComma(value) <= 2;
        }),
        check('vendorId').isInt({ min: 1 }),
    ],
};
