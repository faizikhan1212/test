const { check } = require('express-validator/check');
const moment = require('moment');
const countSymbAfterComma = require('../../helpers/regex/count-symb-after-comma');

module.exports = {
    rules: [
        check('id').isInt({ min: 1 }),
        check('status').custom(value => {
            switch (value) {
                case 'Not Started':
                case 'Completed':
                case 'Cancelled':
                case 'In Progress': {
                    return true;
                }
            }
        }),
        check('vendorId').custom(value => {
            switch (value) {
                case null:
                case undefined: {
                    return true;
                }
            }
            return value >= 1;
        }),
        check('budget').custom(value => {
            switch (value) {
                case null:
                case undefined: {
                    return true;
                }
            }
            return value >= 1 && value <= 1000000000000 && countSymbAfterComma(value) <= 2;
        }),
        check('dateScheduled').custom(value => {
            switch (value) {
                case null:
                case undefined: {
                    return true;
                }
            }
            return moment(value).isValid();
        }),
        check('dateCompleted').custom(value => {
            switch (value) {
                case null:
                case undefined: {
                    return true;
                }
            }
            return moment(value).isValid();
        }),
        check('estimate').custom(value => {
            switch (value) {
                case null:
                case undefined: {
                    return true;
                }
            }
            return value >= 1 && value <= 1000000000000 && countSymbAfterComma(value) <= 2;
        }),
        check('cost').custom(value => {
            switch (value) {
                case null:
                case undefined: {
                    return true;
                }
            }

            return value >= 1 && value <= 1000000000000 && countSymbAfterComma(value) <= 2;
        }),
    ],
};
