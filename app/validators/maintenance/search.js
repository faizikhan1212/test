const { check } = require('express-validator/check');

module.exports = {
    rules: [
        check('q').isLength({
            min: 1,
            max: 255,
        }),
        check('limit').isInt({ min: 1 }),
        check('offset').isInt({ min: 0 }),
        check('filter').custom(value => {
            switch (value) {
                case null:
                case undefined: {
                    return true;
                }
            }
            const filter = JSON.parse(value);
            return (
                Object.keys(filter).every(el => {
                    return ['status'].includes(el);
                }) &&
        [
            'Not Started',
            'In Progress',
            'Completed',
            'Cancelled',
        ].includes(filter['status'])
            );
        }),
    ],
};
