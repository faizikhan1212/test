const create = require('./create');
const update = require('./update');
const show = require('./show');

module.exports = {
    create,
    update,
    show,
};