const { check } = require('express-validator/check');
const { User } = require('../../../database/models');
const Validator = require('./duplicateCustomValidator');
const { allowedData } = require('../../../config');

module.exports = {
    rules: [
        check('list').custom(async (value) => {
            const promises = [];
            for (let i = 0; i < value.length; i++) {
                const p = new Promise(async (resolve, reject) => {
                    // showing
                    if (value[i].showingAgent !== undefined && value[i].showingAgent !== null && typeof value[i].showingAgent !== 'number' || (value[i].showingAgent && value[i].showingAgent < 1)) {
                        return reject(new Error('showingAgent'));
                    }

                    if (value[i].showingAgent) {
                        const user = await User.findByPk(value[i].showingAgent);

                        if (!user) {
                            return reject(new Error('showingAgent'));
                        }
                    }

                    const validator = new Validator.DuplicateCustomValidator(reject);

                    validator.validate(
                        'Unit Rent', value[i].rent,
                        {
                            name: 'number',
                            required: true,
                            min: 0,
                            max: 10000000,
                        },
                    );

                    validator.validate(
                        'Unit Deposit', value[i].deposit,
                        {
                            name: 'number',
                            required: false,
                            min: 0,
                            max: 10000000,
                        },
                    );

                    validator.validate(
                        'Unit status', value[i].status,
                        {
                            name: 'allowedList',
                            required: true,
                            allowedList: allowedData.unitStatus,
                        },
                    );

                    validator.validate(
                        'Amenities', value[i].amenities,
                        {
                            name: 'arrayOfString',
                            required: false,
                        },
                    );

                    validator.validate(
                        'Utilities', value[i].utilities,
                        {
                            name: 'arrayOfString',
                            required: false,
                        },
                    );

                    validator.validate(
                        'Unit Listing Title', value[i].title,
                        {
                            name: 'string',
                            required: false,
                            min: 2,
                            max: 255,
                        },
                    );

                    validator.validate(
                        'Unit Description', value[i].description,
                        {
                            name: 'string',
                            required: false,
                            min: 10,
                            max: 5000,
                        },
                    );

                    validator.validate(
                        'Parking', value[i].parking,
                        {
                            name: 'boolean',
                            required: true,
                        },
                    );

                    validator.validate(
                        'Parking Cost', value[i].parkingCost,
                        {
                            name: 'number',
                            required: false,
                            min: 0,
                            max: 100000000,
                        },
                    );

                    validator.validate(
                        'Parking Covered', value[i].parkingCovered,
                        {
                            name: 'boolean',
                            required: false,
                        },
                    );

                    validator.validate(
                        'Parking Secured', value[i].parkingSecured,
                        {
                            name: 'boolean',
                            required: false,
                        },
                    );

                    validator.validate(
                        'Pet friendly', value[i].petFriendly,
                        {
                            name: 'boolean',
                            required: true,
                        },
                    );

                    validator.validate(
                        'Pet Policy', value[i].petPolicy,
                        {
                            name: 'string',
                            required: false,
                            min: 2,
                            max: 5000,
                        },
                    );

                    validator.validate(
                        'Application Fee', value[i].applicationFee,
                        {
                            name: 'boolean',
                            required: true,
                        },
                    );

                    validator.validate(
                        'appFeeNonRefundable', value[i].appFeeNonRefundable,
                        {
                            name: 'boolean',
                            required: false,
                        },
                    );

                    validator.validate(
                        'Application Fee Cost', value[i].applicationFeeCost,
                        {
                            name: 'number',
                            required: false,
                            min: 0,
                            max: 100000000,
                        },
                    );

                    validator.validate(
                        'Laundry Shared', value[i].sharedLaundry,
                        {
                            name: 'boolean',
                            required: true,
                        },
                    );

                    validator.validate(
                        'Bath Shared', value[i].sharedWashroomBath,
                        {
                            name: 'boolean',
                            required: true,
                        },
                    );

                    validator.validate(
                        'Unit Shared', value[i].unitShared,
                        {
                            name: 'boolean',
                            required: true,
                        },
                    );

                    validator.validate(
                        'Number of people sharing this unit', value[i].unitSharedPeople,
                        {
                            name: 'number',
                            required: false,
                            min: 0,
                            max: 1000000000,
                        },
                    );

                    validator.validate(
                        'Beds', value[i].bed,
                        {
                            name: 'number',
                            required: true,
                            min: 0,
                            max: 10000000,
                        },
                    );

                    validator.validate(
                        'Beds', value[i].bath,
                        {
                            name: 'number',
                            required: true,
                            min: 0,
                            max: 10000000,
                        },
                    );

                    validator.validate(
                        'Unit SqFt', value[i].sqFt,
                        {
                            name: 'number',
                            required: false,
                            min: 0,
                            max: 10000000,
                        },
                    );

                    validator.validate(
                        'Lease Term', value[i].leaseTerm,
                        {
                            name: 'allowedList',
                            required: false,
                            allowedList: allowedData.leaseTerm,
                        },
                    );

                    validator.validate(
                        'Email', value[i].email,
                        {
                            name: 'email',
                            required: false,
                        },
                    );

                    validator.validate(
                        'Listing Phone', value[i].phone,
                        {
                            name: 'phone',
                            required: false,
                        },
                    );

                    validator.validate(
                        'Unit', value[i].unit,
                        {
                            name: 'number',
                            required: true,
                            min: 0,
                            max: 10000000,
                        },
                    );

                    validator.validate(
                        'Property Address', value[i].address,
                        {
                            name: 'string',
                            required: true,
                            min: 2,
                            max: 255,
                        },
                    );

                    validator.validate(
                        'Smoking', value[i].noSmoking,
                        {
                            name: 'boolean',
                            required: true,
                        },
                    );

                    validator.validate(
                        'Move In Date', value[i].moveInDate,
                        {
                            name: 'date',
                            required: true,
                        },
                    );

                    validator.validate(
                        'Unit Utility Bill', value[i].utilityBill,
                        {
                            name: 'number',
                            required: false,
                            min: 0,
                            max: 1000000000,
                        },
                    );

                    validator.validate(
                        'Application Process', value[i].applicationProcess,
                        {
                            name: 'string',
                            required: false,
                            min: 10,
                            max: 5000,
                        },
                    );

                    validator.validate(
                        'Appliances', value[i].appliances,
                        {
                            name: 'arrayOfString',
                            required: false,
                        },
                    );

                    return resolve();
                });

                promises.push(p);

            }

            try {
                await Promise.all(promises);
                return true;
            } catch (err) {
                console.log(err);
                return false;
            }
        }),

    ],
};
