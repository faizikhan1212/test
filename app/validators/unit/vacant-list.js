const { check } = require('express-validator/check');

module.exports = {
    rules: [
        check('offset').isInt({ min: 0 }),
        check('limit').isInt({ min: 1 }),
    ],
};
