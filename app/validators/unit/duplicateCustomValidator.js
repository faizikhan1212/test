const isEmail = require('../../helpers/regex/is-email');
const isPhone = require('../../helpers/regex/is-mobile-phone');
const moment = require('moment');

class DuplicateCustomValidator {

    constructor(reject) {
        this.reject = reject;
    }


    validate(fieldName, value, type) {
        switch (type.name) {
            case 'boolean': {
                return this.booleanValidate(value, type, fieldName);
            }
            case 'allowedList': {
                return this.allowedListValidate(value, type, fieldName, type.allowedList);
            }
            case 'string': {
                return this.stringValidate(value, type, fieldName);
            }
            case 'number': {
                return this.numberValidate(value, type, fieldName);
            }
            case 'phone': {
                return this.phoneValidate(value, type, fieldName);
            }
            case 'email': {
                return this.emailValidate(value, type, fieldName);
            }
            case 'allowedMultiplyList': {
                return this.allowedMultiplyList(value, type, fieldName, type.allowedList);
            }
            case 'date': {
                return this.dateValidator(value, type, fieldName);
            }
            case 'arrayOfString': {
                return this.arrayOfStringValidate(value, type, fieldName);
            }
        }
    }


    booleanValidate(value, type, fieldName) {

        let extraConditionForRequired = false;

        if (!type.required && !value) {
            extraConditionForRequired = !value;
        }

        if (extraConditionForRequired || typeof(value) === 'boolean') {
            return true;
        } else {
            return this.reject(new Error(fieldName));
        }
    }


    stringValidate(value, type, fieldName) {
        let extraConditionForRequired = false;
        let extraMinCondition = false;
        let extraMaxCondition = false;

        if (!type.required && !value) {
            extraConditionForRequired = !value;
        }

        if (type.hasOwnProperty('min')) {
            extraMinCondition = value && value.length >= type.min;
        }

        if (type.hasOwnProperty('max')) {
            extraMaxCondition = value && value.length <= type.max;
        }

        if (extraConditionForRequired || (extraMinCondition && extraMaxCondition)) {
            return true;
        } else {
            return this.reject(new Error(fieldName));

        }
    }

    numberValidate(value, type, fieldName) {
        let extraConditionForRequired = false;
        let extraMinCondition = false;
        let extraMaxCondition = false;

        if (!type.required && !value) {
            extraConditionForRequired = !value;
        }

        if (type.hasOwnProperty('min')) {
            extraMinCondition = value >= type.min;
        }

        if (type.hasOwnProperty('max')) {
            extraMaxCondition = value <= type.max;
        }

        if (extraConditionForRequired || (extraMinCondition && extraMaxCondition)) {
            return true;
        } else {
            return this.reject(new Error(fieldName));

        }
    }


    allowedListValidate(value, type, fieldName, allowedList) {

        let extraConditionForRequired = false;

        if (!type.required && !value) {
            extraConditionForRequired = !value;
        }

        if (extraConditionForRequired || allowedList.includes(value)) {
            return true;
        } else {
            return this.reject(new Error(fieldName));

        }
    }

    allowedMultiplyList(value, type, fieldName, allowedList) {
        let extraConditionForRequired = false;

        if (!type.required && !value) {
            extraConditionForRequired = !value;
        }


        if (extraConditionForRequired || value.every(utility => allowedList.includes(utility.trim()))) {
            return true;
        } else {
            return this.reject(new Error(fieldName));

        }
    }

    phoneValidate(value, type, fieldName) {
        let extraConditionForRequired = false;

        if (!type.required && !value) {
            extraConditionForRequired = !value;
        }

    if (extraConditionForRequired || (value && isPhone(value.replace(/\s/g, '').replace(/[^\x00-\x7F]/g, '')))) { //eslint-disable-line
            return true;
        } else {
            return this.reject(new Error(fieldName));

        }
    }

    emailValidate(value, type, fieldName) {
        let extraConditionForRequired = false;

        if (!type.required && !value) {
            extraConditionForRequired = !value;
        }

        if (extraConditionForRequired || isEmail(value)) {
            return true;
        } else {
            return this.reject(new Error(fieldName));

        }
    }

    dateValidator(value, type, fieldName) {
        let extraConditionForRequired = false;

        if (!type.required && !value) {
            extraConditionForRequired = !value;
        }

        if (extraConditionForRequired || moment(value).isValid()) {
            return true;
        } else {
            return this.reject(new Error(fieldName));
        }
    }

    regexValidate(value, type, fieldName) {
        let extraConditionForRequired = false;

        if (!type.required && !value) {
            extraConditionForRequired = !value;
        }

        if (extraConditionForRequired || type.regex.test(value)) {
            return true;
        } else {
            return this.reject(new Error(fieldName));

        }
    }

    arrayOfStringValidate(value, type, fieldName) {
        let extraConditionForRequired = false;

        if (!type.required && !value) {
            extraConditionForRequired = !value;
        }

        if (extraConditionForRequired || (value && Array.isArray(value) && value.every((i) => typeof i === 'string' || typeof i === 'number'))) {
            return true;
        } else {
            return this.reject(new Error(fieldName));

        }
    }

}

module.exports.DuplicateCustomValidator = DuplicateCustomValidator;
