const { check } = require('express-validator/check');
const { isMobilePhone, isEmail } = require('../../helpers/regex');
const { User } = require('../../../database/models');
const moment = require('moment');
const { allowedData } = require('../../../config');


module.exports = {
    rules: [
        check('showingAgent').custom(async value => {
            if (value && value < 1) {
                return false;
            }

            const user = await User.findByPk(value);

            return value === null || value === undefined || !!user;
        }),
        check('rent').isInt({ min: 0 }),
        check('deposit').custom(value => {
            return value === undefined || value === null || value >= 0;
        }),
        check('status').custom(value => {
            return allowedData.unitStatus.includes(value);
        }),
        check('amenities')
            .custom(value => {
                return value === undefined || value === null || value && Array.isArray(value) && value.every((i) => typeof i === 'string' || typeof i === 'number');
            }),
        check('utilities')
            .custom(value => {
                return value === undefined || value === null || value && Array.isArray(value) && value.every((i) => typeof i === 'string' || typeof i === 'number');
            }),
        check('title').custom(value => {
            return value === null || value === undefined || (value && value.length >= 2 && value.length <= 255);
        }),
        check('description').custom(value => {
            return value === null || value === undefined || (value && value.length >= 10 && value.length <= 5000);
        }),
        check('parking').isBoolean(),
        check('parkingCost').custom((value) => {

            return value === null || value === undefined || +value >= 0;

        }),
        check('parkingCovered').custom((value) => {

            return value === null || value === undefined || value === true || value === false;

        }),
        check('parkingSecured').custom((value) => {

            return value === null || value === undefined || value === true || value === false;

        }),
        check('petFriendly').isBoolean(),
        check('petPolicy').custom((value) => {

            return value === null || value === undefined || (typeof value === 'string' && value.length >= 2 && value.length <= 5000);

        }),
        check('applicationFee').isBoolean(),
        check('appFeeNonRefundable').custom((value) => {

            return value === null || value === undefined || value === true || value === false;

        }),
        check('applicationFeeCost').custom((value) => {

            return value === null || value === undefined || +value >= 0;

        }),
        check('sharedLaundry').isBoolean(),
        check('sharedWashroomBath').isBoolean(),
        check('unitShared').isBoolean(),
        check('unitSharedPeople').custom((value) => {
            return value === null || value === undefined || +value > 0;
        }),
        check('bed').isInt({ min: 0 }),
        check('bath').isInt({ min: 0 }),
        check('sqFt').custom((value) => {
            return value === null || value === undefined || +value > 0;
        }),
        check('leaseTerm')
            .custom(value => {
                return value === undefined || value === null || allowedData.leaseTerm.includes(value);
            }),
        check('email').custom(value => {
            return value === null || value === undefined || isEmail(value);
        }),
        check('phone').custom(value => value === null || value === undefined || isMobilePhone(value)),
        check('unit').isLength({
            min: 1,
            max: 10,
        }),
        check('address').isLength({
            min: 2,
            max: 255,
        }),
        check('noSmoking').isBoolean(),
        check('moveInDate').custom(value => {
            return value && moment(value).isValid();
        }),
        check('utilityBill').custom(value => {
            return value === undefined || value === null || value >= 0;
        }),
        check('applicationProcess').custom(value => {
            return value === undefined || value === null || (value && value.length >= 10 && value.length <= 5000);
        }),
        check('appliances').custom(value => {
            return value === undefined || value === null || value && Array.isArray(value) && value.every((i) => typeof i === 'string' || typeof i === 'number');
        }),
    ],
};
