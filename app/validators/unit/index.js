const search = require('./search');
const list = require('./list');
const create = require('./create');
const update = require('./update');
const destroy = require('./destroy');
const changeStatus = require('./change-status');
const vacantList = require('./vacant-list');
const vacantSearch = require('./vacant-search');
const changeProperty = require('./change-property');
const show = require('./show');
const duplicate = require('./duplicate');

module.exports = {
    search,
    list,
    create,
    duplicate,
    update,
    destroy,
    changeStatus,
    vacantList,
    vacantSearch,
    changeProperty,
    show,
};
