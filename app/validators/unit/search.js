const { check } = require('express-validator/check');

module.exports = {
    rules: [
        check('q').isLength({
            min: 1,
            max: 255,
        }),
        check('limit').isInt({ min: 1 }),
        check('offset').isInt({ min: 0 }),
        check('propertyId').isInt({ min: 1 }),
    ],
};
