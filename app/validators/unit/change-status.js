const { check } = require('express-validator/check');

module.exports = {
    rules: [
        check('id').isInt({ min: 1 }),
        check('status').custom(value => {
            switch (value) {
                case 'Vacant':
                case 'Rented':
                case 'Sold': {
                    return true;
                }
            }
        }),
    ],
};
