const Validator = require('./customValidator');
const { allowedData } = require('../../../config');

module.exports = (row, rowCounter) => {

    const validator = new Validator.Validator();

    if (rowCounter === 1) {
        const headerValidate = Validator.Validator.checkHeader(row, [
            'Property Type *',
            'Property name *',
            'Property Address *',
            'Lease Term *',
            'Application Fee *',
            'Application Fee Cost (optional)',
            'Application Fee Non-refundable (optional)',
            'Parking *',
            'Parking Monthly fee (optional)',
            'Parking Covered (optional)',
            'Parking Secured (optional)',
            'Property Pet friendly *',
            'Pet Policy (optional)',
            'Smoking Allowed *',
            'Amenities (optional)',
            'Utilities (optional)',
            'Unit Number *',
            'Unit Rent *',
            'Unit Deposit (optional)',
            'Unit Utility Bill (optional)',
            'Move In Date *',
            'Beds *',
            'Bath *',
            'Unit SqFt (optional)',
            'Unit status *',
            'Unit Shared *',
            'Number of people sharing this unit (optional)',
            'Appliances (optional)',
            'Laundry Shared *',
            'Bath Shared *',
            'Unit Listing Title (optional)',
            'Unit Description (optional)',
            'Application Process (optional)',
            'Listing Email (optional)',
            'Listing Phone (optional)',
        ]);

        if (headerValidate && headerValidate.isError) {
            return headerValidate;
        }

    } else {

        validator.validate(
            'Property Type', row[0],
            {
                name: 'allowedList',
                required: true,
                allowedList: allowedData.propertyType,
            },
        );

        validator.validate(
            'Property Name', row[1],
            {
                name: 'string',
                required: true,
                min: 2,
                max: 255,
            },
        );

        validator.validate(
            'Property Address', row[2],
            {
                name: 'string',
                required: true,
                min: 2,
                max: 255,
            },
        );

        validator.validate(
            'Lease Term', row[3],
            {
                name: 'allowedList',
                required: true,
                allowedList: allowedData.leaseTerm,
            },
        );

        validator.validate(
            'Application Fee', row[4],
            {
                name: 'boolean',
                required: true,
            },
        );

        validator.validate(
            'Application Fee Cost', row[5],
            {
                name: 'number',
                required: false,
                min: 0,
                max: 100000000,
            },
        );

        validator.validate(
            'Application Fee Non-refundable', row[6],
            {
                name: 'boolean',
                required: false,
            },
        );

        validator.validate(
            'Parking', row[7],
            {
                name: 'boolean',
                required: true,
            },
        );

        validator.validate(
            'Parking Monthly Fee', row[8],
            {
                name: 'number',
                required: false,
                min: 0,
                max: 100000000,
            },
        );

        validator.validate(
            'Parking Covered', row[9],
            {
                name: 'boolean',
                required: false,
            },
        );
        validator.validate(
            'Parking Secured', row[10],
            {
                name: 'boolean',
                required: false,
            },
        );

        validator.validate(
            'Property Pet friendly', row[11],
            {
                name: 'boolean',
                required: true,
            },
        );

        validator.validate(
            'Pet Policy', row[12],
            {
                name: 'string',
                required: false,
                min: 2,
                max: 5000,
            },
        );

        validator.validate(
            'Smoking Allowed', row[13],
            {
                name: 'boolean',
                required: true,
            },
        );

        validator.validate(
            'Amenities', row[14],
            {
                name: 'arrayOfString',
                required: false,
            },
        );

        validator.validate(
            'Utilities', row[15],
            {
                name: 'arrayOfString',
                required: false,
            },
        );

        validator.validate(
            'Unit Number', row[16],
            {
                name: 'number',
                required: true,
                min: 0,
                max: 10000000,
            },
        );

        validator.validate(
            'Unit Rent', row[17],
            {
                name: 'number',
                required: true,
                min: 0,
                max: 10000000,
            },
        );

        validator.validate(
            'Unit Deposit', row[18],
            {
                name: 'number',
                required: false,
                min: 0,
                max: 10000000,
            },
        );

        validator.validate(
            'Unit Utility Bill', row[19],
            {
                name: 'number',
                required: false,
                min: 0,
                max: 1000000000,
            },
        );

        validator.validate(
            'Move In Date', row[20],
            {
                name: 'regex',
                regex: /^((([0-9])|([0-2][0-9])|(3[0-1]))\.(([1-9])|(0[1-9])|(1[0-2]))\.(([0-9][0-9])|([1-2][0,9][0-9][0-9])))$/,
                required: true,
            },
        );

        validator.validate(
            'Beds', row[21],
            {
                name: 'number',
                required: true,
                min: 0,
                max: 10000000,
            },
        );

        validator.validate(
            'Bath', row[22],
            {
                name: 'number',
                required: true,
                min: 0,
                max: 10000000,
            },
        );

        validator.validate(
            'Unit SqFt', row[23],
            {
                name: 'number',
                required: false,
                min: 0,
                max: 10000000,
            },
        );

        validator.validate(
            'Unit status', row[24],
            {
                name: 'allowedList',
                required: true,
                allowedList: allowedData.unitStatus,
            },
        );

        validator.validate(
            'Unit Shared', row[25],
            {
                name: 'boolean',
                required: true,
            },
        );

        validator.validate(
            'Number of people sharing this unit', row[26],
            {
                name: 'number',
                required: false,
                min: 0,
                max: 1000000000,
            },
        );

        validator.validate(
            'Appliances', row[27],
            {
                name: 'arrayOfString',
                required: false,
            },
        );

        validator.validate(
            'Laundry Shared', row[28],
            {
                name: 'boolean',
                required: true,
            },
        );

        validator.validate(
            'Bath Shared', row[29],
            {
                name: 'boolean',
                required: true,
            },
        );

        validator.validate(
            'Unit Listing Title', row[30],
            {
                name: 'string',
                required: false,
                min: 2,
                max: 255,
            },
        );

        validator.validate(
            'Unit Description', row[31],
            {
                name: 'string',
                required: false,
                min: 10,
                max: 5000,
            },
        );

        validator.validate(
            'Application Process', row[32],
            {
                name: 'string',
                required: false,
                min: 10,
                max: 5000,
            },
        );

        validator.validate(
            'Listing Email', row[33],
            {
                name: 'email',
                required: false,
            },
        );

        validator.validate(
            'Listing Phone', row[34],
            {
                name: 'phone',
                required: false,
            },
        );

        const errors = validator.getResultErrors();

        return errors.length ? { isErrors: true, row: rowCounter, errors } : { isErrors: false, errors };
    }

};
