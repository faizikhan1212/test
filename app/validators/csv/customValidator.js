const isEmail = require('../../helpers/regex/is-email');
const isPhone = require('../../helpers/regex/is-mobile-phone');

class Validator {

    constructor() {
        this.errors = [];
    }

    static checkHeader(row, allowedHeader) {

        if (!allowedHeader)
            return { isError: true, error: 'errors' };

        if (row.length !== allowedHeader.length)
            return { isError: true, error: 'errors' };

        for (let i = 0, l = row.length; i < l; i++) {
            if (row[i] !== allowedHeader[i]) {
                return { isError: true, error: row[i] };
            }
        }
        return true;

    }

    validate(fieldName, value, type) {
        switch (type.name) {
            case 'boolean': {
                return this.booleanValidate(value, type, fieldName);
            }
            case 'allowedList': {
                return this.allowedListValidate(value, type, fieldName, type.allowedList);
            }
            case 'string': {
                return this.stringValidate(value, type, fieldName);
            }
            case 'number': {
                return this.numberValidate(value, type, fieldName);
            }
            case 'phone': {
                return this.phoneValidate(value, type, fieldName);
            }
            case 'email': {
                return this.emailValidate(value, type, fieldName);
            }
            case 'allowedMultiplyList': {
                return this.allowedMultiplyList(value, type, fieldName, type.allowedList);
            }
            case 'regex': {
                return this.regexValidate(value, type, fieldName);
            }
            case 'arrayOfString': {
                return this.arrayOfStringValidate(value, type, fieldName);
            }
        }
    }

    getResultErrors() {
        return this.errors;
    }

    booleanValidate(value, type, fieldName) {

        let extraConditionForRequired = false;

        if (!type.required && !value) {
            extraConditionForRequired = !value;
        }

        if (extraConditionForRequired || value === 'Yes' || value === 'No') {
            return true;
        } else {
            this.errors.push(fieldName);
        }
    }


    stringValidate(value, type, fieldName) {
        let extraConditionForRequired = false;
        let extraMinCondition = false;
        let extraMaxCondition = false;

        if (!type.required && !value) {
            extraConditionForRequired = !value;
        }

        if (type.hasOwnProperty('min')) {
            extraMinCondition = value && value.length >= type.min;
        }

        if (type.hasOwnProperty('max')) {
            extraMaxCondition = value && value.length <= type.max;
        }

        if (extraConditionForRequired || (extraMinCondition && extraMaxCondition)) {
            return true;
        } else {
            this.errors.push(fieldName);
        }
    }

    numberValidate(value, type, fieldName) {
        let extraConditionForRequired = false;
        let extraMinCondition = false;
        let extraMaxCondition = false;

        if (!type.required && !value) {
            extraConditionForRequired = !value;
        }

        if (type.hasOwnProperty('min')) {
            extraMinCondition = value >= type.min;
        }

        if (type.hasOwnProperty('max')) {
            extraMaxCondition = value <= type.max;
        }

        if (extraConditionForRequired || (extraMinCondition && extraMaxCondition)) {
            return true;
        } else {
            this.errors.push(fieldName);
        }
    }


    allowedListValidate(value, type, fieldName, allowedList) {

        let extraConditionForRequired = false;

        if (!type.required && !value) {
            extraConditionForRequired = !value;
        }

        if (extraConditionForRequired || allowedList.includes(value)) {
            return true;
        } else {
            this.errors.push(fieldName);
        }
    }

    allowedMultiplyList(value, type, fieldName, allowedList) {
        let extraConditionForRequired = false;

        if (!type.required && !value) {
            extraConditionForRequired = !value;
        }

        let splittedArray = [];

        if (value && value.indexOf(',') > 0) {
            splittedArray = value.split(',');
        } else if (value && value.indexOf(';') > 0) {
            splittedArray = value.split(';');
        }

        if (extraConditionForRequired || splittedArray.every(utility => allowedList.includes(utility.trim()))) {
            return true;
        } else {
            this.errors.push(fieldName);
        }
    }

    phoneValidate(value, type, fieldName) {
        let extraConditionForRequired = false;

        if (!type.required && !value) {
            extraConditionForRequired = !value;
        }

    if (extraConditionForRequired || (value && isPhone(value.replace(/\s/g, '').replace(/[^\x00-\x7F]/g, '')))) { //eslint-disable-line
            return true;
        } else {
            this.errors.push(fieldName);
        }
    }

    emailValidate(value, type, fieldName) {
        let extraConditionForRequired = false;

        if (!type.required && !value) {
            extraConditionForRequired = !value;
        }

        if (extraConditionForRequired || isEmail(value)) {
            return true;
        } else {
            this.errors.push(fieldName);
        }
    }

    regexValidate(value, type, fieldName) {
        let extraConditionForRequired = false;

        if (!type.required && !value) {
            extraConditionForRequired = !value;
        }

        if (extraConditionForRequired || type.regex.test(value)) {
            return true;
        } else {
            this.errors.push(fieldName);
        }
    }

    arrayOfStringValidate(value, type, fieldName) {

        let extraConditionForRequired = false;

        if (!type.required && !value) {
            extraConditionForRequired = !value;
        }

        const splittedValue = value ? value.split(',') : null;

        if (extraConditionForRequired || (splittedValue && Array.isArray(splittedValue) && splittedValue.every((i) => typeof i === 'string' || typeof i === 'number'))) {
            return true;
        } else {
            this.errors.push(fieldName);

        }
    }

}


module.exports.Validator = Validator;
