const { check } = require('express-validator/check');

module.exports = {
    rules: [
        check('number').isCreditCard(),
        check('expMonth').isInt({ min: 1, max: 12 }),
        check('expYear').custom((value, { req }) => {
            const currentDate = new Date();
            const currentYear = currentDate.getFullYear();
            const currentMonth = currentDate.getMonth() + 1;

            return (value > currentYear && value <= currentYear + 10) || (value === currentYear && req.body.expMonth >= currentMonth);
        }),
        check('cvc').isLength({
            min: 3,
            max: 3,
        }),
        check('countUnits').isInt({
            min: 1,
            max: 10000000,
        }),
        check('plan').isLength({
            min: 18,
            max: 25,
        }),
    ],
};
