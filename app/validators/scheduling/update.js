const { check } = require('express-validator/check');
const { isMobilePhone, isEmail } = require('../../helpers/regex');
const moment = require('moment');

module.exports = {
    rules: [
        check('id').isInt({ min: 1 }),
        check('userId').isInt({ min: 1 }),
        check('unitId').isInt({ min: 1 }),
        check('clientName').isLength({
            min: 2,
            max: 255,
        }),
        check('clientEmail').custom(value => {
            return value === null || value === undefined || (value && isEmail(value));
        }),
        check('clientPhone').custom(value => isMobilePhone(value)),
        check('dateOfShowing').isArray().custom(value => {
            return moment(value[0]).isValid() && moment(value[1]).isValid();
        }),
    ],
};
