const create = require('./create');
const update = require('./update');
const destroy = require('./destroy');
const list = require('./list');
const show = require('./show');
const listByLead = require('./listByLead');

module.exports = {
    create,
    update,
    show,
    destroy,
    list,
    listByLead
};
