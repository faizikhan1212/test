const { check } = require('express-validator/check');

module.exports = {
    rules: [
        check('offset').isInt({ min: 0 }),
        check('limit').isInt({ min: 1 }),
        check('order').custom(value => {
            return value !== undefined && value !== null
                && ['asc', 'desc'].includes(value.toLowerCase());
        }),
        check('leadId').isInt({ min: 1 }),
    ],
};
