const { check } = require('express-validator/check');
const moment = require('moment');

module.exports = {
    rules: [
        check('offset').isInt({ min: 0 }),
        check('limit').isInt({ min: 1 }),
        check('order').custom(value => {
            return value !== undefined && value !== null
        && ['asc', 'desc'].includes(value.toLowerCase());
        }),
        check('filter').custom(value => {
            const filter = JSON.parse(value);

            const check = {
                userId: false,
                dateOfShowing: false,
            };

            if (filter !== undefined && filter !== null) {
                if (filter.userId === undefined || Number.isInteger(filter.userId)) {
                    check.userId = true;
                }

                if (filter.dateOfShowing !== undefined
          && filter.dateOfShowing !== null
          && Array.isArray(filter.dateOfShowing)) {

                    check.dateOfShowing = moment(filter.dateOfShowing[0]).isValid() && moment(filter.dateOfShowing[1]).isValid();
                }

                return check.userId && check.dateOfShowing;
            }

        }),
    ],
};
