const { check } = require('express-validator/check');

module.exports = {
    rules: [
        check('email').isEmail(),
    ],
};