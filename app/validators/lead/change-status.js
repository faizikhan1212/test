const { check } = require('express-validator/check');

module.exports = {
    rules: [
        check('type').custom(value => {
            const allowed = ['qualified', 'disqualified'];

            return value && !!(allowed.includes(value.toLowerCase()));
        }),
        check('id').isInt({ min: 1 }),
    ],
};
