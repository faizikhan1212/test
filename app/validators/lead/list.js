const { check } = require('express-validator/check');

module.exports = {
    rules: [
        check('type').isLength({
            min: 4,
            max: 12,
        }),
        check('offset').isInt({ min: 0 }),
        check('limit').isInt({ min: 1 }),
    ],
};
