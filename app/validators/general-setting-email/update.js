const { check } = require('express-validator/check');
const { isEmail, isTime } = require('../../helpers/regex');

module.exports = {
    rules: [
        check('id').isInt({ min: 1 }),
        check('sundayFrom').custom(value => value === null || isTime(value)),
        check('sundayTo').custom(value => value === null || isTime(value)),
        check('mondayFrom').custom(value => value === null || isTime(value)),
        check('mondayTo').custom(value => value === null || isTime(value)),
        check('tuesdayFrom').custom(value => value === null || isTime(value)),
        check('tuesdayTo').custom(value => value === null || isTime(value)),
        check('wednesdayFrom').custom(value => value === null || isTime(value)),
        check('wednesdayTo').custom(value => value === null || isTime(value)),
        check('thursdayFrom').custom(value => value === null || isTime(value)),
        check('thursdayTo').custom(value => value === null || isTime(value)),
        check('fridayFrom').custom(value => value === null || isTime(value)),
        check('fridayTo').custom(value => value === null || isTime(value)),
        check('saturdayFrom').custom(value => value === null || isTime(value)),
        check('saturdayTo').custom(value => value === null || isTime(value)),
        check('email').isEmail(),
        check('message').isLength({
            min: 10,
            max: 5000,
        }),
        check('forwardCall').isBoolean(),
        check('forwardEmail').custom((value, { req }) => {
            if (req.body.forwardCall === true) {
                return value && isEmail(value);
            }
            return value === undefined || value === null;
        }),
    ],
};
