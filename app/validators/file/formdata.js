const multer = require('multer');

module.exports = (req, res, err, next) => {
    if (err instanceof multer.MulterError || req.file === null) {
        return res.status(404).json({ msg: 'Unexpected field. Required key \'file\' not found' });
    } else if (err) {
        return res.status(500).json();
    }
    return next();
};
