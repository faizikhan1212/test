const { check } = require('express-validator/check');
const { User } = require('../../../database/models');
const { isMobilePhone } = require('../../helpers/regex');

module.exports = {
    rules: [
        check('id').isInt({ min: 1 }),
        check('name').isLength({
            min: 2,
            max: 255,
        }),
        check('email')
            .isEmail()
            .custom(async (value, { req }) => {
                const user = await User
                    .findOne({
                        where: {
                            email: value,
                        },
                    });
                if (user && user.id === req.body.id) {
                    return true;
                }
                return !user && (value !== null && value !== undefined);
            }),
        check('phone').custom(value => isMobilePhone(value)),
        check('ext').custom((value) => {
            switch (value) {
                case null:
                case undefined: {
                    return true;
                }
            }
            return value.length >= 1 && value.length <= 255;
        }),
    ],
};
