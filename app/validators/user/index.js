const list = require('./list');
const create = require('./create');
const update = require('./update');
const destroy = require('./destroy');
const resendInvite = require('./resend-invite');

module.exports = {
    list,
    create,
    update,
    destroy,
    resendInvite,
};
