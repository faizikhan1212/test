const { check } = require('express-validator/check');
const _ = require('lodash');

module.exports = {
    rules: [
        check('entityName').custom((value) => {
            switch (_.capitalize(value)) {
                case 'Maintenance': {
                    return true;
                }
            }
        }),
        check('entityId').isInt({ min: 1 }),
        check('offset').isInt({ min: 0 }),
        check('limit').isInt({ min: 1 }),
    ],
};
