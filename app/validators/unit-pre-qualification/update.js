const { check } = require('express-validator/check');

module.exports = {
    rules: [
        check('id').isInt({ min: 1 }),
        check('pet').isBoolean(),
        check('cat').custom((value, { req }) => {
            return !!((req.body.pet === true && value >= 0)
        || (req.body.pet === false && (value === undefined || value === 0)));
        }),
        check('dog').custom((value, { req }) => {
            return !!((req.body.pet === true && value >= 0)
        || (req.body.pet === false && (value === undefined || value === 0)));
        }),
        check('agree').isBoolean(),
    ],
};
