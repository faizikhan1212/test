const { check } = require('express-validator/check');

module.exports = {
    rules: [
        check('offset').isInt({ min: 0 }),
        check('limit').isInt({ min: 1 }),
        check('filter').custom((value) => {
            switch (value) {
                case null:
                case undefined: {
                    return true;
                }
            }
            const filter = JSON.parse(value);
            return Object.keys(filter).every(el => {
                return ['userId'].includes(el);
            });
        }),

    ],
};
