const { check } = require('express-validator/check');
const { allowedData } = require('../../../config');

module.exports = {
    rules: [
        check('type')
            .custom(value => {
                return allowedData.propertyType.includes(value);
            }),
        check('propertyName').isLength({
            min: 1,
            max: 255,
        }),
        check('amenities')
            .custom(value => {
                return value === undefined || value === null || value && Array.isArray(value) && value.every((i) => typeof i === 'string' || typeof i === 'number');
            }),
        check('utilities')
            .custom(value => {
                return value === undefined || value === null || value && Array.isArray(value) && value.every((i) => typeof i === 'string' || typeof i === 'number');
            }),
        check('leaseTerm')
            .custom((value) => {
                return allowedData.leaseTerm.includes(value);
            }),
        check('address').isLength({
            min: 2,
            max: 255,
        }),
        check('parking').isBoolean(),
        check('parkingCost').custom((value) => {

            return value === null || value === undefined || +value >= 0;

        }),
        check('parkingCovered').custom((value) => {

            return value === null || value === undefined || value === true || value === false;

        }),
        check('parkingSecured').custom((value) => {

            return value === null || value === undefined || value === true || value === false;

        }),
        check('petFriendly').isBoolean(),
        check('petPolicy').custom((value) => {

            return value === null || value === undefined || (typeof value === 'string' && value.length >= 2 && value.length <= 5000);

        }),
        check('applicationFee').isBoolean(),
        check('appFeeNonRefundable').custom((value) => {

            return value === null || value === undefined || value === true || value === false;

        }),
        check('applicationFeeCost').custom((value) => {

            return value === null || value === undefined || +value >= 0;

        }),
        check('noSmoking').isBoolean(),
    ],
};
