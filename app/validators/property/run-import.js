const { check } = require('express-validator/check');
const multer = require('../../middlewares/multer');
const _ = require('lodash');

module.exports = {
    rules: [
        check('type').custom((value) => {
            return _.capitalize(value) === 'Csv';
        }),
        multer,
    ],
};
