const { check } = require('express-validator/check');


module.exports = {
    rules: [
        check('items').custom(value => {
            return value && Array.isArray(value) && value.every((i) => typeof i === 'string' || typeof i === 'number');
        }),
        check('type').custom(value => {
            return ['amenities', 'utilities', 'appliances'].includes(value);
        }),
    ],
};
