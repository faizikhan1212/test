const auth = require('./auth');
const update = require('./update');

module.exports = {
    auth,
    update,
};
