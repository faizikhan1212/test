const { check } = require('express-validator/check');

module.exports = {
    rules: [
        check('syncBy').custom(value => {
            switch (value) {
                case 'google':
                case 'microsoft': {
                    return true;
                }
            }

            return false;
        }),
        check('isTwoWaySync').isBoolean(),
        check('alwaysSync').isBoolean(),

    ],
};
