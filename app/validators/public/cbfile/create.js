const { check } = require('express-validator/check');
const _ = require('lodash');
const checkEntity = require('../../../middlewares/check-entity');
const multer = require('../../../middlewares/multer');

module.exports = {
    rules: [
        check('leadId').isInt({ min: 1 }),
        check('entityId').isInt({ min: 1 }),
        check('entityName').custom(value => {
            switch (_.capitalize(value)) {
                case 'Maintenance': {
                    return true;
                }
            }
        }),
        check('type').custom(value => {
            switch (_.capitalize(value)) {
                case 'File':
                case 'Image': {
                    return true;
                }
            }
        }),
        checkEntity,
        multer,
    ],
};
