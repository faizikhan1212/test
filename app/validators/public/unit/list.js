const { check } = require('express-validator/check');

module.exports = {
    rules: [
        check('offset').isInt({ min: 0 }),
        check('limit').isInt({ min: 1 }),
        check('slug').isLength({
            min: 2,
            max: 255,
        }),
    ],
};