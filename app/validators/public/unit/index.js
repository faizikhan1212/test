const list = require('./list');
const show = require('./show');

module.exports = {
    list,
    show,
};