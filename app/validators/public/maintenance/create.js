const { check } = require('express-validator/check');

module.exports = {
    rules: [
        check('leadId').isInt({ min: 1 }),
        check('tenantName').isLength({
            min: 2,
            max: 255,
        }),
        check('category').isLength({
            min: 2,
            max: 255,
        }),
        check('description').isLength({
            min: 10,
            max: 5000,
        }),
    ],
};
