const { check } = require('express-validator/check');

module.exports = {
    rules: [
        check('slug').isLength({
            min: 2,
            max: 255,
        }),
    ],
};
