const { check } = require('express-validator/check');

module.exports = {
    rules: [
        check('password').isLength({
            min: 6,
            max: 255,
        }),
        check('token').isLength({
            min: 256,
            max: 256,
        }),
    ],
};
