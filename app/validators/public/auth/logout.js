const { check } = require('express-validator/check');

module.exports = {
    rules: [
        check('refreshToken').isLength({
            min: 256,
        }),
    ],
};