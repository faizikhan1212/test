const { check } = require('express-validator/check');
const loginAuth = require('../../../../app/middlewares/login-auth');

module.exports = {
    rules: [
        check('email')
            .isEmail(),
        check('password')
            .isLength({
                min: 6,
                max: 255,
            }),
        loginAuth,
    ],
};