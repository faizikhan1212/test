const { check } = require('express-validator/check');
const { User } = require('../../../../database/models');

module.exports = {
    rules: [
        check('name').isLength({
            min: 2,
            max: 255,
        }),
        check('email')
            .isEmail(),
        check('email')
            .custom(async (value, { req }) => {
                const user = await User
                    .findOne({
                        where: {
                            email: req.body.email,
                        },
                    });
                if (user !== null) {
                    throw new Error('Please try another email address');
                }
            }),
        check('password')
            .isLength({
                min: 8,
                max: 255,
            }),
    ],
};


