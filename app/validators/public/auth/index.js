const changePassword = require('./change-password');
const confirm = require('./confirm');
const login = require('./login');
const logout = require('./logout');
const refresh = require('./refresh');
const register = require('./register');
const resetPassword = require('./reset-password');
const checkResetToken = require('./check-reset-token');
const resendRegisterEmail = require('./resend-register-email');

module.exports = {
    changePassword,
    confirm,
    login,
    logout,
    refresh,
    register,
    resetPassword,
    checkResetToken,
    resendRegisterEmail,
};
