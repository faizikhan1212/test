const { check } = require('express-validator/check');

module.exports = {
    rules: [
        check('token').isLength({
            min: 256,
        }),
    ],
};