const { check } = require('express-validator/check');
const _ = require('lodash');

module.exports = {
    rules: [
        check('entityId').isInt({ min: 1 }),
        check('entityName').custom(value => {
            switch (_.capitalize(value)) {
                case 'Company':
                case 'User':
                case 'Unit':
                case 'Property': {
                    return true;
                }
            }
        }),
    ],
};
