const { check } = require('express-validator/check');
const bcrypt = require('bcrypt-nodejs');

module.exports = {
    rules: [
        check('oldPassword').isLength({
            min: 6,
            max: 255,
        }).custom((value, { req }) => {
            return bcrypt.compareSync(value, req.user.password);
        }),
        check('password').isLength({
            min: 6,
            max: 255,
        }),
        check('confirmPassword').isLength({
            min: 6,
            max: 255,
        }).custom((value, { req }) => {
            return value === req.body.password;
        }),
    ],
};
