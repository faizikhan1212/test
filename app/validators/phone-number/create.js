const { check } = require('express-validator/check');
const { isMobilePhone } = require('../../helpers/regex');


module.exports = {
    rules: [
        check('phoneNumber').custom(value => isMobilePhone(value)),
    ],
};