const { check } = require('express-validator/check');
const { isMobilePhone } = require('../../helpers/regex');


module.exports = {
    rules: [
        check('name').isLength({
            min: 2,
            max: 255,
        }),
        check('address').isLength({
            min: 2,
            max: 255,
        }),
        check('email').custom(value => {
            return value === undefined || value === null ||
        value.length >= 2 && value.length <= 255;
        }),
        check('phones').custom(value => {
            return value === undefined || value === null ||
        value instanceof Array &&
        value.every(item => isMobilePhone(item));
        }),
    ],
};
