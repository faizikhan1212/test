const { check } = require('express-validator/check');
const { isMobilePhone } = require('../../helpers/regex');

module.exports = {
    rules: [
        check('id').isInt({ min: 1 }),
        check('propertyId').isInt({ min: 1 }),
        check('name').isLength({
            min: 2,
            max: 255,
        }),
        check('email').isEmail(),
        check('phone').custom(value => isMobilePhone(value)),
        check('areasCovered').isLength({
            min: 2,
            max: 255,
        }),
    ],
};
