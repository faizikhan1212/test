const { check } = require('express-validator/check');

module.exports = {
    rules: [
        check('id').isInt({ min: 1 }),
    ],
};
