const { check } = require('express-validator/check');
const _ = require('lodash');
const checkEntity = require('../../middlewares/check-entity');

module.exports = {
    rules: [
        check('entityId').isInt({ min: 1 }),
        check('entityName').custom(value => {
            switch (_.capitalize(value)) {
                case 'Lead':
                case 'Unit':
                case 'Property': {
                    return true;
                }
            }
        }),
        check('offset').isInt({ min: 0 }),
        check('limit').isInt({ min: 1 }),
        checkEntity,
    ],
};
