const { check } = require('express-validator/check');

module.exports = {
    rules: [
        check('id').isInt({ min: 1 }),
        check('text').isLength({
            min: 1,
            max: 5000,
        }),
    ],
};
