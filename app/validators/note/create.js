const { check } = require('express-validator/check');
const _ = require('lodash');
const checkEntity = require('../../middlewares/check-entity');

module.exports = {
    rules: [
        check('entityId').isInt({ min: 1 }),
        check('entityName').custom(value => {
            switch (_.capitalize(value)) {
                case 'Lead':
                case 'Unit':
                case 'Property': {
                    return true;
                }
            }
        }),
        check('text').isLength({
            min: 1,
            max: 5000,
        }),
        checkEntity,
    ],
};
