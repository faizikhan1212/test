const list = require('./list');
const create = require('./create');
const update = require('./update');
const destroy = require('./destroy');

module.exports = {
    create,
    list,
    update,
    destroy,
};
