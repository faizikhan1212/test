const { check } = require('express-validator/check');
const { isTime } = require('../../helpers/regex');

module.exports = {
    rules: [
        check('unitId').isInt({ min: 1 }),
        check('sundayFrom').custom(value => isTime(value)),
        check('sundayTo').custom(value => isTime(value)),
        check('mondayFrom').custom(value => isTime(value)),
        check('mondayTo').custom(value => isTime(value)),
        check('tuesdayFrom').custom(value => isTime(value)),
        check('tuesdayTo').custom(value => isTime(value)),
        check('wednesdayFrom').custom(value => isTime(value)),
        check('wednesdayTo').custom(value => isTime(value)),
        check('thursdayFrom').custom(value => isTime(value)),
        check('thursdayTo').custom(value => isTime(value)),
        check('fridayFrom').custom(value => isTime(value)),
        check('fridayTo').custom(value => isTime(value)),
        check('saturdayFrom').custom(value => isTime(value)),
        check('saturdayTo').custom(value => isTime(value)),
        check('limitProspects').custom((value) => {
            return value === null || value >= 0;
        }),
        check('showingSlot').custom((value) => {
            return value === null || value >= 0;
        }),
        check('otherProperties').custom((value) => {
            return !!(value === undefined || (value && (value === true || value === false)));
        }),
        check('stopAppointments').custom((value) => {
            return !!(value === undefined || value === null || (value && Number(value) >= 0));
        }),
        check('maxShowings').custom((value) => {
            return value === null || value >= 0;
        }),
    ],
};
