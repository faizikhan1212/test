const { check } = require('express-validator/check');

module.exports = {
    rules: [
        check('unitId').isInt({ min: 1 }),
    ],
};
