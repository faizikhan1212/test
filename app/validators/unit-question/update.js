const { check } = require('express-validator/check');
const numericValidator = require('../../helpers/question/numeric-answer');

module.exports = {
    rules: [
        check('id').isInt({ min: 1 }),
        check('mandatory').isBoolean(),
        check('type')
            .custom(value => {
                switch (value) {
                    case 'Numeric':
                    case 'Yes/No':
                    case 'Text': {
                        return true;
                    }
                }
            }),
        check('question').isLength({
            min: 2,
            max: 5000,
        }),
        check('answer').custom((value, { req }) => {
            switch (req.body.type) {
                case 'Numeric': {
                    return numericValidator(value);
                }
                case 'Text': {
                    return typeof value === 'string';
                }
                case 'Yes/No': {
                    return value && (value.toLowerCase() === 'yes' || value.toLowerCase() === 'no');
                }
            }
        }),
    ],
};
