const create = require('./create');
const destroy = require('./destroy');
const list = require('./list');
const update = require('./update');

module.exports = {
    create,
    destroy,
    list,
    update,
};
