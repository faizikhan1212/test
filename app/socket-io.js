const server = require('http').createServer();
const io = require('socket.io')(server);
const config = require('../config');

io.sockets.on('connection', (socket) => {
    console.log(`Unit Filter: Socket client connected ${socket.id}`);
});

io.listen(config.socketIO.port);

module.exports = io;
