const redis = require('../../services/redis');
const { Unit } = require('../../../database/models');
const { stripe } = require('../../../config');

module.exports = async (req, res, next) => {
    console.log('HERE')
    next(); //Skip Unit Restriction for now
    /*
    const { user } = req;

    const listUnitsLength = req.body.list && req.body.list.length !== 1 ? req.body.list.length : 0;

    try {
        
        const cachedCount = await redis.getAsync(`${user.id}-count-unit`);

        if (!cachedCount) {
            const countUnit = await Unit.countUnits(user.companyId);

            if (countUnit + listUnitsLength >= (user.maxUnits || stripe.trial_max_units)) {
                redis.set(`${user.id}-count-unit`, countUnit);
                return res.status(403).json({
                    type: 'max-units',
                    title: `Billing plan limited`,
                    description: `You already have ${countUnit} units when allowed ${user.maxUnits || stripe.trial_max_units} by billing plan.`,
                });
            } else {
                redis.set(`${user.id}-count-unit`, countUnit);
                next();
            }
        } else {
            if (+cachedCount + listUnitsLength >= (user.maxUnits || stripe.trial_max_units)) {
                return res.status(403).json({
                    type: 'max-units',
                    title: `Billing plan limited`,
                    description: `You already have ${cachedCount} units when allowed ${user.maxUnits || stripe.trial_max_units} by billing plan.`,
                });
            } else {
                next();
            }
        }

    } catch (err) {
        console.log(err);
        res.status(500).json();
    }
    */
};
