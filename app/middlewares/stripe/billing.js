const { User } = require('../../../database/models');

module.exports = async (req, res, next) => {
    const { user } = req;

    const currentTimestamp = (new Date()).getTime();
    const createdTimestamp = user.createdAt.getTime();
    let billingTimestamp;

    async function checkManualInvoice(){
       return await User.checkSubscription(user.companyId);
    };

    if (user.role === 'Owner') {
        const isManualPaid = await checkManualInvoice();
        if(isManualPaid[0][0].subscription)
            return next(); //The Company has manually paid the invoice

        billingTimestamp = user.billingExpire !== null ? user.billingExpire.getTime() : null;
    } else {
        const owner = await User.findOne({
            where: {
                companyId: user.companyId,
                role: 'Owner',
            },
        });

        if (owner !== null) {
            billingTimestamp = owner.billingExpire !== null ? owner.billingExpire.getTime() : null;
        } else {
            return res.status(404).json();
        }

    }


    if (req.url !== undefined && (req.url.includes('/private/stripe/') || req.url.includes('/private/auth/user'))) {
        return next();
    }
    //Check Subscription Of Company (in case of manual invoice) 

    if (billingTimestamp !== null && currentTimestamp > billingTimestamp) {
        
        return res.status(403).json({
            title: `Billing plan expired`,
            description: `Sorry your billing plan has expired. \n To access your account please upgrade your plan.`,
        });
    }

    if (billingTimestamp === null && Math.floor((currentTimestamp - createdTimestamp) / 86400000) > 14) {
        return res.status(403).json({
            title: 'Trial expired',
            description: `Sorry your trial has expired. \n To access your account please upgrade your plan.`,
        });
    }

    return next();



    
};


