const { validationResult } = require('express-validator/check');
const _ = require('lodash');

const {
    Unit,
    Property,
    User,
    Company,
    Maintenance,
    Lead,
} = require('../../database/models');

module.exports = async (req, res, next) => {
    const { params, body, query, user } = req;

    const entityName = params.entityName || body.entityName || query.entityName;
    const entityId = params.entityId || body.entityId || query.entityId;
    const { leadId } = params;

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(400).json({
            errors: errors.array(),
        });
    }

    switch (_.capitalize(entityName)) {
        case 'Property': {
            const property = await Property.findByPk(entityId);

            if (property !== null) {
                const userByProperty = await User.findByPk(property.userId);

                if (
                    userByProperty !== null &&
          userByProperty.companyId === user.companyId
                ) {
                    return next();
                }
            }
            return res.status(404).json();
        }

        case 'Unit': {
            const unit = await Unit.findByPk(entityId);

            if (unit !== null) {
                const userByUnit = await User.findByPk(unit.userId);

                if (
                    userByUnit !== null &&
          userByUnit.companyId === user.companyId
                ) {
                    return next();
                }
            }

            return res.status(404).json();
        }
        case 'Company': {
            const company = await Company.findByPk(entityId);

            if (
                company !== null &&
        company.id === user.companyId &&
        user.role === 'Owner'
            ) {
                return next();
            } else {
                return res.status(404).json();
            }
        }

        case 'User': {
            const userByEntityId = await User.findByPk(entityId);

            if (
                userByEntityId !== null &&
        (userByEntityId === user.id || (userByEntityId.companyId === user.companyId && user.role === 'Owner'))
            ) {
                return next();
            }
            return res.status(404).json();
        }
        case 'Lead': {
            const lead = await Lead.findByPk(entityId);

            if (lead !== null) {
                const userByLead = await User.findByPk(lead.userId);

                if (
                    userByLead !== null &&
          userByLead.companyId === user.companyId
                ) {
                    return next();
                }
            }
            return res.status(404).json();
        }

        case 'Maintenance': {
            const lead = await Lead.findByPk(leadId);
            if (lead !== null) {
                const maintenance = await Maintenance.findByPk(
                    entityId,
                );

                if (
                    maintenance !== null &&
          maintenance.leadId === lead.id
                ) {
                    return next();
                }
            }
            return res.status(404).json();
        }
    }
};
