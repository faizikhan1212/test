module.exports = async (req, res, next) => {
    const { user } = req;

    if (user.role === 'Owner') {
        return next();
    }
    else {
        return res.status(403).json();
    }

};
