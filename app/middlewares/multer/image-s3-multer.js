const multer = require('multer');
const multerS3 = require('multer-s3');
const _ = require('lodash');
const nameGeneration = require('../../helpers/file/file-name-generation');
const fileFilter = require('../../helpers/file/file-filter');
const s3 = require('../../services/aws');
const { file, aws } = require('../../../config');

module.exports = multer({
    storage: multerS3({
        s3,
        bucket: aws.bucket,
        key: nameGeneration,
        acl: (req, file, cb) => {
            const { params } = req;
            switch (_.capitalize(params.entityName)) {
                case 'Maintenance': {
                    return cb(null, 'private');
                }
                default: {
                    return cb(null, 'public-read-write');
                }

            }
        },
    }),
    fileFilter,
    limits: {
        fileSize: +file.file_size,
    },
});
