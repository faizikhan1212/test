const _ = require('lodash');
const { file_store } = require('../../../config/file');
const imageLocalMulter = require('./image-local-multer');
const imageS3Multer = require('./image-s3-multer');
const uploadCSV = require('./csv-multer');
const formdataValidator = require('../../validators/file/formdata');


module.exports = (req, res, next) => {
    const { params } = req;

    switch (_.capitalize(params.type)) {
        case 'Image': {
            switch (file_store) {
                case 'local': {
                    return imageLocalMulter.single('file')(req, res, err => formdataValidator(req, res, err, next));
                }

                case 'aws': {
                    return imageS3Multer.single('file')(req, res, err => formdataValidator(req, res, err, next));
                }
            }
            break;
        }

        case 'Csv': {
            return uploadCSV.single('file')(req, res, err => formdataValidator(req, res, err, next));
        }

        default: {
            return res.status(400).json();
        }
    }


};
