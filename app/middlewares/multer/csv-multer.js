const multer = require('multer');
const path = require('path');
const { csv_size } = require('../../../config/file');

const storage = multer.memoryStorage();

module.exports = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {

        const filetypes = /csv/;
        const extname = filetypes.test(path.extname(file.originalname).toLowerCase());

        if (extname) {
            return cb(null, true);
        }
        req.csvValidationError = 'Only csv file';
        cb(null, false);

    },
    limits: {
        fileSize: +csv_size,
    },
});
