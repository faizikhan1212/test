const multer = require('multer');
const mkdirp = require('mkdirp');
const _ = require('lodash');
const nameGeneration = require('../../helpers/file/file-name-generation');
const fileFilter = require('../../helpers/file/file-filter');
const { local_path, file_size } = require('../../../config/file');

const storageEngine = multer.diskStorage({
    destination: async (req, file, cb) => {
        const { params: { leadId, entityName, entityId } } = req;
        const capitalizeEntityName = _.capitalize(entityName);

        switch (capitalizeEntityName) {
            case 'Maintenance': {
                mkdirp(`${local_path}/private/${capitalizeEntityName}/${leadId}-${entityId}`, () => {
                    const path = `${local_path}/private/${capitalizeEntityName}/${leadId}-${entityId}`;
                    cb(null, path);
                });
                break;
            }
            default: {
                mkdirp(`${local_path}/public/${capitalizeEntityName}`, () => {
                    const path = `${local_path}/public/${capitalizeEntityName}`;
                    cb(null, path);
                });
                break;
            }
        }

    },
    filename: nameGeneration,
});

module.exports =
  multer({
      storage: storageEngine,
      fileFilter,
      limits: {
          fileSize: +file_size,
      },
  });
