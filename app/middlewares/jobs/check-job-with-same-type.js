const kue = require('kue');

module.exports = (req, res, next) => {

    kue.Job.rangeByType(`importCsv-${req.user.id}`, 'active', 0, 1, 'asc', (err, jobs) => {
        if (err) {
            console.log('check-job-w-s-t', err);
            return res.status(500).json();
        } else if (jobs[0]) {
            console.log('check-job-w-s-t', jobs[0]);
            return res.status(400).json();
        } else {
            console.log('check-job-w-s-t next', jobs);
            next();
        }

    });

};

