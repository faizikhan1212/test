module.exports = async (req, res, next) => {
    const { user } = req;
    try {

        if (user.status === 'Deleted') {
            return res.status(410).json({ msg: 'Account has been deleted' });
        }
        return next();

    } catch (err) {
        console.log(err);
        return res.status(500).json();
    }
};
