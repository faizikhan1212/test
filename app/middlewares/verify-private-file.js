const redis = require('../services/redis');

module.exports = async (req, res, next) => {
    const { query } = req;
    try {
        const result = await redis.getAsync(query.token);

        if (result !== null && req.url.includes(result)) {
            return next();
        }

        return res.status(404).json();

    } catch (err) {
        console.log(err);
        return res.status(500).json();
    }
};
