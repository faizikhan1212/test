module.exports = {

    contentType(req, res, next) {

        const contentType = req.headers['content-type'];

        switch (req.method) {
            case 'GET':
            case 'DELETE': {
                delete req.headers['content-type'];
                return next();
            }
            case 'POST': {
                if (contentType === 'application/json' || contentType !== undefined && contentType.startsWith('multipart/form-data')) {
                    return next();
                }
                break;
            }
            case 'PUT': {
                if (contentType === 'application/json') {
                    return next();
                }
                break;
            }
        }

        return res.status(404).json();
    },

};
