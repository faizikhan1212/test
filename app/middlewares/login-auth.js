const { User } = require('../../database/models');

const check = (req, res, next, user) => {
    if (user === null) {
        return res.status(404).json();
    }
    if (user !== undefined && user.status === 'Deleted') {
        return res.status(410).json({ msg: 'Account has been deleted' });
    }

    if (user !== undefined && user.confirm === false) {
        return res.status(403).json({ msg: 'Account has not been confirmed.' });
    } else {
        next();
    }
};

module.exports = async (req, res, next) => {
    const { body } = req;

    try {
        const { email } = body;

        if (email !== undefined) {
            const user = await User.findOne({
                where: {
                    email: email,
                },
            });
            check(req, res, next, user);
        } else {
            next();
        }


    } catch (err) {
        console.log(err);
        return res.status(500).json();
    }
};
