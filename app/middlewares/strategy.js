const { User, Company } = require('../../database/models');
const jwt = require('jsonwebtoken');
const LocalStrategy = require('passport-local').Strategy;
const BearerStrategy = require('passport-http-bearer').Strategy;
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const redis = require('../services/redis');
const auth = require('../../config/auth');
const bcrypt = require('bcrypt-nodejs');
const config = require('../../config');


module.exports = {
    local() {
        return new LocalStrategy(
            {
                usernameField: 'email',
                passwordField: 'password',
            },
            async (username, password, cb) => {
                const user = await User.findOne({
                    where: {
                        email: username,
                    },
                });

                return user !== null ? cb(null, user) : cb(null, false);
            },
        );
    },

    bearer() {
        return new BearerStrategy((token, cb) => {
            jwt.verify(token, auth.token_secret, async (err, decoded) => {
                if (err === null) {
                    const user = await User.findByPk(decoded.id, {
                        raw: false,
                    });

                    const [rep] = await redis.multi().exists(token).execAsync();

                    if (user !== null && rep !== 1) {
                        return cb(null, user);
                    }
                }

                return cb(null, false);
            });
        });
    },

    google() {
        return new GoogleStrategy(
            {
                clientID: auth.google_client_id,
                clientSecret: auth.google_client_secret,
                callbackURL: `${config.app.proxy_host}/public/google/callback`,
                accessType: 'offline',
            },
            async (accessToken, refreshToken, profile, cb) => {
                const emails = profile.emails[0];
                const userFind = await User.findOne({
                    where: {
                        email: emails.value,
                    },
                });

                if (userFind === null) {
                    const company = await Company.create({});

                    let billingExpire = new Date();
                    billingExpire.setDate(billingExpire.getDate() + 15);

                    const userCreated = await User.create({
                        companyId: company.id,
                        name: profile.displayName,
                        email: emails.value,
                        role: 'Owner',
                        status: 'Verified',
                        password: bcrypt.hashSync(
                            profile.id,
                            bcrypt.genSaltSync(),
                        ),
                        confirm: true,
                        billingExpire: billingExpire,
                        maxUnits: 10000,
                    });

                    return cb(null, userCreated);
                }

                if (userFind !== null && !userFind.confirm) {
                    userFind.confirm = true;
                    await userFind.save();
                }

                return cb(null, userFind);
            },
        );
    },
};
