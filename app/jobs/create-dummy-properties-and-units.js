const { Property, Unit, File } = require('../../database/models');
const queue = require('../services/queue');
const { find } = require('lodash');

module.exports = (userId) => {

    queue.create(`create-dummy-${userId}`, { title: 'Create dummy properties' }).priority('high')
        .removeOnComplete(true)
        .save();
    queue.process(`create-dummy-${userId}`, async (job, done) => {
        try {

            const properties = await Promise.all([
                Property.create({
                    userId: userId,
                    type: 'Single-Family House',
                    amenities: ['Bike storage', 'Patio or balcony', 'Air-conditioning', 'Storage', 'Fitness Center', 'Backyard', 'Fireplace', 'Party room', 'Pool', 'High-Tech Appliances', 'Elevators', 'Rooftop', 'Hardwood Floors', 'Electric car charging stations', 'Car sharing service', 'Grilling/BBQ area', 'Security cameras', 'Gated access', 'Recycling center'],
                    utilities: null,
                    propertyName: 'Top Manor',
                    numberUnits: 1,
                    leaseTerm: '1 year',
                    address: '16724 NE 132nd St; Redmond; WA 98052; USA',
                    parking: true,
                    petFriendly: false,
                    noSmoking: true,
                    petPolicy: null,
                    appFeeNonRefundable: true,
                    applicationFeeCost: 35,
                    applicationFee: true,
                    parkingCost: 0,
                    parkingCovered: true,
                    parkingSecured: null,
                }),
                Property.create({
                    userId: userId,
                    type: 'Apartment',
                    amenities: ['Bike storage', 'Patio or balcony', 'Air-conditioning', 'Storage', 'Fitness Center', 'Backyard', 'Elevators', 'Rooftop', 'Hardwood Floors', 'Grilling/BBQ area', 'Security cameras', 'Gated access', 'Recycling center'],
                    utilities: ['Heat', 'Hydro (Electricity)', 'Hot Water', 'Wireless Internet'],
                    propertyName: 'Strathcon',
                    numberUnits: 1,
                    leaseTerm: '1 year',
                    address: '955 E Hastings St; Vancouver; BC V6A 1R9; Canada',
                    parking: true,
                    petFriendly: true,
                    noSmoking: true,
                    petPolicy: 'Pets are allowed (1-pet, 25-lb. limits).\n' +
            'One Time Fees:\n' +
            'Pet Deposit $300 \n' +
            'Pet Fee $200',
                    appFeeNonRefundable: null,
                    applicationFeeCost: null,
                    applicationFee: false,
                    parkingCost: 50,
                    parkingCovered: true,
                    parkingSecured: null,
                }),
            ]);

            const usaProperty = find(properties, { 'address': '16724 NE 132nd St; Redmond; WA 98052; USA' });
            const canadaProperty = find(properties, { 'address': '955 E Hastings St; Vancouver; BC V6A 1R9; Canada' });

            const units = await Promise.all([
                Unit.create({
                    userId: userId,
                    propertyId: canadaProperty.id,
                    rent: 1850,
                    deposit: 950,
                    status: 'Vacant',
                    title: 'Never Lived In Luxury 1 Bedroom in Convenient Downtown',
                    description: 'Highlights: \n' +
            '\n' +
            '\n' +
            '- Brand New & Never Lived In!! \n' +
            '- Absolutely amazing ocean & mountain view from multiple common rooms (see photos) \n' +
            '- Location: Walking score of 94, biker\'s paradise (score of 100);\n' +
            '- 1 parking included\n' +
            '- Grocery: No Frills in 3 blocks (7-minute walk, 1-minute drive or 2-minute by bike)\n' +
            '- Public transit-friendly: bus lines (14, 16, 20, 35) right in front of the building\n' +
            '- Multiple direct buses to UBC, SFU Downtown campus (10 mins), Gastown (5 mins)\n' +
            '\n' +
            '\n' +
            'Units: \n' +
            '- High-end stainless steel appliances and granite countertops\n' +
            '- In-suite laundry, dishwasher, modern oven, fridge, and microwave\n' +
            '- Beautiful wood flooring throughout the unit (no carpet)\n' +
            '- Sunny south facing windows\n' +
            '- Super good sound proofing with no sound from neighbors\n' +
            '- Enough space for 2 people\n' +
            '\n' +
            '\n' +
            'Building & Amenities:\n' +
            '- Brand new luxury condo built in Fall 2018\n' +
            '- Multiple well-equipped common rooms, gyms, lounges, BBQ grill, fire pits, and study rooms\n' +
            '- Absolutely amazing water/mountain view from common rooms\n' +
            '\n' +
            '\n' +
            'Location:\n' +
            '- Conveniently downtown in Strathcona Village (955 East Hastings)\n' +
            '- Easy access to numerous bus lines 14, 16, 20, 35\n' +
            '- Location: Walking score of 94, biker\'s paradise (score of 100)\n' +
            '- Grocery: No Frills in 3 blocks (7-minute walk, 1-minute drive or 2-minute by bike)\n' +
            '- Numerous top-rated local cafes & breweries either downstairs or within a few minutes walking distances, including The Garden Strathcona (4.9/5 Google Rating), Bob Likes Thai Food - coming soon, Strathcona Brewing Company (4.3/5 Google Rating), Callister Brewing Co. (4.4/5 Google Rating), etc. \n' +
            '\n' +
            '\n' +
            'Lease & Rent: \n' +
            '- Rent is $1,850/month, including 1 parking & 1 storage locker\n' +
            '- Deposit: $950\n' +
            '- During the final stage, will request proof of employment (or school enrollment)\n' +
            '- 1-year lease; might be flexible to short-term lease\n' +
            '- No pets\n' +
            '\n' +
            '\n' +
            'Available date: mid-June 20 or July 1st\n' +
            '\n' +
            'If you\'re interested, please email me with a short description of yourself, what do you do for work, and for how long you would prefer to rent this unit.\n' +
            '\n' +
            'Happy to show the unit over the next few weeks.',
                    parking: true,
                    petFriendly: true,
                    bed: 1,
                    bath: 1,
                    amenities: ['Bike storage', 'Patio or balcony', 'Air-conditioning', 'Storage', 'Fitness Center', 'Pool', 'Elevators', 'Rooftop', 'Business center', 'Grilling/BBQ area', 'Security guard', 'Gated access', 'Recycling center'],
                    utilities: ['Heat', 'Hydro (Electricity)', 'Hot Water', 'Wireless Internet'],
                    leaseTerm: '1 year',
                    email: null,
                    phone: null,
                    unit: 1,
                    address: '955 E Hastings St, Vancouver, BC V6A 1R9, Canada',
                    noSmoking: true,
                    petPolicy: 'Pets are allowed (1-pet, 25-lb. limits).\n' +
            'One Time Fees:\n' +
            'Pet Deposit $300 \n' +
            'Pet Fee $200',
                    applicationFee: false,
                    appFeeNonRefundable: null,
                    applicationFeeCost: null,
                    parkingCost: 50,
                    parkingCovered: true,
                    parkingSecured: null,
                    sharedLaundry: false,
                    sharedWashroomBath: false,
                    unitShared: false,
                    unitSharedPeople: null,
                    sqFt: 500,
                    showingAgent: null,
                    moveInDate: '2020-01-01 00:00:00+01',
                    utilityBill: 100,
                    applicationProcess: 'Our qualification process is fairly simple so we encourage everyone to apply with their family.\n' +
            'Once you tour the place and like it, you will complete an application form followed by providing proof of income and two references.\n' +
            'We accept Pay stubs and Employer recommendation letters.',
                    appliances: ['Oven', 'Stove', 'Microwave'],
                }),
                Unit.create({
                    userId: userId,
                    propertyId: usaProperty.id,
                    rent: 4500,
                    deposit: 2250,
                    status: 'Vacant',
                    title: '5 bd 4ba House in Redmond',

                    description: '10 mins drive to Microsoft, Kirkland, Redmond town center. Large 5 bedroom, 3.75 bathroom house with large yard.\n' +
            'Sprawling two story in stately Bristol View. Recent updates include remodeled kitchen & baths. Grand entertaining spaces with gleaming hardwoods. Chef\'s kitchen with gas range, stone counters, island & panty. Private Den. Generous master w/ delightful 5 piece master bath-finished in stone & tile. Plenty of room to spread out including - bonus room, craft room w/ closet and rec. room above the garage. Storage/wine celler in basement. Private yard w/ deck, patio, firepit & lawn for play!',
                    parking: true,
                    petFriendly: false,
                    bed: 5,
                    bath: 4,
                    amenities: ['Bike storage', 'Patio or balcony', 'Air-conditioning', 'Storage', 'Fitness Center', 'Backyard', 'Fireplace', 'Party room', 'Pool', 'High-Tech Appliances', 'Elevators', 'Rooftop', 'Hardwood Floors', 'Electric car charging stations', 'Car sharing service', 'Grilling/BBQ area', 'Security cameras', 'Gated access', 'Recycling center'],
                    utilities: null,
                    leaseTerm: '1 year',
                    email: null,
                    phone: 2168029660,
                    unit: 1,
                    address: '16724 NE 132nd St, Redmond, WA 98052, USA',
                    noSmoking: true,
                    petPolicy: null,
                    applicationFee: true,
                    appFeeNonRefundable: true,
                    applicationFeeCost: 35,
                    parkingCost: 0,
                    parkingCovered: true,
                    parkingSecured: null,
                    sharedLaundry: false,
                    sharedWashroomBath: false,
                    unitShared: false,
                    unitSharedPeople: null,
                    sqFt: 5000,
                    showingAgent: null,
                    moveInDate: '2020-01-01 00:00:00+01',
                    utilityBill: 100,
                    applicationProcess: 'Our qualification process is fairly simple so we encourage everyone to apply with their family.\n' +
            'Once you tour the place and like it, you will complete an application form followed by providing proof of income and two references.\n' +
            'We accept Pay stubs and Employer recommendation letters.',
                    appliances: ['Oven', 'Stove', 'Microwave'],
                }),
                Unit.create({
                    userId: userId,
                    propertyId: canadaProperty.id,
                    rent: 1850,
                    status: 'Vacant',
                    title: 'Never Lived In Luxury 1 Bedroom in Convenient Downtown',
                    description: 'Highlights: \n' +
            '\n' +
            '\n' +
            '- Brand New & Never Lived In!! \n' +
            '- Absolutely amazing ocean & mountain view from multiple common rooms (see photos) \n' +
            '- Location: Walking score of 94, biker\'s paradise (score of 100);\n' +
            '- 1 parking included\n' +
            '- Grocery: No Frills in 3 blocks (7-minute walk, 1-minute drive or 2-minute by bike)\n' +
            '- Public transit-friendly: bus lines (14, 16, 20, 35) right in front of the building\n' +
            '- Multiple direct buses to UBC, SFU Downtown campus (10 mins), Gastown (5 mins)\n' +
            '\n' +
            '\n' +
            'Units: \n' +
            '- High-end stainless steel appliances and granite countertops\n' +
            '- In-suite laundry, dishwasher, modern oven, fridge, and microwave\n' +
            '- Beautiful wood flooring throughout the unit (no carpet)\n' +
            '- Sunny south facing windows\n' +
            '- Super good sound proofing with no sound from neighbors\n' +
            '- Enough space for 2 people\n' +
            '\n' +
            '\n' +
            'Building & Amenities:\n' +
            '- Brand new luxury condo built in Fall 2018\n' +
            '- Multiple well-equipped common rooms, gyms, lounges, BBQ grill, fire pits, and study rooms\n' +
            '- Absolutely amazing water/mountain view from common rooms\n' +
            '\n' +
            '\n' +
            'Location:\n' +
            '- Conveniently downtown in Strathcona Village (955 East Hastings)\n' +
            '- Easy access to numerous bus lines 14, 16, 20, 35\n' +
            '- Location: Walking score of 94, biker\'s paradise (score of 100)\n' +
            '- Grocery: No Frills in 3 blocks (7-minute walk, 1-minute drive or 2-minute by bike)\n' +
            '- Numerous top-rated local cafes & breweries either downstairs or within a few minutes walking distances, including The Garden Strathcona (4.9/5 Google Rating), Bob Likes Thai Food - coming soon, Strathcona Brewing Company (4.3/5 Google Rating), Callister Brewing Co. (4.4/5 Google Rating), etc. \n' +
            '\n' +
            '\n' +
            'Lease & Rent: \n' +
            '- Rent is $1,850/month, including 1 parking & 1 storage locker\n' +
            '- Deposit: $950\n' +
            '- During the final stage, will request proof of employment (or school enrollment)\n' +
            '- 1-year lease; might be flexible to short-term lease\n' +
            '- No pets\n' +
            '\n' +
            '\n' +
            'Available date: mid-June 20 or July 1st\n' +
            '\n' +
            'If you\'re interested, please email me with a short description of yourself, what do you do for work, and for how long you would prefer to rent this unit.\n' +
            '\n' +
            'Happy to show the unit over the next few weeks.',
                    parking: true,
                    petFriendly: true,
                    bed: 1,
                    bath: 1,
                    amenities: ['Bike storage', 'Patio or balcony', 'Air-conditioning', 'Storage', 'Fitness Center', 'Pool', 'Elevators', 'Rooftop', 'Business center', 'Grilling/BBQ area', 'Security guard', 'Gated access', 'Recycling center'],
                    utilities: ['Heat', 'Hydro (Electricity)', 'Hot Water', 'Wireless Internet'],
                    leaseTerm: '1 year',
                    email: null,
                    phone: null,
                    unit: 2,
                    address: '955 E Hastings St, Vancouver, BC V6A 1R9, Canada',
                    noSmoking: true,
                    petPolicy: 'Pets are allowed (1-pet, 25-lb. limits).\n' +
            'One Time Fees:\n' +
            'Pet Deposit $300 \n' +
            'Pet Fee $200',
                    applicationFee: false,
                    appFeeNonRefundable: null,
                    applicationFeeCost: null,
                    parkingCost: 50,
                    parkingCovered: true,
                    parkingSecured: null,
                    sharedLaundry: false,
                    sharedWashroomBath: false,
                    unitShared: false,
                    unitSharedPeople: null,
                    sqFt: 500,
                    showingAgent: null,
                    moveInDate: '2020-01-01 00:00:00+01',
                    utilityBill: 100,
                    applicationProcess: 'Our qualification process is fairly simple so we encourage everyone to apply with their family.\n' +
            'Once you tour the place and like it, you will complete an application form followed by providing proof of income and two references.\n' +
            'We accept Pay stubs and Employer recommendation letters.',
                    appliances: ['Oven', 'Stove', 'Microwave'],
                }),
            ]);

            await Promise.all([
                File.create({
                    userId: userId,
                    entityName: 'Property',
                    entityId: usaProperty.id,
                    type: 'Image',
                    hash: 'prop1.jpg',
                    name: 'prop1.jpg',
                }),
                File.create({
                    userId: userId,
                    entityName: 'Unit',
                    entityId: units[0].id,
                    type: 'Image',
                    hash: 'unit 1-1-1.jpg',
                    name: 'unit 1-1-1.jpg',
                }),
                File.create({
                    userId: userId,
                    entityName: 'Unit',
                    entityId: units[0].id,
                    type: 'Image',
                    hash: 'unit 1-1-2.jpg',
                    name: 'unit 1-1-2.jpg',
                }),
                File.create({
                    userId: userId,
                    entityName: 'Unit',
                    entityId: units[0].id,
                    type: 'Image',
                    hash: 'unit 1-1-3.jpg',
                    name: 'unit 1-1-3.jpg',
                }),
                File.create({
                    userId: userId,
                    entityName: 'Unit',
                    entityId: units[0].id,
                    type: 'Image',
                    hash: 'unit 1-1-4.jpg',
                    name: 'unit 1-1-4.jpg',
                }),
                File.create({
                    userId: userId,
                    entityName: 'Unit',
                    entityId: units[0].id,
                    type: 'Image',
                    hash: 'unit 1-1-5.jpg',
                    name: 'unit 1-1-5.jpg',
                }),
                File.create({
                    userId: userId,
                    entityName: 'Unit',
                    entityId: units[0].id,
                    type: 'Image',
                    hash: 'unit 1-1-6.jpg',
                    name: 'unit 1-1-6.jpg',
                }),
                File.create({
                    userId: userId,
                    entityName: 'Unit',
                    entityId: units[0].id,
                    type: 'Image',
                    hash: 'unit 1-1-7.jpg',
                    name: 'unit 1-1-7.jpg',
                }),
                File.create({
                    userId: userId,
                    entityName: 'Unit',
                    entityId: units[0].id,
                    type: 'Image',
                    hash: 'unit 1-1-8.jpg',
                    name: 'unit 1-1-8.jpg',
                }),
                File.create({
                    userId: userId,
                    entityName: 'Unit',
                    entityId: units[0].id,
                    type: 'Image',
                    hash: 'unit 1-1-9.jpg',
                    name: 'unit 1-1-9.jpg',
                }),
                File.create({
                    userId: userId,
                    entityName: 'Unit',
                    entityId: units[0].id,
                    type: 'Image',
                    hash: 'unit 1-1-10.jpg',
                    name: 'unit 1-1-10.jpg',
                }),
                File.create({
                    userId: userId,
                    entityName: 'Unit',
                    entityId: units[0].id,
                    type: 'Image',
                    hash: 'unit 1-1-11.jpg',
                    name: 'unit 1-1-11.jpg',
                }),
                File.create({
                    userId: userId,
                    entityName: 'Unit',
                    entityId: units[0].id,
                    type: 'Image',
                    hash: 'unit 1-1-12.jpg',
                    name: 'unit 1-1-12.jpg',
                }),
                File.create({
                    userId: userId,
                    entityName: 'Unit',
                    entityId: units[0].id,
                    type: 'Image',
                    hash: 'unit 1-1-13.jpg',
                    name: 'unit 1-1-13.jpg',
                }),
                File.create({
                    userId: userId,
                    entityName: 'Unit',
                    entityId: units[0].id,
                    type: 'Image',
                    hash: 'unit 1-1-14.jpg',
                    name: 'unit 1-1-14.jpg',
                }),
                File.create({
                    userId: userId,
                    entityName: 'Unit',
                    entityId: units[0].id,
                    type: 'Image',
                    hash: 'unit 1-1-15.jpg',
                    name: 'unit 1-1-15.jpg',
                }),
                File.create({
                    userId: userId,
                    entityName: 'Unit',
                    entityId: units[0].id,
                    type: 'Image',
                    hash: 'unit 1-1-16.jpg',
                    name: 'unit 1-1-16.jpg',
                }),
                File.create({
                    userId: userId,
                    entityName: 'Unit',
                    entityId: units[0].id,
                    type: 'Image',
                    hash: 'unit 1-1-17.jpg',
                    name: 'unit 1-1-17.jpg',
                }),
                File.create({
                    userId: userId,
                    entityName: 'Unit',
                    entityId: units[0].id,
                    type: 'Image',
                    hash: 'unit 1-1-18.jpg',
                    name: 'unit 1-1-18.jpg',
                }),
                File.create({
                    userId: userId,
                    entityName: 'Unit',
                    entityId: units[0].id,
                    type: 'Image',
                    hash: 'unit 1-1-19.jpg',
                    name: 'unit 1-1-19.jpg',
                }),
                File.create({
                    userId: userId,
                    entityName: 'Unit',
                    entityId: units[0].id,
                    type: 'Image',
                    hash: 'unit 1-1-20.jpg',
                    name: 'unit 1-1-20.jpg',
                }),
                File.create({
                    userId: userId,
                    entityName: 'Unit',
                    entityId: units[0].id,
                    type: 'Image',
                    hash: 'unit 1-1-21.jpg',
                    name: 'unit 1-1-21.jpg',
                }),
                File.create({
                    userId: userId,
                    entityName: 'Unit',
                    entityId: units[0].id,
                    type: 'Image',
                    hash: 'unit 1-1-22.jpg',
                    name: 'unit 1-1-22.jpg',
                }),
                File.create({
                    userId: userId,
                    entityName: 'Unit',
                    entityId: units[0].id,
                    type: 'Image',
                    hash: 'unit 1-1-23.jpg',
                    name: 'unit 1-1-23.jpg',
                }),
                File.create({
                    userId: userId,
                    entityName: 'Property',
                    entityId: canadaProperty.id,
                    type: 'Image',
                    hash: 'prop2.jpg',
                    name: 'prop2.jpg',
                }),
                File.create({
                    userId: userId,
                    entityName: 'Unit',
                    entityId: units[1].id,
                    type: 'Image',
                    hash: 'unit 2-2-1.jpg',
                    name: 'unit 2-2-1.jpg',
                }),
                File.create({
                    userId: userId,
                    entityName: 'Unit',
                    entityId: units[1].id,
                    type: 'Image',
                    hash: 'unit 2-2-2.jpg',
                    name: 'unit 2-2-2.jpg',
                }),
                File.create({
                    userId: userId,
                    entityName: 'Unit',
                    entityId: units[1].id,
                    type: 'Image',
                    hash: 'unit 2-2-3.jpg',
                    name: 'unit 2-2-3.jpg',
                }),
                File.create({
                    userId: userId,
                    entityName: 'Unit',
                    entityId: units[1].id,
                    type: 'Image',
                    hash: 'unit 2-2-4.jpg',
                    name: 'unit 2-2-4.jpg',
                }),
                File.create({
                    userId: userId,
                    entityName: 'Unit',
                    entityId: units[1].id,
                    type: 'Image',
                    hash: 'unit 2-2-5.jpg',
                    name: 'unit 2-2-5.jpg',
                }),
                File.create({
                    userId: userId,
                    entityName: 'Unit',
                    entityId: units[1].id,
                    type: 'Image',
                    hash: 'unit 2-2-6.jpg',
                    name: 'unit 2-2-6.jpg',
                }),
                File.create({
                    userId: userId,
                    entityName: 'Unit',
                    entityId: units[1].id,
                    type: 'Image',
                    hash: 'unit 2-2-7.jpg',
                    name: 'unit 2-2-7.jpg',
                }),
            ]);

            done();
        } catch (err) {
            console.log(err);
            return done(err);
        }
    });

};
