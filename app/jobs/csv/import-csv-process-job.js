const queue = require('../../services/queue');
const { Property, Unit } = require('../../../database/models');
const fastCsv = require('fast-csv');
const csvValidator = require('../../validators/csv/import');
const redis = require('../../services/redis');
const { stripe } = require('../../../config/stripe');
const transformData = require('../../helpers/csv/transform-data');
const findCustomItemsAndUpdate = require('../dictionary/find-custom-items-and-update');

module.exports = (user, file, date, io) => {

    queue.create(`importCsv-${user.id}-${file.originalname}-${date}`, { title: `importCsv` })
        .removeOnComplete(true)
        .save();

    queue.process(`importCsv-${user.id}-${file.originalname}-${date}`, (job, done) => {
        try {


            console.log('import-csv process');

            let rowCounter = 1;
            let rowResult;
            let errors = [];
            let properties = [];
            let units = [];
            let unitPromises = [];

            let propertiesNames = [];

            let stream = fastCsv
                .fromString(file.buffer.toString().replace(/"/g, '\'').replace(/;/g, ','), {
                    headers: false,
                    delimiter: ',',
                    ignoreEmpty: true,
                    quote: '\'',
                    escape: '\'',
                })
                .validate((data) => {
                    rowResult = csvValidator(data, rowCounter);

                    if (rowResult && rowResult.isError) {

                        errors.push({
                            isErrors: true,
                            row: 1,
                            errors: [`In Header row - ${rowResult.error}`],
                        });

                        stream.emit('end');
                    }

                    if (rowResult && rowResult.isErrors) {
                        errors.push(rowResult);
                    }
                    if (errors.length === 10) {
                        stream.emit('end');
                    }

                    rowCounter++;

                    return rowResult && !rowResult.isErrors;
                })
                .on('data', (row) => {
                    if (errors.length === 0) {

                        const data = transformData(row);

                        if (!propertiesNames.includes(data.propertyName)) {
                            propertiesNames.push(data.propertyName);

                            properties.push(
                                {
                                    userId: user.id,
                                    type: data.type,
                                    propertyName: data.propertyName,
                                    address: data.address,
                                    leaseTerm: data.leaseTerm,
                                    applicationFee: data.applicationFee,
                                    applicationFeeCost: data.applicationFeeCost,
                                    appFeeNonRefundable: data.appFeeNonRefundable,
                                    parking: data.parking,
                                    parkingCost: data.parkingCost,
                                    parkingCovered: data.parkingCovered,
                                    parkingSecured: data.parkingSecured,
                                    petFriendly: data.petFriendly,
                                    petPolicy: data.petPolicy,
                                    noSmoking: data.noSmoking,
                                    amenities: data.amenities,
                                    utilities: data.utilities,
                                },
                            );
                        }

                        units.push({
                            userId: user.id,
                            unit: data.unit,
                            propertyName: data.propertyName,
                            address: data.address,
                            rent: data.rent,
                            deposit: data.deposit,
                            utilityBill: data.utilityBill,
                            moveInDate: data.moveInDate,
                            bed: data.bed,
                            bath: data.bath,
                            sqFt: data.sqFt,
                            status: data.status,
                            unitShared: data.unitShared,
                            unitSharedPeople: data.unitSharedPeople,
                            appliances: data.appliances,
                            sharedLaundry: data.sharedLaundry,
                            sharedWashroomBath: data.sharedWashroomBath,
                            parking: data.parking,
                            parkingCost: data.parkingCost,
                            parkingCovered: data.parkingCovered,
                            parkingSecured: data.parkingSecured,
                            petFriendly: data.petFriendly,
                            petPolicy: data.petPolicy,
                            noSmoking: data.noSmoking,
                            applicationFee: data.applicationFee,
                            applicationFeeCost: data.applicationFeeCost,
                            appFeeNonRefundable: data.appFeeNonRefundable,
                            title: data.title,
                            description: data.description,
                            applicationProcess: data.applicationProcess,
                            email: data.email,
                            phone: data.phone,
                            amenities: data.amenities,
                            utilities: data.utilities,
                        });

                    }

                })
                .on('end', async () => {
                    if (errors.length > 0) {
                        io.sockets.emit('import', JSON.stringify({ errors }));
                        return done();
                    } else {

                        let countUnit = await redis.getAsync(`${user.id}-count-unit`);

                        if (!countUnit) {
                            countUnit = await Unit.countUnits(user.companyId);
                        }

                        if ((+countUnit + units.length) <= (user.maxUnits || stripe.trial_max_units)) {

                            properties = properties.map(el => Property.create(el).catch(() => io.sockets.emit('import', 'internal error')));

                            let propertyResponse = await Promise.all(properties);

                            let nameId = {};
                            propertyResponse.forEach(el => {
                                nameId[el.propertyName] = el.id;
                            });


                            for (let i = 0; i < units.length; i++) {
                                units[i].propertyId = nameId[units[i].propertyName];
                                delete units[i].propertyName;

                                unitPromises.push(Unit.create(units[i]).catch(() => io.sockets.emit('import', 'internal error')));
                                findCustomItemsAndUpdate(user.id, user.companyId, {
                                    amenities: units[i].amenities,
                                    utilities: units[i].utilities,
                                    appliances: units[i].appliances,
                                });
                            }

                            try {
                                await Promise.all(unitPromises);

                                console.log('import ok');
                                io.sockets.emit('import', 'success');
                                return done();

                            } catch (err) {
                                console.log('catch', err);
                                return done(err);
                            }
                        } else {
                            io.sockets.emit('import', JSON.stringify({
                                type: 'max-units',
                                title: `Billing plan limited`,
                                description: `You have ${countUnit} units and trying to add ${units.length} units when allowed ${user.maxUnits || stripe.trial_max_units} by billing plan.`,
                            }));
                            return done();
                        }

                    }
                })
                .on('error', (error) => {
                    console.log('Catch an invalid csv file!!!');
                    io.sockets.emit('import', 'internal error');
                    return done();
                });

        } catch (err) {
            console.log(err);
            return done(err);
        }
    });

};
