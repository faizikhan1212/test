const kue = require('kue');

module.exports = () => {

    kue.Job.rangeByState('complete', 0, 10000000, 'asc', function(err, jobs) {
        jobs.forEach(function(job) {
            if (job.created_at > new Date()) return;
            job.remove();
        });
    });

};
