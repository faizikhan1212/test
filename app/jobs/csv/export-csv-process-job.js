const xlsx = require('xlsx');
const mkdirp = require('mkdirp');
const fs = require('fs');
const randtoken = require('rand-token');
const { Property } = require('../../../database/models');
const {
    file: { local_path },
    app: { proxy_host },
} = require('../../../config');
const queue = require('../../services/queue');
const redis = require('../../services/redis');

module.exports = (user, date, io) => {

    queue.create(`propertyExport-${user.id}-${date}`, { title: 'propertyExport' })
        .removeOnComplete(true)
        .save();

    queue.process(`propertyExport-${user.id}-${date}`, async (job, done) => {
        try {

            const propertiesAndUnits = await Property.getFullExport(user.companyId);

            const ws = xlsx.utils.json_to_sheet(propertiesAndUnits);
            const stream = xlsx.stream.to_csv(ws, { FS: ',' });

            mkdirp(`${local_path}/private/csv`, () => {
                const token = randtoken.uid(256);
                const fileUri = `properties-${user.id}.csv`;

                stream.pipe(fs.createWriteStream(`${local_path}/private/csv/${fileUri}`));

                redis.set(token, fileUri, 'EX', 60 * 60 * 24);

                io.sockets.emit('export', JSON.stringify({ url: `${proxy_host}/private-files/csv/${fileUri}?token=${token}` }));
                done();
            });

        } catch (err) {
            console.log(err);
            done(err);
        }
    });

};
