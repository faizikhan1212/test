const mail = require('@sendgrid/mail');
const redis = require('../../services/redis');
const randtoken = require('rand-token');
const config = require('../../../config');
const { key, from } = config.sendgrid;
const { frontend_url } = config.app;
const queue = require('../../services/queue');
module.exports = (to) => {

    queue.create(`register-email-${to}`, { title: 'registerEmail' }).priority('high')
        .removeOnComplete(true)
        .save();


    queue.process(`register-email-${to}`, async (job, done) => {
        try {

            const token = randtoken.uid(256);

            redis.set(token, `${to}:confirm`, 'EX', 60 * 60 * 24);
            redis.set(`${to}:confirm`, token, 'EX', 60 * 60 * 24);

            const link = `${frontend_url}/#/auth/confirm/${token}`;

            mail.setApiKey(key);

            mail.send({
                to: to,
                from: from,
                subject: 'Confirm email',
                html: `<strong>${link}</strong>`,
            });

            done();

        } catch (err) {
            console.log(err);
            return done(err);
        }
    });

};
