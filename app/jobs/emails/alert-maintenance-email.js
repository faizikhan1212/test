const mail = require('@sendgrid/mail');
const Promise = require('bluebird');
const randtoken = require('rand-token');
const fileUrl = require('../../helpers/file/file-url');
const redis = require('../../services/redis');
const aws = require('../../services/aws');
const queue = require('../../services/queue');
const config = require('../../../config');
const { key, from } = config.sendgrid;


const fs = Promise.promisifyAll(require('fs'));


module.exports = (to, maintenance, files) => {

    queue.create(`alert-email-${to}`, { title: 'alertEmail' }).priority('high')
        .removeOnComplete(true)
        .save();
    queue.process(`alert-email-${to}`, async (job, done) => {
        try {
            mail.setApiKey(key);

            const token = randtoken.uid(256);

            const urls = [];
            const folderName = `${maintenance.leadId}-${maintenance.id}`;
            redis.set(token, folderName, 'EX', 60 * 60 * 24);

            const attachments = [];

            switch (config.file.file_store) {
                case 'local': {
                    const promises = [];

                    files.forEach((file) => {
                        promises.push(fs.readFileAsync(`${config.file.local_path}/private/maintenance/${folderName}/${file.hash}`));

                        attachments.push({
                            disposition: 'attachment',
                            filename: file.name,
                            type: 'image/jpeg',
                        });

                        urls.push(fileUrl('Maintenance', file, folderName, token));

                    });

                    const images = await Promise.all(promises);
                    attachments.forEach((att, i) => {
                        att.content = Buffer.from(images[i], 'binary').toString('base64');
                    });

                    break;
                }
                case 'aws': {
                    const promises = [];

                    files.forEach((file) => {
                        promises.push(aws.getObject({
                            Bucket: config.aws.bucket,
                            Key: `${file.hash}`,
                        }).promise());

                        attachments.push({
                            disposition: 'attachment',
                            filename: file.name,
                            type: 'image/jpeg',
                        });

                        urls.push(fileUrl('Maintenance', file));
                    });

                    const images = await Promise.all(promises);
                    attachments.forEach((att, i) => {
                        att.content = Buffer.from(images[i].Body, 'binary').toString('base64');
                    });

                    break;

                }
            }

            mail.send({
                to: to,
                from: from,
                subject: 'Alert from lethub',
                attachments: attachments,
                html:
          `
                <body>
                <h2>Maintenance</h2>
                <strong>${JSON.stringify(maintenance)}</strong>
                <h2>Photo urls</h2>
                ${urls.map((el, i) => `<a href="${el}">Photo ${i + 1}</a>`)}
                </body>
                `,
            });

            done();
        } catch (err) {
            console.log(err);
            return done(err);
        }
    });

};
