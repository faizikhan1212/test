const registerEmail = require('./register-email');
const resendRegisterEmail = require('./resend-register-email');
const resetPasswordEmail = require('./reset-password-email');
const inviteMemberEmail = require('./invite-member-email');
const resendInviteMemberEmail = require('./resend-invite-member-email');
const alertMaintenanceEmail = require('./alert-maintenance-email');

module.exports = {
    registerEmail,
    resendRegisterEmail,
    resetPasswordEmail,
    alertMaintenanceEmail,
    inviteMemberEmail,
    resendInviteMemberEmail,
};
