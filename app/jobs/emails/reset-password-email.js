const mail = require('@sendgrid/mail');
const redis = require('../../services/redis');
const randtoken = require('rand-token');
const config = require('../../../config');
const { key, from } = config.sendgrid;
const { frontend_url } = config.app;
const queue = require('../../services/queue');

module.exports = (to) => {

    queue.create('reset-password-email', {
        title: `reset password to ${to}`,
        to: to,
    }).priority('high')
        .removeOnComplete(true)
        .save();

    queue.process('reset-password-email', async (job, done) => {

        try {
            const oldToken = await redis.getAsync(`${to}:reset`);
            redis.set(oldToken, false);

            const token = randtoken.uid(256);
            redis.set(token, `${to}:reset`, 'EX', 60 * 60 * 24);
            redis.set(`${to}:reset`, token, 'EX', 60 * 60 * 24);

            mail.setApiKey(key);

            const link = `${frontend_url}/#/auth/change-password/${token}`;

            mail.send({
                to: to,
                from: from,
                subject: 'Change password',
                html: `<strong>${link}</strong>`,
            });

            done();
        } catch (err) {
            console.log(err);
            return done(err);
        }
    });

};
