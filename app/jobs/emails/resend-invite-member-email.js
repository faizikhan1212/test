const mail = require('@sendgrid/mail');
const redis = require('../../services/redis');
const randtoken = require('rand-token');
const config = require('../../../config');
const { key, from } = config.sendgrid;
const { frontend_url } = config.app;
const queue = require('../../services/queue');

module.exports = (to) => {

    queue.create(`resend:invite-email-${to}`, { title: 'inviteEmail' }).priority('high')
        .removeOnComplete(true)
        .save();
    queue.process(`resend:invite-email-${to}`, async (job, done) => {
        try {
            const oldToken = await redis.getAsync(`${to}:invite`);
            redis.set(oldToken, false);

            const token = randtoken.uid(256);
            redis.set(token, `${to}:invite`, 'EX', 60 * 60 * 24);
            redis.set(`${to}:invite`, token, 'EX', 60 * 60 * 24);

            const link = `${frontend_url}/#/auth/confirm/${token}`;

            mail.setApiKey(key);

            mail.send({
                to: to,
                from: from,
                subject: 'Invitation to lethub',
                html: `<strong>${link}</strong>`,
            });

            done();
        } catch (err) {
            console.log(err);
            return done(err);
        }
    });

};
