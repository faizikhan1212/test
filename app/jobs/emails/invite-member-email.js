const mail = require('@sendgrid/mail');
const redis = require('../../services/redis');
const randtoken = require('rand-token');
const config = require('../../../config');
const { key, from } = config.sendgrid;
const { frontend_url } = config.app;
const queue = require('../../services/queue');

module.exports = (to) => {

    queue.create(`invite-email-${to}`, { title: 'inviteEmail' }).priority('high')
        .removeOnComplete(true)
        .save();
    queue.process(`invite-email-${to}`, (job, done) => {
        try {
            const token = randtoken.uid(256);
            const link = `${frontend_url}/#/auth/create-password/${token}`;

            mail.setApiKey(key);

            redis.set(token, `${to}:invite`, 'EX', 60 * 60 * 24);
            redis.set(`${to}:invite`, token, 'EX', 60 * 60 * 24);

            mail.send({
                to: to,
                from: from,
                subject: 'Invitation to lethub',
                html: `<strong>${link}</strong>`,
            });

            done();
        } catch (err) {
            console.log(err);
            return done(err);
        }
    });

};
