const { Dictionary } = require('../../../database/models');
const queue = require('../../services/queue');
const { allowedData } = require('../../../config');
const updateChatbotDictionary = require('../../../kafka/produces/dictionary');

module.exports = (userId, companyId, { amenities, utilities, appliances }) => {

    queue.create(`find-custom-items-and-update-dictionary-${userId}`, { title: 'Find custom items and update dictionary' }).priority('high')
        .removeOnComplete(true)
        .save();
    queue.process(`find-custom-items-and-update-dictionary-${userId}`, async (job, done) => {
        try {

            let dictionary = await Dictionary.findOne({
                where: {
                    companyId,
                },
            });

            if (!dictionary) {
                dictionary = await Dictionary.createAndPopulate(companyId, allowedData);
            }

            const newAmenitiesItems = amenities ? amenities.filter(item => !dictionary.amenities.includes(item)) : null;
            const newUtilitiesItems = utilities ? utilities.filter(item => !dictionary.utilities.includes(item)) : null;
            const newAppliancesItems = appliances ? appliances.filter(item => !dictionary.appliances.includes(item)) : null;

            if (newAmenitiesItems && newAmenitiesItems.length > 0) {
                await Dictionary.pushItems(companyId, 'amenities', newAmenitiesItems);
            }

            if (newUtilitiesItems && newUtilitiesItems.length > 0) {
                await Dictionary.pushItems(companyId, 'utilities', newUtilitiesItems);
            }

            if (newAppliancesItems && newAppliancesItems.length > 0) {
                await Dictionary.pushItems(companyId, 'appliances', newAppliancesItems);
            }

            await updateChatbotDictionary(companyId);

            done();
        } catch (err) {
            console.log(err);
            return done(err);
        }
    });

};
