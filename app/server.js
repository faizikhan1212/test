const express = require('express');
const helmet = require('helmet');
const cors = require('cors');
const passport = require('passport');
const path = require('path');
const morgan = require('morgan');
const socket = require('socket.io');
const kue = require('kue');
const ui = require('kue-ui');
const rateLimit = require('express-rate-limit');
const strategy = require('../app/middlewares/strategy');
const checkUserStatus = require('./middlewares/check-user-status');
const checkHeader = require('../app/middlewares/check-header');
const billing = require('./middlewares/stripe/billing');
const verifyPrivateFile = require('../app/middlewares/verify-private-file');
const config = require('../config');
const { publicRoute, privateRoute, swaggerRoute, webhookRoute } = require('../routes');

passport.use(strategy.local());
passport.use(strategy.google());
passport.use(strategy.bearer());

require('../kafka/consumer');

const app = express();

// var mailgun = require('mailgun-js')({apiKey: config.mailgun.api_key , domain: config.mailgun.domain});
// mailgun.post('/routes', {"priority": 0, "description": 'Autoresponder Webhook', "expression": 'catch_all()', "action": 'forward("http://enviromonitor.pagekite.me/public/autoresponder")'}, function (error, body) {
//   console.log(body);
// });
const io = socket.listen(app.listen(config.app.port));

app.use(cors());
app.use(helmet());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(passport.initialize());
app.use(rateLimit({
    windowMs: 60 * 1000,
    max: 600,
    message: 'Too many requests',
}));
app.use(express.static('public'));


passport.serializeUser((user, cb) => user !== null ? cb(null, user) : cb(null, false));


app.use('/email', publicRoute);

app.all('/private/*', passport.authenticate('bearer', { session: false }));
app.all('/private/*', [checkHeader.contentType, checkUserStatus, billing]);
app.all('/public/*', [checkHeader.contentType]);


app.use((req, res, next) => {
    req.io = io;
    next();
});


app.use('/public', publicRoute);
app.use('/private', privateRoute);
app.use('/webhook', webhookRoute);


if (String(config.swagger.swagger_doc) === 'true') {

    app.use(morgan(':method | :status | :url | :response-time'));
    app.use('/doc', swaggerRoute);

    ui.setup({
        apiURL: '/api',
        baseURL: '/kue',
        updateInterval: 15000,
    });

    app.use('/api', kue.app);
    app.use('/kue', ui.app);
}


app.use('/public-files', express.static(path.join(__dirname, `/../${config.file.local_path}/public`)));
app.use('/private-files', verifyPrivateFile, express.static(path.join(__dirname, `/../${config.file.local_path}/private`)));

io.sockets.on('connection', () => {
    console.log('Express: Socket client connected');
});


module.exports = app;
