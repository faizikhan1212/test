const stripe = require('../services/stripe');
const { User, Customer, Subscription } = require('../../database/models');
const config = require('../../config');

module.exports = {

    async paymentSucceeded(req, res) {
        const { body } = req;
        const currentDate = new Date();

        try {
            const object = body.data && body.data.object ? body.data.object : null;

            if (object !== null && object.subscription !== null && object.customer !== null) {
                const stripeSubscription = await stripe.subscriptions.retrieve(object.subscription);
                const stripeCustomer = await stripe.customers.retrieve(object.customer);

                const user = await User.findOne({
                    raw: false,
                    where: {
                        email: stripeCustomer.email,
                    },
                });

                const customer = await Customer.findOne({
                    raw: false,
                    where: {
                        userId: user.id,
                    },
                });

                if (customer === null) {

                    await Subscription.create({
                        userId: user.id,
                        subscription: stripeSubscription.id,
                    });

                    await Customer.create({
                        userId: user.id,
                        customer: stripeCustomer.id,
                    });

                } else {

                    if (user.subscription) {
                        const subscription = await Subscription.findOne({
                            attributes: [
                                'subscription',
                            ],
                            where: {
                                userId: user.id,
                            },
                        });

                        stripe.subscriptions.del(subscription.subscription, (err, confirmation) => {
                        });
                    }

                    await Subscription.update({
                        subscription: stripeSubscription.id,
                    }, {
                        where: {
                            userId: user.id,
                        },
                    });
                }

                await user.update({
                    maxUnits: stripeSubscription.quantity,
                    billingPlan: stripeSubscription.plan.id,
                    billingExpire: currentDate.setMonth(currentDate.getMonth() + 1),
                    subscription: true,
                });

            }

            return res.status(200).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    async paymentintentSucceeded(req, res) {
        const { body } = req;

        try {
            const object = body.data && body.data.object ? body.data.object : null;

            if (object !== null && object.customer !== null) {

                const stripeCustomer = await stripe.customers.retrieve(object.customer);
                const listSubscriptions = await stripe.subscriptions.list({
                    limit: 1,
                    customer: stripeCustomer.id,
                });
                const listPlans = await stripe.plans.list({
                    limit: 1,
                    product: config.stripe.product_billing_id,
                });

                if (listPlans && listPlans.data && listPlans.data.length > 0) {

                    const extendUnits = object.amount_received / listPlans.data[0].amount;

                    stripe.invoiceItems.create({
                        customer: object.customer,
                        amount: object.amount_received,
                        currency: 'usd',
                        description: 'One-time payment',
                    }, async (err) => {
                        if (!err) {
                            const invoice = await stripe.invoices.create({
                                customer: object.customer,
                                auto_advance: true,
                            });
                            await stripe.invoices.finalizeInvoice(invoice.id);
                        }
                    });

                    const user = await User.findOne({
                        raw: false,
                        where: {
                            email: stripeCustomer.email,
                        },
                    });

                    if (user !== null) {

                        if (listSubscriptions && listSubscriptions.data && listSubscriptions.data.length > 0) {

                            stripe.subscriptions.update(listSubscriptions.data[0].id, {
                                prorate: false,
                                items: [{
                                    id: listSubscriptions.data[0].items.data[0].id,
                                    quantity: user.dataValues.maxUnits + extendUnits,
                                }],
                            });

                        }

                        user.update({
                            maxUnits: user.dataValues.maxUnits + extendUnits,
                        });

                    }

                }
            }

            return res.status(200).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }

    },

};
