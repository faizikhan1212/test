const { stripe } = require('../../config');

module.exports = require('stripe')(stripe.secret_key);
