const { Scheduling, CalendarAccount } = require('../../database/models');
const config = require('../../config/google');
const { google } = require('googleapis');
const uuid = require('uuid');
const moment = require('moment');
const base64url = require('base64url');

const oAuth2Client = new google.auth.OAuth2(
    config.client_id,
    config.client_secret,
    config.redirect_uris[0],
);

oAuth2Client.on('tokens', async (tokens) => {
    let auth = getAuth(tokens);
    let profileInfo = await getProfileAsync(auth);
    if (tokens.refresh_token && profileInfo) {
        await CalendarAccount.update({
            accessToken: tokens.access_token,
            refreshToken: tokens.refresh_token,
        }, {
            where: {
                email: profileInfo.emailAddress,
            },
        });
    }
});

const getAuthClient = function() {
    return new google.auth.OAuth2(
        config.client_id,
        config.client_secret,
        config.redirect_uris[0],
    );
};

const getAuthUrl = (userId, accountSetting) => {
    const state = base64url(JSON.stringify({ userId, by: 'google', accountSetting }));

    const authUrl = oAuth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: config.scopes,
        state,
    });
    return authUrl;
};

const getAccessTokenAsync = code => {
    return new Promise((resolve, reject) => {
        oAuth2Client.getToken(code, (err, token) => {
            if (err) {
                return reject(err);
            }
            return resolve(token);
        });
    });
};

const getAuth = token => {
    let newOAuth2Client = getAuthClient();
    token['access_type'] = 'offline';
    newOAuth2Client.setCredentials(token);
    return newOAuth2Client;
};

const getProfileAsync = auth => {
    return new Promise((resolve, reject) => {
        const gmail = google.gmail({ version: 'v1', auth });
        gmail.users.getProfile(
            {
                auth: auth,
                userId: 'me',
            },
            function(err, res) {
                if (err) {
                    console.log('getProfile', err);
                    return reject(err);
                } else {
                    console.log('getProfile', res.data);
                    return resolve(res.data);
                }
            },
        );
    });
};

const listEventsAsync = (authToken, user, accountId) => {
    return new Promise((resolve, reject) => {
        let auth = getAuth(authToken);
        const calendar = google.calendar({ version: 'v3', auth });
        try {
            let startDate = moment().startOf('day');
            let endDate = moment(startDate).add(100, 'days');
            // The start and end date are passed as query parameters
            let startDateTime = startDate.toISOString();
            let endDateTime = endDate.toISOString();
            let scheduling;

            calendar.events.list(
                {
                    calendarId: 'primary',
                    timeMin: startDateTime,
                    timeMax: endDateTime,
                    // maxResults: 200,
                    showDeleted: true,
                    singleEvents: true,
                    orderBy: 'startTime',
                },
                async (err, res) => {
                    if (err) {
                        return reject(err);
                    }
                    const events = res.data.items;
                    if (events.length) {
                        let updateEventsAsync = events.map(async (event, i) => {
                            return new Promise(async (resolve, reject) => {
                                try {
                                    const start = event.start.dateTime || event.start.date;
                                    const end = event.end.dateTime || event.end.date;
                                    // check if event already exists
                                    const eventFound = await Scheduling.findOne({
                                        where: { googleEventId: event.id },
                                    });

                                    if (eventFound) {

                                        let updateData = {};
                                        if (event.status === 'cancelled') {
                                            updateData.isDeleted = true;
                                        } else {
                                            updateData.dateOfShowing = [start, end];
                                            updateData.clientName = event.summary;
                                            updateData.calendarAccountId = accountId;
                                        }
                                        scheduling = await Scheduling.update(updateData, {
                                            where: {
                                                googleEventId: event.id,
                                            },
                                            returning: true,
                                            plain: true,
                                        });
                                    } else if (event.status !== 'cancelled') {
                                        scheduling = await Scheduling.create({
                                            userId: user.id,
                                            calendarAccountId: accountId,
                                            clientName: event.summary || 'No Title',
                                            createdOnApp: 'google',
                                            googleEventId: event.id,
                                            clientEmail: user.email,
                                            clientPhone: user.phone,
                                            dateOfShowing: [start, end],
                                        });

                                    }

                                    return resolve(scheduling);
                                } catch (error) {
                                    return reject(error);
                                }
                            });
                        });

                        let updatedFetchedEvents = await Promise.all(updateEventsAsync);
                        return resolve(updatedFetchedEvents);
                    } else {
                        return resolve([]);
                    }
                },
            );
        } catch (error) {
            reject(error);
        }
    });
};

function addEventsAsync(authToken, eventData) {
    return new Promise((resolve, reject) => {

        try {
            let auth = getAuth(authToken);
            const calendar = google.calendar({ version: 'v3', auth });
            const googleEvent = {
                summary: eventData.clientName,
                location: `${eventData.propertyAddress}, ${eventData.unit}`,
                description: `${eventData.clientEmail || ''}, ${eventData.clientPhone || ''}, ${eventData.noteText || ''}`,
                start: {
                    dateTime: eventData.dateOfShowing[0],
                    timeZone: 'GMT',
                },
                end: {
                    dateTime: eventData.dateOfShowing[1],
                    timeZone: 'GMT',
                },
                reminders: {
                    useDefault: false,
                },
            };

            if (eventData.googleEventId) {
                googleEvent.id = eventData.googleEventId;
            }

            calendar.events.insert(
                {
                    auth: auth,
                    calendarId: 'primary',
                    resource: googleEvent,
                },
                async function(err, res) {
                    if (err) {
                        // if identifier already exists i.e the resource id
                        if (err.code === 409) {
                            // check if the event is deleted/updated on google
                            let googleEventRes = await getEventAsync(authToken, { id: googleEvent.id });
                            if (googleEventRes && googleEventRes.data && googleEventRes.data.status === 'cancelled') {
                                return resolve(null);
                            }

                            let googleEventFound = googleEventRes && googleEventRes.data && googleEventRes.data || null;

                            if (googleEventFound && eventData.updatedAt && moment(eventData.updatedAt).isAfter(googleEventFound.updated)) {
                                calendar.events.update(
                                    {
                                        auth: auth,
                                        calendarId: 'primary',
                                        eventId: googleEvent.id,
                                        resource: googleEvent,
                                    },
                                    function(err, res) {
                                        if (err) {
                                            return reject(err);
                                        }
                                        return resolve(res);
                                    },
                                );
                            }
                        } else {
                            return reject(err);
                        }
                    }
                    return resolve(res);
                },
            );
        } catch (error) {
            return reject(error);
        }

    });
}

function updateEventsAsync(authToken, eventData) {
    return new Promise((resolve, reject) => {

        let auth = getAuth(authToken);
        const calendar = google.calendar({ version: 'v3', auth });

        const googleUpdateEvent = {
            id: eventData.id,
            summary: eventData.clientName,
            location: `${eventData.propertyAddress},${eventData.unit}`,
            description: `${eventData.clientEmail || ''}, ${eventData.clientPhone || ''}, ${eventData.noteText || ''}`,
            start: {
                dateTime: eventData.dateOfShowing[0],
                timeZone: 'GMT',
            },
            end: {
                dateTime: eventData.dateOfShowing[1],
                timeZone: 'GMT',
            },
        };


        calendar.events.update(
            {
                auth: auth,
                calendarId: 'primary',
                eventId: eventData.id,
                resource: googleUpdateEvent,
            },
            function(err, res) {
                if (err) {
                    return reject(err);
                }
                return resolve(res);
            },
        );
    });
}

function removeEventsAsync(authToken, event) {
    return new Promise((resolve, reject) => {
        let auth = getAuth(authToken);
        const calendar = google.calendar({ version: 'v3', auth });

        calendar.events.delete(
            {
                auth: auth,
                calendarId: 'primary',
                eventId: event.id,
            },
            function(err) {
                if (err && err.code !== 410) {
                    return reject(err);
                }
                return resolve(true);
            },
        );
    });
}

function getEventAsync(authToken, event) {
    return new Promise((resolve, reject) => {
        let auth = getAuth(authToken);
        const calendar = google.calendar({ version: 'v3', auth });

        calendar.events.get(
            {
                auth: auth,
                calendarId: 'primary',
                eventId: event.id,
            },
            function(err, res) {
                if (err) {
                    return reject(err);
                }
                return resolve(res);
            },
        );
    });
}

function watch(auth) {
    const calendar = google.calendar({ version: 'v3', auth });

    const options = {
        calendarId: 'primary',
        resource: {
            id: uuid(),
            token: 'myToken123',
            type: 'web_hook',
            address: 'https://webhook.site/8dd0acc3-e33d-4de2-98a5-f09b83df0032',
        },
    };

    calendar.events.watch(options, (err, res) => {
        return null;
    });
}

module.exports = {
    getAuthClient,
    getAuthUrl,
    getAccessTokenAsync,
    getAuth,
    listEventsAsync,
    addEventsAsync,
    removeEventsAsync,
    getProfileAsync,
    updateEventsAsync,
    watch,
};
