const AWS = require('aws-sdk');
const aws = require('../../config/aws');

AWS.config.update({
    bucket: aws.bucket,
    accessKeyId: aws.access_key_id,
    secretAccessKey: aws.secret_access_key,
    region: aws.region,
});
const s3 = new AWS.S3();

module.exports = s3;
