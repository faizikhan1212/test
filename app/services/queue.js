const kue = require('kue');
const config = require('../../config');
const queue = kue.createQueue({
    prefix: 'q',
    redis: {
        port: config.redis.port,
        host: config.redis.host,
        db: 3,
        options: {},
    },
});


queue.on('failed', (id) => {
    console.log(`job ${id} failed`);
});


queue.on('job complete', (id) => {
    console.log(`job ${id} complete`);
});

queue.watchStuckJobs(6000);


module.exports = queue;
