const config = require('../../config/microsoft');
var outlook = require('node-outlook');
const moment = require('moment');
const base64url = require('base64url');

const credentials = {
    client: {
        id: config.client_id,
        secret: config.client_secret,
    },
    auth: {
        tokenHost: 'https://login.microsoftonline.com',
        tokenPath: 'common/oauth2/v2.0/token',
        authorizePath: 'common/oauth2/v2.0/authorize',
    },
};

const { Scheduling, CalendarAccount } = require('../../database/models');

const oauth2 = require('simple-oauth2').create(credentials);


const getAuthUrl = (userId, accountSetting) => {
    const state = base64url(JSON.stringify({ userId, by: 'office', accountSetting }));
    const authUrl = oauth2.authorizationCode.authorizeURL({
        redirect_uri: config.redirect_uris[0],
        scope: 'offline_access openid profile email https://outlook.office.com/calendars.readwrite ',
        state,
    });
    return authUrl;
};

const getAccessTokenAsync = code => {
    return new Promise(async (resolve, reject) => {
        try {
            const result = await oauth2.authorizationCode.getToken(
                {
                    code: code,
                    redirect_uri: config.redirect_uris[0],
                    scope: 'offline_access openid profile email  https://outlook.office.com/calendars.readwrite ',
                });

            const accessToken = oauth2.accessToken.create(result);
            resolve(accessToken.token);

        } catch (error) {
            console.log('Access error', error);
            reject(error);
        }

    });
};

const getEmailFromIdToken = id_token => {
    // JWT is in three parts, separated by a '.'
    var token_parts = id_token.split('.');

    // Token content is in the second part, in urlsafe base64
    var encoded_token = Buffer.from(
        token_parts[1].replace('-', '+').replace('_', '/'),
        'base64',
    );

    var decoded_token = encoded_token.toString();

    var jwt = JSON.parse(decoded_token);

    return jwt.preferred_username;
};

const tokenReceivedUpdateAsync = (userId, calendarAccountId, token) => {
    return new Promise(async (resolve, reject) => {
        try {
            let accessToken = token.token.access_token;
            let refreshToken = token.token.refresh_token;
            let idToken = token.token.id_token;

            let account = await CalendarAccount.findByPk(calendarAccountId, {
                raw: false,
            });

            if (account) {
                account = await account.update({
                    accessToken: accessToken,
                    expiryDate: token.token.expires_at.getTime(),
                    refreshToken: refreshToken,
                    idToken: idToken,
                },
                {
                    returning: true,
                    plain: true,
                });
                return resolve(account.dataValues);
            }
        } catch (error) {
            return reject(error);
        }
    });
};

const getTokenFromRefreshTokenAsync = (userId, calendarAccountId, refreshToken, accessToken) => {
    return new Promise(async (resolve, reject) => {
        try {
            var token = oauth2.accessToken.create({
                refresh_token: refreshToken,
                access_token: accessToken,
                expires_in: 7200,
            });
            const result = await token.refresh();
            if (!result) {
                return reject(new Error('Error with token refresh'));
            } else {
                let updatedAccount = await tokenReceivedUpdateAsync(userId, calendarAccountId, result);

                return resolve(updatedAccount);
            }

        } catch (error) {
            return reject(error);
        }
    });
};

const getEventsAsync = (accessToken, email, user, accountId) => {
    return new Promise((resolve, reject) => {
        outlook.base.setApiEndpoint('https://outlook.office.com/api/v2.0');

        let startDate = moment().startOf('day');
        let endDate = moment(startDate).add(100, 'days');
        // The start and end date are passed as query parameters
        let startDateTime = startDate.toISOString();
        let endDateTime = endDate.toISOString();

        let filter = `Start/DateTime ge '${startDateTime}' and Start/DateTime le '${endDateTime}'`;

        console.log('events filter', filter);
        let queryParams = {
            $select: 'Subject,Start,End,Location,IsCancelled',
            $orderby: 'Start/DateTime desc',
            $top: 2000,
            $filter: filter,
        };

        let userInfo = {
            email: email,
        };

        outlook.calendar.getEvents(
            {
                token: accessToken,
                folderId: 'Inbox',
                odataParams: queryParams,
                user: userInfo,
            },
            async (err, res) => {
                if (err) {
                    console.log('err', err);
                    return reject(err);
                } else if (res) {
                    const events = res.value;
                    if (events.length) {
                        let updateEventsAsync = events.map(async (event, i) => {
                            return new Promise(async (resolve, reject) => {
                                try {
                                    const start = event.Start.DateTime;
                                    const end = event.End.DateTime;
                                    // check if event already exists
                                    const eventFound = await Scheduling.findOne(
                                        {
                                            where: { officeEventId: event.Id },
                                        },
                                    );

                                    if (eventFound) {
                                        let updateData = {};
                                        if (event.IsCancelled) {
                                            updateData.isDeleted = true;
                                        } else {
                                            updateData.dateOfShowing = [
                                                start,
                                                end,
                                            ];
                                            updateData.calendarAccountId = accountId;
                                            updateData.clientName = event.Subject || 'No Title';
                                        }
                                        await Scheduling.update(updateData, {
                                            where: {
                                                officeEventId: event.Id,
                                            },
                                        });
                                    } else if (event.Status !== 'cancelled') {
                                        await Scheduling.create(
                                            {
                                                userId: user.id,
                                                calendarAccountId: accountId,
                                                unitId: null,
                                                propertyId: null,
                                                clientName: event.Subject || 'No Title',
                                                createdOnApp: 'office',
                                                officeEventId: event.Id,
                                                clientEmail: email,
                                                clientPhone: user.phone,
                                                dateOfShowing: [start, end],
                                            },
                                        );
                                    }
                                    return resolve(event);
                                } catch (error) {
                                    console.log('error', error);
                                    return reject(error);
                                }
                            });
                        });
                        let updatedFetchedEvents = await Promise.all(
                            updateEventsAsync,
                        );
                        return resolve(updatedFetchedEvents);
                    } else {
                        return resolve([]);
                    }
                }
            },
        );
    });
};

const getEventAsync = (accessToken, email, eventId) => {
    return new Promise((resolve, reject) => {
        outlook.base.setApiEndpoint(`https://outlook.office.com/api/v2.0`);
        outlook.calendar.getEvent(
            { token: accessToken, eventId: eventId },
            async (err, res) => {
                if (err) {
                    if (err && err.includes('404')) {
                        return resolve(null);
                    }
                    return reject(err);
                } else if (res) {
                    return resolve(res);
                }
            },
        );
    });
};

function addEventsAsync(accessToken, eventData) {
    return new Promise(async (resolve, reject) => {
        outlook.base.setApiEndpoint('https://outlook.office.com/api/v2.0');
        if (!eventData || !accessToken) {
            return new Error('Invalid input data to update');
        }

        const officeEvent = {
            Subject: eventData.clientName || 'No Title',
            Location: {
                DisplayName: `${eventData.propertyAddress},${eventData.unit}`,
            },
            Start: {
                DateTime: eventData.dateOfShowing[0],
                TimeZone: 'India Standard Time',
            },
            End: {
                DateTime: eventData.dateOfShowing[1],
                TimeZone: 'India Standard Time',
            },
        };

        if (eventData.officeEventId) {
            let foundOfficeEvent = await getEventAsync(
                accessToken,
                '',
                eventData.officeEventId,
            );
            if (
                foundOfficeEvent &&
        eventData.updatedAt &&
        moment(eventData.updatedAt).isAfter(
            foundOfficeEvent.LastModifiedDateTime,
        )
            ) {
                eventData.id = eventData.officeEventId;
                let updatedEvent = await updateEventsAsync(
                    accessToken,
                    eventData,
                );
                return resolve(updatedEvent);
            } else {
                return resolve(eventData);
            }
        }

        let addEventParameters = {
            token: accessToken,
            event: officeEvent,
        };

        outlook.calendar.createEvent(addEventParameters, (error, event) => {
            if (error) {
                return reject(error);
            } else {
                return resolve(event);
            }
        });
    });
}

function updateEventsAsync(accessToken, eventData) {
    return new Promise((resolve, reject) => {
        outlook.base.setApiEndpoint('https://outlook.office.com/api/v2.0');
        try {
            if (!eventData.id || !accessToken) {
                return new Error('Invalid input data to update');
            }

            const officeEvent = {
                Subject: eventData.clientName || 'No Title',
                Location: {
                    DisplayName: `${eventData.propertyId},${eventData.unitId}`,
                },
                Start: {
                    DateTime: eventData.dateOfShowing[0],
                    TimeZone: 'India Standard Time',
                },
                End: {
                    DateTime: eventData.dateOfShowing[1],
                    TimeZone: 'India Standard Time',
                },
            };

            let updateEventParameters = {
                token: accessToken,
                eventId: eventData.id,
                update: officeEvent,
            };

            outlook.calendar.updateEvent(
                updateEventParameters,
                (error, event) => {
                    if (error) {
                        return reject(error);
                    } else {
                        return resolve(event);
                    }
                },
            );
        } catch (error) {
            return reject(error);
        }
    });
}

function removeEventsAsync(accessToken, event) {
    return new Promise((resolve, reject) => {
        if (!event.id || !accessToken) {
            return new Error('Invalid input data to remove event');
        }

        const requestParams = {
            token: accessToken,
            eventId: event.id,
        };

        outlook.calendar.deleteEvent(requestParams, (error, res) => {
            if (error) {
                if (error && error.includes('404')) {
                    return resolve(null);
                }
                return reject(error);
            } else {
                return resolve(res);
            }
        });
    });
}

const watchAsync = (token, email, syncUrl) => {
    return new Promise((resolve, reject) => {
        try {
            if (token === undefined || email === undefined) {
                return;
            }

            // Set the endpoint to API v2
            outlook.base.setApiEndpoint('https://outlook.office.com/api/v2.0');
            // Set the user's email as the anchor mailbox
            outlook.base.setAnchorMailbox(email);
            // Set the preferred time zone
            // outlook.base.setPreferredTimeZone('IST');

            // Use the syncUrl if available
            var requestUrl = syncUrl;
            if (!requestUrl) {
            // Calendar sync works on the CalendarView endpoint
                requestUrl = outlook.base.apiEndpoint() + '/Me/CalendarView';
            }

            // Set up our sync window from midnight on the current day to
            // midnight 100 days from now.
            var startDate = moment().startOf('day');
            var endDate = moment(startDate).add(100, 'days');
            // The start and end date are passed as query parameters
            var params = {
                startDateTime: startDate.toISOString(),
                endDateTime: endDate.toISOString(),
            };

            // Set the required headers for sync
            var headers = {
                Prefer: [
                    // Enables sync functionality
                    'odata.track-changes',
                    // Requests only 5 changes per response
                    'odata.maxpagesize=1',
                ],
            };

            var apiOptions = {
                url: requestUrl,
                token: token,
                headers: headers,
                query: params,
            };

            outlook.base.makeApiCall(apiOptions, function(error, response) {
                if (error) {
                    console.log(JSON.stringify(error));
                } else {
                    if (response.statusCode !== 200) {
                        return resolve(null);
                    } else {
                        let nextSyncUrl = null;
                        var nextLink = response.body['@odata.nextLink'];
                        if (nextLink !== undefined) {
                            nextSyncUrl = nextLink;
                        }
                        var deltaLink = response.body['@odata.deltaLink'];
                        if (deltaLink !== undefined) {
                            nextSyncUrl = deltaLink;
                        }

                        let result = {
                            nextSyncUrl: nextSyncUrl,
                            data: response.body.value,
                        };
                        return resolve(result);
                    }
                }
            });
        } catch (error) {
            return reject(error);
        }
    });
};

module.exports = {
    getEventAsync,
    getAuthUrl,
    getAccessTokenAsync,
    getTokenFromRefreshTokenAsync,
    getEventsAsync,
    addEventsAsync,
    removeEventsAsync,
    updateEventsAsync,
    watchAsync,
    getEmailFromIdToken,
};
