const redis = require('redis');
const bluebird = require('bluebird');
const config = require('../../config');

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

const client = redis.createClient(config.redis.port, config.redis.host);


client.on('connect', function() {
    console.log('Redis client connected');
});

client.on('error', function(err) {
    console.log('Something went wrong ' + err);
});

module.exports = client;
