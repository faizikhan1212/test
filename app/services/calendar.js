const {
    CalendarAccount,
} = require('../../database/models');
const googleService = require('./google');
const microsoftService = require('./microsoft');

const syncEventsAsync = user => {
    let returnObj = { status: null, message: '', data: null };
    return new Promise(async (resolve, reject) => {
        try {
            let account = await CalendarAccount.findOne({
                where: { userId: user.id },
            });

            if (account) {

                let returnRes = { pushedEventsRes: null, pullEventsRes: null };

                if (account.type === 'google') {

                    let authToken = await CalendarAccount.getGoogleAccessToken(
                        user.id,
                    );

                    returnRes.pullEventsRes = await googleService.listEventsAsync(
                        authToken,
                        user,
                        account.id,
                    );
                } else if (account.type === 'office') {

                    const expAccessToken = account.expiryDate;
                    const now = new Date().getTime();
                    let updatedAccount;
                    if (now + 1000 >= expAccessToken) {
                        if (account.refreshToken) {
                            updatedAccount = await microsoftService.getTokenFromRefreshTokenAsync(user.id, account.id, account.refreshToken, account.accessToken);
                        } else {
                            returnObj.status = 404;
                            return resolve(returnObj);
                        }
                    }

                    let accessToken = updatedAccount ? updatedAccount.accessToken : account.accessToken;
                    let email = account.email;

                    returnRes.pullEventsRes = await microsoftService.getEventsAsync(
                        accessToken,
                        email,
                        user,
                        account.id,
                    );
                }
                returnObj.status = 200;
                returnObj.data = returnRes;
                return resolve(returnObj);
            } else {
                returnObj.status = 404;
                return resolve(returnObj);
            }
        } catch (err) {
            return reject(err);
        }
    });
};

module.exports = {
    syncEventsAsync,
};
