const { User, CalendarAccount } = require('../../../database/models');
const googleService = require('../../services/google');
const officeService = require('../../services/microsoft');
const base64url = require('base64url');

module.exports = {
    async callbackAuth(req, res) {
        const { code } = req.query;
        let state = JSON.parse(base64url.decode(req.query.state));
        const { userId, by, accountSetting } = state;

        console.log('accountSetting', accountSetting);

        try {

            if (by === 'google') {

                const authDetails = await googleService.getAccessTokenAsync(code);
                const auth = await googleService.getAuth(authDetails);

                let profileInfo = null;

                profileInfo = await googleService.getProfileAsync(auth);
                const expectedUser = await User.findByPk(userId);

                if (expectedUser !== null) {
                    const account = await CalendarAccount.findOne({
                        where: { userId: expectedUser.id },
                    });
                    if (account) {

                        const result = await CalendarAccount.destroy({
                            where: {
                                userId: expectedUser.id,
                            },
                        });

                        if (result < 0) {
                            return res.status(404).json();
                        }

                    }
                    await CalendarAccount.create({
                        userId: expectedUser.id,
                        type: 'google',
                        email: profileInfo.emailAddress,
                        accessToken: authDetails.access_token,
                        idToken: authDetails.id_token,
                        tokenType: authDetails.token_type,
                        refreshToken: authDetails.refresh_token,
                        scope: authDetails.scope,
                        expiryDate: authDetails.expiry_date,
                        isTwoWaySync: accountSetting.isTwoWaySync,
                        alwaysSync: accountSetting.alwaysSync,
                    });
                }

                res.type('.html');
                res.send('<html><body>Success</body>' +
          '<script>window.opener.postMessage(\'Google sync success\', \'*\');  setTimeout(() => { window.close() }, 1000)</script>' +
          '</html>');


            } else if (by === 'office') {
                const authDetails = await officeService.getAccessTokenAsync(code);

                const expectedUser = await User.findByPk(userId);

                if (expectedUser !== null) {
                    const account = await CalendarAccount.findOne({
                        where: { userId: expectedUser.id },
                    });

                    console.log('authDetails', authDetails);
                    const email = officeService.getEmailFromIdToken(authDetails.id_token);

                    const expiry_date = authDetails.expires_at.getTime();

                    if (account) {

                        const result = await CalendarAccount.destroy({
                            where: {
                                userId: expectedUser.id,
                            },
                        });

                        if (result < 0) {
                            return res.status(404).json();
                        }

                    }

                    await CalendarAccount.create({
                        userId: expectedUser.id,
                        type: 'office',
                        email: email,
                        accessToken: authDetails.access_token,
                        idToken: authDetails.id_token,
                        tokenType: authDetails.token_type,
                        refreshToken: authDetails.refresh_token,
                        scope: authDetails.scope,
                        expiryDate: expiry_date,
                        isTwoWaySync: accountSetting.isTwoWaySync,
                        alwaysSync: accountSetting.alwaysSync,
                    });


                }

                res.type('.html');
                res.send('<html><body>Success</body>' +
          '<script>window.opener.postMessage(\'Office 365 sync success\', \'*\');  setTimeout(() => { window.close() }, 1000)</script>' +
          '</html>');

            }

            return res.status(404).json();
        } catch (err) {
            console.log('err', err);
            return res.status(500).json();
        }
    },
    async callbackWatch(req, res) {
        console.log('req.query on watch', req.query);
        console.log('req.body on watch', req.body);
    },
};
