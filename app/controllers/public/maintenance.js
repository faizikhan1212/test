const { Maintenance, Lead } = require('../../../database/models');
const { validationResult } = require('express-validator/check');

module.exports = {
    /**
   * @swagger
   * /public/maintenance/create:
   *   post:
   *     tags:
   *       - Maintenance
   *     description: Create maintenance
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *              leadId: 1,
   *              tenantName: Ivanov Ivan,
   *              category: Security,
   *              description: Broken door,
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *         schema:
   *           type: object
   *           $ref: '#/definitions/Maintenance'
   *       400:
   *          description: Bad Request
   *       404:
   *          description: Not found
   */

    async create(req, res) {
        const { body } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const lead = await Lead.findByPk(body.leadId);

            if (lead !== null) {
                const maintenance = await Maintenance.create({
                    leadId: body.leadId,
                    tenantName: body.tenantName,
                    category: body.category,
                    description: body.description,
                });

                return res.status(200).json({ maintenance: maintenance });
            }

            return res.status(404).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },
};
