const { Responders } = require('../../../database/models');
const config = require('../../../config');
const mailgun = require('mailgun-js')({apiKey: config.mailgun.api_key , domain: config.mailgun.domain});
module.exports = {


    async getEmail(req,res){
        const body = req.body;
        const recipient = body.recipient;
        const sender = body.sender;
        const data = await Responders.getresponse(recipient);
        let autoresponse = data[0][0]['autoresponse'];

        // Send Auto Reply 
        const emailData = {
            from: recipient,
            to: sender,
            subject: 'Re: Listings Inquiry',
            text: autoresponse
          };
          
          mailgun.messages().send(emailData, (error, body) => {
            console.log(body);
          });
        return res.status(200).send('ok');
    }
    // ,

    // async sendResponse(rec , snd){
        
    // }
};