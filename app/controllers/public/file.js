const { validationResult } = require('express-validator/check');
const _ = require('lodash');
const { File } = require('../../../database/models');
const fileUrl = require('../../helpers/file/file-url');

module.exports = {
    /**
   * @swagger
   * /public/file/show:
   *   get:
   *     tags:
   *       - File
   *     description: Show first file
   *     parameters:
   *         - name: entityName
   *           in: query
   *           required: true
   *           type: string
   *           example: Property
   *         - name: entityId
   *           required: true
   *           in: query
   *           type: integer
   *           example: 1
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async show(req, res) {
        const { query } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const capitalizeEntityName = _.capitalize(query.entityName);

            let file = await File.findOne({
                where: {
                    entityId: query.entityId,
                    entityName: capitalizeEntityName,
                },
            });

            if (file) {
                return res.status(200).json({
                    file: {
                        id: file.id,
                        url: fileUrl(capitalizeEntityName, file),
                        name: file.name,
                    },
                });
            }

            return res.status(200).json({ file: {} });
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /public/file/list:
   *   get:
   *     tags:
   *       - File
   *     description: Get list files
   *     parameters:
   *         - name: entityName
   *           in: query
   *           required: true
   *           type: string
   *           example: Property
   *         - name: entityId
   *           in: query
   *           required: true
   *           type: integer
   *           example: 1
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async list(req, res) {
        const { query } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const capitalizeEntityName = _.capitalize(query.entityName);

            let files = await File.findAll({
                where: {
                    entityId: query.entityId,
                    entityName: capitalizeEntityName,
                },
            });

            if (files && files.length > 0) {
                files = files.map(file => ({
                    id: file.id,
                    url: fileUrl(capitalizeEntityName, file),
                    name: file.name,
                }));

                return res.status(200).json({ files: files });
            }

            return res.status(200).json({ files: [] });
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },
};
