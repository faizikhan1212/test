const bcrypt = require('bcrypt-nodejs');
const redis = require('../../services/redis');
const { User } = require('../../../database/models');
const { validationResult } = require('express-validator/check');


module.exports = {

    /**
   * @swagger
   * /public/user/confirm:
   *   post:
   *     tags:
   *       - User
   *     description: Confirm member
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *              password: password,
   *              token: token
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       400:
   *         description: Bad request/Confirm token is expired/Email isn't confirmed.
   *       404:
   *         description: Confirm token is invalid.
   *       500:
   *         description: Internal server error
   */

    async confirm(req, res) {
        const { token, password } = req.body;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const result = await redis.getAsync(token);
            redis.del(token);

            if (result === null) {
                return res.status(400).json({ msg: 'Confirm token is expired.' });
            }

            const email = result.substring(0, result.indexOf(':invite'));

            const member = await User.findOne({
                raw: false,
                where: {
                    email: email,
                },
            });

            if (member !== null) {
                await member.update({
                    confirm: true,
                    status: 'Verified',
                    password: bcrypt.hashSync(password, bcrypt.genSaltSync()),
                });
                return res.status(200).json();
            }
            return res.status(400).json({ msg: 'Email isn\'t confirmed.' });

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

};
