const { Company } = require('../../../database/models');
const fileUrl = require('../../helpers/file/file-url');
const { validationResult } = require('express-validator/check');

module.exports = {

    /**
   * @swagger
   * /public/company/show:
   *   get:
   *     tags:
   *       - Company
   *     description: Show company info
   *     parameters:
   *         - name: slug
   *           in: query
   *           type: string
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       400:
   *          description: Bad Request
   *       404:
   *          description: Not found
   */

    async show(req, res) {
        const { query } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const [[company]] = await Company.getCompanyInfo(query.slug);

            if (company) {
                let url = company.fileHash
                    ? fileUrl('Company', {
                        hash: company.fileHash,
                        name: company.fileName,
                        createdAt: company.fileCreatedAt,
                    })
                    : null;

                return res.status(200).json({ name: company.name, url: url });
            }

            return res.status(404).json();
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },
};
