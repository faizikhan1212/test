const { Unit } = require('../../../database/models');
const { validationResult } = require('express-validator/check');

module.exports = {

    /**
   * @swagger
   * /public/unit/list:
   *   get:
   *     tags:
   *       - Unit
   *     description: Public unit list
   *     parameters:
   *         - name: slug
   *           in: query
   *           required: true
   *           type: string
   *         - name: offset
   *           in: query
   *           required: true
   *           type: integer
   *         - name: limit
   *           in: query
   *           required: true
   *           type: integer
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *         example: {units: units}
   *       404:
   *         description: Not Found
   */

    async list(req, res) {
        const { query } = req;

        try {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }
            const $units = Unit.getPublicList(query.slug, [query.limit, query.offset]);
            const $countUnits = Unit.countPublicListResult(query.slug);

            const [
                [units],
                countUnits,
            ] = [
                await $units,
                await $countUnits,
            ];

            return res.status(200).json({ units: units, countUnits: countUnits });


        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }

    },

    /**
   * @swagger
   * /public/unit/show:
   *   get:
   *     tags:
   *       - Unit
   *     description: Show a public unit
   *     parameters:
   *         - name: id
   *           in: query
   *           required: true
   *           type: string
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *         schema:
   *           type: object
   *           properties:
   *              id:
   *                type: integer
   *                format: int64
   *                description: Database identifier, unique
   *              rent:
   *                 type: number
   *                 minLength: 1
   *                 description: Unit rent
   *              deposit:
   *                 type: number
   *                 description: Unit deposit
   *              status:
   *                type: string
   *                enum: [Available, Rented, Sold]
   *                description: Unit status
   *              title:
   *                 type: string
   *                 minLength: 2
   *                 maxLength: 255
   *                 description: Unit title
   *              description:
   *                 type: string
   *                 minLength: 10
   *                 maxLength: 5000
   *                 description: Unit description
   *              parking:
   *                 type: boolean
   *              petFriendly:
   *                 type: boolean
   *              bed:
   *                 type: number
   *                 minLength: 1
   *                 description: Count unit bed
   *              bath:
   *                 type: number
   *                 minLength: 1
   *                 description: Count unit bath
   *              leaseTerm:
   *                 type: string
   *                 enum: [Month-to-month, 1 year, 6 months]
   *              email:
   *                type: string
   *                minLength: 2
   *                maxLength: 255
   *              phone:
   *                type: string
   *                minLength: 2
   *                maxLength: 255
   *              unit:
   *                type: string
   *                minLength: 2
   *                maxLength: 255
   *              address:
   *                type: string
   *                minLength: 2
   *                maxLength: 255
   *       404:
   *         description: Not Found
   */

    async show(req, res) {
        const { query } = req;

        try {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const unit = await Unit.findByPk(query.id, {
                attributes: {
                    exclude: [
                        'createdAt',
                        'propertyId',
                        'updatedAt',
                    ],
                },
            });

            if (unit !== null) {
                return res.status(200).json({
                    unit: unit,
                });
            }

            return res.status(404).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }

    },

};
