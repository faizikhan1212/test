const { User, Company, Dictionary } = require('../../../database/models');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt-nodejs');
const { validationResult } = require('express-validator/check');
const redis = require('../../services/redis');
const randtoken = require('rand-token');
const config = require('../../../config');
const { registerEmail, resendRegisterEmail, resetPasswordEmail } = require('../../jobs/emails');
const createDummyProperties = require('../../jobs/create-dummy-properties-and-units');

module.exports = {
    /**
   * @swagger
   * /public/register:
   *   post:
   *     tags:
   *       - Auth
   *     description: Register
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *             name: exampleName,
   *             email: example@email.com,
   *             password: qwerty123
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: User has registered
   *       500:
   *          description: Internal Server Error
   *       400:
   *          description: Bad Request
   */
    async register(req, res) {
        const { body } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const company = await Company.create({});

            Dictionary.createAndPopulate(
                company.id,
                config.allowedData,
            );


            let billingExpire = new Date();
            billingExpire.setDate(billingExpire.getDate() +100);

            const user = await User.create({
                companyId: company.id,
                name: body.name,
                email: body.email,
                confirm:true,
                role: 'Owner',
                status: 'Verified',
                password: bcrypt.hashSync(body.password, bcrypt.genSaltSync()),
                maxUnits: 10000,
                billingExpire: billingExpire,
            });

            console.log(user);
            createDummyProperties(user.id);

            //registerEmail(body.email);

            return res.status(200).json();
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /public/resend-register-email:
   *   post:
   *     tags:
   *       - Auth
   *     description: Resend register email
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *             email: example@email.com,
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: User has registered
   *       400:
   *          description: Account already confirmed
   *       404:
   *          description: Not Found
   *       500:
   *          description: Internal Server Error

   */
    async resendRegisterEmail(req, res) {
        const { body } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const user = await User.findOne({
                where: {
                    email: body.email,
                },
            });

            if (user !== null) {
                if (user.confirm === false) {
                    resendRegisterEmail(body.email);
                    return res.status(200).json();
                } else {
                    return res.status(400).json({ msg: 'Account already confirmed' });
                }
            }
            return res.status(404).json();
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },


    /**
   * @swagger
   * /public/login:
   *   post:
   *     tags:
   *       - Auth
   *     description: Login
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *              email: example@email.com,
   *              password: qwerty123,
   *              rememberme: 1
   *          }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: User has logged.
   *         example: {
   *              token: token1h,
   *              refreshToken: refreshToken
   *         }
   *       400:
   *         description: Bad request.
   *       403:
   *         description: Account has not been confirmed.
   *       500:
   *         description: Internal server error.
   */

    login(req, res) {
        const { body, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            if (user !== null && bcrypt.compareSync(body.password, user.password)) {
                const randToken = randtoken.uid(256);

                if (parseInt(body.rememberme) === 1) {
                    const token30d = jwt.sign(
                        { id: user.id },
                        config.auth.token_secret,
                        { expiresIn: config.auth.token_expiry_for_remember },
                    );
                    const refreshToken = randToken.replace(
                        new RegExp('^.{' + token30d.length + '}'),
                        jwt.sign(
                            { id: user.id },
                            config.auth.token_secret,
                            { expiresIn: config.auth.token_expiry_for_remember },
                        ),
                    );

                    redis.set(refreshToken, token30d);

                    return res.status(200).json({
                        token: token30d,
                        refreshToken: refreshToken,
                    });
                }

                const token1h = jwt.sign(
                    { id: user.id },
                    config.auth.token_secret,
                    { expiresIn: config.auth.token_expiry },
                );
                const refreshToken = randToken.replace(
                    new RegExp('^.{' + token1h.length + '}'),
                    jwt.sign({ id: user.id }, config.auth.token_secret, {
                        expiresIn: config.auth.refresh_token_expiry,
                    }),
                );

                redis.set(refreshToken, token1h);

                return res.status(200).json({
                    token: token1h,
                    refreshToken: refreshToken,
                });
            }
            return res.status(401).json();
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    google(req, res) {
        try {
            const randToken = randtoken.uid(256);
            const token = jwt.sign(
                { id: req.user.id },
                config.auth.token_secret,
                { expiresIn: config.auth.token_expiry_for_remember },
            );
            const refreshToken = randToken.replace(
                new RegExp('^.{' + token.length + '}'),
                jwt.sign({ id: req.user.id }, config.auth.token_secret, {
                    expiresIn: config.auth.token_expiry_for_remember,
                }),
            );

            redis.set(refreshToken, token);

            return res.redirect(`${config.app.frontend_url}/#/auth/google-login/${token}/${refreshToken}`);
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /public/logout:
   *   post:
   *     tags:
   *       - Auth
   *     description: Logout
   *     parameters:
   *         - name: refreshToken
   *           in: body
   *           type: string
   *           example: {
   *              refreshToken: refreshToken
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       400:
   *         description: Bad request
   *       404:
   *         description: Refresh token is invalid
   *       500:
   *         description: Internal server error
   *
   */

    async logout(req, res) {
        const { body } = req;

        try {
            const errors = validationResult(req);
            const refreshToken = body.refreshToken;

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const result = await redis.getAsync(refreshToken);

            redis.del(refreshToken);

            if (result === null) {
                return res.status(400).json({ msg: 'Refresh token is used.' });
            }
            redis.set(result, false);

            return res.status(200).json();
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /public/confirm:
   *   post:
   *     tags:
   *       - Auth
   *     description: Confirm
   *     parameters:
   *         - name: token
   *           in: body
   *           type: string
   *           example: {
   *              token: token
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       400:
   *         description: Bad request/Confirm token is expired/Email isn't confirmed.
   *       404:
   *         description: Confirm token is invalid.
   *       500:
   *         description: Internal server error
   */

    async confirm(req, res) {
        const { token } = req.body;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const result = await redis.getAsync(token);
            redis.del(token);

            if (result === null) {
                return res.status(400).json({ msg: 'Confirm token is expired.' });
            }

            const email = result.substring(0, result.indexOf(':confirm'));

            const user = await User.findOne({
                raw: false,
                where: {
                    email: email,
                },
            });

            if (user !== null) {
                await user.update({ confirm: true, status: 'Verified' });
                return res.status(200).json();
            }

            return res.status(400).json({ msg: 'Email isn\'t confirmed.' });
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /public/reset-password:
   *   post:
   *     tags:
   *       - Auth
   *     description: Reset password
   *     parameters:
   *         - name: email
   *           in: body
   *           type: string
   *           example: {
   *              email: example@gmail.com
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       400:
   *         description: Bad request.
   *       404:
   *         description: Not found.
   *       500:
   *         description: Internal server error
   */

    async resetPassword(req, res) {
        const { body } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const user = await User.findOne({
                where: {
                    email: body.email,
                },
            });

            if (user !== null) {
                resetPasswordEmail(body.email);
                return res.status(200).json();
            }

            return res.status(404).json();
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /public/check-reset-token:
   *   post:
   *     tags:
   *       - Auth
   *     description: Check reset token
   *     parameters:
   *         - name: token
   *           in: body
   *           type: string
   *           example: {
   *              token: token
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       404:
   *          description: Not found
   *       400:
   *          description: Bad Request
   */

    async checkResetToken(req, res) {
        const { body } = req;

        try {
            const errors = validationResult(req);
            const { token } = body;

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const result = await redis.getAsync(token);

            if (result === null) {
                return res.status(400).json({ msg: 'Reset token is expired.' });
            }

            const email = result.substring(0, result.indexOf(':reset'));

            const user = await User.findOne({
                where: {
                    email: email,
                },
            });

            if (user !== null) {
                return res.status(200).json();
            }

            return res.status(404).json();
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /public/change-password:
   *   post:
   *     tags:
   *       - Auth
   *     description: Change password
   *     parameters:
   *         - name: password
   *           in: body
   *           type: string
   *           example: {
   *              password: password
   *           }
   *         - name: token
   *           in: body
   *           type: string
   *           example: {
   *              token: token
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       400:
   *         description: Bad request/Change token is expired/Password isn't changed
   *       404:
   *         description: Change token is invalid..
   *       500:
   *         description: Internal server error
   */

    async changePassword(req, res) {
        const { body } = req;

        try {
            const errors = validationResult(req);
            const { token } = body;

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const result = await redis.getAsync(token);
            redis.del(token);

            if (result === null) {
                return res.status(400).json({ msg: 'Reset token is expired.' });
            }

            const email = result.substring(0, result.indexOf(':reset'));

            const user = await User.findOne({
                raw: false,
                where: {
                    email: email,
                },
            });

            if (user !== null) {
                await user.update({
                    password: bcrypt.hashSync(body.password, bcrypt.genSaltSync()),
                });
                return res.status(200).json();
            }

            return res.status(400).json({ msg: 'Password isn\'t changed.' });
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /public/refresh:
   *   post:
   *     tags:
   *       - Auth
   *     description: Refresh token
   *     parameters:
   *         - name: refreshToken
   *           in: body
   *           type: string
   *           example: {
   *              refreshToken: refreshToken
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *         example: {
   *            token: token,
   *            refreshToken: refreshToken
   *         }
   *       400:
   *         description: Bad request/Refresh token is expired.
   *       404:
   *         description: Refresh token is invalid.
   *       500:
   *         description: Internal server error
   */

    async refresh(req, res) {
        const { body } = req;

        try {
            const errors = validationResult(req);
            const refreshToken = body.refreshToken;

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const result = await redis.getAsync(refreshToken);
            redis.del(refreshToken);

            if (result === null) {
                return res.status(400).json({ msg: 'Refresh token is used.' });
            }

            const decoded = jwt.decode(result);
            const refreshDecoded = jwt.decode(
                refreshToken.replace(
                    new RegExp('.{' + result.length + '}$'),
                    '',
                ),
            );

            if (refreshDecoded.exp < Math.floor(Date.now() / 1000)) {
                return res.status(400).json({ msg: 'Refresh token is expired.' });
            }

            const user = await User.findByPk(decoded.id, {
                raw: false,
            });

            if (user !== null) {
                const token = jwt.sign(
                    { id: user.id },
                    config.auth.token_secret,
                    {
                        expiresIn: decoded.exp - decoded.iat,
                    },
                );

                const randToken = randtoken.uid(256);
                const refreshToken = randToken.replace(
                    new RegExp('^.{' + token.length + '}'),
                    jwt.sign({ id: user.id }, config.auth.token_secret, {
                        expiresIn: config.auth.token_expiry_for_remember,
                    }),
                );

                redis.set(refreshToken, token);
                redis.set(result, false);

                return res.status(200).json({ token: token, refreshToken: refreshToken });
            }

            return res.status(401).json();
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },
};
