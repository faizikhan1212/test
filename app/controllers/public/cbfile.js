const _ = require('lodash');
const { CBFile } = require('../../../database/models');
const { file_store } = require('../../../config/file');

module.exports = {

    /**
   * @swagger
   * /public/cbfile/create/{leadId}/{type}/{entityName}/{entityId}:
   *   post:
   *     tags:
   *       - CBFile
   *     description: Upload image
   *     produces:
   *       - application/json
   *     consumes:
   *       - multipart/form-data
   *     parameters:
   *       - in: path
   *         name: leadId
   *         required: true
   *         type: integer
   *         description: The id of lead.
   *       - in: path
   *         name: type
   *         required: true
   *         type: string
   *         description: The type of file.
   *       - in: path
   *         name: entityName
   *         required: true
   *         type: string
   *         description: Entity name
   *       - in: path
   *         name: entityId
   *         required: true
   *         type: integer
   *         description: Entity id
   *       - in: formData
   *         name: file
   *         type: file
   *         required: true
   *         description: The file to upload.
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Unexpected field. Required key 'file' not found
   *       400:
   *          description: Bad Request
   */

    async create(req, res) {
        const { params, file, imageValidationError } = req;
        const { leadId, entityName, entityId, type } = params;

        try {
            if (imageValidationError) {
                return res.status(400).json({
                    errors: imageValidationError,
                });
            }

            const capitalizeEntityName = _.capitalize(entityName);
            const capitalizeType = _.capitalize(type);

            switch (capitalizeEntityName) {
                case 'Maintenance': {
                    const countItems = await CBFile.count({
                        where: {
                            leadId: leadId,
                            entityName: capitalizeEntityName,
                            entityId: entityId,
                            type: capitalizeType,
                        },
                    });

                    if (countItems < 4) {
                        await CBFile.create({
                            leadId: leadId,
                            entityName: capitalizeEntityName,
                            entityId: entityId,
                            type: capitalizeType,
                            hash: file_store === 'aws' ? file.key : file.filename,
                            name: file.originalname,
                        });

                        return res.status(200).json();
                    }

                    return res.status(400).json();
                }
                default: {
                    return res.status(404).json();
                }
            }
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },
};
