const { Email } = require('../../../database/models');
const { validationResult } = require('express-validator/check');

module.exports = {

    /**
   * @swagger
   * /private/email/list:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Email
   *     description: Create phone number
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async list(req, res) {
        const { user } = req;

        try {

            const emails = await Email.findAll({
                where: {
                    userId: user.id,
                },
            });

            return res.status(200).json({ emails: emails });

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/email/create:
   *   post:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Email
   *     description: Create email
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *              email: "example@gmail.com"
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *         schema:
   *           type: object
   *           $ref: '#/definitions/Email'
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async create(req, res) {
        const { body, user } = req;

        try {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const email = await Email.create({
                userId: user.id,
                email: body.email,
            });

            return res.status(200).json({ email: email });

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

};