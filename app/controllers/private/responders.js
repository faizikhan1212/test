const { Responders } = require('../../../database/models');
module.exports = {

    async list(req, res) {
        const { query, user } = req;

        try {
            
            const $responderList = Responders.list(user.companyId);
            let respList = await $responderList;
            return res.status(200).json({
                emailResponders: respList[0]
            });
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    async save(req, res) {
        const { query, user } = req;

        try {
            let emailResponderData = req.body;

                for(let i=0;i<emailResponderData.length;i++) {
                    let element = emailResponderData[i];

                if(element.generatedEmail !== ''){
                    const $res =  Responders.update(element);
                    let tryUpdate = await $res;
                    if(tryUpdate[0].length === 0){
                        const $res2 = Responders.insert(element,user.companyId);
                        let tryInsert = await $res2;
                    }
                }
                
            }

            return res.status(200).json();
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },
};