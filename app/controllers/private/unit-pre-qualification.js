const { UnitPreQualification } = require('../../../database/models');
const { validationResult } = require('express-validator/check');
const _ = require('lodash');

module.exports = {

    /**
   * @swagger
   * /private/unit-pre-qualification/show:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Unit Pre-qualification
   *     description: Create a unit pre-qualification
   *     parameters:
   *         - name: unitId
   *           required: true
   *           in: query
   *           type: integer
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Show unit preQualification
   *         schema:
   *           type: object
   *           $ref: '#/definitions/UnitPreQualification'
   *       400:
   *          description: Bad Request
   *       401:
   *          description: Unauthorized
   */

    async show(req, res) {
        const { query, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const [unitPreQualification] = await UnitPreQualification.extendedShow(query.unitId, user.companyId);

            if (!_.isEmpty(unitPreQualification)) {
                return res.status(200).json({ unitPreQualification: unitPreQualification });
            }

            return res.status(404).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/unit-pre-qualification/create:
   *   post:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Unit Pre-qualification
   *     description: Create a unit pre-qualification
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *               unitId: 1,
   *               pet: true,
   *               cat: 2,
   *               dog: 1,
   *               agree: true,
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Unit PreQualification has created
   *         schema:
   *           type: object
   *           $ref: '#/definitions/UnitPreQualification'
   *       400:
   *          description: Bad Request
   *       401:
   *          description: Unauthorized
   */

    async create(req, res) {
        const { body, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const unitPreQualification = await UnitPreQualification.extendedCreate(user, body);

            if (unitPreQualification) {
                return res.status(200).json();
            }

            return res.status(404).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/unit-pre-qualification/update:
   *   put:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Unit Pre-qualification
   *     description: Update a unit pre-qualification
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *               id: 1,
   *               pet: true,
   *               cat: 2,
   *               dog: 1,
   *               agree: true,
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Unit PreQualification has updated
   *         schema:
   *           type: object
   *           $ref: '#/definitions/UnitPreQualification'
   *       400:
   *          description: Bad Request
   *       401:
   *          description: Unauthorized
   */

    async update(req, res) {
        const { body, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const unitPreQualification = await UnitPreQualification.extendedUpdate(user.companyId, body);

            if (unitPreQualification) {
                return res.status(200).json();
            }

            return res.status(404).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },


};
