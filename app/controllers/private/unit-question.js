const { UnitQuestion } = require('../../../database/models');
const { validationResult } = require('express-validator/check');

module.exports = {

    /**
   * @swagger
   * /private/unit-question/list:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Unit Question
   *     description: List unit questions
   *     parameters:
   *         - name: unitId
   *           required: true
   *           in: query
   *           type: integer
   *         - name: offset
   *           required: true
   *           in: query
   *           type: integer
   *         - name: limit
   *           required: true
   *           in: query
   *           type: integer
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Not Found
   *       400:
   *          description: Bad Request
   */

    async list(req, res) {
        const { query, user } = req;

        try {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const [[unitQuestions], countUnitQuestions] = [
                await UnitQuestion.getList(user.companyId, [query.unitId, query.limit, query.offset, 'ASC']),
                await UnitQuestion.getCountList(user.companyId, query.unitId),
            ];

            return res.status(200).json({
                unitQuestions: unitQuestions,
                countUnitQuestions: countUnitQuestions,
            });


        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/unit-question/create:
   *   post:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Unit Question
   *     description: Create unit question
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *              unitId: 1,
   *              mandatory: true,
   *              type: Yes/No,
   *              question: Are you vegan?,
   *              answer: Yes
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *         schema:
   *           -$ref: '#/definitions/UnitQuestion'
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Not Found
   *       400:
   *          description: Bad Request
   */

    async create(req, res) {
        const { body, user } = req;

        try {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const unitQuestion = await UnitQuestion.extendedCreate(user, body);

            if (unitQuestion) {
                return res.status(200).json();
            }

            return res.status(404).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/unit-question/update:
   *   put:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Unit Question
   *     description: Update question
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *              id: 1,
   *              mandatory: true,
   *              type: Yes/No,
   *              question: Are you politican?,
   *              answer: Yes
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Not found
   *       400:
   *          description: Bad Request
   */

    async update(req, res) {
        const { body, user } = req;

        try {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const unitQuestion = await UnitQuestion.extendedUpdate(user.companyId, body);

            if (unitQuestion) {
                return res.status(200).json();
            }

            return res.status(404).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/unit-question/destroy:
   *   delete:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Unit Question
   *     description: Update question
   *     parameters:
   *         - name: id
   *           required: true
   *           in: query
   *           type: integer
   *           example: 1
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Not Found
   *       400:
   *          description: Bad Request
   */

    async destroy(req, res) {
        const { query, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const unitQuestion = await UnitQuestion.extendedDelete(user.companyId, query.id);

            if (unitQuestion) {
                return res.status(200).json();
            }

            return res.status(404).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

};
