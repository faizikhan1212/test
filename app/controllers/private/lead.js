const _ = require('lodash');
const { Lead } = require('../../../database/models');
const redis = require('../../services/redis');
const { validationResult } = require('express-validator/check');

module.exports = {



    /**
     * @swagger
     * /private/lead/change-status:
     *   put:
     *     security:
     *       - Bearer: []
     *     tags:
     *       - Lead
     *     description: Search leads
     *     parameters:
     *         - name: data
     *           in: body
     *           type: object
     *           example: {
     *            id: 1,
     *            type: Qualified,
     *           }
     *     produces:
     *       - application/json
     *     responses:
     *       200:
     *         description: Success
     *       401:
     *          description: Unauthorized
     *       400:
     *          description: Bad Request
     */

    async changeStatus(req, res) {
        const { body, user } = req;

        try {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const [[lead]] = await Lead.extendedShow(body.id, [user.companyId]);

            if (lead !== null && lead !== undefined) {
                await Lead.update({
                    type: body.type
                },
                {
                    where: {
                        id: body.id,
                    },
                });

                return res.status(200).json();

            }

            return res.status(404).json();


        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },


    /**
   * @swagger
   * /private/lead/search:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Lead
   *     description: Search leads
   *     parameters:
   *         - name: q
   *           required: true
   *           in: query
   *           type: string
   *         - name: limit
   *           required: true
   *           in: query
   *           type: string
   *         - name: offset
   *           required: true
   *           in: query
   *           type: string
   *         - name: filter
   *           in: query
   *           type: object
   *           example: {
   *              type: Qualified
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async search(req, res) {
        const { query, user } = req;

        try {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const $leads = Lead.search(query.q, [user.companyId, query.limit, query.offset, 'DESC', query.filter]);
            const $countSearch = Lead.countSearchResult(query.q, [user.companyId, query.filter]);


            let [
                [leads],
                countLeads,
            ] = [
                await $leads,
                await $countSearch,
            ];

            return res.status(200).json({ leads: leads, countLeads: countLeads });

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/lead/show:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Lead
   *     description: Show lead
   *     parameters:
   *         - name: id
   *           in: query
   *           type: integer
   *           required: true
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Not Found
   *       400:
   *          description: Bad Request
   */

    async show(req, res) {
        const { query, user } = req;

        try {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const [[lead]] = await Lead.extendedShow(query.id, [user.companyId]);

            const leadDetailsFromChatbot =  await redis.getAsync(`lead-${lead.id}`);

            if (!_.isEmpty(lead)) {
                return res.status(200).json({ lead: lead, leadDetails: JSON.parse(leadDetailsFromChatbot) });
            }

            return res.status(404).json();
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },


    /**
   * @swagger
   * /private/lead/list:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Lead
   *     description: List lead
   *     parameters:
   *         - name: type
   *           in: query
   *           type: string
   *           required: true
   *           enum: [
   *             Qualified,
   *             Disqualified,
   *             Waitlist
   *           ]
   *         - name: offset
   *           in: query
   *           type: integer
   *           required: true
   *         - name: limit
   *           in: query
   *           type: integer
   *           required: true
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async list(req, res) {
        const { query, user } = req;

        try {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const $leads = Lead.getList(query.type, [user.companyId, query.limit, query.offset, 'ASC']);
            const $countLeads = Lead.countListResult(query.type, [user.companyId]);

            let [
                [leads],
                countLeads,
            ] = [
                await $leads,
                await $countLeads,
            ];

            return res.status(200).json({ leads: leads, countLeads: countLeads });

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/lead/report:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Lead
   *     description: Report leads
   *     parameters:
   *         - name: propertyId
   *           in: query
   *           type: integer
   *         - name: startDate
   *           in: query
   *           type: string
   *           format: date
   *         - name: endDate
   *           in: query
   *           type: string
   *           format: date
   *         - name: offset
   *           in: query
   *           type: integer
   *           required: true
   *         - name: limit
   *           in: query
   *           type: integer
   *           required: true
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async report(req, res) {
        const { query, user } = req;

        try {

            if (query.propertyId === 'all') {
                delete query.propertyId;
            }
            if (query.userId === 'all') {
                delete query.userId;
            }

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }


            const $reports = Lead.getReport(['ASC', query.limit, query.offset, user.companyId, user.id, query.propertyId, query.startDate, query.endDate]);
            const $countReports = Lead.countReportsResult([user.companyId, user.id, query.propertyId, query.startDate, query.endDate]);


            let [
                [reports],
                reportsCount,
            ] = [
                await $reports,
                await $countReports,
            ];

            return res.status(200).json({ reports: reports, reportsCount: reportsCount });

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

};
