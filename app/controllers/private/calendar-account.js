const { CalendarAccount, User } = require('../../../database/models');
const googleService = require('../../services/google');
const microsoftService = require('../../services/microsoft');

module.exports = {
    /**
   * @swagger
   * /private/calendar/accounts/auth:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Company
   *     description: Authenticate for new account
   *     parameters:
   *         - name: syncBy
   *           in: query
   *           required: true
   *           type: string
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *         schema:
   *           type: object
   *           $ref: '#/definitions/CalendarAccount'
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Not Found
   */

    async auth(req, res) {
        const { user } = req;
        const { syncBy, isTwoWaySync, alwaysSync } = req.query;

        let accountSetting = {
            syncBy,
            isTwoWaySync,
            alwaysSync,
        };
        let authUrl = null;


        if (syncBy === 'google') {
            authUrl = googleService.getAuthUrl(user.id, accountSetting);
        } else if (syncBy === 'office') {
            authUrl = microsoftService.getAuthUrl(user.id, accountSetting);
        }


        return res
            .status(200)
            .json({ url: authUrl, accountSetting: accountSetting });
    },

    async getCalendarAccount(req, res) {
        const { user } = req;
        try {
            const expectedUser = await User.findByPk(user.id);
            if (expectedUser !== null) {
                const account = await CalendarAccount.findOne({
                    where: { userId: expectedUser.id },
                });
                if (account) {
                    return res.status(200).json(account);
                }
            }

            return res.status(404).json();
        } catch (err) {
            return res.status(500).json();
        }
    },

    async destroy(req, res) {
        const { user } = req;

        try {
            const result = await CalendarAccount.destroy({
                where: {
                    userId: user.id,
                },
            });

            if (result > 0) {
                return res.status(200).json();
            }

            return res.status(404).json();
        } catch (err) {
            return res.status(500).json();
        }
    },

    async updateCalendarAccount(req, res) {
        const { user } = req;
        const { code, syncBy } = req.body;

        try {
            let authDetails = {};
            let profileInfo = {};

            if (syncBy === 'google') {
                authDetails = await googleService.getAccessTokenAsync(code);
                let auth = await googleService.getAuth(authDetails);
                profileInfo = await googleService.getProfileAsync(auth);
            } else if (syncBy === 'office') {
                authDetails = await microsoftService.getAccessTokenAsync(code);
                authDetails['expiry_date'] = authDetails.expires_in;
                profileInfo['emailAddress'] = microsoftService.getEmailFromIdToken(authDetails.id_token);
            } else {
                return res.status(400).json();
            }
            // get user account info
            const expectedUser = await User.findByPk(user.id);

            if (expectedUser !== null) {
                const account = await CalendarAccount.findOne({
                    where: { userId: expectedUser.id },
                });
                if (account) {
                    await CalendarAccount.update(
                        {
                            type: syncBy,
                            email: profileInfo.emailAddress,
                            accessToken: authDetails.access_token,
                            idToken: authDetails.id_token,
                            tokenType: authDetails.token_type,
                            refreshToken: authDetails.refresh_token,
                            scope: authDetails.scope,
                            expiryDate: authDetails.expiry_date,
                        },
                        {
                            where: {
                                userId: user.id,
                            },
                        },
                    );

                } else {
                    await CalendarAccount.create({
                        userId: user.id,
                        type: syncBy,
                        email: profileInfo.emailAddress,
                        accessToken: authDetails.access_token,
                        idToken: authDetails.id_token,
                        tokenType: authDetails.token_type,
                        refreshToken: authDetails.refresh_token,
                        scope: authDetails.scope,
                        expiryDate: authDetails.expiry_date,
                    });

                }

                return res.status(200).json();
            }

            return res.status(404).json();
        } catch (err) {
            return res.status(500).json();
        }
    },

    async updateCalendarAccountSetting(req, res) {
        const { user } = req;
        const { isTwoWaySync, alwaysSync } = req.body;
        try {
            const account = await CalendarAccount.findOne({
                where: { userId: user.id },
            });

            if (account) {
                await CalendarAccount.update(
                    {
                        isTwoWaySync,
                        alwaysSync,
                    },
                    {
                        where: {
                            userId: user.id,
                        },
                    },
                );

                return res.status(200).json();
            }

            return res.status(404).json();
        } catch (err) {
            return res.status(500).json();
        }
    },

    async calendarWatch(req, res) {
        const { user } = req;

        try {
            let authToken = await CalendarAccount.getGoogleAccessToken(user.id);
            let auth = googleService.getAuth(authToken);
            await googleService.watch(auth);
            return res.status(200).json();
        } catch (err) {
            return res.status(500).json();
        }
    },
};
