const { Maintenance, Vendor, CBFile } = require('../../../database/models');
const { validationResult } = require('express-validator/check');
const { alertMaintenanceEmail } = require('../../jobs/emails');

module.exports = {
    /**
   * @swagger
   * /private/maintenance/list:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Maintenance
   *     description: List maintenances
   *     parameters:
   *         - name: offset
   *           required: true
   *           in: query
   *           type: integer
   *         - name: limit
   *           required: true
   *           in: query
   *           type: integer
   *         - name: filter
   *           in: query
   *           type: object
   *           example: {"status": 'In Progress'}
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Not Found
   *       400:
   *          description: Bad Request
   */

    async list(req, res) {
        const { query, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const [[maintenances], countMaintenances] = [
                await Maintenance.getList(
                    user.companyId,
                    query.filter,
                    'ASC',
                    query.limit,
                    query.offset,
                ),
                await Maintenance.getCountList(user.companyId, query.filter),
            ];

            return res.status(200).json({
                maintenances: maintenances,
                countMaintenances: countMaintenances,
            });
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/maintenance/search:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Maintenance
   *     description: Search maintenances
   *     parameters:
   *         - name: q
   *           required: true
   *           in: query
   *           type: string
   *         - name: offset
   *           required: true
   *           in: query
   *           type: integer
   *         - name: limit
   *           required: true
   *           in: query
   *           type: integer
   *         - name: filter
   *           in: query
   *           type: object
   *           example: {"status": 'In Progress'}
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Not Found
   *       400:
   *          description: Bad Request
   */

    async search(req, res) {
        const { query, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const [[maintenances], countMaintenances] = [
                await Maintenance.search(query.q, [
                    user.companyId,
                    query.filter,
                    query.limit,
                    query.offset,
                    'ASC',
                ]),
                await Maintenance.countSearchResult(query.q, [user.companyId, query.filter]),
            ];

            return res.status(200).json({
                maintenances: maintenances,
                countMaintenances: countMaintenances,
            });
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/maintenance/update:
   *   put:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Maintenance
   *     description: Update maintenances
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *              id: 1,
   *              status: In Progress,
   *              vendorId: 1,
   *              budget: 500,
   *              dateScheduled: 2017-07-21T17:32:28Z,
   *              dateCompleted: 2017-07-21T17:32:28Z,
   *              estimate: 200,
   *              cost: 600,
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Not Found
   *       400:
   *          description: Bad Request
   */

    async update(req, res) {
        const { body, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const maintenance = await Maintenance.extendedUpdate(user.companyId, body);

            if (maintenance) {
                return res.status(200).json();
            }

            return res.status(404).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/maintenance/alert:
   *   post:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Maintenance
   *     description: Alert maintenance to vendor
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *              maintenanceId: 1,
   *              budget: 300,
   *              vendorId: 2
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Not Found
   *       400:
   *          description: Bad Request
   */

    async alert(req, res) {
        const { body, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const [[maintenance]] = await Maintenance.extendedShow(user.companyId, body.maintenanceId);

            if (maintenance !== null && maintenance !== undefined) {

                const files = await CBFile.findAll({
                    where: {
                        entityName: 'Maintenance',
                        entityId: maintenance.id,
                    },
                });

                const [[vendor]] = await Vendor.extendedShow(body.vendorId, user.companyId);

                if (vendor !== null && vendor !== undefined) {
                    alertMaintenanceEmail(vendor.email, { ...maintenance, budget: body.budget }, files);
                    return res.status(200).json();
                }
            }

            return res.status(404).json();
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
     * @swagger
     * /private/maintenance/report:
     *   get:
     *     security:
     *       - Bearer: []
     *     tags:
     *       - Maintenance
     *     description: Report maintenance
     *     parameters:
     *         - name: propertyId
     *           in: query
     *           type: integer
     *         - name: startDate
     *           in: query
     *           type: string
     *           format: date
     *         - name: endDate
     *           in: query
     *           type: string
     *           format: date
     *         - name: offset
     *           in: query
     *           type: integer
     *           required: true
     *         - name: limit
     *           in: query
     *           type: integer
     *           required: true
     *     produces:
     *       - application/json
     *     responses:
     *       200:
     *         description: Success
     *       401:
     *          description: Unauthorized
     *       400:
     *          description: Bad Request
     */

    async report(req, res) {
        const { query, user } = req;
        try {
            console.log('in');

            if (query.propertyId === 'all') {
                delete query.propertyId;
            }
            if (query.userId === 'all') {
                delete query.userId;
            }

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const $reports = Maintenance.getReport([
                'ASC',
                query.limit,
                query.offset,
                user.companyId,
                query.userId,
                query.propertyId,
                query.startDate,
                query.endDate,
            ]);

            const $countReports = Maintenance.countReportsResult([
                user.companyId,
                query.userId,
                query.propertyId,
                query.startDate,
                query.endDate,
            ]);
            console.log($reports);

            let [[reports], reportsCount] = [
                await $reports,
                await $countReports,
            ];

            return res.status(200).json({
                reports: reports,
                reportsCount: reportsCount,
            });
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },
};
