const { Scheduling, User, Unit, CalendarAccount, Lead } = require('../../../database/models');
const googleService = require('../../services/google');
const microsoftService = require('../../services/microsoft');
const calendarService = require('../../services/calendar');
const { validationResult } = require('express-validator/check');
const moment = require('moment');

module.exports = {

    /**
   * @swagger
   * /private/scheduling/create:
   *   post:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Scheduling
   *     description: Create scheduling (add manually showing)
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *              userId: 1,
   *              unitId: 1,
   *              clientName: 'Example name',
   *              clientEmail: 'example@email.com',
   *              clientPhone: '+12223334444',
   *              dateOfShowing: [
   *                  '2019-03-19T11:00:00+00',
   *                  '2019-03-20T11:50:00+00',
   *              ],
   *              notes: 'Call before'
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       404:
   *          description: Not Found
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async create(req, res) {
        const { body, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const expectedUser = await User.findByPk(body.userId);

            if (expectedUser !== null && expectedUser.companyId === user.companyId) {
                const unit = await Unit.findByIdWithCompanyId(body.unitId, user.companyId);

                if (unit !== null) {
                    const addedSchedule = await Scheduling.create({
                        userId: expectedUser.id,
                        unitId: unit.id,
                        clientName: body.clientName,
                        clientEmail: body.clientEmail,
                        clientPhone: body.clientPhone,
                        dateOfShowing: body.dateOfShowing,
                        notes: body.notes || null,
                    });

                    res.status(200).json();

                    const calendarAccount = await CalendarAccount.findOne({
                        where: {
                            userId: user.id,
                        },
                    });


                    if (calendarAccount && calendarAccount.isTwoWaySync) {
                        let authToken = await CalendarAccount.getGoogleAccessToken(
                            user.id,
                        );

                        if (calendarAccount.type === 'google') {
                            const eventData = { ...body };
                            eventData.unit = unit.unit;
                            eventData.propertyAddress = unit.address;
                            eventData.id = addedSchedule.id;
                            let googleAddRes = await googleService.addEventsAsync(authToken, eventData);
                            if (googleAddRes.status === 200) {
                                await Scheduling.update({
                                    googleEventId: googleAddRes.data.id,
                                }, {
                                    where: {
                                        id: addedSchedule.id,
                                    },
                                });
                            }
                        } else if (calendarAccount.type === 'office') {
                            const eventData = { ...body };
                            eventData.unit = unit.unit;
                            eventData.propertyAddress = unit.address;
                            eventData.id = addedSchedule.id;
                            let officeAddRes = await microsoftService.addEventsAsync(authToken.access_token, eventData);
                            if (officeAddRes.Id) {
                                await Scheduling.update({
                                    officeEventId: officeAddRes.Id,
                                }, {
                                    where: {
                                        id: addedSchedule.id,
                                    },
                                });
                            }
                        }
                    }

                    return;

                }
            }

            return res.status(404).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },




    /**
   * @swagger
   * /private/scheduling/update:
   *   put:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Scheduling
   *     description: Update scheduling (edit showing)
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *              id: 1,
   *              userId: 1,
   *              unitId: 1,
   *              clientName: 'Example name',
   *              clientEmail: 'example@email.com',
   *              clientPhone: '+12223334444',
   *              dateOfShowing: [
   *                  '2019-03-19T11:00:00+00',
   *                  '2019-03-20T11:50:00+00',
   *              ],
   *              notes: 'Call before'
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       404:
   *          description: Not Found
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async update(req, res) {
        const { body, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const expectedUser = await User.findByPk(body.userId);

            if (expectedUser !== null && expectedUser.companyId === user.companyId) {
                const unit = await Unit.findByIdWithCompanyId(body.unitId, user.companyId);

                if (unit !== null) {

                    const schedulingFound = await Scheduling.findByPk(
                        body.id,
                    );

                    if (!schedulingFound) {
                        return res.status(404).json();
                    }

                    await Scheduling.update({
                        userId: expectedUser.id,
                        unitId: body.unitId,
                        clientName: body.clientName,
                        clientEmail: body.clientEmail,
                        clientPhone: body.clientPhone,
                        dateOfShowing: body.dateOfShowing,
                        notes: body.notes || null,
                        createdOnApp: 'lethub',
                        calendarAccountId: null,
                        googleEventId: null,
                    }, {
                        where: {
                            id: body.id,
                        },
                    });

                    res.status(200).json();

                    const calendarAccount = await CalendarAccount.findOne({
                        where: {
                            userId: user.id,
                        },
                    });


                    if (calendarAccount && calendarAccount.isTwoWaySync) {
                        let authToken = await CalendarAccount.getGoogleAccessToken(
                            user.id,
                        );
                        if (calendarAccount.type === 'google') {
                            const eventData = { ...body };
                            eventData.id = schedulingFound.googleEventId;
                            await googleService.updateEventsAsync(authToken, eventData);
                        } else if (calendarAccount.type === 'office') {
                            let eventData = body;
                            eventData.id = schedulingFound.officeEventId;
                            await microsoftService.updateEventsAsync(authToken.access_token, eventData);
                        }
                    }
                    return;
                }
            }

            return res.status(404).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/scheduling/destroy:
   *   delete:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Scheduling
   *     description: Destroy scheduling
   *     parameters:
   *         - name: id
   *           required: true
   *           in: query
   *           type: integer
   *           example: 1
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       404:
   *          description: Not Found
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async destroy(req, res) {
        const { query, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const scheduling = await Scheduling.findByPk(query.id, {
                raw: false,
            });

            if (scheduling !== null) {
                const userByScheduling = await User.findByPk(scheduling.userId);

                if (userByScheduling && userByScheduling.companyId === user.companyId) {
                    await scheduling.destroy();

                    res.status(200).json();

                    const account = await CalendarAccount.findOne({
                        where: { userId: user.id },
                    });

                    if (account) {
                        let authToken = await CalendarAccount.getGoogleAccessToken(
                            user.id,
                        );
                        if (account.type === 'google' && scheduling.googleEventId) {
                            const eventData = { id: scheduling.googleEventId };
                            await googleService.removeEventsAsync(authToken, eventData);
                        } else if (account.type === 'office' && scheduling.officeEventId) {
                            const eventData = { id: scheduling.officeEventId };
                            await microsoftService.removeEventsAsync(authToken.access_token, eventData);
                        }

                    }

                }
            }

            return res.status(404).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },


    /**
     * @swagger
     * /private/scheduling/show:
     *   get:
     *     security:
     *       - Bearer: []
     *     tags:
     *       - Scheduling
     *     description: Show scheduling
     *     parameters:
     *         - name: id
     *           required: true
     *           in: query
     *           type: integer
     *     produces:
     *       - application/json
     *     responses:
     *       200:
     *         description: Success
     *       401:
     *          description: Unauthorized
     *       400:
     *          description: Bad Request
     */

    async show(req, res) {
        const { query, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const [[scheduling]] = await Scheduling.extendedShow(user.companyId, query.id);

            scheduling.start = moment(scheduling.dateOfShowing[0]).format();
            scheduling.end = moment(scheduling.dateOfShowing[1]).format();

            if (scheduling) {
                return res.status(200).json({ event: scheduling });
            }
            return res.status(404).json();
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/scheduling/list:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Scheduling
   *     description: List scheduling
   *     parameters:
   *         - name: offset
   *           required: true
   *           in: query
   *           type: integer
   *         - name: limit
   *           required: true
   *           in: query
   *           type: integer
   *         - name: order
   *           required: true
   *           in: query
   *           type: string
   *           enum: [
   *             "ASC",
   *             "DESC"
   *           ]
   *         - name: filter
   *           in: query
   *           type: object
   *           example: {
   *              userId: 1,
   *              dateOfShowing: [
   *                  '2019-03-19T11:00:00+00',
   *                  '2019-03-21T11:50:00+00',
   *              ],
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async list(req, res) {
        const { query, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const $calendarInfo = Scheduling.getCalendarInfo([user.companyId, query.filter, query.limit, query.offset, query.order]);
            const $countCalendarInfo = Scheduling.countCalendarInfo([user.companyId, query.filter]);

            const [
                [calendarInfo],
                [[{ countCalendarInfo }]],
            ] = [
                await $calendarInfo,
                await $countCalendarInfo,
            ];

            return res.status(200).json({ calendarInfo: calendarInfo, countCalendarInfo: countCalendarInfo });

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },


    /**
     * @swagger
     * /private/scheduling/list-by-lead:
     *   get:
     *     security:
     *       - Bearer: []
     *     tags:
     *       - Scheduling
     *     description: List scheduling by leadPhone
     *     parameters:
     *         - name: offset
     *           required: true
     *           in: query
     *           type: integer
     *         - name: limit
     *           required: true
     *           in: query
     *           type: integer
     *         - name: order
     *           required: true
     *           in: query
     *           type: string
     *           enum: [
     *             "ASC",
     *             "DESC"
     *           ]
     *         - name: leadId
     *           in: query
     *           type: number
     *           example: 1
     *     produces:
     *       - application/json
     *     responses:
     *       200:
     *         description: Success
     *       401:
     *          description: Unauthorized
     *       400:
     *          description: Bad Request
     */



    async listByLead(req, res) {
        const { query, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const lead = await Lead.findByPk(query.leadId, {
                raw: false
            });

            if (lead) {
                const $calendarInfo = Scheduling.getByLeadPhone(user.companyId, lead.phone, query);
                const $countCalendarInfo = Scheduling.countSchedulingsByLeadPhone(user.companyId, lead.phone);


                const [
                    [calendarInfo],
                    [[{countCalendarInfo}]],
                ] = [
                    await $calendarInfo,
                    await $countCalendarInfo,
                ];

                return res.status(200).json({calendarInfo: calendarInfo, countCalendarInfo: countCalendarInfo});
            }

            return res.status(404).json({});

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },


    /**
   * @swagger
   * /private/scheduling/sync:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Scheduling
   *     description: Sync scheduling
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async sync(req, res) {
        const { user } = req;

        try {
            let syncRes = await calendarService.syncEventsAsync(user);
            return res.status(syncRes.status).json();
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },
};
