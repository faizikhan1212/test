const { Question, PreQualification } = require('../../../database/models');
const { validationResult } = require('express-validator/check');
const updateChatbotQuestions = require('../../../kafka/produces/questions');

module.exports = {

    /**
   * @swagger
   * /private/question/list:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Question
   *     description: List questions
   *     parameters:
   *         - name: offset
   *           required: true
   *           in: query
   *           type: integer
   *         - name: limit
   *           required: true
   *           in: query
   *           type: integer
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Not Found
   *       400:
   *          description: Bad Request
   */

    async list(req, res) {
        const { query, user } = req;

        try {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const preQualification = await PreQualification.findOne({
                where: {
                    userId: user.id,
                },
            });

            if (preQualification !== null) {

                const questions = await Question.findAll({
                    order: [
                        ['createdAt', 'ASC'],
                    ],
                    where: {
                        preQualificationId: preQualification.id,
                    },
                    offset: query.offset,
                    limit: query.limit,
                });

                return res.status(200).json({ questions: questions });
            }

            return res.status(404).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/question/create:
   *   post:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Question
   *     description: Create question
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *              mandatory: true,
   *              type: Yes/No,
   *              question: Are you vegan?,
   *              answer: Yes
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *         schema:
   *           -$ref: '#/definitions/Question'
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Not Found
   *       400:
   *          description: Bad Request
   */

    async create(req, res) {
        const { body, user } = req;

        try {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const preQualification = await PreQualification.findOne({
                where: {
                    userId: user.id,
                },
            });

            if (preQualification !== null) {
                const question = await Question.create({
                    preQualificationId: preQualification.id,
                    mandatory: body.mandatory,
                    type: body.type,
                    question: body.question,
                    answer: body.answer.length > 0 ? body.answer : '',
                });

                updateChatbotQuestions(user.companyId);

                return res.status(200).json({ question: question });
            }

            return res.status(404).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/question/update:
   *   put:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Question
   *     description: Update question
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *              id: 1,
   *              mandatory: true,
   *              type: Yes/No,
   *              question: Are you politican?,
   *              answer: Yes
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Not found
   *       400:
   *          description: Bad Request
   */

    async update(req, res) {
        const { body, user } = req;

        try {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const preQualification = await PreQualification.findOne({
                where: {
                    userId: user.id,
                },
            });

            if (preQualification !== null) {

                const question = await Question.findOne({
                    raw: false,
                    where: {
                        id: body.id,
                        preQualificationId: preQualification.id,
                    },
                });

                if (question !== null) {
                    await question.update({
                        preQualificationId: preQualification.id,
                        mandatory: body.mandatory,
                        type: body.type,
                        question: body.question,
                        answer: body.answer.length > 0 ? body.answer : '',
                    });

                    
                    updateChatbotQuestions(user.companyId);

                    return res.status(200).json();
                }
            }

            return res.status(404).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/question/destroy:
   *   delete:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Question
   *     description: Update question
   *     parameters:
   *         - name: id
   *           required: true
   *           in: query
   *           type: integer
   *           example: 1
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Not Found
   *       400:
   *          description: Bad Request
   */

    async destroy(req, res) {
        const { query, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const preQualification = await PreQualification.findOne({
                where: {
                    userId: user.id,
                },
            });

            if (preQualification !== null) {
                const result = await Question.destroy({
                    where: {
                        id: query.id,
                        preQualificationId: preQualification.id,
                    },
                });

                if (result > 0) {
                    updateChatbotQuestions(user.companyId);

                    return res.status(200).json();
                }
            }

            return res.status(404).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

};
