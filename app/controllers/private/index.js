const auth = require('./auth');
const availability = require('./availability');
const company = require('./company');
const dashboard = require('./dashboard');
const file = require('./file');
const preQualification = require('./pre-qualification');
const property = require('./property');
const question = require('./question');
const unit = require('./unit');
const lead = require('./lead');
const user = require('./user');
const stripe = require('./stripe');
const generalSettingPhone = require('./general-setting-phone');
const phoneNumber = require('./phone-number');
const generalSettingEmail = require('./general-setting-email');
const email = require('./email');
const vendor = require('./vendor');
const maintenance = require('./maintenance');
const scheduling = require('./scheduling');
const note = require('./note');
const unitAvailability = require('./unit-availability');
const unitPreQualification = require('./unit-pre-qualification');
const unitQuestion = require('./unit-question');
const cbfile = require('./cbfile');
const calendarAccount = require('./calendar-account');
const calendarSetting = require('./calendar-setting');
const dictionary = require('./dictionary');
const responders = require('./responders');

module.exports = {
    auth,
    availability,
    company,
    dashboard,
    file,
    preQualification,
    property,
    question,
    unit,
    responders,
    lead,
    user,
    stripe,
    generalSettingPhone,
    vendor,
    maintenance,
    phoneNumber,
    generalSettingEmail,
    email,
    scheduling,
    note,
    unitAvailability,
    unitPreQualification,
    unitQuestion,
    cbfile,
    calendarAccount,
    calendarSetting,
    dictionary,
};
