const { PhoneNumber } = require('../../../database/models');
const { validationResult } = require('express-validator/check');

module.exports = {

    /**
   * @swagger
   * /private/phone-number/list:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Phone Number
   *     description: Create phone number
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async list(req, res) {
        const { user } = req;

        try {

            const phoneNumbers = await PhoneNumber.findAll({
                where: {
                    userId: user.id,
                },
            });

            return res.status(200).json({ phoneNumbers: phoneNumbers });

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/phone-number/create:
   *   post:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Phone Number
   *     description: Create phone number
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *              phoneNumber: "12345678"
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *         schema:
   *           type: object
   *           $ref: '#/definitions/PhoneNumber'
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async create(req, res) {
        const { body, user } = req;

        try {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const phoneNumber = await PhoneNumber.create({
                userId: user.id,
                phoneNumber: body.phoneNumber,
            });

            return res.status(200).json({ phoneNumber: phoneNumber });

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

};