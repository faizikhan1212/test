const { Property, Unit, User } = require('../../../database/models');
const { validationResult } = require('express-validator/check');
const findCustomItemsAndUpdate = require('../../jobs/dictionary/find-custom-items-and-update');

module.exports = {
    /**
   * @swagger
   * /private/unit/search:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Unit
   *     description: Search units
   *     parameters:
   *         - name: q
   *           required: true
   *           in: query
   *           type: string
   *         - name: limit
   *           required: true
   *           in: query
   *           type: string
   *         - name: offset
   *           required: true
   *           in: query
   *           type: string
   *         - name: propertyId
   *           required: true
   *           in: query
   *           type: integer
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async search(req, res) {
        const { query, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            let [[units], countUnits] = [
                await Unit.search(query.q, [
                    user.companyId,
                    query.limit,
                    query.offset,
                    'DESC',
                    query.propertyId,
                ]),
                await Unit.countSearchResult(query.q, [
                    user.companyId,
                    query.propertyId,
                ]),
            ];

            return res
                .status(200)
                .json({ units: units, countUnits: countUnits });
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/unit/show:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Unit
   *     description: Show a private unit
   *     parameters:
   *         - name: id
   *           in: query
   *           required: true
   *           type: string
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *         schema:
   *           type: object
   *           properties:
   *              id:
   *                type: integer
   *                format: int64
   *                description: Database identifier, unique
   *              rent:
   *                 type: number
   *                 minLength: 1
   *                 description: Unit rent
   *              deposit:
   *                 type: number
   *                 description: Unit deposit
   *              status:
   *                type: string
   *                enum: [Available, Rented, Sold]
   *                description: Unit status
   *              title:
   *                 type: string
   *                 minLength: 2
   *                 maxLength: 255
   *                 description: Unit title
   *              description:
   *                 type: string
   *                 minLength: 10
   *                 maxLength: 5000
   *                 description: Unit description
   *              parking:
   *                 type: boolean
   *              petFriendly:
   *                 type: boolean
   *              bed:
   *                 type: number
   *                 minLength: 1
   *                 description: Count unit bed
   *              bath:
   *                 type: number
   *                 minLength: 1
   *                 description: Count unit bath
   *              leaseTerm:
   *                 type: string
   *                 enum: [Month-to-month, 1 year, 6 months]
   *              email:
   *                type: string
   *                minLength: 2
   *                maxLength: 255
   *              phone:
   *                type: string
   *                minLength: 2
   *                maxLength: 255
   *              unit:
   *                type: string
   *                minLength: 2
   *                maxLength: 255
   *              address:
   *                type: string
   *                minLength: 2
   *                maxLength: 255
   *       404:
   *         description: Not Found
   */

    async show(req, res) {
        const { user, query } = req;

        try {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const unit = await Unit.findByPk(query.id);

            if (unit !== null) {

                const userByUnit = await User.findByPk(unit.userId);

                if (userByUnit !== null && userByUnit.companyId === user.companyId) {
                    return res.status(200).json({
                        unit: unit,
                    });
                }
            }

            return res.status(404).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }

    },

    /**
   * @swagger
   * /private/unit/vacant-search:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Unit
   *     description: Search vacant units
   *     parameters:
   *         - name: q
   *           required: true
   *           in: query
   *           type: string
   *         - name: limit
   *           required: true
   *           in: query
   *           type: string
   *         - name: offset
   *           required: true
   *           in: query
   *           type: string
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async vacantSearch(req, res) {
        const { query, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const $units = Unit.vacantSearch(query.q, [
                user.companyId,
                query.limit,
                query.offset,
                'DESC',
            ]);
            const $vacantCountUnits = Unit.countVacantSearchResult(query.q, [
                user.companyId,
            ]);

            let [[units], countUnits] = [await $units, await $vacantCountUnits];

            return res
                .status(200)
                .json({ units: units, countUnits: countUnits });
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/unit/vacant-list:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Unit
   *     description: List of vacant units
   *     parameters:
   *         - name: offset
   *           required: true
   *           in: query
   *           type: integer
   *         - name: limit
   *           required: true
   *           in: query
   *           type: integer
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async vacantList(req, res) {
        const { query, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const $vacantUnits = Unit.getVacantList(user.companyId, [
                query.limit,
                query.offset,
                'DESC',
            ]);

            const $countVacantUnits = Unit.countVacantListResult(user.companyId);

            let [[vacantUnits], countVacantUnits] = [
                await $vacantUnits,
                await $countVacantUnits,
            ];

            return res.status(200).json({
                units: vacantUnits,
                countUnits: countVacantUnits,
            });
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/unit/list:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Unit
   *     description: List units
   *     parameters:
   *         - name: offset
   *           required: true
   *           in: query
   *           type: integer
   *         - name: limit
   *           required: true
   *           in: query
   *           type: integer
   *         - name: propertyId
   *           required: true
   *           in: query
   *           type: integer
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async list(req, res) {
        const { query, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const $units = Unit.getListByProperty(query.propertyId, [
                user.companyId,
                query.limit,
                query.offset,
                'DESC',
            ]);
            const $countUnits = Unit.countListByPropertyResult(query.propertyId, [
                user.companyId,
            ]);

            const [[units], countUnits] = [await $units, await $countUnits];

            return res.status(200).json({
                units: units,
                countUnits: countUnits,
            });
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/unit/create:
   *   post:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Unit
   *     description: Create unit
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *              propertyId: 1,
   *              showingAgent: 1,
   *              rent: 100,
   *              deposit: 10,
   *              status: Vacant,
   *              title: title,
   *              description: description text,
   *              parking: true,
   *              parkingCost: 45,
   *              parkingCovered: true,
   *              parkingSecured: false,
   *              petFriendly: true,
   *              petPolicy: One time fees. Pet Deposit $300,
   *              applicationFee: true,
   *              appFeeNonRefundable: true,
   *              applicationFeeCost: 45,
   *              sharedLaundry: true,
   *              unitShared: true,
   *              unitSharedPeople: 2,
   *              sharedWashroomBath: true,
   *              bed: 10,
   *              bath: 3,
   *              sqFt: 540,
   *              leaseTerm: Month-to-month,
   *              email: test@test.com,
   *              phone: 12223344555,
   *              unit: 1,
   *              address: address,
   *              utilities: null,
   *              amenities: null,
   *              noSmoking: true,
   *              unitUtility: 100,
   *              moveInDate: '2019-03-19T11:00:00+00',
   *              appliances: null,
   *              applicationProcess: Our qualification process is fairly simple so we encourage everyone to apply with their family.
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *         schema:
   *           type: object
   *           $ref: '#/definitions/Unit'
   *       400:
   *          description: Bad Request
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Not Found
   */

    async create(req, res) {
        const { body, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const property = await Property.findByPk(body.propertyId);

            if (property !== null) {
                const userByProperty = await User.findByPk(property.userId);

                if (userByProperty !== null && userByProperty.companyId === user.companyId) {
                    const unit = await Unit.create({
                        userId: user.id,
                        propertyId: property.id,
                        showingAgent: body.showingAgent !== undefined ? body.showingAgent : null,
                        rent: body.rent,
                        deposit: body.deposit,
                        status: body.status,
                        title: body.title,
                        description: body.description,
                        applicationFee: body.applicationFee,
                        appFeeNonRefundable: body.applicationFee && body.appFeeNonRefundable !== undefined ? body.appFeeNonRefundable : null,
                        applicationFeeCost: body.applicationFee && body.applicationFeeCost !== undefined ? body.applicationFeeCost : null,
                        parking: body.parking,
                        parkingCost: body.parking && body.parkingCost !== undefined ? body.parkingCost : null,
                        parkingCovered: body.parking && body.parkingCovered !== undefined ? body.parkingCovered : null,
                        parkingSecured: body.parking && body.parkingSecured !== undefined ? body.parkingSecured : null,
                        petFriendly: body.petFriendly,
                        petPolicy: body.petFriendly && body.petPolicy !== undefined ? body.petPolicy : null,
                        sharedLaundry: body.sharedAccountId,
                        unitShared: body.unitShared,
                        unitSharedPeople: body.unitShared && body.unitSharedPeople !== undefined ? body.unitSharedPeople : null,
                        sharedWashroomBath: body.sharedWashroomBath,
                        bed: body.bed,
                        bath: body.bath,
                        sqFt: body.sqFt,
                        leaseTerm: body.leaseTerm,
                        email: body.email,
                        phone: body.phone,
                        unit: body.unit,
                        address: body.address.replace('/,/g', ';'),
                        utilities: body.utilities,
                        amenities: body.amenities,
                        noSmoking: body.noSmoking,
                        unitUtility: body.unitUtility,
                        moveInDate: body.moveInDate,
                        appliances: body.appliances,
                        applicationProcess: body.applicationProcess,
                    });

                    findCustomItemsAndUpdate(user.id, user.companyId, {
                        amenities: body.amenities,
                        utilities: body.utilities,
                        appliances: body.appliances,
                    });

                    return res.status(200).json({ unit: unit });
                }

            }

            return res.status(404).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/unit/duplicate:
   *   post:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Unit
   *     description: Duplicate unit
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *              list: [
   *                {
   *                  propertyId: 1,
   *                  showingAgent: 1,
   *                  rent: 100,
   *                  deposit: 10,
   *                  status: Vacant,
   *                  title: title,
   *                  description: description text,
   *                  parking: true,
   *                  parkingCost: 45,
   *                  parkingCovered: true,
   *                  parkingSecured: false,
   *                  petFriendly: true,
   *                  petPolicy: One time fees. Pet Deposit $300,
   *                  applicationFee: true,
   *                  appFeeNonRefundable: true,
   *                  applicationFeeCost: 45,
   *                  sharedLaundry: true,
   *                  unitShared: true,
   *                  unitSharedPeople: 2,
   *                  sharedWashroomBath: true,
   *                  bed: 10,
   *                  bath: 3,
   *                  sqFt: 540,
   *                  leaseTerm: Month-to-month,
   *                  email: test@test.com,
   *                  phone: 12223344555,
   *                  unit: 1,
   *                  address: address,
   *                  utilities: null,
   *                  amenities: null,
   *                  noSmoking: true,
   *                  unitUtility: 100,
   *                  moveInDate: '2019-03-19T11:00:00+00',
   *                  appliances: null,
   *                  applicationProcess: Our qualification process is fairly simple so we encourage everyone to apply with their family.
   *                }
   *              ]
   *
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *         schema:
   *           type: object
   *           $ref: '#/definitions/Unit'
   *       400:
   *          description: Bad Request
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Not Found
   */

    async duplicate(req, res) {
        const { body, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const promises = [];
            for (let i = 0; i < body.list.length; i++) {
                let p = new Promise(async (resolve, reject) => {
                    const property = await Property.findByPk(body.list[i].propertyId);

                    if (property !== null) {
                        const userByProperty = await User.findByPk(property.userId);

                        if (userByProperty !== null && userByProperty.companyId === user.companyId) {
                            const unit = await Unit.create({
                                userId: user.id,
                                propertyId: property.id,
                                showingAgent: body.list[i].showingAgent !== undefined ? body.list[i].showingAgent : null,
                                rent: body.list[i].rent,
                                deposit: body.list[i].deposit,
                                status: body.list[i].status,
                                title: body.list[i].title,
                                description: body.list[i].description,
                                applicationFee: body.list[i].applicationFee,
                                appFeeNonRefundable: body.list[i].applicationFee && body.list[i].appFeeNonRefundable !== undefined ? body.list[i].appFeeNonRefundable : null,
                                applicationFeeCost: body.list[i].applicationFee && body.list[i].applicationFeeCost !== undefined ? body.list[i].applicationFeeCost : null,
                                parking: body.list[i].parking,
                                parkingCost: body.list[i].parking && body.list[i].parkingCost !== undefined ? body.list[i].parkingCost : null,
                                parkingCovered: body.list[i].parking && body.list[i].parkingCovered !== undefined ? body.list[i].parkingCovered : null,
                                parkingSecured: body.list[i].parking && body.list[i].parkingSecured !== undefined ? body.list[i].parkingSecured : null,
                                petFriendly: body.list[i].petFriendly,
                                petPolicy: body.list[i].petFriendly && body.list[i].petPolicy !== undefined ? body.list[i].petPolicy : null,
                                sharedLaundry: body.list[i].sharedAccountId,
                                unitShared: body.list[i].unitShared,
                                unitSharedPeople: body.list[i].unitShared && body.list[i].unitSharedPeople !== undefined ? body.list[i].unitSharedPeople : null,
                                sharedWashroomBath: body.list[i].sharedWashroomBath,
                                bed: body.list[i].bed,
                                bath: body.list[i].bath,
                                sqFt: body.list[i].sqFt,
                                leaseTerm: body.list[i].leaseTerm,
                                email: body.list[i].email,
                                phone: body.list[i].phone,
                                unit: body.list[i].unit,
                                address: body.list[i].address.replace('/,/g', ';'),
                                utilities: body.list[i].utilities,
                                amenities: body.list[i].amenities,
                                noSmoking: body.list[i].noSmoking,
                                unitUtility: body.list[i].unitUtility,
                                moveInDate: body.list[i].moveInDate,
                                appliances: body.list[i].appliances,
                                applicationProcess: body.list[i].applicationProcess,
                            });

                            return resolve(unit);
                        }

                    }

                    return reject(new Error('error'));
                });

                promises.push(p);
            }

            try {
                const units = await Promise.all(promises);
                return res.status(200).json({ units });

            } catch (err) {
                return res.status(404).json();
            }


        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/unit/update:
   *   put:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Unit
   *     description: Update unit
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *              id: 1,
   *              showingAgent: 1,
   *              rent: 100,
   *              deposit: 10,
   *              status: Vacant,
   *              title: title,
   *              description: description text,
   *              parking: true,
   *              parkingCost: 45,
   *              parkingCovered: true,
   *              parkingSecured: false,
   *              petFriendly: true,
   *              petPolicy: One time fees. Pet Deposit $300,
   *              applicationFee: true,
   *              appFeeNonRefundable: true,
   *              applicationFeeCost: 45,
   *              sharedLaundry: true,
   *              unitShared: true,
   *              unitSharedPeople: 2,
   *              sharedWashroomBath: 2,
   *              bed: 10,
   *              bath: 4,
   *              sqFt: 432,
   *              leaseTerm: Month-to-month,
   *              email: test@test.com,
   *              phone: +12223344555,
   *              unit: 1,
   *              address: address,
   *              amenities: null,
   *              utilities: null,
   *              noSmoking: true,
   *              unitUtility: 100,
   *              moveInDate: '2019-03-19T11:00:00+00',
   *              appliances: null,
   *              applicationProcess: Our qualification process is fairly simple so we encourage everyone to apply with their family.
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       400:
   *          description: Bad Request
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Not Found
   */

    async update(req, res) {
        const { body, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const unit = await Unit.extendedUpdate(user.companyId, body);

            if (unit) {
                findCustomItemsAndUpdate(user.id, user.companyId, {
                    amenities: body.amenities,
                    utilities: body.utilities,
                    appliances: body.appliances,
                });

                return res.status(200).json();
            }

            return res.status(404).json();
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/unit/change-status:
   *   put:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Unit
   *     description: Change unit status
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *             id: 1,
   *             status: "Sold"
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       400:
   *          description: Bad Request
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Not Found
   */

    async changeStatus(req, res) {
        const { body, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const unit = await Unit.findByPk(body.id, {
                raw: false,
            });

            if (unit !== null) {
                const userByUnit = await User.findByPk(unit.userId);

                if (
                    userByUnit !== null &&
          user.companyId === userByUnit.companyId
                ) {
                    await unit.update({
                        status: body.status,
                    });

                    return res.status(200).json();
                }
            }
            return res.status(404).json();
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/unit/change-property:
   *   put:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Unit
   *     description: Change property to unit
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *             unitId: 1,
   *             propertyId: 1
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       400:
   *          description: Bad Request
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Not Found
   */

    async changeProperty(req, res) {
        const { body, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const unit = await Unit.changeProperty(user, body);

            if (unit) {
                return res.status(200).json();
            }

            return res.status(404).json();
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/unit/destroy:
   *   delete:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Unit
   *     description: Destroy unit
   *     parameters:
   *         - name: id
   *           required: true
   *           in: query
   *           type: integer
   *           example: 1
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   *       404:
   *          description: Not Found
   */

    async destroy(req, res) {
        const { query, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const unit = await Unit.findByPk(query.id, {
                raw: false,
            });

            if (unit !== null) {
                const userByUnit = await User.findByPk(unit.userId);

                if (userByUnit && userByUnit.companyId === user.companyId) {
                    await unit.destroy();
                    return res.status(200).json();
                }
            }
            return res.status(404).json();
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },
};
