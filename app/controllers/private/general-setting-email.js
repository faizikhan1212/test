const { GeneralSettingEmail } = require('../../../database/models');
const { validationResult } = require('express-validator/check');

module.exports = {

    /**
   * @swagger
   * /private/general-setting-email/create:
   *   post:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - General Setting
   *     description: Create a general setting email
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *               sundayFrom: "04:00",
   *               sundayTo: "05:00",
   *               mondayFrom: "04:00",
   *               mondayTo: "05:00",
   *               tuesdayFrom: "04:00",
   *               tuesdayTo: "05:00",
   *               wednesdayFrom: "04:00",
   *               wednesdayTo: "05:00",
   *               thursdayFrom: "04:00",
   *               thursdayTo: "05:00",
   *               fridayFrom: "04:00",
   *               fridayTo: "05:00",
   *               saturdayFrom: "04:00",
   *               saturdayTo: "05:00",
   *               email: "example@gmail.com",
   *               message: "Hello. Thanks for contacting us.",
   *               forwardCall: true,
   *               forwardEmail: "forwardExample@gmail.com"
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: General Setting Email has created
   *         schema:
   *           type: object
   *           $ref: '#/definitions/GeneralSettingEmail'
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async create(req, res) {
        const { body, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const countEmail = await GeneralSettingEmail.count({
                where: {
                    userId: user.id,
                },
            });

            if (countEmail < 5) {
                const generalSettingEmail = await GeneralSettingEmail.create({
                    userId: user.id,
                    sundayFrom: body.sundayFrom,
                    sundayTo: body.sundayTo,
                    mondayFrom: body.mondayFrom,
                    mondayTo: body.mondayTo,
                    tuesdayFrom: body.tuesdayFrom,
                    tuesdayTo: body.tuesdayTo,
                    wednesdayFrom: body.wednesdayFrom,
                    wednesdayTo: body.wednesdayTo,
                    thursdayFrom: body.thursdayFrom,
                    thursdayTo: body.thursdayTo,
                    fridayFrom: body.fridayFrom,
                    fridayTo: body.fridayTo,
                    saturdayFrom: body.saturdayFrom,
                    saturdayTo: body.saturdayTo,
                    email: body.email,
                    message: body.message,
                    forwardCall: body.forwardCall,
                    forwardEmail: body.forwardEmail !== undefined && body.forwardEmail !== null ? body.forwardEmail : null,
                });

                return res.status(200).json({ generalSettingEmail: generalSettingEmail });
            }

            return res.status(400).json();


        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/general-setting-email/update:
   *   put:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - General Setting
   *     description: Update a general setting email
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *               id: 1,
   *               sundayFrom: "04:00",
   *               sundayTo: "05:00",
   *               mondayFrom: "04:00",
   *               mondayTo: "05:00",
   *               tuesdayFrom: "04:00",
   *               tuesdayTo: "05:00",
   *               wednesdayFrom: "04:00",
   *               wednesdayTo: "05:00",
   *               thursdayFrom: "04:00",
   *               thursdayTo: "05:00",
   *               fridayFrom: "04:00",
   *               fridayTo: "05:00",
   *               saturdayFrom: "04:00",
   *               saturdayTo: "05:00",
   *               email: "example@gmail.com",
   *               message: "Hello. Thanks for contacting us.",
   *               forwardCall: true,
   *               forwardEmail: "forwardExample@gmail.com"
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: General Setting Email has updated
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async update(req, res) {
        const { body, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const generalSettingEmail = await GeneralSettingEmail.findByPk(body.id, {
                raw: false,
            });

            if (generalSettingEmail !== null && generalSettingEmail.userId === user.id) {

                await generalSettingEmail.update({
                    sundayFrom: body.sundayFrom,
                    sundayTo: body.sundayTo,
                    mondayFrom: body.mondayFrom,
                    mondayTo: body.mondayTo,
                    tuesdayFrom: body.tuesdayFrom,
                    tuesdayTo: body.tuesdayTo,
                    wednesdayFrom: body.wednesdayFrom,
                    wednesdayTo: body.wednesdayTo,
                    thursdayFrom: body.thursdayFrom,
                    thursdayTo: body.thursdayTo,
                    fridayFrom: body.fridayFrom,
                    fridayTo: body.fridayTo,
                    saturdayFrom: body.saturdayFrom,
                    saturdayTo: body.saturdayTo,
                    email: body.email,
                    message: body.message,
                    forwardCall: body.forwardCall,
                    forwardEmail: body.forwardEmail !== undefined && body.forwardEmail !== null ? body.forwardEmail : null,
                });

                return res.status(200).json();

            }

            return res.status(404).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/general-setting-email/list:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - General Setting
   *     description: Show a general setting email
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: General Setting Email has updated
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async list(req, res) {
        const { user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const generalSettingEmail = await GeneralSettingEmail.findAll({
                where: {
                    userId: user.id,
                },
            });

            if (generalSettingEmail !== null) {
                return res.status(200).json({ generalSettingEmail: generalSettingEmail });

            }
            return res.status(404).json();


        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

};
