const { Availability, User } = require('../../../database/models');
const { validationResult } = require('express-validator/check');
const updateChatbotAvailability = require('../../../kafka/produces/availability');

module.exports = {


    /**
   * @swagger
   * /private/availability/show:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Availability
   *     description: Get an availability
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         schema:
   *           type: object
   *           $ref: '#/definitions/Availability'
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async show(req, res) {
        const { user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            if (user.role === 'Owner') {
                const availability = await Availability.findOne({
                    where: {
                        userId: user.id,
                    },
                });

                if (availability !== null) {
                    return res.status(200).json({ availability: availability });
                }
            } else {
                const admin = await User.findOne({
                    where: {
                        companyId: user.companyId,
                        role: 'Owner',
                    },
                });

                if (admin !== null) {
                    const availability = await Availability.findOne({
                        where: {
                            userId: admin.id,
                        },
                    });

                    if (availability !== null) {
                        return res.status(200).json({ availability: availability });
                    }
                }
            }

            return res.status(404).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }

    },


    /**
   * @swagger
   * /private/availability/create:
   *   post:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Availability
   *     description: Create an availability
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *               sundayFrom: 04:00,
   *               sundayTo: 05:00,
   *               mondayFrom: 04:00,
   *               mondayTo: 05:00,
   *               tuesdayFrom: 04:00,
   *               tuesdayTo: 05:00,
   *               wednesdayFrom: 04:00,
   *               wednesdayTo: 05:00,
   *               thursdayFrom: 04:00,
   *               thursdayTo: 05:00,
   *               fridayFrom: 04:00,
   *               fridayTo: 05:00,
   *               saturdayFrom: 04:00,
   *               saturdayTo: 05:00,
   *               limitProspects: 1,
   *               showingSlot: 1,
   *               stopAppointments: 1,
   *               maxShowings: 1,
   *               otherProperties: false
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Availability has created
   *         schema:
   *           type: object
   *           $ref: '#/definitions/Availability'
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async create(req, res) {
        const { body, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const [availability] = await Availability.findOrCreate({
                where: {
                    userId: user.id,
                },
                defaults: {
                    userId: user.id,
                    sundayFrom: body.sundayFrom,
                    sundayTo: body.sundayTo,
                    mondayFrom: body.mondayFrom,
                    mondayTo: body.mondayTo,
                    tuesdayFrom: body.tuesdayFrom,
                    tuesdayTo: body.tuesdayTo,
                    wednesdayFrom: body.wednesdayFrom,
                    wednesdayTo: body.wednesdayTo,
                    thursdayFrom: body.thursdayFrom,
                    thursdayTo: body.thursdayTo,
                    fridayFrom: body.fridayFrom,
                    fridayTo: body.fridayTo,
                    saturdayFrom: body.saturdayFrom,
                    saturdayTo: body.saturdayTo,
                    limitProspects: body.limitProspects !== undefined ? body.limitProspects : null,
                    showingSlot: body.showingSlot !== undefined ? body.showingSlot : null,
                    bufferTime: body.bufferTime !== undefined ? body.bufferTime : null,
                    otherProperties: body.otherProperties !== undefined && body.otherProperties !== null ? body.otherProperties : false,
                    stopAppointments: body.stopAppointments !== undefined ? body.stopAppointments : null,
                    maxShowings: body.maxShowings !== undefined ? body.maxShowings : null,
                },
            });

            updateChatbotAvailability(user.id);

            return res.status(200).json({ availability: availability });

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }

    },

    /**
   * @swagger
   * /private/availability/update:
   *   put:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Availability
   *     description: Update an availability
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *               id: 1,
   *               sundayFrom: 04:00,
   *               sundayTo: 05:00,
   *               mondayFrom: 04:00,
   *               mondayTo: 05:00,
   *               tuesdayFrom: 04:00,
   *               tuesdayTo: 05:00,
   *               wednesdayFrom: 04:00,
   *               wednesdayTo: 05:00,
   *               thursdayFrom: 04:00,
   *               thursdayTo: 05:00,
   *               fridayFrom: 04:00,
   *               fridayTo: 05:00,
   *               saturdayFrom: 04:00,
   *               saturdayTo: 05:00,
   *               limitProspects: 1,
   *               showingSlot: 1,
   *               bufferTime: 1,
   *               stopAppointments: 1,
   *               maxShowings: 1,
   *               otherProperties: false
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Availability has updated
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async update(req, res) {
        const { body, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }


            const availability = await Availability.findByPk(body.id, {
                raw: false,
            });

            if (availability !== null && availability.userId === user.id) {

                availability.update({
                    sundayFrom: body.sundayFrom,
                    sundayTo: body.sundayTo,
                    mondayFrom: body.mondayFrom,
                    mondayTo: body.mondayTo,
                    tuesdayFrom: body.tuesdayFrom,
                    tuesdayTo: body.tuesdayTo,
                    wednesdayFrom: body.wednesdayFrom,
                    wednesdayTo: body.wednesdayTo,
                    thursdayFrom: body.thursdayFrom,
                    thursdayTo: body.thursdayTo,
                    fridayFrom: body.fridayFrom,
                    fridayTo: body.fridayTo,
                    saturdayFrom: body.saturdayFrom,
                    saturdayTo: body.saturdayTo,
                    limitProspects: body.limitProspects !== undefined ? body.limitProspects : null,
                    showingSlot: body.showingSlot !== undefined ? body.showingSlot : null,
                    bufferTime: body.bufferTime !== undefined ? body.bufferTime : null,
                    otherProperties: body.otherProperties !== undefined && body.otherProperties !== null ? body.otherProperties : false,
                    stopAppointments: body.stopAppointments !== undefined ? body.stopAppointments : null,
                    maxShowings: body.maxShowings !== undefined ? body.maxShowings : null,
                });

                updateChatbotAvailability(user.id);

                return res.status(200).json();

            }

            return res.status(404).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }

    },

};
