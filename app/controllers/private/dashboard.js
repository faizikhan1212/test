const { Company } = require('../../../database/models');
const config = require('../../../config');

module.exports = {

    /**
   * @swagger
   * /private/dashboard/statistic:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Dashboard
   *     description: Get dashboard statistic
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *         example: {
   *           countProperties: 0,
   *           countUnitsVacant: 0,
   *           countTotalInquiries: 0,
   *           countQualifiedLeads: 0,
   *           countShowingsScheduled: 0,
   *           countMaintenanceRequests: 0
   *       }
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async statistic(req, res) {
        const { user } = req;

        try {

            const [[statistic]] = await Company.dashboardCount(user.companyId);

            return res.status(200).json({
                ...statistic,
            });

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/dashboard/link:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Dashboard
   *     description: Get dashboard link
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *         example: {
   *           link: link
   *       }
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async link(req, res) {
        const { user } = req;

        try {
            const company = await Company.findByPk(user.companyId);

            if (company !== null && company.slug !== null) {
                return res.status(200).json({
                    link: `${config.app.frontend_url}/#/company/${company.slug}`,
                });
            }

            return res.status(404).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

};
