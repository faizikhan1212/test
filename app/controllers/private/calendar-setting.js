const { CalendarSetting, User, CalendarAccount } = require('../../../database/models');
const googleService = require('../../services/google');

module.exports = {

    async getCalendarSetting(req, res) {
        const { user } = req;
        try {
            const expectedUser = await User.findByPk(user.id);
            if (expectedUser !== null) {
                const setting = await CalendarSetting.findOne({ where: { userId: expectedUser.id } });
                if (setting) {
                    return res.status(200).json(setting);
                } else {
                    return res.status(404).json();
                }
            }

            return res.status(404).json();

        } catch (err) {
            return res.status(500).json();
        }
    },

    async updateCalendarSetting(req, res) {
        const { user } = req;
        const { isSync } = req.body;

        try {
            // get user setting info
            const expectedUser = await User.findByPk(user.id);

            if (expectedUser !== null) {
                const setting = await CalendarSetting.findOne({ where: { userId: expectedUser.id } });
                if (setting) {
                    await CalendarSetting.update({
                        isSync: isSync,
                    }, {
                        where: {
                            userId: expectedUser.id,
                        },
                    });
                } else {
                    await CalendarSetting.create({
                        userId: user.id,
                        isSync: isSync,
                    });
                }

                if (isSync) {
                    let authToken = await CalendarAccount.getGoogleAccessToken(expectedUser.id);
                    googleService.listEventsAsync(authToken, user);
                }

                return res.status(200).json();
            }

            return res.status(404).json();

        } catch (err) {
            return res.status(500).json();
        }
    },
};
