const { Dictionary } = require('../../../database/models');
const { validationResult } = require('express-validator/check');
const updateChatbotDictionary = require('../../../kafka/produces/dictionary');

module.exports = {

    /**
   * @swagger
   * /private/dictionary/show:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Dictionary
   *     description: Show dictionary
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *         schema:
   *           type: object
   *           $ref: '#/definitions/Dictionary'
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Not Found
   */

    async show(req, res) {
        const { user } = req;
        try {

            const dictionary = await Dictionary.findOne({
                where: {
                    companyId: user.companyId,
                },
            });

            return res.status(200).json({ dictionary: dictionary });
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },


    /**
   * @swagger
   * /private/dictionary/push:
   *   put:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Dictionary
   *     description: Add items to dictionary
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *              type: amenities,
   *              items: ["Fridge", "Microwave"]
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   *       404:
   *          description: Not Found
   */

    async push(req, res) {
        const { body, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            Dictionary.pushItem(user.companyId, body.type, body.items);

            await updateChatbotDictionary(user.companyId);

            return res.status(200).json();
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },
};
