const bcrypt = require('bcrypt-nodejs');
const { validationResult } = require('express-validator/check');
const randtoken = require('rand-token');
const config = require('../../../config');
const redis = require('../../services/redis');
const jwt = require('jsonwebtoken');
const { User } = require('../../../database/models');

module.exports = {
    /**
   * @swagger
   * /private/auth/user:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Auth
   *     description: Refresh Token
   *     consumes:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *         schema:
   *           type: object
   *           $ref: '#/definitions/User'
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    getUser(req, res) {
        const { user } = req;

        try {
            return res.status(200).json({ user: user });
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/auth/change-password:
   *   put:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Auth
   *     description: Change Password
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *              oldPassword: Aa12345678!!,
   *              password: Aa123456!!,
   *              confirmPassword: Aa123456!!,
   *              rememberme: 1
   *          }
   *     consumes:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *         example: {
   *            token: token,
   *            refreshToken: refreshToken
   *         }
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */
    async changePassword(req, res) {
        const { body, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            await User.update({
                password: bcrypt.hashSync(body.password, bcrypt.genSaltSync()),
            }, {
                where: {
                    id: user.id,
                },
            });
            const randToken = randtoken.uid(256);

            if (parseInt(body.rememberme) === 1) {

                const token30d = jwt.sign({ id: user.id }, config.auth.token_secret, { expiresIn: config.auth.token_expiry_for_remember });
                const refreshToken = randToken.replace(new RegExp('^.{' + token30d.length + '}'), jwt.sign({ id: user.id }, config.auth.token_secret, { expiresIn: config.auth.token_expiry_for_remember }));

                redis.set(refreshToken, token30d);

                return res.status(200).json({
                    token: token30d,
                    refreshToken: refreshToken,
                });
            }

            const token1h = jwt.sign({ id: user.id }, config.auth.token_secret, { expiresIn: config.auth.token_expiry });
            const refreshToken = randToken.replace(new RegExp('^.{' + token1h.length + '}'), jwt.sign({ id: user.id }, config.auth.token_secret, { expiresIn: config.auth.refresh_token_expiry }));

            redis.set(refreshToken, token1h);

            return res.status(200).json({
                token: token1h,
                refreshToken: refreshToken,
            });
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },
};
