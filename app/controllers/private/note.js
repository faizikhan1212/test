const { Note } = require('../../../database/models');
const { validationResult } = require('express-validator/check');
const _ = require('lodash');

module.exports = {
    /**
   * @swagger
   * /private/note/list:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Note
   *     description: Show list of notes
   *     parameters:
   *         - name: entityName
   *           required: true
   *           in: query
   *           type: string
   *         - name: entityId
   *           required: true
   *           in: query
   *           type: number
   *         - name: offset
   *           required: true
   *           in: query
   *           type: string
   *         - name: limit
   *           required: true
   *           in: query
   *           type: string
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *         schema:
   *           type: object
   *           $ref: '#/definitions/Note'
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Not Found
   */

    async list(req, res) {
        const { query } = req;
        try {

            const capitalizeEntityName = _.capitalize(query.entityName);

            const $notes = Note.findAll({
                where: {
                    entityName: capitalizeEntityName,
                    entityId: query.entityId,
                },
                limit: query.limit,
                offset: query.offset,
            });

            const $countNotes = Note.count({
                where: {
                    entityName: capitalizeEntityName,
                    entityId: query.entityId,
                },
            });

            const [notes, countNotes] = [await $notes, await $countNotes];

            return res.status(200).json({ notes: notes, countNotes: countNotes });
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/note/create:
   *   post:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Note
   *     description: Create a note
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *              entityName: unit,
   *              entityId: 1,
   *              text: long answer to message,
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   *       404:
   *          description: Not Found
   */

    async create(req, res) {
        const { body, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            await Note.create({
                userId: user.id,
                entityName: _.capitalize(body.entityName),
                entityId: body.entityId,
                text: body.text,
            });

            return res.status(200).json();
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/note/update:
   *   put:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Note
   *     description: Update note
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *              id: 1,
   *              text: Some text,
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   *       404:
   *          description: Not Found
   */

    async update(req, res) {
        const { body, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const note = await Note.findByPk(body.id, {
                raw: false,
            });

            if (note !== null && user.id === note.userId) {
                await note.update({
                    text: body.text,
                });

                return res.status(200).json();
            }

            return res.status(404).json();
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/note/destroy:
   *   delete:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Note
   *     description: Destroy note
   *     parameters:
   *         - name: id
   *           required: true
   *           in: query
   *           type: integer
   *           example: 1
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   *       404:
   *          description: Not Found
   */

    async destroy(req, res) {
        const { query, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const result = await Note.destroy({
                where: {
                    id: query.id,
                    userId: user.id,
                },
            });

            if (result > 0) {
                return res.status(200).json();
            }

            return res.status(404).json();
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },
};
