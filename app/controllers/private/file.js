const { validationResult } = require('express-validator/check');
const _ = require('lodash');
const { File } = require('../../../database/models');
const deleteS3 = require('../../helpers/file/file-delete-s3');
const deleteLocal = require('../../helpers/file/file-delete-local');
const fileUrl = require('../../helpers/file/file-url');
const { file_store } = require('../../../config/file');
const { bucket } = require('../../../config/aws');

module.exports = {

    /**
   * @swagger
   * /private/file/create/{type}/{entityName}/{entityId}:
   *   post:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - File
   *     description: Upload image
   *     produces:
   *       - application/json
   *     consumes:
   *       - multipart/form-data
   *     parameters:
   *       - in: path
   *         name: type
   *         required: true
   *         type: string
   *         description: The type of file.
   *       - in: path
   *         name: entityName
   *         required: true
   *         type: string
   *         description: Entity name
   *       - in: path
   *         name: entityId
   *         required: true
   *         type: integer
   *         description: Entity id
   *       - in: formData
   *         name: file
   *         type: file
   *         description: The file to upload.
   *     responses:
   *       200:
   *         description: Success
   *         example: {
   *           id: 1,
   *           url: https://lethub.s3.eu-west-1.amazonaws.com/1549454100735.jpg,
   *           name: girl.jpg
   *         }
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Unexpected field. Required key 'file' not found
   *       400:
   *          description: Bad Request
   */

    async create(req, res) {
        const { user, params, file, imageValidationError } = req;
        const { entityName, entityId, type } = params;

        try {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            if (imageValidationError) {
                return res.status(400).json({
                    errors: imageValidationError,
                });
            }

            const capitalizeEntityName = _.capitalize(entityName);
            const capitalizeType = _.capitalize(type);

            switch (capitalizeEntityName) {
                case 'User':
                case 'Company':
                case 'Property': {
                    let object = await File.findOrCreate({
                        where: {
                            userId: user.id,
                            entityName: capitalizeEntityName,
                            entityId: entityId,
                            type: capitalizeType,
                        },
                        defaults: {
                            userId: user.id,
                            entityName: capitalizeEntityName,
                            entityId: entityId,
                            type: capitalizeType,
                            hash: file_store === 'aws' ? file.key : file.filename,
                            name: file.originalname,
                        },
                    });

                    let [item, isFileCreated] = object;

                    if (!isFileCreated) {

                        item = await File.findByPk(item.id, {
                            raw: false,
                        });


                        switch (file_store) {
                            case 'aws': {
                                deleteS3({
                                    Bucket: bucket,
                                    Key: item.hash,
                                });
                                break;
                            }
                            case 'local': {
                                deleteLocal(capitalizeEntityName, item);
                                break;
                            }
                        }

                        item = await File.findByPk(item.id, {
                            raw: false,
                        });

                        item = await item.update({
                            userId: user.id,
                            entityName: capitalizeEntityName,
                            entityId: entityId,
                            type: capitalizeType,
                            hash: file_store === 'aws' ? file.key : file.filename,
                            name: file.originalname,
                        });
                    }

                    return res.status(200).json({
                        id: item.id,
                        url: fileUrl(capitalizeEntityName, item),
                        name: item.name,
                    });
                }

                case 'Unit': {

                    const countItems = await File.count({
                        where: {
                            userId: user.id,
                            entityName: capitalizeEntityName,
                            entityId: entityId,
                            type: type,
                        },
                    });

                    if (countItems < 24) {
                        const item = await File.create({
                            userId: user.id,
                            entityName: capitalizeEntityName,
                            entityId: entityId,
                            type: capitalizeType,
                            hash: file_store === 'aws' ? file.key : file.filename,
                            name: file.originalname,
                        });

                        return res.status(200).json({
                            id: item.id,
                            url: fileUrl(capitalizeEntityName, item),
                            name: item.name,
                        });
                    }

                    return res.status(400).json();
                }
            }
        } catch (err) {
            console.log(err);
            return res.status(500).json();

        }

    },

    /**
   * @swagger
   * /private/file/destroy:
   *   delete:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - File
   *     description: Destroy file
   *     parameters:
   *         - name: id
   *           in: query
   *           required: true
   *           type: integer
   *           example: 1
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Not found
   *       400:
   *          description: Bad Request
   */

    async destroy(req, res) {
        const { query, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const result = await File.destroy({
                where: {
                    id: query.id,
                    userId: user.id,
                },
            });

            if (result > 0) {
                return res.status(200).json();
            }

            return res.status(404).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },
};
