const { Vendor, Property, User } = require('../../../database/models');
const { validationResult } = require('express-validator/check');

module.exports = {
    /**
   * @swagger
   * /private/vendor/list:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Vendor
   *     description: List vendors
   *     parameters:
   *         - name: offset
   *           required: true
   *           in: query
   *           type: integer
   *         - name: limit
   *           required: true
   *           in: query
   *           type: integer
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Not Found
   *       400:
   *          description: Bad Request
   */

    async list(req, res) {
        const { query, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const [[vendors], countVendors] = [
                await Vendor.getList(
                    user.companyId,
                    'ASC',
                    query.limit,
                    query.offset,
                ),
                await Vendor.getCountList(user.companyId),
            ];

            return res
                .status(200)
                .json({
                    vendors: vendors,
                    countVendors: countVendors,
                });

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/vendor/create:
   *   post:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Vendor
   *     description: Create vendor
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *              propertyId: 1,
   *              name: Best Cleaning Company,
   *              email: cleaning_company@cleaning.com,
   *              phone: 322322322,
   *              areasCovered: [Cleaning, Duct]
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *         schema:
   *           -$ref: '#/definitions/Vendor'
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Not Found
   *       400:
   *          description: Bad Request
   */

    async create(req, res) {
        const { body, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const property = await Property.findByPk(body.propertyId);

            if (property !== null) {

                const userByProperty = await User.findByPk(property.userId);

                if (userByProperty.companyId === user.companyId) {
                    const vendor = await Vendor.create({
                        userId: user.id,
                        propertyId: body.propertyId,
                        name: body.name,
                        email: body.email,
                        phone: body.phone,
                        areasCovered: body.areasCovered,
                    });

                    return res.status(200).json({ vendor: vendor });
                }
            }

            return res.status(404).json();


        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/vendor/update:
   *   put:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Vendor
   *     description: Update vendor
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *              id: 1,
   *              propertyId: 1,
   *              name: Best Cleaning Company,
   *              email: cleaning_company@cleaning.com,
   *              phone: 322322322,
   *              areasCovered: [Cleaning, Duct]
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Not found
   *       400:
   *          description: Bad Request
   */

    async update(req, res) {
        const { body, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const property = await Property.findByPk(body.propertyId);

            if (property !== null) {

                const userByProperty = await User.findByPk(property.userId);

                if (userByProperty.companyId === user.companyId) {
                    await Vendor.update({
                        propertyId: body.propertyId,
                        name: body.name,
                        email: body.email,
                        phone: body.phone,
                        areasCovered: body.areasCovered,
                    },
                    {
                        where: {
                            id: body.id,
                        },
                    });

                    return res.status(200).json();
                }
            }

            return res.status(404).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/vendor/destroy:
   *   delete:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Vendor
   *     description: Update vendor
   *     parameters:
   *         - name: id
   *           required: true
   *           in: query
   *           type: integer
   *           example: 1
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Not Found
   *       400:
   *          description: Bad Request
   */

    async destroy(req, res) {
        const { query, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const vendor = await Vendor.findByPk(query.id, {
                raw: false,
            });

            const userByVendor = await User.findByPk(vendor.userId);

            if (vendor && userByVendor && user.companyId === userByVendor.companyId) {
                await vendor.destroy();

                return res.status(200).json();
            }

            return res.status(404).json();
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },
};
