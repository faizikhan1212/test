const { UnitAvailability } = require('../../../database/models');
const { validationResult } = require('express-validator/check');
const _ = require('lodash');

module.exports = {

    /**
   * @swagger
   * /private/unit-availability/show:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Unit Availability
   *     description: Get an unit availability
   *     parameters:
   *         - name: unitId
   *           required: true
   *           in: query
   *           type: integer
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Unit availability has created
   *         schema:
   *           type: object
   *           $ref: '#/definitions/UnitAvailability'
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async show(req, res) {
        const { query, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const [unitAvailability] = await UnitAvailability.extendedShow(query.unitId, user.companyId);

            if (!_.isEmpty(unitAvailability)) {
                return res.status(200).json({ unitAvailability: unitAvailability });
            }

            return res.status(404).json();
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/unit-availability/create:
   *   post:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Unit Availability
   *     description: Create an unit availability
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *               unitId: 1,
   *               sundayFrom: 04:00,
   *               sundayTo: 05:00,
   *               mondayFrom: 04:00,
   *               mondayTo: 05:00,
   *               tuesdayFrom: 04:00,
   *               tuesdayTo: 05:00,
   *               wednesdayFrom: 04:00,
   *               wednesdayTo: 05:00,
   *               thursdayFrom: 04:00,
   *               thursdayTo: 05:00,
   *               fridayFrom: 04:00,
   *               fridayTo: 05:00,
   *               saturdayFrom: 04:00,
   *               saturdayTo: 05:00,
   *               limitProspects: 1,
   *               showingSlot: 1,
   *               stopAppointments: 1,
   *               maxShowings: 1
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Unit availability has created
   *         schema:
   *           type: object
   *           $ref: '#/definitions/UnitAvailability'
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async create(req, res) {
        const { body, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const unitAvailability = await UnitAvailability.extendedCreate(user, body);

            if (unitAvailability) {
                return res.status(200).json();
            }

            return res.status(404).json();
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/unit-availability/update:
   *   put:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Unit Availability
   *     description: Update an unit availability
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *               id: 1,
   *               sundayFrom: 04:00,
   *               sundayTo: 05:00,
   *               mondayFrom: 04:00,
   *               mondayTo: 05:00,
   *               tuesdayFrom: 04:00,
   *               tuesdayTo: 05:00,
   *               wednesdayFrom: 04:00,
   *               wednesdayTo: 05:00,
   *               thursdayFrom: 04:00,
   *               thursdayTo: 05:00,
   *               fridayFrom: 04:00,
   *               fridayTo: 05:00,
   *               saturdayFrom: 04:00,
   *               saturdayTo: 05:00,
   *               limitProspects: 1,
   *               showingSlot: 1,
   *               stopAppointments: 1,
   *               maxShowings: 1
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Unit availability has created
   *         schema:
   *           type: object
   *           $ref: '#/definitions/UnitAvailability'
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async update(req, res) {
        const { body, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const unitAvailability = await UnitAvailability.extendedUpdate(user.companyId, body);

            if (unitAvailability) {
                return res.status(200).json();
            }

            return res.status(404).json();
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },
};
