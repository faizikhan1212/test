const { GeneralSettingPhone } = require('../../../database/models');
const { validationResult } = require('express-validator/check');

module.exports = {

    /**
   * @swagger
   * /private/general-setting-phone/create:
   *   post:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - General Setting
   *     description: Create a general setting phone
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *               sundayFrom: "04:00",
   *               sundayTo: "05:00",
   *               mondayFrom: "04:00",
   *               mondayTo: "05:00",
   *               tuesdayFrom: "04:00",
   *               tuesdayTo: "05:00",
   *               wednesdayFrom: "04:00",
   *               wednesdayTo: "05:00",
   *               thursdayFrom: "04:00",
   *               thursdayTo: "05:00",
   *               fridayFrom: "04:00",
   *               fridayTo: "05:00",
   *               saturdayFrom: "04:00",
   *               saturdayTo: "05:00",
   *               phoneNumber: "2508888376",
   *               message: "Hello. Thanks for contacting us.",
   *               forwardCall: true,
   *               forwardPhoneNumber: "250777777"
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: General Setting Phone has created
   *         schema:
   *           type: object
   *           $ref: '#/definitions/GeneralSettingPhone'
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async create(req, res) {
        const { body, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const [generalSettingPhone] = await GeneralSettingPhone.findOrCreate({
                where: {
                    userId: user.id,
                },
                defaults: {
                    userId: user.id,
                    sundayFrom: body.sundayFrom,
                    sundayTo: body.sundayTo,
                    mondayFrom: body.mondayFrom,
                    mondayTo: body.mondayTo,
                    tuesdayFrom: body.tuesdayFrom,
                    tuesdayTo: body.tuesdayTo,
                    wednesdayFrom: body.wednesdayFrom,
                    wednesdayTo: body.wednesdayTo,
                    thursdayFrom: body.thursdayFrom,
                    thursdayTo: body.thursdayTo,
                    fridayFrom: body.fridayFrom,
                    fridayTo: body.fridayTo,
                    saturdayFrom: body.saturdayFrom,
                    saturdayTo: body.saturdayTo,
                    phoneNumber: body.phoneNumber,
                    message: body.message,
                    forwardCall: body.forwardCall,
                    forwardPhoneNumber: body.forwardPhoneNumber !== undefined && body.forwardPhoneNumber !== null ? body.forwardPhoneNumber : null,
                },
            });

            return res.status(200).json({ generalSettingPhone: generalSettingPhone });

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/general-setting-phone/update:
   *   put:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - General Setting
   *     description: Update a general setting phone
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *               id: 1,
   *               sundayFrom: "04:00",
   *               sundayTo: "05:00",
   *               mondayFrom: "04:00",
   *               mondayTo: "05:00",
   *               tuesdayFrom: "04:00",
   *               tuesdayTo: "05:00",
   *               wednesdayFrom: "04:00",
   *               wednesdayTo: "05:00",
   *               thursdayFrom: "04:00",
   *               thursdayTo: "05:00",
   *               fridayFrom: "04:00",
   *               fridayTo: "05:00",
   *               saturdayFrom: "04:00",
   *               saturdayTo: "05:00",
   *               phoneNumber: "2508888376",
   *               message: "Hello. Thanks for contacting us.",
   *               forwardCall: true,
   *               forwardPhoneNumber: "250777777"
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: General Setting Phone has updated
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async update(req, res) {
        const { body, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const generalSettingPhone = await GeneralSettingPhone.findByPk(body.id, {
                raw: false,
            });

            if (generalSettingPhone !== null && generalSettingPhone.userId === user.id) {
                await generalSettingPhone.update({
                    sundayFrom: body.sundayFrom,
                    sundayTo: body.sundayTo,
                    mondayFrom: body.mondayFrom,
                    mondayTo: body.mondayTo,
                    tuesdayFrom: body.tuesdayFrom,
                    tuesdayTo: body.tuesdayTo,
                    wednesdayFrom: body.wednesdayFrom,
                    wednesdayTo: body.wednesdayTo,
                    thursdayFrom: body.thursdayFrom,
                    thursdayTo: body.thursdayTo,
                    fridayFrom: body.fridayFrom,
                    fridayTo: body.fridayTo,
                    saturdayFrom: body.saturdayFrom,
                    saturdayTo: body.saturdayTo,
                    message: body.message,
                    forwardCall: body.forwardCall,
                    forwardPhoneNumber: body.forwardPhoneNumber !== undefined && body.forwardPhoneNumber !== null ? body.forwardPhoneNumber : null,
                });

                return res.status(200).json();

            }

            return res.status(404).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/general-setting-phone/show:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - General Setting
   *     description: Show a general setting phone
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: General Setting Phone has updated
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async show(req, res) {
        const { user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const generalSettingPhone = await GeneralSettingPhone.findOne({
                where: {
                    userId: user.id,
                },
            });

            return res.status(200).json({ generalSettingPhone: generalSettingPhone || {} });

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

};
