const { validationResult } = require('express-validator/check');
const randtoken = require('rand-token');
const bcrypt = require('bcrypt-nodejs');
const { User } = require('../../../database/models');
const { Property } = require('../../../database/models');
const { inviteMemberEmail, resendInviteMemberEmail } = require('../../jobs/emails');

module.exports = {
    /**
   * @swagger
   * /private/user/list:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - User
   *     description: Users list
   *     parameters:
   *         - name: offset
   *           in: query
   *           required: true
   *           type: integer
   *         - name: limit
   *           in: query
   *           required: true
   *           type: integer
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Not Found
   *       400:
   *          description: Bad Request
   */

    async list(req, res) {
        const { user, query } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const [users, countUsers] = [
                await User.findAll({
                    where: {
                        companyId: user.companyId,
                        status: {
                            $notLike: 'Deleted',
                        },
                    },
                    offset: query.offset,
                    limit: query.limit,
                }),
                await User.count({
                    where: {
                        companyId: user.companyId,
                        status: {
                            $notLike: 'Deleted',
                        },
                    },
                }),
            ];

            return res
                .status(200)
                .json({ users: users, countUsers: countUsers });
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    async checkMaxLimit(req, res) {
        const { user } = req;
        try{
        const count = await Property.countProperties(user.id);
        const maxLimit = await User.getMaxLimit(user.id);
        console.log(count)
        console.log(maxLimit)
        if(count[0][0].count < maxLimit[0][0].maxUnits){
        return res.status(200).json({ status: true});
        }else{
            return res.status(200).json({ status: false});
        }
        }catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },
    /**
   * @swagger
   * /private/user/create:
   *   post:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - User
   *     description: Create member
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *             name: Petr Petrov,
   *             email: petrov@email.com,
   *             phone: +1231123456,
   *             ext: 356
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Not Found
   *       400:
   *          description: Bad Request
   */

    async create(req, res) {
        const { body, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const member = await User.create({
                companyId: user.companyId,
                
                name: body.name,
                phone: body.phone,
                ext: body.ext !== undefined ? body.ext : null,
                email: body.email,
                role: 'Member',
                status: 'Invited',
                password: bcrypt.hashSync(
                    randtoken.uid(16),
                    bcrypt.genSaltSync(),
                ),
                maxUnits: user.maxUnits,
            });

            inviteMemberEmail(body.email);

            return res.status(200).json({ member: member });
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/user/resend-invite:
   *   post:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - User
   *     description: Resend invite to member
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *             email: petrov@email.com,
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       400:
   *          description: Account already confirmed
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Not Found
   */

    async resendInvite(req, res) {
        const { body, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const member = await User.findOne({
                where: {
                    email: body.email,
                },
            });

            if (member !== null && member.companyId === user.companyId) {
                if (member.confirm === false) {
                    resendInviteMemberEmail(body.email);
                    return res.status(200).json();
                } else {
                    return res.status(400).json({ msg: 'Account already confirmed' });
                }
            }
            return res.status(404).json();
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/user/update:
   *   put:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - User
   *     description: Update member
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *            id: 1,
   *              name: Sasha Drozdov,
   *              email: drozdov@email.com,
   *              phone: 312-412-321,
   *              ext: 32
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Not Found
   *       400:
   *          description: Bad Request
   */

    async update(req, res) {
        const { body, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const member = await User.findByPk(body.id, {
                raw: false,
            });

            if (member !== null && member.companyId === user.companyId) {
                await member.update({
                    name: body.name,
                    email: body.email,
                    phone: body.phone,
                    ext: body.ext !== undefined ? body.ext : null,
                });

                return res.status(200).json({ member: member });
            }

            return res.status(404).json();
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/user/destroy:
   *   put:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - User
   *     description: Destroy member
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {id: 1}
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Not found
   *       400:
   *          description: Bad Request
   */

    async destroy(req, res) {
        const { body, user } = req;
        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            let member = await User.findByPk(body.id, {
                raw: false,
            });


            if (member !== null && user.companyId === member.companyId && member.role !== 'Owner') {
                await member.update({
                    status: 'Deleted',
                    email: bcrypt.hashSync(member.email, bcrypt.genSaltSync()),
                    removedEmail: member.email,
                });

                return res.status(200).json();
            }

            return res.status(404).json();
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },
};
