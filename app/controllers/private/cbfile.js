const { CBFile } = require('../../../database/models');
const fileUrl = require('../../helpers/file/file-url');
const { validationResult } = require('express-validator/check');
const _ = require('lodash');
const redis = require('../../services/redis');
const randtoken = require('rand-token');

module.exports = {

    /**
   * @swagger
   * /private/cbfile/list:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - CBFile
   *     description: Get a list of cbfiles
   *     parameters:
   *         - name: entityName
   *           required: true
   *           in: query
   *           type: string
   *         - name: entityId
   *           required: true
   *           in: query
   *           type: number
   *         - name: offset
   *           required: true
   *           in: query
   *           type: string
   *         - name: limit
   *           required: true
   *           in: query
   *           type: string
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *          description: List of cbfiles
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async list(req, res) {
        const { user, query } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const capitalizeEntityName = _.capitalize(query.entityName);

            switch (capitalizeEntityName) {
                case 'Maintenance': {


                    let [
                        [CBFiles],
                        countCBFiles,
                    ] = [
                        await CBFile.getList(capitalizeEntityName, query.entityId, [user.companyId, query.limit, query.offset, 'ASC']),
                        await CBFile.countList(capitalizeEntityName, query.entityId, [user.companyId]),
                    ];

                    if (CBFiles && CBFiles.length > 0) {

                        const token = randtoken.uid(256);
                        const folderName = `${CBFiles[0].leadId}-${query.entityId}`;
                        redis.set(token, folderName, 'EX', 60 * 60 * 24);


                        CBFiles = CBFiles.map(file => ({
                            id: file.id,
                            url: fileUrl(capitalizeEntityName, file, folderName, token),
                            name: file.name,
                        }));

                        return res.status(200).json({ CBFile: CBFiles, count: countCBFiles });
                    }

                    return res.status(404).json();
                }

                default: {
                    return res.status(400).json();
                }
            }
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },
};
