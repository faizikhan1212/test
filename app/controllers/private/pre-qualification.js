const { PreQualification, User } = require('../../../database/models');
const { validationResult } = require('express-validator/check');

module.exports = {

    /**
   * @swagger
   * /private/pre-qualification/show:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Pre-qualification
   *     description: Show a pre-qualification
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         schema:
   *           type: object
   *           $ref: '#/definitions/PreQualification'
   *       400:
   *          description: Bad Request
   *       401:
   *          description: Unauthorized
   */

    async show(req, res) {
        const { user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            if (user.role === 'Owner') {
                const preQualification = await PreQualification.findOne({
                    where: {
                        userId: user.id,
                    },
                });

                if (preQualification !== null) {
                    return res.status(200).json({ preQualification: preQualification });
                }
            } else {
                const admin = await User.findOne({
                    where: {
                        companyId: user.companyId,
                        role: 'Owner',
                    },
                });

                if (admin !== null) {
                    const preQualification = await PreQualification.findOne({
                        where: {
                            userId: admin.id,
                        },
                    });

                    if (preQualification !== null) {
                        return res.status(200).json({ preQualification: preQualification });
                    }

                }
            }


            return res.status(404).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }

    },

    /**
   * @swagger
   * /private/pre-qualification/create:
   *   post:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Pre-qualification
   *     description: Create a pre-qualification
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *               pet: true,
   *               cat: 2,
   *               dog: 1,
   *               agree: true,
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: PreQualification has created
   *         schema:
   *           type: object
   *           $ref: '#/definitions/PreQualification'
   *       400:
   *          description: Bad Request
   *       401:
   *          description: Unauthorized
   */

    async create(req, res) {
        const { body, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const [preQualification] = await PreQualification.findOrCreate({
                where: {
                    userId: user.id,
                },
                defaults: {
                    userId: user.id,
                    pet: body.pet,
                    cat: body.cat !== undefined ? body.cat : null,
                    dog: body.dog !== undefined ? body.dog : null,
                    accept: body.agree,
                },
            });

            return res.status(200).json({ preQualification: preQualification });

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }

    },


    /**
   * @swagger
   * /private/pre-qualification/update:
   *   put:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Pre-qualification
   *     description: Update a pre-qualification
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *               id: 1,
   *               pet: true,
   *               cat: 2,
   *               dog: 1,
   *               agree: true,
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: PreQualification has updated
   *       400:
   *          description: Bad Request
   *       401:
   *          description: Unauthorized
   */

    async update(req, res) {
        const { body, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const preQualification = await PreQualification.findByPk(body.id, {
                raw: false,
            });

            if (preQualification !== null && preQualification.userId === user.id) {
                preQualification.update({
                    pet: body.pet,
                    cat: body.cat !== undefined ? body.cat : null,
                    dog: body.dog !== undefined ? body.dog : null,
                    accept: body.agree,
                });

                return res.status(200).json();
            }

            return res.status(404).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }

    },

};
