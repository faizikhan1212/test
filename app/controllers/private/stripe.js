const stripe = require('../../services/stripe');
const { validationResult } = require('express-validator/check');
const { User, Customer, Subscription, Company } = require('../../../database/models');
const config = require('../../../config');

module.exports = {

    /**
   * @swagger
   * /private/stripe/buy:
   *   post:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Stripe
   *     description: Create Stripe Token
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *              number: '4242424242424242',
   *              expMonth: 12,
   *              expYear: 2020,
   *              cvc: '123',
   *              plan: 'plan_',
   *              countUnits: 200
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   */

    async buy(req, res) {
        const { body, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const token = await stripe.tokens.create({
                card: {
                    number: body.number,
                    exp_month: body.expMonth,
                    exp_year: body.expYear,
                    cvc: body.cvc,
                },
            });

            let customer = await Customer.findOne({
                raw: false,
                where: {
                    userId: user.id,
                },
            });

            if (customer !== null) {
                try {
                    await stripe.customers.retrieve(customer.customer);
                } catch (err) {
                    await customer.destroy();
                    customer = null;
                }
            }

            if (customer === null) {

                const company = await Company.findByPk(user.companyId, {
                    raw: false
                });


                if (company !== null && user.name !== null && company.address !== null) {

                    const stripeCustomer = await stripe.customers.create({
                        source: token.id,
                        email: user.email,
                        name: user.name,
                        address: {
                            line1: company.address,
                        },
                        shipping: {
                            name: user.name,
                            address: {
                                line1: company.address
                            }
                        },
                        description: 'Good luck'
                    });

                    await stripe.subscriptions.create({
                        customer: stripeCustomer.id,
                        items: [
                            {
                                plan: body.plan,
                                quantity: body.countUnits,
                            },
                        ],
                    });

                }

            } else {

                await stripe.subscriptions.create({
                    customer: customer.customer,
                    items: [
                        {
                            plan: body.plan,
                            quantity: body.countUnits,
                        },
                    ],
                });
            }

            return res.status(200).json();
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/stripe/plan-list:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Stripe
   *     description: Plan list
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   */

    async planList(req, res) {

        try {
            const plans = await stripe.plans.list({
                limit: 3,
                product: config.stripe.product_billing_id,
            });

            return res.status(200).json({ plans: plans.data });

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/stripe/invoices:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Stripe
   *     description: Get invoices
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Not Found
   *       500:
   *          description: Internal Error
   */

    async invoices(req, res) {
        const { user } = req;

        try {

            let customer = await Customer.findOne({
                raw: false,
                where: {
                    userId: user.id,
                },
            });

            if (customer !== null) {

                let invoices = await stripe.invoices.list({
                    customer: customer.customer,
                    limit: 100000,
                });

                if (invoices !== null) {
                    invoices = invoices.data.map(invoice => ({
                        date: invoice.created,
                        urlPdf: invoice.invoice_pdf,
                    }));

                    return res.status(200).json({ invoices });
                }

            }

            return res.status(404).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },


    /**
   * @swagger
   * /private/stripe/deactivate:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Stripe
   *     description: Deactivate billing
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   */

    async deactivate(req, res) {
        const { user } = req;

        try {

            if (user.subscription) {
                const subscription = await Subscription.findOne({
                    attributes: [
                        'subscription',
                    ],
                    where: {
                        userId: user.id,
                    },
                });

                stripe.subscriptions.del(subscription.subscription, (err, confirmation) => {
                });

                await Subscription.destroy({
                    where: {
                        id: subscription.id,
                    },
                });
            }

            await User.update({
                subscription: false,
            }, {
                where: {
                    id: user.id,
                },
            });

            return res.status(200).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/stripe/extend-plan:
   *   post:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Stripe
   *     description: Extend existing plan
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *              number: '4242424242424242',
   *              expMonth: 12,
   *              expYear: 2020,
   *              cvc: '123',
   *              plan: 'plan_',
   *              countUnits: 200
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Unauthorized
   */

    async extendPlan(req, res) {
        const { body, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const currentTimestamp = (new Date()).getTime() + 2000;
            let billingTimestamp = user.billingExpire !== null ? user.billingExpire.getTime() : null;

            if (billingTimestamp !== null) {

                if (currentTimestamp < billingTimestamp) {

                    let customer = await Customer.findOne({
                        raw: false,
                        where: {
                            userId: user.id,
                        },
                    });

                    try {
                        await stripe.customers.retrieve(customer.customer);
                    } catch (err) {
                        await customer.destroy();
                        customer = null;
                    }

                    if (customer !== null) {

                        const listPlans = await stripe.plans.list({
                            limit: 1,
                            product: config.stripe.product_billing_id,
                        });

                        if (listPlans && listPlans.data && listPlans.data.length > 0) {

                            const paymentIntent = await stripe.paymentIntents.create({
                                amount: body.countUnits * listPlans.data[0].amount,
                                currency: 'USD',
                                customer: customer.customer,
                                payment_method_types: ['card'],
                            });

                            const listOfPaymentMethods = await stripe.paymentMethods.list({
                                customer: customer.customer,
                                type: 'card',
                            });

                            let paymentMethod;

                            if (listOfPaymentMethods !== null && listOfPaymentMethods.data && listOfPaymentMethods.data.length > 0) {
                                paymentMethod = listOfPaymentMethods.data[0];
                            } else {

                                paymentMethod = await stripe.paymentMethods.create({
                                    type: 'card',
                                    card: {
                                        number: body.number,
                                        exp_month: body.expMonth,
                                        exp_year: body.expYear,
                                        cvc: body.cvc,
                                    },
                                });

                                paymentMethod = await stripe.paymentMethods.attach(paymentMethod.id, { customer: customer.customer });
                            }

                            const confirm = await stripe.paymentIntents.confirm(paymentIntent.id, { payment_method: paymentMethod.id });

                            return res.status(200).json({ paymentIntent, paymentMethod, confirm });

                        }

                    }

                }

            }

            return res.status(404).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

};
