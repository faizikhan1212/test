const { Property, Unit } = require('../../../database/models');
const { validationResult } = require('express-validator/check');
const { exportCsvProcess, importCsvProcess } = require('../../jobs/csv');
const findCustomItemsAndUpdate = require('../../jobs/dictionary/find-custom-items-and-update');
const redis = require('../../services/redis');


module.exports = {

    /**
   * @swagger
   * /private/property/search:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Property
   *     description: Search properties
   *     parameters:
   *         - name: q
   *           required: true
   *           in: query
   *           type: string
   *         - name: limit
   *           required: true
   *           in: query
   *           type: string
   *         - name: offset
   *           required: true
   *           in: query
   *           type: string
   *         - name: filter
   *           in: query
   *           type: object
   *           example: {"userId": 1}
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async search(req, res) {
        const { query, user } = req;

        try {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const $properties = Property.search(query.q, [user.companyId, query.filter, query.limit, query.offset, 'DESC']);
            const $countProperties = Property.countSearchResult(query.q, [user.companyId, query.filter]);

            let [
                [properties],
                countProperties,
            ] = [
                    await $properties,
                    await $countProperties,
                ];

            return res.status(200).json({ properties: properties, countProperties: countProperties });

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/property/show:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Property
   *     description: Show property by id
   *     parameters:
   *         - name: id
   *           required: true
   *           in: query
   *           type: integer
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   *       500:
   *          description: Bad Request
   */

    async show(req, res) {
        const { query, user } = req;

        try {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const [[property]] = await Property.extendedShow(user.companyId, query.id);

            if (property) {
                return res.status(200).json({ property: property });
            }

            return res.status(404).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/property/list:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Property
   *     description: List property
   *     parameters:
   *         - name: offset
   *           required: true
   *           in: query
   *           type: integer
   *         - name: limit
   *           required: true
   *           in: query
   *           type: integer
   *         - name: filter
   *           in: query
   *           type: object
   *           example: {
   *              userId: 1
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async list(req, res) {
        const { query, user } = req;

        try {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const $properties = Property.getExtendedPropertyList([user.companyId, query.filter, query.limit, query.offset, 'DESC']);

            const $countProperties = Property.countListResult([user.companyId, query.filter]);

            let [
                [properties],
                countProperties,
            ] = [
                    await $properties,
                    await $countProperties,
                ];

            return res.status(200).json({ properties: properties, countProperties: countProperties });

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/property/create:
   *   post:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Property
   *     description: Create property
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *              type: Apartment,
   *              propertyName: MordorApartment,
   *              numberUnits: 23,
   *              leaseTerm: 1 year,
   *              address: address,
   *              parking: true,
   *              parkingCost: 45,
   *              parkingCovered: true,
   *              parkingSecured: false,
   *              petFriendly: true,
   *              petPolicy: One time fees. Pet Deposit $300,
   *              applicationFee: true,
   *              appFeeNonRefundable: true,
   *              applicationFeeCost: 45,
   *              noSmoking: true,
   *              utilities: null,
   *              amenities: null
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *         schema:
   *           type: object
   *           $ref: '#/definitions/Property'
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   */

    async create(req, res) {
        const { body, user } = req;

        try {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const property = await Property.create({
                userId: user.id,
                type: body.type,
                propertyName: body.propertyName,
                numberUnits: body.numberUnits,
                leaseTerm: body.leaseTerm,
                address: body.address.replace('/,/g', ';'),
                applicationFee: body.applicationFee,
                appFeeNonRefundable: body.applicationFee && body.appFeeNonRefundable !== undefined ? body.appFeeNonRefundable : null,
                applicationFeeCost: body.applicationFee && body.applicationFeeCost !== undefined ? body.applicationFeeCost : null,
                parking: body.parking,
                parkingCost: body.parking && body.parkingCost !== undefined ? body.parkingCost : null,
                parkingCovered: body.parking && body.parkingCovered !== undefined ? body.parkingCovered : null,
                parkingSecured: body.parking && body.parkingSecured !== undefined ? body.parkingSecured : null,
                petFriendly: body.petFriendly,
                petPolicy: body.petFriendly && body.petPolicy !== undefined ? body.petPolicy : null,
                noSmoking: body.noSmoking,
                utilities: body.utilities !== undefined ? body.utilities : null,
                amenities: body.amenities !== undefined ? body.amenities : null,
                url: body.url
            });

            findCustomItemsAndUpdate(user.id, user.companyId, {
                amenities: body.amenities,
                utilities: body.utilities,
                appliances: body.appliances,
            });

            return res.status(200).json({ property: property });

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/property/update:
   *   put:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Property
   *     description: Update property
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *              id: 1,
   *              type: Apartment,
   *              propertyName: MordorApartment,
   *              numberUnits: 23,
   *              leaseTerm: 1 year,
   *              address: address,
   *              parking: true,
   *              parkingCost: 45,
   *              parkingCovered: true,
   *              parkingSecured: false,
   *              petFriendly: true,
   *              petPolicy: One time fees. Pet Deposit $300,
   *              applicationFee: true,
   *              appFeeNonRefundable: true,
   *              applicationFeeCost: 45,
   *              noSmoking: true,
   *              utilities: null,
   *              amenities: null
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   *       404:
   *          description: Not Found
   */

    async update(req, res) {
        const { body, user } = req;

        try {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const property = await Property.findByPk(body.id, {
                raw: false,
            });

            if (property !== null && user.id === property.userId) {

                if (property.address !== body.address) {
                    await Unit.update({
                        address: body.address,
                    }, {
                            where: {
                                propertyId: property.id,
                            },
                        });
                }

                const newProperty = await property.update({
                    userId: user.id,
                    type: body.type,
                    propertyName: body.propertyName,
                    numberUnits: body.numberUnits,
                    leaseTerm: body.leaseTerm,
                    address: body.address.replace(/,/g, ';'),
                    applicationFee: body.applicationFee,
                    appFeeNonRefundable: body.applicationFee && body.appFeeNonRefundable !== undefined ? body.appFeeNonRefundable : null,
                    applicationFeeCost: body.applicationFee && body.applicationFeeCost !== undefined ? body.applicationFeeCost : null,
                    parking: body.parking,
                    parkingCost: body.parking && body.parkingCost !== undefined ? body.parkingCost : null,
                    parkingCovered: body.parking && body.parkingCovered !== undefined ? body.parkingCovered : null,
                    parkingSecured: body.parking && body.parkingSecured !== undefined ? body.parkingSecured : null,
                    petFriendly: body.petFriendly,
                    petPolicy: body.petFriendly && body.petPolicy !== undefined ? body.petPolicy : null,
                    noSmoking: body.noSmoking,
                    utilities: body.utilities !== undefined ? body.utilities : null,
                    amenities: body.amenities !== undefined ? body.amenities : null,
                    url: body.url
                });

                findCustomItemsAndUpdate(user.id, user.companyId, {
                    amenities: body.amenities,
                    utilities: body.utilities,
                    appliances: body.appliances,
                });


                return res.status(200).json({ property: newProperty });

            }
            return res.status(404).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/property/destroy:
   *   delete:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Property
   *     description: Destroy property
   *     parameters:
   *         - name: id
   *           required: true
   *           in: query
   *           type: integer
   *           example: 1
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   *       404:
   *          description: Not Found
   */

    async destroy(req, res) {
        const { query, user } = req;

        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            const property = await Property.findByPk(query.id, {
                raw: false,
            });

            if (property !== null && property !== undefined) {

                let countUnits = await Unit.countListByPropertyResult(property.id, [user.companyId]);

                await property.destroy();

                redis.decrby(`${user.id}-count-unit`, countUnits);

                return res.status(200).json();

            }

            return res.status(404).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },

    /**
   * @swagger
   * /private/property/run-import-validate/{type}:
   *   post:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Property
   *     produces:
   *       - application/json
   *     consumes:
   *       - multipart/form-data
   *     parameters:
   *       - in: path
   *         name: type
   *         required: true
   *         type: string
   *         description: The type of file.
   *       - in: formData
   *         name: file
   *         type: file
   *         description: The file to upload.
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   *       404:
   *          description: Not Found
   */

    runImportValidate(req, res) {
        const { user, file, csvValidationError, io } = req;

        try {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            if (csvValidationError) {
                return res.status(400).json({ error: csvValidationError });
            }

            let dateJob = +new Date();

            importCsvProcess(user, file, dateJob, io);

            return res.status(200).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }

    },

    /**
   * @swagger
   * /private/property/run-export:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Property
   *     description: Export property
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   *       404:
   *          description: Not Found
   */

    runExport(req, res) {
        const { user, io } = req;

        try {

            let dateJob = +new Date();

            exportCsvProcess(user, dateJob, io);

            return res.status(200).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },
    async validatePropertyURL(req, res) {
        const { body, user } = req;
        try {
            const property = await Property.findOne({
                where: {
                    url: body.url,
                },
            });
            if (property == null) {
                return res.status(200).json({ property: null });
            } else if (body.id != null && +body.id == +property.id) {
                return res.status(200).json({ property: null });
            } else {
                return res.status(200).json({ property: property });
            }
        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }
    },
};
