const { Company } = require('../../../database/models');
const { validationResult } = require('express-validator/check');
const buildSlug = require('slug');

module.exports = {

    /**
   * @swagger
   * /private/company/show:
   *   get:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Company
   *     description: Show company info
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *         schema:
   *           type: object
   *           $ref: '#/definitions/Company'
   *       401:
   *          description: Unauthorized
   *       404:
   *          description: Not Found
   */

    async show(req, res) {
        const { user } = req;

        try {

            const company = await Company.findByPk(user.companyId, {
                raw: false,
            });

            if (company !== null) {
                return res.status(200).json({ company: company });
            }

            return res.status(404).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }

    },


    /**
   * @swagger
   * /private/company/update:
   *   put:
   *     security:
   *       - Bearer: []
   *     tags:
   *       - Company
   *     description: Just update a company
   *     parameters:
   *         - name: data
   *           in: body
   *           type: object
   *           example: {
   *              name: name,
   *              address: address,
   *              email: example@email.com,
   *              phones: [
   *                  phone1,
   *                  phone2
   *              ]
   *           }
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Success
   *       401:
   *          description: Unauthorized
   *       400:
   *          description: Bad Request
   *       404:
   *          description: Not Found
   */

    async update(req, res) {
        const { body, user } = req;

        try {

            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                });
            }

            let slug;

            const company = await Company.findByPk(user.companyId, {
                raw: false,
            });

            if (company.name !== body.name) {

                const countCompaniesWhereName = await Company.countOtherCompanies(user.companyId, 'name', body.name);
                slug = countCompaniesWhereName ? buildSlug(body.name + '_' + (countCompaniesWhereName + 1)) : buildSlug(body.name);

                const countCompaniesWhereSlug = await Company.countOtherCompanies(user.companyId, 'slug', slug);
                slug = countCompaniesWhereSlug ? buildSlug(slug + '_' + (countCompaniesWhereSlug + 1)) : slug;

            } else {

                slug = company.slug;

            }

            await company.update({
                name: body.name,
                address: body.address,
                slug: slug,
                email: body.email !== undefined && body.email !== null ? body.email : null,
                phones: body.phones !== undefined && body.phones !== null ? body.phones : null,
            });

            return res.status(200).json();

        } catch (err) {
            console.log(err);
            return res.status(500).json();
        }

    },
};
