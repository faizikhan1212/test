module.exports = x => {

    const matchString = x
        .toString()
        .match(/([^d.]*)\.(\d*)/);

    return matchString ? matchString[2].length : null;

};
