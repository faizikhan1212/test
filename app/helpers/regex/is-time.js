module.exports = (value, options = {}) => {
    const opts = Object.assign({
        required: false,
    }, options);

    if (opts.required) {
        return /^(([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9])$/.test(value);
    }

    return /^(([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9])?$/.test(value) || value === undefined || value === null;
};