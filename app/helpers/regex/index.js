const isEmail = require('./is-email');
const isMobilePhone = require('./is-mobile-phone');
const isTime = require('./is-time');

module.exports = {
    isEmail,
    isMobilePhone,
    isTime,
};