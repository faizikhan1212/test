module.exports = (value) => typeof value === 'string' && /^\+?[0-9]\d{1,14}$/.test(value);
