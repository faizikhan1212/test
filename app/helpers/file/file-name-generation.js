const path = require('path');
const _ = require('lodash');
const { file_store } = require('../../../config/file');
const randtoken = require('rand-token');


module.exports = function(req, file, cb) {
    const { params: { entityName } } = req;
    switch (_.capitalize(entityName)) {
        case 'Maintenance': {
            return cb(null, `${file_store === 'aws' ? 'private' : ''}/${randtoken.uid(100).toString() + path.extname(file.originalname)}`);
        }
        default: {
            return cb(null, `${file_store === 'aws' ? 'public' : ''}/${randtoken.uid(100).toString() + path.extname(file.originalname)}`);
        }
    }
};
