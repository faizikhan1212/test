const s3 = require('../../services/aws');

module.exports = params => {
    s3.deleteObject(params, (err, data) => {
        if (data) {
            console.log('File deleted successfully');
        } else {
            console.log('File deleted from s3 failure : ' + err);
        }
    });
};
