const moment = require('moment');
const {
    aws: { bucket, endpoint, link_expiry, region },
    app: { proxy_host },
    file: { file_store },
} = require('../../../config');
const aws = require('../../services/aws');
const _ = require('lodash');

module.exports = (entityName, file, folderName, token) => {
    switch (file_store) {
        case 'aws': {
            switch (_.capitalize(entityName)) {

                case 'Maintenance': {
                    return aws.getSignedUrl('getObject', {
                        Bucket: bucket,
                        Key: `${file.hash}`,
                        Expires: link_expiry,
                    });
                }

                default: {

                    if (region === 'ca-central-1') {
                        return `https://${bucket}.s3.${region}.amazonaws.com/${file.hash}`;
                    } else {
                        return `${endpoint}/${bucket}/${file.hash}`;
                    }
                }

            }

        }
        case 'local': {
            const date = new Date(2019, 1, 1, 1, 1, 1, 1);
            if (moment(file.createdAt).format('YYYY-MM-DDTHH:mm:ss.SSSZ') === moment(date).format('YYYY-MM-DDTHH:mm:ss.SSSZ')) {
                return `${proxy_host}/seeds/${file.name}`;
            }

            if (entityName === 'Maintenance') {
                return `${proxy_host}/private-files/${entityName}/${folderName}/${file.hash}?token=${token}`;
            }

            return `${proxy_host}/public-files/${entityName}/${file.hash}`;
        }
    }
};
