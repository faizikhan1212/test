const fs = require('fs');
const { local_path } = require('../../../config/file');

module.exports = (entityName, existingImage) => {
    fs.unlink(`${local_path}/${entityName}/${existingImage.hash}`, err => {
        if (err) {
            console.log('File deleted from locally storage failure');
        } else {
            console.log('File deleted successfully');
        }
    });
};
