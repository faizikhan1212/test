module.exports = value => {
    if (typeof value !== 'string') {
        return false;
    }

    if (value.indexOf(';') < 0) {
        return false;
    }

    const values = value.split(';');

    if (values.length > 2 || (values[0].length === values[1].length && values[0].length === 0)) {
        return false;
    }

    const isTwoPositiveNumber = values.every(el => !isNaN(el) && el >= 0);

    if (isTwoPositiveNumber && parseInt(values[0]) > parseInt(values[1])) {
        return false;
    }

    return values.every(el => el === '' || isTwoPositiveNumber);
};
