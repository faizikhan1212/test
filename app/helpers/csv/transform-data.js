const moment = require('moment');

module.exports = (data) => {

    let splittedAmenities;
    if (data[14] && data[14].indexOf(',') > 0) {
        splittedAmenities = data[14].split(',');
    } else if (data[14] && data[14].indexOf(';') > 0) {
        splittedAmenities = data[14].split(';');
    }
    let propertyAmenities = data[14] && splittedAmenities ? splittedAmenities.map(el => el.trim()) : null;

    let splittedUtilities;
    if (data[15] && data[15].indexOf(',') > 0) {
        splittedUtilities = data[15].split(',');
    } else if (data[15] && data[15].indexOf(';') > 0) {
        splittedUtilities = data[15].split(';');
    }
    let propertyUtilities = data[15] && splittedUtilities ? splittedUtilities.map(el => el.trim()) : null;

    let splittedAppliances;
    if (data[27] && data[27].indexOf(',') > 0) {
        splittedAppliances = data[27].split(',');
    } else if (data[27] && data[27].indexOf(';') > 0) {
        splittedAppliances = data[27].split(';');
    }

    let appliances = splittedAppliances ? splittedAppliances.map(el => el.trim()) : null;

  const deposit = !data[18] ? null : (data[18][0] === '$' ? data[18].replace(/[^\x00-\x7F]/g, '').replace(/ /g, '').slice(1) : data[18].replace(/[^\x00-\x7F]/g, '').replace(/ /g, '')); //eslint-disable-line

    return {
        type: data[0],
        propertyName: data[1],
        address: data[2],
        leaseTerm: data[3],
        applicationFee: !!(data[4] && data[4] === 'Yes'),
        applicationFeeCost: data[5] || data[5] === 0 ? data[5] : null,
        appFeeNonRefundable: !!(data[6] && data[6] === 'Yes'),
        parking: !!(data[7] && data[7] === 'Yes'),
        parkingCost: data[8] || data[8] === 0 ? data[8] : null,
        parkingCovered: !!(data[9] && data[9] === 'Yes'),
        parkingSecured: !!(data[10] && data[10] === 'Yes'),
        petFriendly: !!(data[11] && data[11] === 'Yes'),
        petPolicy: data[12] || null,
        noSmoking: !!(data[13] && data[13] === 'Yes'),
        amenities: propertyAmenities,
        utilities: propertyUtilities,
        deposit,
        appliances,
        unit: data[16],
        rent: data[17][0] === '$' ? data[17].replace(/ /g, '').replace(String.fromCharCode(65533), '').slice(1) : data[17].replace(/ /g, ''),
        utilityBill: data[19] || null,
        moveInDate: moment(data[20], 'DD.MM.YYYY').format(),
        bed: data[21],
        bath: data[22],
        sqFt: data[23] || null,
        status: data[24],
        unitShared: !!(data[25] && data[25] === 'Yes'),
        unitSharedPeople: data[26] || data[26] === 0 ? data[26] : null,
        sharedLaundry: !!(data[28] && data[28] === 'Yes'),
        sharedWashroomBath: !!(data[29] && data[29] === 'Yes'),
        title: data[30],
        description: data[31],
        applicationProcess: data[32],
        email: data[33] || null,
    phone: (data[34] && data[34].replace(/[^\x00-\x7F]/g, '').replace(/\s/g, '')) || null, //eslint-disable-line
    };
};
