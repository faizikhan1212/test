define start_task =
    yarn start
endef

define build_task =
    yarn install
endef

define build_start_task =
    yarn install
    yarn start
endef

run: ; $(value $(TASK))
.ONESHELL: