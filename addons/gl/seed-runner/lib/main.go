package main

import (
    "C"
	"fmt"
	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
)

type User struct {
	Id     int64
	Name   string
	Emails []string
}

func MyFunction(arg1, arg2 int, arg3 string) int64 {
    return 34
}

func (u User) String() string {
	return fmt.Sprintf("User<%d %s %v>", u.Id, u.Name, u.Emails)
}

type Story struct {
	Id       int64
	Title    string
	AuthorId int64
	Author   *User
}

func (s Story) String() string {
	return fmt.Sprintf("Story<%d %s %s>", s.Id, s.Title, s.Author)
}

//export doSomething
func doSomething () {

    channel := make(chan int)

    db := pg.Connect(&pg.Options{
        User:     "user",
        Password: "pass",
        Database: "postgres",
        Addr:     "localhost:5432",
    })
    defer db.Close()

    err := createSchema(db)

            if err == nil {
                 go func () {
                     user1 := &User{
                         Name:   "admin",
                         Emails: []string{"admin1@admin", "admin2@admin"},
                     }
                     err = db.Insert(user1)
                     if err == nil {
                         err = db.Insert(&User{
                             Name:   "root",
                             Emails: []string{"root1@root", "root2@root"},
                         })
                         if err != nil {
                             panic(err)
                         }

                         story1 := &Story{
                             Title:    "Cool story",
                             AuthorId: user1.Id,
                         }
                         err = db.Insert(story1)
                         if err != nil {
                             panic(err)
                         }

                         user := &User{Id: user1.Id}
                         err = db.Select(user)
                         if err != nil {
                             panic(err)
                         }

                         var users []User
                         err = db.Model(&users).Select()
                         if err != nil {
                             panic(err)
                         }

                         story := new(Story)
                         err = db.Model(story).
                             Relation("Author").
                             Where("story.id = ?", story1.Id).
                             Select()
                         if err != nil {
                             panic(err)
                         }
                       }
                  channel <- 200
                }()

            }



        fmt.Println("Channel code is ", channel)

//        close(channel)
}

func createSchema(db *pg.DB) error {
	for _, model := range []interface{}{(*User)(nil), (*Story)(nil)} {
		err := db.CreateTable(model, &orm.CreateTableOptions{
			Temp: false,
		})
		if err != nil {
			return err
		}
	}
	return nil
}

func main() {}