#include "lib/libgo.h"
#include <node.h>

namespace seedRunner {

  using v8::FunctionCallbackInfo;
  using v8::Isolate;
  using v8::Local;
  using v8::Object;
  using v8::String;
  using v8::Value;
  using v8::Number;
  using v8::Exception;

  void runSomething(const FunctionCallbackInfo<Value>& args) {
    doSomething();
  }

  void init(Local<Object> exports) {
    NODE_SET_METHOD(exports, "runSomething", runSomething);
  }

  NODE_MODULE(seedRunner, init)
}