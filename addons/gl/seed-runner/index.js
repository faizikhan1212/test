const { runSomething } = require('./build/Release/addon');
const { Suite } = require('benchmark');

new Suite()
    .add('Run Something', async () => {
        try {
            runSomething();
        } catch (err) {
            console.log(err);
        }
    })
    .run();