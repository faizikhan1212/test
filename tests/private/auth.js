module.exports = (app, request) => {

    const PRIVATE_AUTH_USER = '/private/auth/user';

    // TODO: other constants of current controller

    describe(`GET ${PRIVATE_AUTH_USER}`, () => {
        it('respond with json containing a current auth user', (done) => {
            request(app)
                .get(PRIVATE_AUTH_USER)
                .set('Accept', 'application/json')
                .expect(200, done);
        });
    });

    // TODO: other describes of current controller

};

