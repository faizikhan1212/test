'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Responders', {
            id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
            },
            userId: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            label: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            generatedEmail: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            forwardEmail: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            autoresponse: {
                type: Sequelize.TEXT,
                allowNull: false,
            },
            inquiries: {
                type: Sequelize.INTEGER,
                allowNull: false,
            }
        });
    },
    down: queryInterface => {
        return queryInterface.dropTable('Responders');
    },
};
