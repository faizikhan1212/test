const express = require('express');
const router = express.Router();
const passport = require('passport');


const auth = require('../app/controllers/public/auth');
const unit = require('../app/controllers/public/unit');
const file = require('../app/controllers/public/file');
const user = require('../app/controllers/public/user');
const autoresponder = require('../app/controllers/public/autoresponder');

const maintenance = require('../app/controllers/public/maintenance');
const cbfile = require('../app/controllers/public/cbfile');
const company = require('../app/controllers/public/company');
const calendarAccount = require('../app/controllers/public/calendar-account');

const authValidator = require('../app/validators/public/auth');
const fileValidator = require('../app/validators/public/file');
const unitValidator = require('../app/validators/public/unit');
const userConfirmValidator = require('../app/validators/public/user/confirm');
const maintenanceValidator = require('../app/validators/public/maintenance');
const cbfileValidator = require('../app/validators/public/cbfile');
const showCompanyValidator = require('../app/validators/public/company/show');


router.post('/autoresponder' , [] , autoresponder.getEmail);

router.post('/register', [...authValidator.register.rules], auth.register);
router.post('/resend-register-email', [...authValidator.resendRegisterEmail.rules], auth.resendRegisterEmail);
router.post('/login', [...authValidator.login.rules, passport.authenticate('local')], auth.login);
router.post('/refresh', [...authValidator.refresh.rules], auth.refresh);
router.get('/google', passport.authenticate('google', { scope: ['email', 'profile'] }));
router.get('/google/callback', passport.authenticate('google'), auth.google);
router.post('/logout', [...authValidator.logout.rules], auth.logout);
router.post('/confirm', [...authValidator.confirm.rules], auth.confirm);
router.post('/reset-password', [...authValidator.resetPassword.rules], auth.resetPassword);
router.post('/check-reset-token', [...authValidator.checkResetToken.rules], auth.checkResetToken);
router.post('/change-password', [...authValidator.changePassword.rules], auth.changePassword);


router.get('/unit/list', [...unitValidator.list.rules], unit.list);
router.get('/unit/show', [...unitValidator.show.rules], unit.show);


router.get('/file/list', [...fileValidator.list.rules], file.list);
router.get('/file/show', [...fileValidator.list.rules], file.show);


router.post('/user/confirm', [...userConfirmValidator.rules], user.confirm);


router.post('/maintenance/create', [...maintenanceValidator.create.rules], maintenance.create);


router.post('/cbfile/create/:leadId/:type/:entityName/:entityId', [...cbfileValidator.create.rules], cbfile.create);


router.get('/company/show', [...showCompanyValidator.rules], company.show);


router.get('/calendar/accounts/oauthcallback', calendarAccount.callbackAuth);
router.get('/calendar/accounts/watchcallback', calendarAccount.callbackWatch);


module.exports = router;
