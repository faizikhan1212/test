const publicRoute = require('./public');
const privateRoute = require('./private');
const swaggerRoute = require('./swagger');
const webhookRoute = require('./webhook');

module.exports = {
    publicRoute,
    privateRoute,
    swaggerRoute,
    webhookRoute,
};
