const express = require('express');
const router = express.Router();

const checkCompanyOwner = require('../app/middlewares/check-company-owner');
const checkPlan = require('../app/middlewares/stripe/plan');

const {
    auth,
    company,
    property,
    dashboard,
    availability,
    preQualification,
    question,
    unit,
    lead,
    file,
    responders,
    stripe,
    user,
    generalSettingPhone,
    phoneNumber,
    generalSettingEmail,
    email,
    vendor,
    maintenance,
    scheduling,
    note,
    unitAvailability,
    unitPreQualification,
    unitQuestion,
    cbfile,
    calendarAccount,
    calendarSetting,
    dictionary,
} = require('../app/controllers/private');


const createAvailabilityValidator = require('../app/validators/availability');
const createPreQualificationValidator = require('../app/validators/pre-qualification');


const companyValidator = require('../app/validators/company');
const propertyValidator = require('../app/validators/property');
const questionValidator = require('../app/validators/question');
const fileValidator = require('../app/validators/file');
const unitValidator = require('../app/validators/unit');
const leadValidator = require('../app/validators/lead');
const authValidator = require('../app/validators/auth');
const stripeValidator = require('../app/validators/stripe');
const userValidator = require('../app/validators/user');
const generalSettingPhoneValidator = require('../app/validators/general-setting-phone');
const phoneNumberValidator = require('../app/validators/phone-number');
const generalSettingEmailValidator = require('../app/validators/general-setting-email');
const emailValidator = require('../app/validators/email');
const schedulingValidator = require('../app/validators/scheduling');
const noteValidator = require('../app/validators/note');
const vendorValidator = require('../app/validators/vendor');
const maintenanceValidator = require('../app/validators/maintenance');
const unitAvailabilityValidator = require('../app/validators/unit-availability');
const unitPreQualificationValidator = require('../app/validators/unit-pre-qualification');
const unitQuestionValidator = require('../app/validators/unit-question');
const cbfileValidator = require('../app/validators/cbfile');
const calendarAccountValidator = require('../app/validators/calendar-account');
const dictionaryValidator = require('../app/validators/dictionary');

router.get('/auth/user', [], auth.getUser);
router.put('/auth/change-password', [...authValidator.changePassword.rules], auth.changePassword);


router.get('/user/list', [...userValidator.list.rules], user.list);
router.post('/user/create', [...userValidator.create.rules, checkCompanyOwner], user.create);
router.post('/user/resend-invite', [...userValidator.resendInvite.rules, checkCompanyOwner], user.resendInvite);
router.put('/user/update', [...userValidator.update.rules, checkCompanyOwner], user.update);
router.put('/user/destroy', [...userValidator.destroy.rules, checkCompanyOwner], user.destroy);
router.get('/user/checklimit', [] ,user.checkMaxLimit);

router.get('/responders/list', [] ,responders.list);
router.post('/responders/save', [] ,responders.save);

router.post('/company/create', [...companyValidator.update.rules, checkCompanyOwner], company.update);
router.put('/company/update', [...companyValidator.update.rules, checkCompanyOwner], company.update);
router.get('/company/show', [], company.show);


router.get('/property/search', [...propertyValidator.search.rules], property.search);
router.get('/property/show', [...propertyValidator.show.rules], property.show);
router.get('/property/list', [...propertyValidator.list.rules], property.list);
router.post('/property/create', [...propertyValidator.create.rules], property.create);
router.put('/property/update', [...propertyValidator.update.rules], property.update);
router.delete('/property/destroy', [...propertyValidator.destroy.rules], property.destroy);
router.get('/property/run-export', [checkCompanyOwner], property.runExport);
router.post('/property/run-import-validate/:type', [...propertyValidator.runImport.rules], property.runImportValidate);
router.post('/property/validate-property-url', property.validatePropertyURL);

router.get('/dashboard/statistic', [], dashboard.statistic);
router.get('/dashboard/link', [], dashboard.link);


router.get('/availability/show', [], availability.show);
router.post('/availability/create', [...createAvailabilityValidator.create.rules], availability.create);
router.put('/availability/update', [...createAvailabilityValidator.update.rules], availability.update);


router.get('/pre-qualification/show', [], preQualification.show);
router.post('/pre-qualification/create', [...createPreQualificationValidator.create.rules], preQualification.create);
router.put('/pre-qualification/update', [...createPreQualificationValidator.update.rules], preQualification.update);


router.get('/question/list', [...questionValidator.list.rules], question.list);
router.post('/question/create', [...questionValidator.create.rules], question.create);
router.put('/question/update', [...questionValidator.update.rules], question.update);
router.delete('/question/destroy', [...questionValidator.destroy.rules], question.destroy);


router.post('/file/create/:type/:entityName/:entityId', [
    ...fileValidator.create.rules,
], file.create);
router.delete('/file/destroy', [...fileValidator.destroy.rules], file.destroy);


router.get('/unit/vacant-search', [...unitValidator.vacantSearch.rules], unit.vacantSearch);
router.get('/unit/search', [...unitValidator.search.rules], unit.search);
router.get('/unit/show', [...unitValidator.show.rules], unit.show);
router.get('/unit/list', [...unitValidator.list.rules], unit.list);
router.get('/unit/vacant-list', [...unitValidator.vacantList.rules], unit.vacantList);
router.post('/unit/create', [...unitValidator.create.rules, checkPlan], unit.create);
router.post('/unit/duplicate', [...unitValidator.duplicate.rules, checkPlan], unit.duplicate);
router.put('/unit/update', [...unitValidator.update.rules], unit.update);
router.put('/unit/change-property', [...unitValidator.changeProperty.rules], unit.changeProperty);
router.put('/unit/change-status', [...unitValidator.changeStatus.rules], unit.changeStatus);
router.delete('/unit/destroy', [...unitValidator.destroy.rules], unit.destroy);


router.get('/unit-availability/show', [...unitAvailabilityValidator.show.rules], unitAvailability.show);
router.post('/unit-availability/create', [...unitAvailabilityValidator.create.rules], unitAvailability.create);
router.put('/unit-availability/update', [...unitAvailabilityValidator.update.rules], unitAvailability.update);


router.get('/unit-pre-qualification/show', [...unitPreQualificationValidator.show.rules], unitPreQualification.show);
router.post('/unit-pre-qualification/create', [...unitPreQualificationValidator.create.rules], unitPreQualification.create);
router.put('/unit-pre-qualification/update', [...unitPreQualificationValidator.update.rules], unitPreQualification.update);


router.get('/unit-question/list', [...unitQuestionValidator.list.rules], unitQuestion.list);
router.post('/unit-question/create', [...unitQuestionValidator.create.rules], unitQuestion.create);
router.put('/unit-question/update', [...unitQuestionValidator.update.rules], unitQuestion.update);
router.delete('/unit-question/destroy', [...unitQuestionValidator.destroy.rules], unitQuestion.destroy);


router.get('/lead/search', [...leadValidator.search.rules], lead.search);
router.get('/lead/list', [...leadValidator.list.rules], lead.list);
router.get('/lead/show', [...leadValidator.show.rules], lead.show);
router.get('/lead/report', [...leadValidator.report.rules], lead.report);
router.put('/lead/change-status', [], lead.changeStatus);

router.get('/stripe/plan-list', [checkCompanyOwner], stripe.planList);
router.post('/stripe/buy', [...stripeValidator.buy.rules, checkCompanyOwner], stripe.buy);
router.get('/stripe/deactivate', [checkCompanyOwner], stripe.deactivate);
router.get('/stripe/invoices', [checkCompanyOwner], stripe.invoices);
router.post('/stripe/extend-plan', [checkCompanyOwner], stripe.extendPlan);


router.get('/general-setting-phone/show', [...generalSettingPhoneValidator.show.rules, checkCompanyOwner], generalSettingPhone.show);
router.post('/general-setting-phone/create', [...generalSettingPhoneValidator.create.rules, checkCompanyOwner], generalSettingPhone.create);
router.put('/general-setting-phone/update', [...generalSettingPhoneValidator.update.rules, checkCompanyOwner], generalSettingPhone.update);


router.get('/phone-number/list', [...phoneNumberValidator.create.rules, checkCompanyOwner], phoneNumber.list);
router.post('/phone-number/create', [...phoneNumberValidator.create.rules, checkCompanyOwner], phoneNumber.create);


router.get('/general-setting-email/list', [...generalSettingEmailValidator.list.rules, checkCompanyOwner], generalSettingEmail.list);
router.post('/general-setting-email/create', [...generalSettingEmailValidator.create.rules, checkCompanyOwner], generalSettingEmail.create);
router.put('/general-setting-email/update', [...generalSettingEmailValidator.update.rules, checkCompanyOwner], generalSettingEmail.update);


router.get('/email/list', [...emailValidator.create.rules, checkCompanyOwner], email.list);
router.post('/email/create', [...emailValidator.create.rules, checkCompanyOwner], email.create);


router.get('/vendor/list', [...vendorValidator.list.rules], vendor.list);
router.post('/vendor/create', [...vendorValidator.create.rules, checkCompanyOwner], vendor.create);
router.put('/vendor/update', [...vendorValidator.update.rules, checkCompanyOwner], vendor.update);
router.delete('/vendor/destroy', [...vendorValidator.destroy.rules, checkCompanyOwner], vendor.destroy);


router.get('/maintenance/list', [...maintenanceValidator.list.rules], maintenance.list);
router.get('/maintenance/search', [...maintenanceValidator.search.rules], maintenance.search);
router.put('/maintenance/update', [...maintenanceValidator.update.rules], maintenance.update);
router.post('/maintenance/alert', [...maintenanceValidator.alert.rules], maintenance.alert);
router.get('/maintenance/report', [...maintenanceValidator.report.rules], maintenance.report);


router.get('/note/list', [...noteValidator.list.rules], note.list);
router.post('/note/create', [...noteValidator.create.rules], note.create);
router.put('/note/update', [...noteValidator.update.rules], note.update);
router.delete('/note/destroy', [...noteValidator.destroy.rules], note.destroy);


router.post('/scheduling/create', [...schedulingValidator.create.rules], scheduling.create);
router.put('/scheduling/update', [...schedulingValidator.update.rules], scheduling.update);
router.get('/scheduling/show', [...schedulingValidator.show.rules], scheduling.show);
router.delete('/scheduling/destroy', [...schedulingValidator.destroy.rules], scheduling.destroy);
router.get('/scheduling/list', [...schedulingValidator.list.rules], scheduling.list);
router.get('/scheduling/list-by-lead', [...schedulingValidator.listByLead.rules], scheduling.listByLead);
router.get('/scheduling/sync', scheduling.sync);

router.get('/calendar/accounts/auth', [...calendarAccountValidator.auth.rules], calendarAccount.auth);
router.get('/calendar/account', calendarAccount.getCalendarAccount);
router.post('/calendar/accounts', calendarAccount.updateCalendarAccount);
router.put('/calendar/accounts', calendarAccount.updateCalendarAccountSetting);
router.delete('/calendar/accounts/destroy', calendarAccount.destroy);
router.get('/calendar/accounts/watch', calendarAccount.calendarWatch);

router.get('/calendar/setting', calendarSetting.getCalendarSetting);
router.put('/calendar/setting', calendarSetting.updateCalendarSetting);

router.get('/cbfile/list', [...cbfileValidator.list.rules], cbfile.list);


router.get('/dictionary/show', dictionary.show);
router.put('/dictionary/push', [...dictionaryValidator.push.rules], dictionary.push);

module.exports = router;
