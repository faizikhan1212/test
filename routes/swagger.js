const express = require('express');
const router = express.Router();
const config = require('../config');
const swaggerUi = require('swagger-ui-express');
const swaggerJSDoc = require('swagger-jsdoc');

const swaggerDefinition = {
    info: {
        title: 'LetHub API',
        version: '1.0.0',
        description: 'LetHub API',
    },
    host: `${config.swagger.swagger_host}:${config.swagger.swagger_port}`,
    basePath: '/',
    securityDefinitions: {
        Bearer: {
            type: 'apiKey',
            name: 'Authorization',
            in: 'header',
        },
    },

};
const options = {
    swaggerDefinition,
    apis: ['./app/controllers/**/*.js', './database/models/*.js'],
};

const swaggerSpec = swaggerJSDoc(options);

router.get('/swagger.json', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    res.send(swaggerSpec);
});
router.use('/v1/', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

module.exports = router;
