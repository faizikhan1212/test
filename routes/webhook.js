const express = require('express');
const router = express.Router();


const stripe = require('../app/webhooks/stripe');


router.post('/payment-succeeded', stripe.paymentSucceeded);
router.post('/paymentintent-succeeded', stripe.paymentintentSucceeded);

module.exports = router;
