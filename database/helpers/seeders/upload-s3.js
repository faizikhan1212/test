const AWS = require('aws-sdk');
const fs = require('fs');
const path = require('path');
const _ = require('lodash');
const { aws } = require('../../../config');

module.exports = async () => {

    AWS.config.update({
        accessKeyId: aws.access_key_id,
        secretAccessKey: aws.secret_access_key,
    });

    const s3 = new AWS.S3();

    let images = [];
    fs.readdirSync(path.resolve('database/storage/app/file/seeds/')).forEach(file => {
        images.push(file);
    });

    let i = 0;
    let promises = [];
    _.times(images.length, async () => {

        const file = fs.readFileSync(path.resolve('database/storage/app/file/seeds/', images[i]));

        const params = {
            Bucket: aws.bucket,
            Key: images[i],
            Body: file,
        };

        promises.push(s3.upload(params).promise());
        i++;
    });

    return Promise.all(promises);

};

