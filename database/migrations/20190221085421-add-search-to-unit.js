'use strict';
const searchUp = require('../helpers/search/migrations/tsvector-up.js');
const searchDown = require('../helpers/search/migrations/tsvector-down.js');

module.exports = {
    up: (queryInterface) => {
        return searchUp(queryInterface.sequelize,
            'Unit',
            [
                'unit',
                'address',
            ],
            'postText');
    },

    down: (queryInterface) => {
        return searchDown(queryInterface.sequelize,
            'Unit',
            [
                'unit',
                'address',
            ],
            'postText');
    },
};
