'use strict';
const trigramSearchUp = require('../helpers/search/migrations/trigram-up');
const trigramSearchDown = require('../helpers/search/migrations/trigram-down');

module.exports = {
    up: (queryInterface) => {
        return Promise.all([
            trigramSearchUp(queryInterface.sequelize,
                'Unit',
                [
                    'unit',
                    'address',
                ],
            ),
            trigramSearchUp(queryInterface.sequelize,
                'Property',
                [
                    'country',
                    'state',
                    'city',
                    '"streetAddress"',
                    'zip',
                ]),
        ]);
    },
    down: (queryInterface) => {
        return Promise.all([
            trigramSearchDown(queryInterface.sequelize,
                'Unit'),
            trigramSearchDown(queryInterface.sequelize,
                'Property')],
        );
    },

};
