'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.addColumn('Unit', 'showingAgent', {
                type: Sequelize.INTEGER,
                allowNull: true,
                references: {
                    model: 'User',
                    key: 'id',
                    deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
                },
            }),
        ]);
    },

    down: (queryInterface, Sequelize) => {

        return Promise.all([
            queryInterface.removeColumn('Unit', 'showingAgent'),
        ]);
    },
};



