'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.addColumn('Company', 'email',
                {
                    type: Sequelize.STRING,
                    allowNull: true,
                },
            ),
            queryInterface.addColumn('Company', 'phones',
                {
                    type: Sequelize.ARRAY(Sequelize.STRING),
                    allowNull: true,
                },
            ),
        ]);
    },

    down: (queryInterface) => {
        return Promise.all([
            queryInterface.removeColumn('Company', 'email'),
            queryInterface.removeColumn('Company', 'phones'),
        ]);
    },
};
