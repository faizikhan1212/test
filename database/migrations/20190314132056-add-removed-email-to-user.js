'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.addColumn('User', 'removedEmail',
                {
                    type: Sequelize.STRING,
                    allowNull: true,
                },
            ),
        ]);
    },

    down: (queryInterface) => {
        return Promise.all([
            queryInterface.removeColumn('User', 'removedEmail'),
        ]);
    },
};
