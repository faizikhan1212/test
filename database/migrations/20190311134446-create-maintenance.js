'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Maintenance', {
            id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
            },
            leadId: {
                type: Sequelize.INTEGER,
                allowNull: false,
                onDelete: 'CASCADE',
                references: {
                    model: 'Lead',
                    key: 'id',
                    deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
                },
            },
            vendorId: {
                type: Sequelize.INTEGER,
                allowNull: true,
                onDelete: 'CASCADE',
                references: {
                    model: 'Vendor',
                    key: 'id',
                    deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
                },
            },
            tenantName: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            category: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            description: {
                type: Sequelize.TEXT,
                allowNull: false,
            },
            cost: {
                type: Sequelize.DECIMAL,
                allowNull: true,
            },
            budget: {
                type: Sequelize.DECIMAL,
                allowNull: true,
            },
            estimate: {
                type: Sequelize.DECIMAL,
                allowNull: true,
            },
            status: {
                type: Sequelize.STRING,
                allowNull: false,
                defaultValue: 'Not Started',
            },
            dateScheduled: {
                type: Sequelize.DATE,
                allowNull: true,
            },
            dateCompleted: {
                type: Sequelize.DATE,
                allowNull: true,
            },
            createdAt: {
                type: Sequelize.DATE,
                allowNull: false,
            },
            updatedAt: {
                type: Sequelize.DATE,
                allowNull: false,
            },
        });
    },
    down: (queryInterface) => {
        return queryInterface.dropTable('Maintenance');
    },
};
