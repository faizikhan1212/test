'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.addColumn('Scheduling', 'calendarAccountId',
                {
                    type: Sequelize.INTEGER,
                    allowNull: true,
                    onDelete: 'CASCADE',
                    references: {
                        model: 'CalendarAccounts',
                        key: 'id',
                        deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
                    },
                },
            ),
            queryInterface.addColumn('Scheduling', 'createdOnApp',
                {
                    type: Sequelize.STRING,
                    allowNull: false,
                    defaultValue: 'lethub',
                },
            ),
            queryInterface.addColumn('Scheduling', 'googleEventId',
                {
                    type: Sequelize.STRING,
                    allowNull: true,
                },
            ),
            queryInterface.addColumn('Scheduling', 'officeEventId',
                {
                    type: Sequelize.STRING,
                    allowNull: true,
                },
            ),
            queryInterface.addColumn('Scheduling', 'propertyId',
                {
                    type: Sequelize.INTEGER,
                    allowNull: true,
                    references: {
                        model: 'Property',
                        key: 'id',
                        deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
                    },
                },
            ),
            queryInterface.changeColumn('Scheduling', 'unitId',
                {
                    type: Sequelize.INTEGER,
                    allowNull: true,
                },
            ),
            queryInterface.addColumn('Scheduling', 'isDeleted',
                {
                    type: Sequelize.BOOLEAN,
                    allowNull: false,
                    defaultValue: false,
                },
            ),
        ]);

    },

    down: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.removeColumn('CalendarAccounts', 'calendarAccountId'),
            queryInterface.removeColumn('Scheduling', 'createdOnApp'),
            queryInterface.removeColumn('Scheduling', 'googleEventId'),
            queryInterface.removeColumn('Scheduling', 'officeEventId'),
            queryInterface.removeColumn('Scheduling', 'propertyId'),
            queryInterface.removeColumn('Scheduling', 'isDeleted'),
        ]);
    },
};
