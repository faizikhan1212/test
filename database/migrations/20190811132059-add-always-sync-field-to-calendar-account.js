'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.addColumn('CalendarAccounts', 'alwaysSync', {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: true,
            }),
        ]);
    },

    down: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.removeColumn('CalendarAccounts', 'alwaysSync'),
        ]);
    },
};
