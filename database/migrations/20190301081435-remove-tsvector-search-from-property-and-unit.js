'use strict';
const tsvectorSearchDown = require('../helpers/search/migrations/tsvector-down.js');
const tsvectorSearchUp = require('../helpers/search/migrations/tsvector-up.js');

module.exports = {
    up: (queryInterface) => {
        return Promise.all([
            tsvectorSearchDown(queryInterface.sequelize,
                'Unit',
                [
                    'unit',
                    'address',
                ],
                'postText'),
            tsvectorSearchDown(queryInterface.sequelize,
                'Property',
                [
                    '"propertyName"',
                    'country',
                    'state',
                    'city',
                    '"streetAddress"',
                ],
                'postText'),
        ]);
    },

    down: (queryInterface) => {
        return Promise.all([
            tsvectorSearchUp(queryInterface.sequelize,
                'Unit',
                [
                    'unit',
                    'address',

                ],
                'postText'),
            tsvectorSearchUp(queryInterface.sequelize,
                'Property',
                [
                    '"propertyName"',
                    'country',
                    'state',
                    'city',
                    '"streetAddress"',
                ],
                'postText')]);
    },
};
