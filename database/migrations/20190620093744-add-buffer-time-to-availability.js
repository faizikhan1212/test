'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.addColumn('Availability', 'bufferTime', {
                type: Sequelize.INTEGER,
                allowNull: true,
            }),
        ]);
    },

    down: (queryInterface) => {
        return Promise.all([
            queryInterface.removeColumn('Availability', 'bufferTime'),
        ]);
    },
};
