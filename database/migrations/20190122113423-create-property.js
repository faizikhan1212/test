'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Property', {
            id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
            },
            userId: {
                type: Sequelize.INTEGER,
                allowNull: false,
                onDelete: 'CASCADE',
                references: {
                    model: 'User',
                    key: 'id',
                    deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
                },
            },
            type: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            amenities: {
                type: Sequelize.ARRAY(Sequelize.STRING),
                allowNull: true,
            },
            utilities: {
                type: Sequelize.ARRAY(Sequelize.STRING),
                allowNull: true,
            },
            propertyName: {
                type: Sequelize.STRING,
                allowNull: true,
            },
            numberUnits: {
                type: Sequelize.INTEGER,
                allowNull: true,
            },
            leaseTerm: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            streetAddress: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            city: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            state: {
                type: Sequelize.STRING,
                allowNull: true,
            },
            country: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            zip: {
                type: Sequelize.STRING,
                allowNull: true,
            },
            parking: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            petFriendly: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            noSmoking: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            createdAt: Sequelize.DATE,
            updatedAt: Sequelize.DATE,
        });
    },
    down: (queryInterface) => {
        return queryInterface.dropTable('Property');
    },
};
