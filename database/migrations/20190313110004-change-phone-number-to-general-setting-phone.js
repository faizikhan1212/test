'use strict';
module.exports = {
    up: (queryInterface) => {
        return Promise.all([
            queryInterface.removeConstraint('GeneralSettingPhone', 'GeneralSettingPhone_phoneNumber_key'),
        ]);
    },
    down: (queryInterface) => {
        return Promise.all([
            queryInterface.addConstraint('GeneralSettingPhone', ['phoneNumber'],
                {
                    type: 'unique',
                    name: 'GeneralSettingPhone_phoneNumber_key',
                },
            ),
        ]);
    },
};
