'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.removeColumn('Company', 'userId'),
            queryInterface.changeColumn('Company', 'name',
                {
                    type: Sequelize.STRING,
                    unique: true,
                    allowNull: true,
                },
            ),
            queryInterface.changeColumn('Company', 'slug',
                {
                    type: Sequelize.STRING,
                    unique: true,
                    allowNull: true,
                },
            ),
            queryInterface.changeColumn('Company', 'address',
                {
                    type: Sequelize.STRING,
                    allowNull: true,
                },
            ),
            queryInterface.changeColumn('Company', 'email',
                {
                    type: Sequelize.STRING,
                    allowNull: true,
                },
            ),
            queryInterface.changeColumn('Company', 'phones',
                {
                    type: Sequelize.ARRAY(Sequelize.STRING),
                    allowNull: true,
                },
            ),
        ]);
    },

    down: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.addColumn('Company', 'userId',
                {
                    type: Sequelize.INTEGER,
                    allowNull: false,
                    onDelete: 'CASCADE',
                    references: {
                        model: 'User',
                        key: 'id',
                        deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
                    },
                },
            ),
        ]);
    },
};
