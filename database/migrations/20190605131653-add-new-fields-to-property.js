'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.addColumn('Property', 'petPolicy', {
                type: Sequelize.TEXT,
                allowNull: true,
            }),
            queryInterface.addColumn('Property', 'applicationFee', {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            }),
            queryInterface.addColumn('Property', 'appFeeNonRefundable', {
                type: Sequelize.BOOLEAN,
                allowNull: true,
            }),
            queryInterface.addColumn('Property', 'applicationFeeCost', {
                type: Sequelize.DECIMAL,
                allowNull: true,
            }),
            queryInterface.addColumn('Property', 'parkingCost', {
                type: Sequelize.DECIMAL,
                allowNull: true,
            }),
            queryInterface.addColumn('Property', 'parkingCovered', {
                type: Sequelize.BOOLEAN,
                allowNull: true,
            }),
            queryInterface.addColumn('Property', 'parkingSecured', {
                type: Sequelize.BOOLEAN,
                allowNull: true,
            }),
            queryInterface.addColumn('Property', 'sharedLaundry', {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            }),
            queryInterface.addColumn('Property', 'showingAgent', {
                type: Sequelize.INTEGER,
                allowNull: true,
                references: {
                    model: 'User',
                    key: 'id',
                    deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
                },
            }),
        ]);
    },
    down: (queryInterface) => {
        return Promise.all([
            queryInterface.removeColumn('Property', 'petPolicy'),
            queryInterface.removeColumn('Property', 'applicationFee'),
            queryInterface.removeColumn('Property', 'appFeeNonRefundable'),
            queryInterface.removeColumn('Property', 'applicationFeeCost'),
            queryInterface.removeColumn('Property', 'parkingCost'),
            queryInterface.removeColumn('Property', 'parkingCovered'),
            queryInterface.removeColumn('Property', 'parkingSecured'),
            queryInterface.removeColumn('Property', 'sharedLaundry'),
            queryInterface.removeColumn('Property', 'showingAgent'),
        ]);
    },
};
