'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.addColumn('User', 'companyId',
                {
                    type: Sequelize.INTEGER,
                    allowNull: true,
                    references: {
                        model: 'Company',
                        key: 'id',
                        deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
                    },
                },
            ),
            queryInterface.addColumn('User', 'role',
                {
                    type: Sequelize.STRING,
                    allowNull: false,
                    defaultValue: 'Owner',
                },
            ),
            queryInterface.addColumn('User', 'phone',
                {
                    type: Sequelize.STRING,
                    allowNull: true,
                },
            ),
            queryInterface.addColumn('User', 'ext',
                {
                    type: Sequelize.STRING,
                    allowNull: true,
                },
            ),
        ]);
    },

    down: (queryInterface) => {
        return Promise.all([
            queryInterface.removeColumn('User', 'companyId'),
            queryInterface.removeColumn('User', 'role'),
            queryInterface.removeColumn('User', 'phone'),
            queryInterface.removeColumn('User', 'ext'),
        ]);
    },
};
