'use strict';
const searchDown = require('../helpers/search/migrations/tsvector-down');
const searchUp = require('../helpers/search/migrations/tsvector-up');

module.exports = {
    up: (queryInterface) => {
        return searchDown(queryInterface.sequelize,
            'Lead',
            [
                'name',
                'email',
            ],
            'postText');
    },

    down: (queryInterface) => {
        return searchUp(queryInterface.sequelize,
            'Lead',
            [
                'name',
                'email',
            ],
            'postText');
    },
};
