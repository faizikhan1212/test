'use strict';

'use strict';

module.exports = {
    up: (queryInterface) => {

        return Promise.all([
            queryInterface.sequelize.query(
                `CREATE INDEX IF NOT EXISTS "userCompanyIdIndex" ON "User" ("companyId")`,
            ),
            queryInterface.sequelize.query(
                `CREATE INDEX IF NOT EXISTS "propertyUserIdIndex" ON "Property" ("userId")`,
            ),
            queryInterface.sequelize.query(
                `CREATE INDEX IF NOT EXISTS "unitUserIdIndex" ON "Unit" ("userId")`,
            ),
            queryInterface.sequelize.query(
                `CREATE INDEX IF NOT EXISTS "unitPropertyIdIndex" ON "Unit" ("propertyId")`,
            ),
            queryInterface.sequelize.query(
                `CREATE INDEX IF NOT EXISTS "unitStatusIndex" ON "Unit" ("status")`,
            ),
            queryInterface.sequelize.query(
                `CREATE INDEX IF NOT EXISTS "leadUserIdIndex" ON "Lead" ("userId")`,
            ),
            queryInterface.sequelize.query(
                `CREATE INDEX IF NOT EXISTS "leadUnitIdIndex" ON "Lead" ("unitId")`,
            ),
            queryInterface.sequelize.query(
                `CREATE INDEX IF NOT EXISTS "leadTypeIndex" ON "Lead" ("type")`,
            ),
            queryInterface.sequelize.query(
                `CREATE INDEX IF NOT EXISTS "fileUserIdIndex" ON "File" ("userId")`,
            ),
            queryInterface.sequelize.query(
                `CREATE INDEX IF NOT EXISTS "availabilityUserIdIndex" ON "Availability" ("userId")`,
            ),
            queryInterface.sequelize.query(
                `CREATE INDEX IF NOT EXISTS "cbfileLeadIdIndex" ON "CBFile" ("leadId")`,
            ),
            queryInterface.sequelize.query(
                `CREATE INDEX IF NOT EXISTS "customerUserIdIndex" ON "Customer" ("userId")`,
            ),
            queryInterface.sequelize.query(
                `CREATE INDEX IF NOT EXISTS "emailUserIdIndex" ON "Email" ("userId")`,
            ),
            queryInterface.sequelize.query(
                `CREATE INDEX IF NOT EXISTS "generalSettingEmailUserIdIndex" ON "GeneralSettingEmail" ("userId")`,
            ),
            queryInterface.sequelize.query(
                `CREATE INDEX IF NOT EXISTS "generalSettingPhoneUserIdIndex" ON "GeneralSettingPhone" ("userId")`,
            ),
            queryInterface.sequelize.query(
                `CREATE INDEX IF NOT EXISTS "maintenanceLeadIdIndex" ON "Maintenance" ("leadId")`,
            ),
            queryInterface.sequelize.query(
                `CREATE INDEX IF NOT EXISTS "maintenanceVendorIdIndex" ON "Maintenance" ("vendorId")`,
            ),
            queryInterface.sequelize.query(
                `CREATE INDEX IF NOT EXISTS "vendorUserIdIndex" ON "Vendor" ("userId")`,
            ),
            queryInterface.sequelize.query(
                `CREATE INDEX IF NOT EXISTS "phoneNumberUserIdIndex" ON "PhoneNumber" ("userId")`,
            ),
            queryInterface.sequelize.query(
                `CREATE INDEX IF NOT EXISTS "preQualificationUserIdIndex" ON "PreQualification" ("userId")`,
            ),
            queryInterface.sequelize.query(
                `CREATE INDEX IF NOT EXISTS "questionPreQualificationIdIndex" ON "Question" ("preQualificationId")`,
            ),
            queryInterface.sequelize.query(
                `CREATE INDEX IF NOT EXISTS "schedulingUserIdIndex" ON "Scheduling" ("userId")`,
            ),
            queryInterface.sequelize.query(
                `CREATE INDEX IF NOT EXISTS "schedulingUnitIdIndex" ON "Scheduling" ("unitId")`,
            ),
            queryInterface.sequelize.query(
                `CREATE INDEX IF NOT EXISTS "subscriptionUserIdIndex" ON "Subscription" ("userId")`,
            ),

        ]);

    },

    down: (queryInterface) => {
        return Promise.all([
            queryInterface.sequelize.query(
                `DROP INDEX IF EXISTS "userCompanyIdIndex" CASCADE`,
            ),
            queryInterface.sequelize.query(
                `DROP INDEX IF EXISTS "propertyUserIdIndex" CASCADE`,
            ),
            queryInterface.sequelize.query(
                `DROP INDEX IF EXISTS "unitUserIdIndex" CASCADE`,
            ),
            queryInterface.sequelize.query(
                `DROP INDEX IF EXISTS "unitPropertyIdIndex" CASCADE`,
            ),
            queryInterface.sequelize.query(
                `DROP INDEX IF EXISTS "leadUserIdIndex" CASCADE`,
            ),
            queryInterface.sequelize.query(
                `DROP INDEX IF EXISTS "leadUnitIdIndex" CASCADE`,
            ),
            queryInterface.sequelize.query(
                `DROP INDEX IF EXISTS "leadTypeIndex" CASCADE`,
            ),
            queryInterface.sequelize.query(
                `DROP INDEX IF EXISTS "unitStatusIndex" CASCADE`,
            ),
            queryInterface.sequelize.query(
                `DROP INDEX IF EXISTS "fileUserIdIndex" CASCADE`,
            ),
            queryInterface.sequelize.query(
                `DROP INDEX IF EXISTS "availabilityUserIdIndex" CASCADE`,
            ),
            queryInterface.sequelize.query(
                `DROP INDEX IF EXISTS "cbfileLeadIdIndex" CASCADE`,
            ),
            queryInterface.sequelize.query(
                `DROP INDEX IF EXISTS "customerUserIdIndex" CASCADE`,
            ),
            queryInterface.sequelize.query(
                `DROP INDEX IF EXISTS "emailUserIdIndex" CASCADE`,
            ),
            queryInterface.sequelize.query(
                `DROP INDEX IF EXISTS "generalSettingEmailUserIdIndex" CASCADE`,
            ),
            queryInterface.sequelize.query(
                `DROP INDEX IF EXISTS "generalSettingPhoneUserIdIndex" CASCADE`,
            ),
            queryInterface.sequelize.query(
                `DROP INDEX IF EXISTS "maintenanceLeadIdIndex" CASCADE`,
            ),
            queryInterface.sequelize.query(
                `DROP INDEX IF EXISTS "maintenanceVendorIdIndex" CASCADE`,
            ),
            queryInterface.sequelize.query(
                `DROP INDEX IF EXISTS "vendorUserIdIndex" CASCADE`,
            ),
            queryInterface.sequelize.query(
                `DROP INDEX IF EXISTS "phoneNumberUserIdIndex" CASCADE`,
            ),
            queryInterface.sequelize.query(
                `DROP INDEX IF EXISTS "preQualificationUserIdIndex" CASCADE`,
            ),
            queryInterface.sequelize.query(
                `DROP INDEX IF EXISTS "questionPreQualificationIdIndex" CASCADE`,
            ),
            queryInterface.sequelize.query(
                `DROP INDEX IF EXISTS "schedulingUserIdIndex" CASCADE`,
            ),
            queryInterface.sequelize.query(
                `DROP INDEX IF EXISTS "schedulingUnitIdIndex" CASCADE`,
            ),
            queryInterface.sequelize.query(
                `DROP INDEX IF EXISTS "subscriptionUserIdIndex" CASCADE`,
            ),
        ]);
    },
};
