'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.removeColumn('Unit', 'moveInDate')
                .then(() => {
                    queryInterface.addColumn('Unit', 'moveInDate',
                        {
                            type: Sequelize.DATE,
                            allowNull: false,
                            defaultValue: new Date(Date.UTC(2018, 0, 1)),
                        },
                    );
                }),

        ]);
    },

    down: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.removeColumn('Unit', 'moveInDate')
                .then(() => {
                    queryInterface.addColumn('Unit', 'moveInDate',
                        {
                            type: Sequelize.RANGE(Sequelize.DATE),
                            allowNull: false,
                            defaultValue: [new Date(Date.UTC(2018, 0, 1)), new Date(Date.UTC(2020, 1, 1))],
                        },
                    );
                }),
        ]);
    },
};
