'use strict';
const searchUp = require('../helpers/search/migrations/tsvector-up');
const searchDown = require('../helpers/search/migrations/tsvector-down');

module.exports = {
    up: (queryInterface) => {
        return searchUp(queryInterface.sequelize,
            'Lead',
            [
                'name',
                'email',
            ],
            'postText');
    },

    down: (queryInterface) => {
        return searchDown(queryInterface.sequelize,
            'Lead',
            [
                'name',
                'email',
            ],
            'postText');
    },
};
