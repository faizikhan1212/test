'use strict';

module.exports = {
    up: (queryInterface) => {
        return Promise.all([
            queryInterface.sequelize.query(
                `ALTER TABLE "Company"
                 DROP CONSTRAINT IF EXISTS name_unique_idx,
                 DROP CONSTRAINT IF EXISTS "Company_name_key"
                 `,
            ),
        ]);
    },

    down: () => {
        return Promise.resolve();
    },
};
