'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.addColumn('User', 'status', {
                type: Sequelize.STRING,
                allowNull: false,
                defaultValue: 'Active',
            }),
            queryInterface.sequelize.query(`ALTER TABLE "User" ALTER confirm TYPE boolean
                                            USING CASE confirm WHEN 'true' THEN TRUE ELSE FALSE END;`),
        ]);
    },

    down: (queryInterface) => {
        return Promise.all([
            queryInterface.removeColumn('User', 'status'),
            queryInterface.sequelize.query(`ALTER TABLE "User" ALTER confirm TYPE varchar(255)
                                            USING CASE confirm WHEN TRUE THEN 'true' ELSE 'false' END;`),
        ]);
    },
};
