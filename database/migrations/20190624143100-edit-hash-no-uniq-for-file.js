'use strict';

module.exports = {
    up: (queryInterface) => {
        return Promise.all([
            queryInterface.sequelize.query(
                `ALTER TABLE "File"
                 DROP CONSTRAINT IF EXISTS "File_hash_key"
                 `,
            ),
        ]);
    },

    down: () => {
        return Promise.resolve();
    },
};
