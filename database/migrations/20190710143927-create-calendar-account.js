'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('CalendarAccounts', {
            id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
            },
            userId: {
                type: Sequelize.INTEGER,
                allowNull: false,
                onDelete: 'CASCADE',
                references: {
                    model: 'User',
                    key: 'id',
                    deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
                },
            },
            isDeleted: {
                type: Sequelize.BOOLEAN,
            },
            type: {
                type: Sequelize.STRING,
            },
            email: {
                type: Sequelize.STRING,
            },
            accessToken: {
                type: Sequelize.TEXT,
            },
            refreshToken: {
                type: Sequelize.TEXT,
            },
            idToken: {
                type: Sequelize.TEXT,
            },
            tokenType: {
                type: Sequelize.STRING,
            },
            scope: {
                type: Sequelize.STRING,
            },
            expiryDate: {
                type: Sequelize.DOUBLE,
            },
            isTwoWaySync: {
                type: Sequelize.BOOLEAN,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('CalendarAccount');
    },
};
