'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.removeColumn('Property', 'sharedLaundry'),
            queryInterface.removeColumn('Property', 'showingAgent'),
        ]);
    },

    down: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.addColumn('Property', 'sharedLaundry', {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            }),
            queryInterface.addColumn('Property', 'showingAgent', {
                type: Sequelize.INTEGER,
                allowNull: true,
                references: {
                    model: 'User',
                    key: 'id',
                    deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
                },
            }),
        ]);
    },
};
