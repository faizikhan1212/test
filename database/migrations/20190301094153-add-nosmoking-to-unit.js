'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.addColumn('Unit', 'noSmoking',
            {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
        );
    },
    down: queryInterface => {
        return queryInterface.removeColumn('Unit', 'noSmoking');
    },
};
