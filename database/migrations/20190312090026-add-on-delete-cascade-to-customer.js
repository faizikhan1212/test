'use strict';
module.exports = {
    up: (queryInterface) => {
        return queryInterface
            .removeConstraint('Customer', 'Customer_userId_fkey')
            .then(() => {
                return queryInterface.addConstraint('Customer', ['userId'], {
                    type: 'foreign key',
                    name: 'Customer_userId_fkey',
                    references: {
                        table: 'User',
                        field: 'id',
                    },
                    onDelete: 'CASCADE',
                });
            });
    },

    down: (queryInterface) => {
        return queryInterface
            .removeConstraint('Customer', 'Customer_userId_fkey')
            .then(() => {
                return queryInterface.addConstraint('Customer', ['userId'], {
                    type: 'foreign key',
                    name: 'Customer_userId_fkey',
                    references: {
                        table: 'User',
                        field: 'id',
                    },
                });
            });
    },
};
