'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.addColumn('Unit', 'moveInDate', {
                type: Sequelize.RANGE(Sequelize.DATE),
                allowNull: false,
                defaultValue: [new Date(Date.UTC(2018, 0, 1)), new Date(Date.UTC(2020, 1, 1))],
            }),
            queryInterface.addColumn('Unit', 'utilityBill', {
                type: Sequelize.DECIMAL,
                allowNull: true,
            }),
            queryInterface.addColumn('Unit', 'applicationProcess', {
                type: Sequelize.TEXT,
                allowNull: true,
            }),
            queryInterface.addColumn('Unit', 'appliances', {
                type: Sequelize.ARRAY(Sequelize.STRING),
                allowNull: true,
            }),
        ]);
    },

    down: (queryInterface) => {
        return Promise.all([
            queryInterface.removeColumn('Unit', 'moveInDate'),
            queryInterface.removeColumn('Unit', 'utilityBill'),
            queryInterface.removeColumn('Unit', 'applicationProcess'),
            queryInterface.removeColumn('Unit', 'appliances'),
        ]);
    },
};
