'use strict';
 module.exports = {
    up: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.addColumn('Property', 'url', {
                type: Sequelize.STRING,
                allowNull: true,
                unique: true
            }),
        ]);
    },

    down: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.removeColumn('Property', 'url'),
        ]);
    },
};

