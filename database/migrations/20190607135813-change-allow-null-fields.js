'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.changeColumn('Property', 'propertyName',
                {
                    type: Sequelize.STRING,
                    allowNull: false,
                },
            ),
            queryInterface.changeColumn('Unit', 'unit',
                {
                    type: Sequelize.STRING,
                    allowNull: false,
                },
            ),
            queryInterface.changeColumn('Unit', 'bed',
                {
                    type: Sequelize.INTEGER,
                    allowNull: false,
                },
            ),
            queryInterface.changeColumn('Unit', 'bath',
                {
                    type: Sequelize.INTEGER,
                    allowNull: false,
                },
            ),
            queryInterface.changeColumn('Unit', 'title',
                {
                    type: Sequelize.STRING,
                    allowNull: true,
                },
            ),
            queryInterface.changeColumn('Unit', 'description',
                {
                    type: Sequelize.TEXT,
                    allowNull: true,
                },
            ),
            queryInterface.changeColumn('Unit', 'email',
                {
                    type: Sequelize.STRING,
                    allowNull: true,
                },
            ),
            queryInterface.changeColumn('Unit', 'phone',
                {
                    type: Sequelize.STRING,
                    allowNull: true,
                },
            ),
            queryInterface.changeColumn('Unit', 'leaseTerm',
                {
                    type: Sequelize.STRING,
                    allowNull: true,
                },
            ),
        ]);
    },

    down: (queryInterface, Sequelize) => {
        return Promise.all([]);
    },
};
