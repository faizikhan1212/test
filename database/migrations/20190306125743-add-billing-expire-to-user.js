'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.addColumn('User', 'billingExpire',
                {
                    type: Sequelize.DATE,
                    allowNull: true,
                },
            ),
        ]);
    },

    down: (queryInterface) => {
        return Promise.all([
            queryInterface.removeColumn('User', 'billingExpire'),
        ]);
    },
};
