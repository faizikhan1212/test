'use strict';
const searchUp = require('../helpers/search/migrations/tsvector-up');
const searchDown = require('../helpers/search/migrations/tsvector-down');

module.exports = {
    up: (queryInterface) => {
        return searchUp(queryInterface.sequelize,
            'Property',
            [
                '"propertyName"',
                'country',
                'state',
                'city',
                '"streetAddress"',
            ],
            'postText');
    },

    down: (queryInterface) => {
        return searchDown(queryInterface.sequelize,
            'Property',
            [
                '"propertyName"',
                'country',
                'state',
                'city',
                '"streetAddress"',
            ],
            'postText');
    },
};
