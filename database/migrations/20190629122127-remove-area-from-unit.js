'use strict';

module.exports = {
    up: (queryInterface) => {
        return Promise.all([
            queryInterface.removeColumn('Unit', 'area'),
        ]);
    },

    down: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.addColumn('Unit', 'area', {
                type: Sequelize.INTEGER,
                allowNull: true,
            }),
        ]);
    },
};
