'use strict';

module.exports = {
    up: (queryInterface) => {
        return Promise.all([
            queryInterface.sequelize.query(
                `CREATE INDEX "unitAvailabilityUnitIdIndex" ON "UnitAvailability" ("unitId")`,
            ),
            queryInterface.sequelize.query(
                `CREATE INDEX "unitAvailabilityUserIdIndex" ON "UnitAvailability" ("userId")`,
            ),
            queryInterface.sequelize.query(
                `CREATE INDEX "unitPreQualificationUnitIdIndex" ON "UnitPreQualification" ("unitId")`,
            ),
            queryInterface.sequelize.query(
                `CREATE INDEX "unitPreQualificationUserIdIndex" ON "UnitPreQualification" ("userId")`,
            ),
            queryInterface.sequelize.query(
                `CREATE INDEX "unitQuestionUnitPreQualificationIndex" ON "UnitQuestion" ("unitPreQualificationId")`,
            ),
        ]);
    },

    down: (queryInterface) => {
        return Promise.all([
            queryInterface.sequelize.query(
                `DROP INDEX "unitAvailabilityUnitIdIndex" CASCADE`,
            ),
            queryInterface.sequelize.query(
                `DROP INDEX "unitAvailabilityUserIdIndex" CASCADE`,
            ),
            queryInterface.sequelize.query(
                `DROP INDEX "unitPreQualificationUnitIdIndex" CASCADE`,
            ),
            queryInterface.sequelize.query(
                `DROP INDEX "unitPreQualificationUserIdIndex" CASCADE`,
            ),
            queryInterface.sequelize.query(
                `DROP INDEX "unitQuestionUnitPreQualificationIndex" CASCADE`,
            ),
        ]);
    },
};
