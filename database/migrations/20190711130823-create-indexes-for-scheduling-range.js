'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.sequelize.query(
                `CREATE INDEX IF NOT EXISTS  "dateOfShowingIndex" ON "Scheduling" USING GiST ("dateOfShowing") `,
            ),

        ]);
    },

    down: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.sequelize.query(
                `DROP INDEX IF EXISTS "dateOfShowingIndex" CASCADE`,
            ),

        ]);
    },
};
