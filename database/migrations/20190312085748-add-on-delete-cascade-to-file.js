'use strict';
module.exports = {
    up: (queryInterface) => {
        return queryInterface
            .removeConstraint('File', 'File_userId_fkey')
            .then(() => {
                return queryInterface.addConstraint('File', ['userId'], {
                    type: 'foreign key',
                    name: 'File_userId_fkey',
                    references: {
                        table: 'User',
                        field: 'id',
                    },
                    onDelete: 'CASCADE',
                });
            });
    },

    down: (queryInterface) => {
        return queryInterface
            .removeConstraint('File', 'File_userId_fkey')
            .then(() => {
                return queryInterface.addConstraint('File', ['userId'], {
                    type: 'foreign key',
                    name: 'File_userId_fkey',
                    references: {
                        table: 'User',
                        field: 'id',
                    },
                });
            });
    },
};
