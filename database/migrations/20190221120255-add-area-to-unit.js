'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.addColumn('Unit', 'area',
                {
                    type: Sequelize.DECIMAL,
                    allowNull: true,
                },
            ),
        ]);
    },

    down: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.removeColumn('Unit', 'area'),
        ]);
    },
};
