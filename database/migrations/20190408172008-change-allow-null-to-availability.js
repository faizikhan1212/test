'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.changeColumn('Availability', 'limitProspects', {
                type: Sequelize.INTEGER,
                allowNull: true,
            }),
            queryInterface.changeColumn('Availability', 'showingSlot', {
                type: Sequelize.INTEGER,
                allowNull: true,
            }),
            queryInterface.changeColumn('Availability', 'maxShowings', {
                type: Sequelize.INTEGER,
                allowNull: true,
            }),
        ]);
    },

    down: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.changeColumn('Availability', 'limitProspects', {
                type: Sequelize.INTEGER,
                allowNull: false,
            }),
            queryInterface.changeColumn('Availability', 'showingSlot', {
                type: Sequelize.INTEGER,
                allowNull: false,
            }),
            queryInterface.changeColumn('Availability', 'maxShowings', {
                type: Sequelize.INTEGER,
                allowNull: false,
            }),
        ]);
    },
};
