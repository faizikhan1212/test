'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.addColumn('User', 'subscription',
                {
                    type: Sequelize.BOOLEAN,
                    allowNull: true,
                    defaultValue: false,
                },
            ),
        ]);
    },

    down: (queryInterface) => {
        return Promise.all([
            queryInterface.removeColumn('User', 'subscription'),
        ]);
    },
};
