'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Unit', {
            id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
            },
            userId: {
                type: Sequelize.INTEGER,
                allowNull: false,
                onDelete: 'CASCADE',
                references: {
                    model: 'User',
                    key: 'id',
                    deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
                },
            },
            propertyId: {
                type: Sequelize.INTEGER,
                allowNull: false,
                onDelete: 'CASCADE',
                references: {
                    model: 'Property',
                    key: 'id',
                    deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
                },
            },
            rent: {
                type: Sequelize.DECIMAL,
                allowNull: false,
            },
            deposit: {
                type: Sequelize.DECIMAL,
                allowNull: true,
            },
            status: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            title: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            description: {
                type: Sequelize.TEXT,
                allowNull: false,
            },
            parking: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            petFriendly: {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            },
            bed: {
                type: Sequelize.INTEGER,
                allowNull: true,
            },
            bath: {
                type: Sequelize.INTEGER,
                allowNull: true,
            },
            amenities: {
                type: Sequelize.ARRAY(Sequelize.STRING),
                allowNull: true,
            },
            utilities: {
                type: Sequelize.ARRAY(Sequelize.STRING),
                allowNull: true,
            },
            leaseTerm: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            email: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            phone: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            createdAt: {
                type: Sequelize.DATE,
                allowNull: false,
            },
            updatedAt: {
                type: Sequelize.DATE,
                allowNull: false,
            },
        });
    },
    down: (queryInterface) => {
        return queryInterface.dropTable('Unit');
    },
};
