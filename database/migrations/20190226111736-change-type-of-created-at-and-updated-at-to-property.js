'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.changeColumn('Property', 'createdAt',
                {
                    type: Sequelize.DATE,
                    allowNull: false,
                },
            ),
            queryInterface.changeColumn('Property', 'updatedAt',
                {
                    type: Sequelize.DATE,
                    allowNull: false,
                },
            ),
        ]);
    },

    down: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.changeColumn('Property', 'createdAt',
                {
                    type: Sequelize.DATE,
                },
            ),
            queryInterface.changeColumn('Property', 'updatedAt',
                {
                    type: Sequelize.DATE,
                },
            ),
        ]);
    },
};
