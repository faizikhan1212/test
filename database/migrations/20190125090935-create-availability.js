'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Availability', {
            id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
            },
            userId: {
                type: Sequelize.INTEGER,
                allowNull: false,
                onDelete: 'CASCADE',
                references: {
                    model: 'User',
                    key: 'id',
                    deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
                },
            },
            sundayFrom: {
                type: Sequelize.TIME,
                allowNull: true,
            },
            sundayTo: {
                type: Sequelize.TIME,
                allowNull: true,
            },
            mondayFrom: {
                type: Sequelize.TIME,
                allowNull: true,
            },
            mondayTo: {
                type: Sequelize.TIME,
                allowNull: true,
            },
            tuesdayFrom: {
                type: Sequelize.TIME,
                allowNull: true,
            },
            tuesdayTo: {
                type: Sequelize.TIME,
                allowNull: true,
            },
            wednesdayFrom: {
                type: Sequelize.TIME,
                allowNull: true,
            },
            wednesdayTo: {
                type: Sequelize.TIME,
                allowNull: true,
            },
            thursdayFrom: {
                type: Sequelize.TIME,
                allowNull: true,
            },
            thursdayTo: {
                type: Sequelize.TIME,
                allowNull: true,
            },
            fridayFrom: {
                type: Sequelize.TIME,
                allowNull: true,
            },
            fridayTo: {
                type: Sequelize.TIME,
                allowNull: true,
            },
            saturdayFrom: {
                type: Sequelize.TIME,
                allowNull: true,
            },
            saturdayTo: {
                type: Sequelize.TIME,
                allowNull: true,
            },
            limitProspects: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            showingSlot: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            otherProperties: {
                type: Sequelize.BOOLEAN,
                allowNull: true,
            },
            stopAppointments: {
                type: Sequelize.INTEGER,
                allowNull: true,
            },
            maxShowings: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            createdAt: {
                type: Sequelize.DATE,
                allowNull: false,
            },
            updatedAt: {
                type: Sequelize.DATE,
                allowNull: false,
            },
        });
    },
    down: (queryInterface) => {
        return queryInterface.dropTable('Availability');
    },
};
