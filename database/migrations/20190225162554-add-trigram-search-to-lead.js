'use strict';
const searchUp = require('../helpers/search/migrations/trigram-up');
const searchDown = require('../helpers/search/migrations/trigram-down');

module.exports = {
    up: (queryInterface) => {
        return searchUp(queryInterface.sequelize,
            'Lead',
            [
                'name',
                'email',
            ],
        );
    },

    down: (queryInterface) => {
        return searchDown(queryInterface.sequelize,
            'Lead',
        );
    },

};
