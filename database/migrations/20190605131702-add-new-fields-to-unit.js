'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.addColumn('Unit', 'petPolicy', {
                type: Sequelize.TEXT,
                allowNull: true,
            }),
            queryInterface.addColumn('Unit', 'applicationFee', {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            }),
            queryInterface.addColumn('Unit', 'appFeeNonRefundable', {
                type: Sequelize.BOOLEAN,
                allowNull: true,
            }),
            queryInterface.addColumn('Unit', 'applicationFeeCost', {
                type: Sequelize.DECIMAL,
                allowNull: true,
            }),
            queryInterface.addColumn('Unit', 'parkingCost', {
                type: Sequelize.DECIMAL,
                allowNull: true,
            }),
            queryInterface.addColumn('Unit', 'parkingCovered', {
                type: Sequelize.BOOLEAN,
                allowNull: true,
            }),
            queryInterface.addColumn('Unit', 'parkingSecured', {
                type: Sequelize.BOOLEAN,
                allowNull: true,
            }),
            queryInterface.addColumn('Unit', 'sharedLaundry', {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            }),
            queryInterface.addColumn('Unit', 'sharedWashroomBath', {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            }),
            queryInterface.addColumn('Unit', 'unitShared', {
                type: Sequelize.BOOLEAN,
                allowNull: false,
                defaultValue: false,
            }),
            queryInterface.addColumn('Unit', 'unitSharedPeople', {
                type: Sequelize.INTEGER,
                allowNull: true,
            }),
            queryInterface.addColumn('Unit', 'sqFt', {
                type: Sequelize.INTEGER,
                allowNull: true,
            }),

        ]);
    },
    down: (queryInterface) => {
        return Promise.all([
            queryInterface.removeColumn('Unit', 'petPolicy'),
            queryInterface.removeColumn('Unit', 'applicationFee'),
            queryInterface.removeColumn('Unit', 'appFeeNonRefundable'),
            queryInterface.removeColumn('Unit', 'applicationFeeCost'),
            queryInterface.removeColumn('Unit', 'parkingCost'),
            queryInterface.removeColumn('Unit', 'parkingCovered'),
            queryInterface.removeColumn('Unit', 'parkingSecured'),
            queryInterface.removeColumn('Unit', 'sharedLaundry'),
            queryInterface.removeColumn('Unit', 'sharedWashroomBath'),
            queryInterface.removeColumn('Unit', 'unitShared'),
            queryInterface.removeColumn('Unit', 'unitSharedPeople'),
            queryInterface.removeColumn('Unit', 'sqFt'),
            // queryInterface.removeColumn('Unit', 'showingAgent'),

        ]);
    },
};
