'use strict';

module.exports = {
    up: (queryInterface) => {

        return Promise.all([
            queryInterface.sequelize.query(
                `CREATE INDEX IF NOT EXISTS "fileEntityNameIndex" ON "File" ("entityName")`,
            ),
            queryInterface.sequelize.query(
                `CREATE INDEX IF NOT EXISTS "fileEntityIdIndex" ON "File" ("entityId")`,
            ),

        ]);

    },

    down: (queryInterface) => {
        return Promise.all([
            queryInterface.sequelize.query(
                `DROP INDEX IF EXISTS "fileEntityNameIndex" CASCADE`,
            ),
            queryInterface.sequelize.query(
                `DROP INDEX IF EXISTS "fileEntityIdIndex" CASCADE`,
            ),
        ]);
    },
};
