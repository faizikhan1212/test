'use strict';

module.exports = {
    up: (queryInterface) => {
        return Promise.all([
            queryInterface.sequelize
                .query(`ALTER TABLE "Property"
                      DROP COLUMN IF EXISTS city CASCADE,
                      DROP COLUMN IF EXISTS state CASCADE,
                      DROP COLUMN IF EXISTS country CASCADE,
                      DROP COLUMN IF EXISTS zip CASCADE;`),
            queryInterface.renameColumn('Property', 'streetAddress', 'address')
                .then(() =>
                    queryInterface.sequelize
                        .query(`DROP INDEX IF EXISTS np_search_property_idx CASCADE;`)
                        .then(() =>
                            queryInterface.sequelize
                                .query(`CREATE INDEX CONCURRENTLY np_search_property_idx ON "Property" 
                                USING gin(address gin_trgm_ops);`)),
                ),
        ]);
    },

    down: (queryInterface) => {
        return Promise.all([
            queryInterface.renameColumn('Property', 'address', 'streetAddress')
                .then(() =>
                    queryInterface.sequelize
                        .query(`DROP INDEX IF EXISTS np_search_property_idx CASCADE;`)
                        .then(() =>
                            queryInterface.sequelize
                                .query(`CREATE INDEX CONCURRENTLY np_search_property_idx ON "Property" 
                                        USING gin("streetAddress" gin_trgm_ops);`)),
                ),
        ]);
    },
};
