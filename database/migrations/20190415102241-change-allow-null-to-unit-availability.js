'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.changeColumn('UnitAvailability', 'limitProspects', {
                type: Sequelize.INTEGER,
                allowNull: true,
            }),
            queryInterface.changeColumn('UnitAvailability', 'showingSlot', {
                type: Sequelize.INTEGER,
                allowNull: true,
            }),
            queryInterface.changeColumn('UnitAvailability', 'maxShowings', {
                type: Sequelize.INTEGER,
                allowNull: true,
            }),
        ]);
    },

    down: (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.changeColumn('UnitAvailability', 'limitProspects', {
                type: Sequelize.INTEGER,
                allowNull: false,
            }),
            queryInterface.changeColumn('UnitAvailability', 'showingSlot', {
                type: Sequelize.INTEGER,
                allowNull: false,
            }),
            queryInterface.changeColumn('UnitAvailability', 'maxShowings', {
                type: Sequelize.INTEGER,
                allowNull: false,
            }),
        ]);
    },
};
