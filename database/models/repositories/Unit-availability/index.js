module.exports = (UnitAvailability, sequelize) => {

    UnitAvailability.extendedShow = (unitId, companyId) => {
        return sequelize
            .query(
                `SELECT * FROM "UnitAvailability"
                WHERE "unitId" = ${unitId}
                AND "userId" IN (
                    select id from "User" where "companyId" = ${companyId}
                )`,
            );
    };

    UnitAvailability.extendedUpdate = async (companyId, body) => {
        const res = await sequelize
            .query(
                `UPDATE "UnitAvailability"
                 SET "sundayFrom" = ${body.sundayFrom === undefined ? null : `'${body.sundayFrom}'`},
                     "sundayTo" =  ${body.sundayTo === undefined ? null : `'${body.sundayTo}'`},
                     "mondayFrom" = ${body.mondayTo === undefined ? null : `'${body.mondayTo}'`},
                     "mondayTo" = ${body.mondayFrom === undefined ? null : `'${body.mondayFrom}'`},
                     "tuesdayFrom" = ${body.tuesdayFrom === undefined ? null : `'${body.tuesdayFrom}'`},
                     "tuesdayTo" = ${body.tuesdayTo === undefined ? null : `'${body.tuesdayTo}'`},
                     "wednesdayFrom" = ${body.wednesdayFrom === undefined ? null : `'${body.wednesdayFrom}'`},
                     "wednesdayTo" = ${body.wednesdayTo === undefined ? null : `'${body.wednesdayTo}'`},
                     "thursdayFrom" = ${body.thursdayFrom === undefined ? null : `'${body.thursdayFrom}'`},
                     "thursdayTo" = ${body.thursdayTo === undefined ? null : `'${body.thursdayTo}'`},
                     "fridayFrom" =  ${body.fridayFrom === undefined ? null : `'${body.fridayFrom}'`},
                     "fridayTo" = ${body.fridayTo === undefined ? null : `'${body.fridayTo}'`},
                     "saturdayFrom" = ${body.saturdayFrom === undefined ? null : `'${body.saturdayFrom}'`},
                     "saturdayTo" = ${body.saturdayTo === undefined ? null : `'${body.saturdayTo}'`},
                     "limitProspects" = ${body.limitProspects},
                     "showingSlot" = ${body.showingSlot},
                     ${body.otherProperties !== undefined ? `"otherProperties" = ${body.otherProperties},` : ''}
                     ${body.stopAppointments !== undefined ? `"stopAppointments" = ${body.stopAppointments},` : ''}
                     "maxShowings" = ${body.maxShowings}
                WHERE 
                id = ${body.id} 
                AND "userId" in (
                  select id from "User" where "companyId" = ${companyId}
                )`,
            );

        return res[1].rowCount > 0;
    };

    UnitAvailability.extendedCreate = async (user, body) => {
        const res = await sequelize
            .query(
                `INSERT INTO "UnitAvailability" 
                    ("userId", "unitId",
                    "sundayFrom", "sundayTo", "mondayFrom", "mondayTo", "tuesdayFrom", "tuesdayTo", "wednesdayFrom", "wednesdayTo",
                    "thursdayFrom", "thursdayTo", "fridayFrom", "fridayTo", "saturdayFrom", "saturdayTo", "limitProspects",
                    "showingSlot", 
                    ${body.otherProperties !== undefined && body.otherProperties !== null ? `"otherProperties",` : ''} 
                    ${body.stopAppointments !== undefined && body.stopAppointments !== null ? `"stopAppointments",` : ''}
                    "maxShowings", "createdAt", "updatedAt")
                SELECT 
                    ${user.id}, ${body.unitId},
                    ${body.sundayFrom === undefined ? null : `'${body.sundayFrom}'`}, 
                    ${body.sundayTo === undefined ? null : `'${body.sundayTo}'`}, 
                    ${body.mondayTo === undefined ? null : `'${body.mondayTo}'`}, 
                     ${body.mondayFrom === undefined ? null : `'${body.mondayFrom}'`},
                    ${body.tuesdayFrom === undefined ? null : `'${body.tuesdayFrom}'`}, 
                    ${body.tuesdayTo === undefined ? null : `'${body.tuesdayTo}'`},
                    ${body.wednesdayFrom === undefined ? null : `'${body.wednesdayFrom}'`}, 
                    ${body.wednesdayTo === undefined ? null : `'${body.wednesdayTo}'`},
                    ${body.thursdayFrom === undefined ? null : `'${body.thursdayFrom}'`}, 
                    ${body.thursdayTo === undefined ? null : `'${body.thursdayTo}'`},
                    ${body.fridayFrom === undefined ? null : `'${body.fridayFrom}'`}, 
                    ${body.fridayTo === undefined ? null : `'${body.fridayTo}'`},
                    ${body.saturdayFrom === undefined ? null : `'${body.saturdayFrom}'`}, 
                    ${body.saturdayTo === undefined ? null : `'${body.saturdayTo}'`}, 
                     ${body.limitProspects},
                    ${body.showingSlot}, 
                    ${body.otherProperties !== undefined && body.otherProperties !== null ? `${body.otherProperties},` : ''} 
                    ${body.stopAppointments !== undefined && body.stopAppointments !== null ? `${body.stopAppointments},` : ''}
                    ${body.maxShowings}, current_timestamp, current_timestamp
                WHERE ${body.unitId} IN (
                    select id from "Unit" where "userId" in (
                           select id from "User" where "companyId" = ${user.companyId}
                    )
                ) AND NOT EXISTS (
                    select id from "UnitAvailability" where "unitId" = ${body.unitId}
                );`,
            );

        return res[1] > 0;
    };


    return UnitAvailability;
};
