module.exports = (Vendor, sequelize) => {

    Vendor.extendedShow = (vendorId, companyId) => {
        return sequelize.query(
            `SELECT * FROM "Vendor"
          WHERE id = ${vendorId}
          AND "userId" IN (
            select id from "User" where "companyId" = ${companyId}
          )`);
    };


    Vendor.getList = async (companyId, order, limit, offset) => {
        return await sequelize.query(`
        SELECT * FROM "Vendor"
        WHERE "userId" IN (
          select id from "User" where  "companyId" = ${companyId}
        ) 
        ORDER BY id ${order} LIMIT ${limit} OFFSET ${offset}
        `);
    };

    Vendor.getCountList = async (companyId) => {
        const res = await sequelize
            .query(`SELECT COUNT(*) FROM "Vendor"
                    WHERE "userId" IN (
                        select id from "User" where  "companyId" = ${companyId})`);

        return parseInt(res[0][0].count);
    };

    return Vendor;
};
