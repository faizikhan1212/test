const filterToSql = require('../../../helpers/migrations/filter');

module.exports = (Property, sequelize) => {


    Property.extendedShow = async (companyId, propertyId) => {
        return await sequelize.query(
            `SELECT * FROM "Property"
             WHERE id = ${propertyId} AND "userId" in (
                select id from "User" where "companyId" = ${companyId}
             )`);
    };

    Property.countProperties = async (userId) => {
        return await sequelize.query(`
        SELECT COUNT(*) FROM "Property" WHERE  "Property"."userId" = ${userId}
        `);
    };

    Property.getExtendedPropertyList = ([companyId, filter, limit = 'ALL', offset = 0, order = 'ASC']) => {
        return sequelize
            .query(`SELECT Prop.*, coalesce(un.countAllUnits, 0) AS "countAllUnits", coalesce(un.countVacantUnits, 0) AS "countVacantUnits"
                    FROM "Property" AS Prop
                    JOIN LATERAL
                    (
                     SELECT COUNT(*) AS countAllUnits,
                     COUNT(*) filter(where "Unit".status = 'Vacant') as countVacantUnits
                     FROM "Unit"
                     WHERE Prop."id" = "Unit"."propertyId"
                    ) AS un ON true
                    WHERE Prop."userId" IN (
                          SELECT id FROM "User" WHERE  "companyId" = ${companyId}
                    )
                    ${filterToSql(filter, 'Prop')}
                    ORDER BY Prop."createdAt" ${order} LIMIT ${limit} OFFSET ${offset}`);
    };

    Property.countListResult = async ([companyId, filter]) => {
        const res = await sequelize
            .query(
                `SELECT COUNT(*) FROM "Property" as Prop
                   WHERE  Prop."userId" IN (
                          SELECT id FROM "User" WHERE  "companyId" = ${companyId}
                    )
                    ${filterToSql(filter, 'Prop')}
                  `);

        return parseInt(res[0][0].count);
    };

    Property.search = (query, [companyId, filter, limit = 'ALL', offset = 0, order = 'ASC']) =>
        sequelize
            .query(`SELECT Prop.*, coalesce(un.countAllUnits, 0) AS "countAllUnits", coalesce(un.countVacantUnits, 0) AS "countVacantUnits"
                    FROM "Property" AS Prop
                    JOIN LATERAL
                    (
                     SELECT COUNT(*) AS countAllUnits,
                     COUNT(*) filter(where "Unit".status = 'Vacant') as countVacantUnits
                     FROM "Unit"
                     WHERE Prop."id" = "Unit"."propertyId"
                    ) AS un ON true
                    WHERE Prop."userId" IN (
                          SELECT id FROM "User" WHERE  "companyId" = ${companyId}
                    )
                    ${filterToSql(filter, 'Prop')}
                    AND Prop.address ILIKE '%${query}%' 
                    ORDER BY id ${order} LIMIT ${limit} OFFSET ${offset}`);

    Property.countSearchResult = async (query, [companyId, filter]) => {
        const res = await sequelize
            .query(`SELECT COUNT(*) FROM "Property" as Prop
                     WHERE  Prop."userId" IN (
                          SELECT id FROM "User" WHERE  "companyId" = ${companyId}
                    )
                    ${filterToSql(filter, 'Prop')}
                    AND address ILIKE '%${query}%';`);

        return parseInt(res[0][0].count);
    };

    Property.getFullExport = async (companyId) => {
        const res = await sequelize
            .query(`SELECT                  
                        Prop.type as "Property Type *",
                        Prop."propertyName" as "Property name *",
                        Prop."address" as "Property Address *",
                        Prop."leaseTerm" as "Lease Term *",
                        case when Prop."applicationFee" = TRUE then 'Yes' else 'No' end as "Application Fee *",  
                        Prop."applicationFeeCost" as "Application Fee Cost (optional)",
                        case when Prop."appFeeNonRefundable" = TRUE then 'Yes' else 'No' end as "Application Fee Non-refundable (optional)",
                        case when Prop."parking" = TRUE then 'Yes' else 'No' end as "Parking *",
                        Prop."parkingCost" as "Parking Monthly fee (optional)",
                        case when Prop."parkingCovered" = TRUE then 'Yes' else 'No' end as "Parking Covered (optional)",
                        case when Prop."parkingSecured" = TRUE then 'Yes' else 'No' end as "Parking Secured (optional)",
                        case when Prop."petFriendly" = TRUE then 'Yes' else 'No' end as "Property Pet friendly *",
                        Prop."petPolicy" as "Pet Policy (optional)",
                        case when Prop."noSmoking" = TRUE then 'Yes' else 'No' end as "Smoking Allowed *",
                        array_to_string(Prop."amenities", ';', '*') as "Amenities (optional)", 
                        array_to_string(Prop."utilities", ';', '*') as "Utilities (optional)",
                        Un.unit as "Unit Number *",
                        Un.rent as "Unit Rent *",
                        Un.deposit as "Unit Deposit (optional)",
                        Un.bed as "Beds *",
                        Un.bath as "Bath *",
                        Un."sqFt" as "Unit SqFt (optional)",
                        Un.status  as "Unit status *",
                        case when Un."unitShared" = TRUE then 'Yes' else 'No' end as "Unit Shared *",
                        Un."unitSharedPeople" as "Number of people sharing this unit (optional)",
                        case when Un."sharedLaundry" = TRUE then 'Yes' else 'No' end as "Laundry Shared *",
                        case when Un."sharedWashroomBath" = TRUE then 'Yes' else 'No' end as "Bath Shared *",
                        Un.title as "Unit Listing Title (optional)",
                        Un.description as "Unit Description (optional)",
                        Un.email as "Listing Email (optional)",
                        Un.phone as "Listing Phone (optional)"
                    FROM "Property" AS Prop
                    LEFT JOIN LATERAL (
                        select 
                            *
                        from "Unit"
                        where "propertyId" = Prop.id
                    ) As Un ON true
                    WHERE Prop."userId" in (
                      select id from "User" where "companyId" = ${companyId}
                    )`);

        return res[0];

    };

    return Property;
};
