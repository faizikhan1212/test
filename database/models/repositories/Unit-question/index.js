module.exports = (UnitQuestion, sequelize) => {

    UnitQuestion.getList = (companyId, [unitId, limit, offset, order = 'DESC']) => {
        return sequelize
            .query(
                `SELECT * FROM "UnitQuestion"
                 WHERE "unitPreQualificationId" IN (
                      select id from "UnitPreQualification" where "unitId" = ${unitId} AND "userId" in (
                          select id from "User" where "companyId" = ${companyId}
                      )
                ) 
                ORDER BY "createdAt" ${order} LIMIT ${limit} OFFSET ${offset}`,
            );
    };

    UnitQuestion.getCountList = async (companyId, unitId) => {
        const res = await sequelize
            .query(
                `SELECT COUNT(*) FROM "UnitQuestion"
                 WHERE "unitPreQualificationId" IN (
                        select id from "UnitPreQualification" where "unitId" = ${unitId} AND "userId" in (
                            select id from "User" where "companyId" = ${companyId}
                        )
                 )`,
            );

        return parseInt(res[0][0].count);
    };


    UnitQuestion.extendedCreate = async (user, body) => {
        const res = await sequelize
            .query(
                `INSERT INTO "UnitQuestion" 
                ("unitPreQualificationId",
                "mandatory", "type", "question", 
                "answer", "createdAt", "updatedAt")
                SELECT 
                    (select id from "UnitPreQualification" where "unitId" = ${body.unitId}), 
                    ${body.mandatory}, '${body.type}', '${body.question}',
                    '${body.answer}', current_timestamp, current_timestamp
                WHERE (select id from "UnitPreQualification" where "unitId" = ${body.unitId}) IN (
                    select id from "UnitPreQualification" where "unitId" in (
                           select id from "Unit" where "userId" in (
                                select id from "User" where "companyId" = ${user.companyId}
                           )
                    )
                )`,
            );

        return res[1] > 0;
    };

    UnitQuestion.extendedUpdate = async (companyId, body) => {
        const res = await sequelize
            .query(
                `UPDATE "UnitQuestion"
                 SET "mandatory" = ${body.mandatory},
                     "type" = ${body.type !== undefined ? `'${body.type}'` : null},
                     "question" = ${body.question !== undefined ? `'${body.question}'` : null},
                     "answer" = '${body.answer}',
                     "updatedAt" = current_timestamp
                WHERE 
                id = ${body.id} 
                AND "unitPreQualificationId" IN (
                    select id from "UnitPreQualification" where "unitId" in (
                           select id from "Unit" where "userId" in (
                                select id from "User" where "companyId" = ${companyId}
                           )
                    )
                )`);

        return res[1].rowCount > 0;
    };

    UnitQuestion.extendedDelete = async (companyId, unitQuestionId) => {
        const res = await sequelize
            .query(`DELETE FROM "UnitQuestion"
                    WHERE id = ${unitQuestionId} AND
                    "unitPreQualificationId" IN (
                        select id from "UnitPreQualification" where "unitId" in (
                            select id from "Unit" where "userId" in (
                                select id from "User" where "companyId" = ${companyId}
                            )
                        )
                    )`,
            );

        return res[1].rowCount > 0;

    };

    return UnitQuestion;

};
