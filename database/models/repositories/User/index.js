module.exports = (User, sequelize) => {

    User.checkSubscription = async (userId) =>{
        return await sequelize.query(`
        SELECT "User"."subscription" FROM "User" WHERE "User"."id" = ${userId}
        `);
    };

    User.getMaxLimit = async (userId) =>{
        return await sequelize.query(`
        SELECT "User"."maxUnits" FROM "User" WHERE "User"."id" = ${userId}
        `);
    };

    User.prototype.toJSON = function() {
        const user = this.get();
        delete user.password;
        return user;
    };

    return User;
};
