const filterToSql = require('../../../helpers/migrations/filter');

module.exports = (Lead, sequelize) => {
    Lead.associate = function(models) {
        Lead.belongsTo(models.Unit, { foreignKey: 'unitId' });
    };


    Lead.extendedShow = (id, [companyId]) => {
        return sequelize.query(
            `SELECT Le.* FROM "Lead" as Le
              WHERE Le."userId" IN (SELECT id FROM "User" WHERE "companyId" = ${companyId})
              AND Le.id = ${id}
              `);
    };

    Lead.getList = (type, [companyId, limit, offset, order]) => {
        return sequelize.query(`
            SELECT Le.*, Un.unit AS "unitNumber", Un.address AS "unitAddress" FROM "Lead" as Le
                        JOIN LATERAL (
                          select address, unit
                          from "Unit"
                          where id = Le."unitId"
                        ) AS Un ON true
                        WHERE Le."userId" IN (
                         select id from "User" where "companyId" = ${companyId} 
                        )
                        AND Le.type = '${type}'
                        ORDER BY "createdAt" ${order} LIMIT ${limit} OFFSET ${offset};`);
    };

    Lead.countListResult = async (type, [companyId]) => {
        const res = await sequelize.query(`
            SELECT COUNT(*) FROM "Lead"
                        WHERE "userId" IN (
                         select id from "User" where "companyId" = ${companyId} 
                        )
                        AND type = '${type}'`);

        return parseInt(res[0][0].count);
    };

    Lead.search = (query, [companyId, limit = 'ALL', offset = 0, order = 'ASC', filter]) => {
        return sequelize.query(`SELECT Le.*, Un.unit AS "unitNumber", Un.address AS "unitAddress" FROM "Lead" AS Le 
                        JOIN LATERAL (
                          select address, unit
                          from "Unit"
                          where id = Le."unitId"
                        ) AS Un ON true
                        WHERE Le."userId" IN (
                         select id from "User" where "companyId" = ${companyId} 
                        )
                        ${filterToSql(filter, 'Le')}
                        AND (Le.name ILIKE '%${query}%' OR Le.email ILIKE '%${query}%') 
                        ORDER BY "createdAt" ${order} LIMIT ${limit} OFFSET ${offset};`);
    };

    Lead.countSearchResult = async (query, [companyId, filter]) => {
        const res = await sequelize.query(`SELECT COUNT(*) FROM "Lead" as Le
                        WHERE Le."userId" IN (
                         select id from "User" where "companyId" = ${companyId} 
                        )
                        ${filterToSql(filter, 'Le')}
                        AND (Le.name ILIKE '%${query}%' OR Le.email ILIKE '%${query}%');`);

        return parseInt(res[0][0].count);

    };

    Lead.getReport = async ([order, limit, offset, companyId, userId, propertyId, startDate, endDate]) => {
        return await sequelize
            .query(`SELECT  
                            users.name AS "team",
                            coalesce(preQU.totalQ, 0) AS "totalquestion",
                        Un."address" AS "address",
                        Un."unit" AS "unit",
                        Un."createdAt" AS "date",
                        coalesce(Le.total, 0) AS "total",
                        coalesce(Le.qualified, 0) AS "qualified",
                        coalesce(Le.disqualified, 0) AS "disqualified",
                        coalesce(Le.scheduled, 0) AS "scheduled",
                        coalesce(Le.rescheduled, 0) AS "rescheduled",
                        coalesce(Le.canceled, 0) AS "canceled"
                            FROM "Unit" AS Un
                            JOIN LATERAL
                            (
                                SELECT COUNT(*) AS total,
                                COUNT(*) filter(where "Lead".type = 'Qualified') as qualified,
                                COUNT(*) filter(where "Lead".type = 'Disqualified') as disqualified,
                                COUNT(*) filter(where "Lead".status = 'Showing scheduled') as scheduled,
                                COUNT(*) filter(where "Lead".status = 'Showing rescheduled') as rescheduled,
                                COUNT(*) filter(where "Lead".status = 'Showing cancelled') as canceled
                                FROM "Lead"
                                WHERE "unitId" = Un.id
                            ) AS Le ON true
                            JOIN
                            (
                                SELECT id, name, "companyId"
                                FROM "User"
                                GROUP BY "id"
                            ) AS users ON Un."userId" = users.id

                                LEFT JOIN
                            (
                    SELECT "unitPreQualificationId",COUNT(*) AS totalQ
                        FROM "UnitQuestion"
                        GROUP BY "unitPreQualificationId"
                            ) AS preQU ON Un.id = preQU."unitPreQualificationId"
                                        WHERE users."companyId" = ${companyId}
                        ${userId !== null && userId !== undefined ? `AND Un."userId" = ${userId}` : ''}
                        ${propertyId !== null && propertyId !== undefined ? `AND Un."propertyId" = ${propertyId}` : ''}
                        ${startDate !== null && startDate !== undefined ? `AND Un."createdAt" >= '${startDate}'::date` : ''}
                        ${endDate !== null && endDate !== undefined ? `AND Un."createdAt" <= '${endDate}'::date` : ''}
                        ORDER BY name ${order} LIMIT ${limit} OFFSET ${offset}`);
    };

    Lead.countReportsResult = async ([companyId, userId, propertyId, startDate, endDate]) => {
        const res = await sequelize
            .query(`SELECT COUNT(*) FROM "Unit" as Un
                         WHERE  Un."userId" IN (
                          SELECT id FROM "User" WHERE  "companyId" = ${companyId}
                          )
                        ${userId !== null && userId !== undefined ? `AND Un."userId" = ${userId}` : ''}
                        ${propertyId !== null && propertyId !== undefined ? `AND Un."propertyId" = ${propertyId}` : ''}
                        ${startDate !== null && startDate !== undefined ? `AND Un."createdAt" >= '${startDate}'::date` : ''}
                        ${endDate !== null && endDate !== undefined ? `AND Un."createdAt" <= '${endDate}'::date` : ''}
                        `);

        return parseInt(res[0][0].count);
    };



    Lead.showByPhoneAndUserId = async (userId, phone) => {
        return await sequelize
            .query(`SELECT ld.* FROM "Lead" as ld
                    WHERE ld."userId" in (
                      select id from "User" where "companyId" in (
                         select "companyId" from "User" where id=${userId}
                      )
                    ) AND ld.phone = '${phone}'
            `);
    };

    return Lead;
};
