module.exports = (UnitPreQualification, sequelize) => {

    UnitPreQualification.extendedShow = (unitId, companyId) => {
        return sequelize
            .query(
                `SELECT * FROM "UnitPreQualification"
                 WHERE "unitId" = ${unitId}
                 AND "userId" IN (
                    select id from "User" where "companyId" = ${companyId}
                )`,
            );
    };

    UnitPreQualification.extendedUpdate = async (companyId, body) => {
        const res = await sequelize
            .query(
                `UPDATE "UnitPreQualification"
                 SET "pet" = '${body.pet}',
                     "cat" = ${body.cat !== undefined ? body.cat : null},
                     "dog" = ${body.dog !== undefined ? body.dog : null},
                     "accept" = '${body.agree}',
                     "updatedAt" = current_timestamp
                WHERE 
                id = ${body.id} 
                AND "userId" in (
                  select id from "User" where "companyId" = ${companyId}
                )`,
            );

        return res[1].rowCount > 0;
    };

    UnitPreQualification.extendedCreate = async (user, body) => {
        const res = await sequelize
            .query(
                `INSERT INTO "UnitPreQualification" 
                    ("userId", "unitId",
                    "pet", 
                    "cat", "dog", 
                    "accept", "createdAt", "updatedAt")
                SELECT 
                    ${user.id}, ${body.unitId},
                    '${body.pet}',
                    ${body.cat !== undefined ? `${body.cat},` : null} 
                    ${body.dog !== undefined ? `${body.dog},` : null}
                    ${body.agree}, current_timestamp, current_timestamp
                WHERE ${body.unitId} IN (
                    select id from "Unit" where "userId" in (
                           select id from "User" where "companyId" = ${user.companyId}
                    )
                ) AND NOT EXISTS (
                    select id from "UnitPreQualification" where "unitId" = ${body.unitId}
                )`,
            );

        return res[1] > 0;
    };

    return UnitPreQualification;
};
