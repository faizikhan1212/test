module.exports = (CalendarAccount, sequelize) => {

    CalendarAccount.associate = function(models) {
        CalendarAccount.hasMany(models.Scheduling, { foreignKey: 'calendarAccountId', onDelete: 'CASCADE' });
    };

    CalendarAccount.getList = async (userId, order, limit, offset) => {
        return await sequelize.query(`
        SELECT * FROM "CalendarAccounts"
        WHERE "userId" IN (
          select id from "User" where  "userId" = ${userId}
        ) 
        ORDER BY id ${order} LIMIT ${limit} OFFSET ${offset}
        `);
    };

    CalendarAccount.getGoogleAccessToken = async userId => {
        return new Promise(async (resolve, reject) => {
            try {
                let query = await sequelize.query(`
                    SELECT * FROM "CalendarAccounts"
                    WHERE "userId" IN (
                    select id from "User" where  "userId" = ${userId}
                    )
                `);

                let resultRes = await query;
                let result = resultRes[0][0];
                if (result) {
                    let token = {
                        access_token: result.accessToken,
                        refresh_token: result.refreshToken,
                        scope: result.scope,
                        token_type: result.tokenType,
                        id_token: result.idToken,
                        expiry_date: result.expiryDate,
                    };
                    return resolve(token);
                }
                return resolve(null);
            } catch (error) {
                return reject(error);
            }
        });
    };

    CalendarAccount.getCountList = async userId => {
        const res = await sequelize.query(`SELECT COUNT(*) FROM "CalendarAccounts"
                    WHERE "userId" IN (
                        select id from "User" where  "userId" = ${userId})`);

        return parseInt(res[0][0].count);
    };

    return CalendarAccount;
};
