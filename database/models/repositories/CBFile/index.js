module.exports = (CBFile, sequelize) => {

    CBFile.getList = (entityName, entityId, [companyId, limit, offset, order]) => {
        return sequelize
            .query(
                `SELECT * FROM "CBFile"
                 WHERE "entityName" = '${entityName}'
                 AND "entityId" = ${entityId}
                 AND "leadId" IN (
                    select id from "Lead" where "userId" in (
                      select id from "User" where "companyId" = ${companyId}
                    )
                )
                ORDER BY "createdAt" ${order} LIMIT ${limit} OFFSET ${offset};`,
            );
    };

    CBFile.countList = async (entityName, entityId, companyId) => {
        const res = await sequelize
            .query(
                `SELECT COUNT(*) FROM "CBFile"
                 WHERE "entityName" = '${entityName}'
                 AND "entityId" = ${entityId}
                 AND "leadId" IN (
                    select id from "Lead" where "userId" in (
                      select id from "User" where "companyId" = ${companyId}
                    )
                );`,
            );

        return parseInt(res[0][0].count);

    };

    return CBFile;
};
