const redis = require('../../../../app/services/redis');

module.exports = (Unit, sequelize) => {
    Unit.associate = function(models) {
        Unit.hasMany(models.Lead, { foreignKey: 'unitId', onDelete: 'CASCADE' });
    };

    Unit.addHook('afterCreate', (unit) => {
        redis.incr(`${unit.userId}-count-unit`);
    });

    Unit.addHook('afterDestroy', (unit) => {
        redis.decrby(`${unit.userId}-count-unit`, 1);
    });

    Unit.getVacantList = (companyId, [limit, offset, order = 'DESC']) => {
        return sequelize.query(
            `SELECT Un.*, Co.slug as slug FROM "Unit" as Un
            JOIN "Company"as Co ON Co.id = ${companyId}
             WHERE Un."userId" IN (
                  SELECT id FROM "User" WHERE  "companyId" = ${companyId}
            ) 
            AND Un.status = 'Vacant'
            ORDER BY "createdAt" ${order} LIMIT ${limit} OFFSET ${offset}
          `,
        );
    };

    Unit.countVacantListResult = async (companyId) => {
        const res = await sequelize.query(
            `SELECT COUNT(*) FROM "Unit" as Un
             WHERE Un."userId" IN (
                  SELECT id FROM "User" WHERE  "companyId" = ${companyId}
             ) 
            AND Un.status = 'Vacant'`);

        return parseInt(res[0][0].count);
    };


    Unit.getListByProperty = (propertyId, [companyId, limit, offset, order = 'DESC']) => {
        return sequelize.query(
            `SELECT Un.* FROM "Unit" as Un
             WHERE Un."userId" IN (
                select id from "User" where  "companyId" = ${companyId}
             ) 
             AND Un."propertyId" = ${propertyId}
             ORDER BY "createdAt" ${order} LIMIT ${limit} OFFSET ${offset}
          `,
        );
    };

    Unit.countListByPropertyResult = async (propertyId, [companyId]) => {
        const res = await sequelize.query(
            `SELECT COUNT(*) FROM "Unit" as Un
             WHERE Un."userId" IN (
                select id from "User" where  "companyId" = ${companyId}
             ) 
             AND Un."propertyId" = ${propertyId}`);

        return parseInt(res[0][0].count);
    };

    Unit.getPublicList = (slug, [limit = 'All', offset = 0, order = 'ASC']) => {
        /**
         * SELECT * FROM public."Unit" LEFT JOIN public."Property" ON public."Property"."id" = public."Unit"."propertyId" WHERE public."Unit"."userId" = 4
         */
        return sequelize
            .query(
                `SELECT Un.*, f."countImages"::integer
                 FROM "Unit" as Un
                 LEFT JOIN LATERAL (
                    select count(*) AS "countImages"
                    from "File"
                    where "entityName"='Unit' and "entityId"=Un.id
                 ) AS f ON true
                 WHERE Un.status = 'Vacant' AND Un."userId" IN (
                   select id from "User" where  "companyId" in (
                     select id from "Company" where slug = '${slug}'
                   )
                 ) 
                 ORDER BY id ${order} LIMIT ${limit} OFFSET ${offset}`,
            );
    };

    Unit.countPublicListResult = async slug => {
        const res = await sequelize
            .query(`SELECT COUNT(*) FROM "Unit" as Un
                    WHERE Un.status = 'Vacant' AND Un."userId" IN (
                      select id from "User" where  "companyId" in (
                        select id from "Company" where slug = '${slug}'
                      )
                    ) 
                   `);

        return parseInt(res[0][0].count);
    };

    Unit.search = (query, [companyId, limit = 'ALL', offset = 0, order = 'ASC', propertyId]) => {
        return sequelize
            .query(`SELECT Un.* FROM "Unit" as Un
                    WHERE Un."userId" IN (
                      select id from "User" where  "companyId" = ${companyId}
                    ) 
                    AND "propertyId" = '${propertyId}'
                    AND unit ILIKE '${query}%'
                    ORDER BY "createdAt" ${order} LIMIT ${limit} OFFSET ${offset}`);
    };


    Unit.vacantSearch = (query, [companyId, limit = 'ALL', offset = 0, order = 'ASC']) => {
        return sequelize
            .query(`SELECT Un.*, Co.slug as slug FROM "Unit" as Un
                    JOIN "Company" as Co ON Co.id = ${companyId}
                    WHERE Un."userId" IN (
                      select id from "User" where  "companyId" = ${companyId}
                    ) 
                    AND Un.status = 'Vacant'
                    AND (Un.address ILIKE '%${query}%'
                    OR Un.unit ILIKE '${query}%') 
                    ORDER BY "createdAt" ${order} LIMIT ${limit} OFFSET ${offset}`);

    };


    Unit.countSearchResult = async (query, [companyId, propertyId]) => {
        const res = await sequelize
            .query(`SELECT COUNT(*) FROM "Unit" as Un
                    WHERE Un."userId" IN (
                      select id from "User" where  "companyId" = ${companyId}
                    ) 
                    AND "propertyId" = '${propertyId}'
                    AND unit ILIKE '${query}%'`);

        return parseInt(res[0][0].count);
    };

    Unit.countVacantSearchResult = async (query, [companyId]) => {
        const res = await sequelize
            .query(`SELECT COUNT(*) FROM "Unit" as Un
                    WHERE Un."userId" IN (
                      select id from "User" where  "companyId" = ${companyId}
                    ) 
                    AND status = 'Vacant'
                    AND (address ILIKE '%${query}%'
                    OR unit ILIKE '${query}%') `);

        return parseInt(res[0][0].count);
    };

    Unit.changeProperty = async (user, body) => {
        const res = await sequelize.query(
            `UPDATE "Unit"
                SET "userId" = (select id from "User" where id in (select "userId" from "Property" where id = ${body.propertyId})),
                "propertyId" = ${body.propertyId}
                
            WHERE id = ${body.unitId}
            AND   
            "userId" in (
               select id from "User" where "companyId" = ${user.companyId}
            )
            AND
            ${body.propertyId} in (
             select id from "Property" where "userId" in (
                select id from "User" where "companyId" = ${user.companyId}
             )
            )`);

        return res[1].rowCount > 0;
    };

    Unit.extendedUpdate = async (companyId, body) => {
        const res = await sequelize
            .query(
                `UPDATE "Unit"
                 SET "rent" = ${body.rent},
                    "showingAgent" = ${body.showingAgent !== undefined ? body.showingAgent : null},
                    "deposit" = ${body.deposit},
                    "status" = '${body.status}',
                    "title" = ${!body.title ? null : `'${body.title}'`},
                    description = ${!body.description ? null : `$$${body.description}$$`},
                    parking = ${body.parking},
                    "parkingCost" = ${body.parking && body.parkingCost !== undefined ? body.parkingCost : null},
                    "parkingCovered" = ${body.parking && body.parkingCovered !== undefined ? body.parkingCovered : null},
                    "parkingSecured" = ${body.parking && body.parkingSecured !== undefined ? body.parkingSecured : null},
                    "petFriendly" = ${body.petFriendly},
                    "petPolicy" = '${body.petFriendly && body.petPolicy !== undefined ? body.petPolicy : null}',
                    "applicationFee" = ${body.applicationFee},
                    "appFeeNonRefundable" = ${body.applicationFee && body.appFeeNonRefundable !== undefined ? body.appFeeNonRefundable : null},
                    "applicationFeeCost" = ${body.applicationFee && body.applicationFeeCost !== undefined ? body.applicationFeeCost : null},
                    "unitShared" = ${body.unitShared},
                    "unitSharedPeople" = ${body.unitShared && body.unitSharedPeople !== undefined ? body.unitSharedPeople : null},
                    "sharedWashroomBath" = ${body.sharedWashroomBath},
                    "sharedLaundry" = ${body.sharedLaundry},
                    bed = ${body.bed},
                    bath = ${body.bath},
                    "sqFt" = ${body.sqFt !== undefined ? body.sqFt : null},
                    "leaseTerm" = '${body.leaseTerm}',
                    email = ${!body.email ? null : `'${body.email}'`},
                    "phone" = ${!body.phone ? null : `'${body.phone}'`},
                    ${body.utilities !== null && body.utilities !== undefined ? `"utilities" = '{${body.utilities}}',` : ''}
                    ${body.amenities !== null && body.amenities !== undefined ? `"amenities" = '{${body.amenities}}',` : ''}
                    ${body.appliances !== null && body.appliances !== undefined ? `"appliances" = '{${body.appliances}}',` : ''}
                    "unit" = '${body.unit}',
                    address = '${body.address}',
                    "noSmoking" = ${body.noSmoking},
                    "moveInDate" = '${body.moveInDate}',
                    "utilityBill" = ${body.utilityBill && body.utilityBill !== undefined ? body.utilityBill : null},
                    "applicationProcess" = '${body.applicationProcess}'
                WHERE 
                id = ${body.id} 
                AND "userId" in (
                  select id from "User" where "companyId" = ${companyId}
                )`);

        return res[1].rowCount > 0;
    };

    Unit.findByIdWithCompanyId = async (id, companyId) => {
        const res = await sequelize.query(`
            SELECT * FROM "Unit" AS un
                WHERE un."userId" IN (
                SELECT id FROM "User" WHERE "companyId" = ${companyId}
            ) AND un.id = ${id}
        `);

        return res[1].rowCount > 0 ? res[1].rows[0] : null;
    };

    Unit.countUnits = async (companyId) => {
        const res = await sequelize.query(`
            SELECT COUNT(*) FROM "Unit"
                WHERE "userId" IN (
                    select id from "User" where "companyId" = ${companyId}
                )
        `);

        return parseInt(res[0][0].count);
    };

    Unit.getPublicListByParams = (slug, payload = {}, [limit = 'All', offset = 0, order = 'ASC']) => {
        return sequelize
            .query(
                `SELECT *
                 FROM "Unit" as Un
                 WHERE Un."userId" IN (
                   SELECT id FROM "User" where "companyId" IN (
                     SELECT id FROM "Company" WHERE slug = '${slug}'
                   )
                 )
                 AND Un.status = 'Vacant'
                 ${payload.pets !== undefined && payload.pets === true ? `AND Un."petFriendly" = true` : ''}
                 ${payload.smoke !== undefined && payload.smoke === true ? `AND Un."noSmoking" = false` : ''}
                 ${payload.bedrooms !== undefined ? `AND COALESCE(Un."bed", 0) >= ${payload.bedrooms}` : ''}
                 ${payload.bathrooms !== undefined ? `AND COALESCE(Un."bath", 0) >= ${payload.bathrooms}` : ''}
                 ${payload.square !== undefined ? `AND COALESCE(Un."sqFt", 0) >= ${payload.square}` : ''}
                 ${payload.cars !== undefined && payload.cars === true ? `AND Un."parking" = true` : ''}
                 GROUP BY Un.id
                 ${payload.income !== undefined ? `HAVING SUM(COALESCE(Un."rent", 0) + COALESCE(Un."deposit", 0)) < ${payload.income}` : ''}
                 ORDER BY Un.id ${order} LIMIT ${limit} OFFSET ${offset}`,
            );
    };

    Unit.countPublicListByParams = async (slug, payload = {}, unitId = undefined) => {
        const res = await sequelize
            .query(`SELECT COUNT(*) FROM (
                        SELECT * FROM "Unit" as Un
                        WHERE Un."userId" IN (
                          SELECT id FROM "User" where "companyId" IN (
                            SELECT id FROM "Company" WHERE slug = '${slug}'
                          )
                        )
                        AND Un.status = 'Vacant'
                        ${payload.pets !== undefined && payload.pets === true ? `AND Un."petFriendly" = true` : ''}
                        ${payload.smoke !== undefined && payload.smoke === true ? `AND Un."noSmoking" = false` : ''}
                        ${payload.bedrooms !== undefined ? `AND COALESCE(Un."bed", 0) >= ${payload.bedrooms}` : ''}
                        ${payload.bathrooms !== undefined ? `AND COALESCE(Un."bath", 0) >= ${payload.bathrooms}` : ''}
                        ${payload.square !== undefined ? `AND COALESCE(Un."sqFt", 0) >= ${payload.square}` : ''}
                        ${payload.cars !== undefined && payload.cars === true ? `AND Un."parking" = true` : ''}
                        ${unitId !== undefined ? `AND Un."id" = ${unitId}` : ''}
                        GROUP BY Un.id
                        ${payload.income !== undefined ? `HAVING SUM(COALESCE(Un."rent", 0) + COALESCE(Un."deposit", 0)) < ${payload.income}` : ''}
                        ) count
                   `);

        return parseInt(res[0][0].count);
    };

    return Unit;

};
