module.exports = (Responders, sequelize) => {

    Responders.list = async (userId) => {
        return await sequelize.query(
            `SELECT "Responders"."label" , "Responders"."generatedEmail" , "Responders"."forwardEmail" , "Responders"."autoresponse"  FROM "Responders" WHERE "Responders"."userId" = ${userId}`
        )
    }
    

    Responders.update = async (data) => {
        return await sequelize.query(`
        UPDATE "Responders" SET "label" = '${data.label}' , "autoresponse" = ' ${data.autoresponse} ' , "forwardEmail" = '${data.forwardEmail}' WHERE "Responders"."generatedEmail" = '${data.generatedEmail}'
        `)
    }

    Responders.insert = async (data,userId) => {
        return await sequelize.query(`
        INSERT INTO "Responders" SELECT ${userId} , '${data.label}' , '${data.generatedEmail}' , '${data.forwardEmail}' , '${data.autoresponse}' , 0 WHERE NOT EXISTS (SELECT * FROM "Responders" WHERE "generatedEmail" = '${data.generatedEmail}');
    `)
    }

    Responders.getresponse = async (generatedEmail) => {
        return await sequelize.query(`
        SELECT "Responders"."autoresponse" FROM "Responders" WHERE "Responders"."generatedEmail" = '${generatedEmail}'
        `);
    }

    return Responders;
}