module.exports = (Company, sequelize) => {


    Company.dashboardCount = async (companyId) => {
        return await sequelize
            .query(`SELECT
                      SUM (coalesce(properties.count, 0))::int as "countProperties",
                      SUM (coalesce("unitsVacant".count, 0))::int AS "countUnitsVacant", 
                      SUM (coalesce(le.count, 0))::int AS "countTotalInquiries",
                      SUM (coalesce(le."qualifiedLeads", 0))::int AS "countQualifiedLeads", 
                      SUM (coalesce(null, 0))::int AS "countShowingsScheduled",
                      SUM (coalesce(null, 0))::int AS "countMaintenanceRequests"
                    FROM "User" as Us
                    LEFT JOIN LATERAL
                    ( 
                     SELECT "userId", COUNT(*) AS count
                     FROM "Property" WHERE "userId" = Us.id
                     GROUP BY "userId"
                    ) AS properties ON true
                    LEFT JOIN LATERAL
                    (
                     SELECT "userId", COUNT(*) AS count
                     FROM "Unit"
                     WHERE "Unit".status = 'Vacant' AND "userId" = Us.id
                     GROUP BY "userId"
                    ) AS "unitsVacant" ON true
                    LEFT JOIN LATERAL
                    (
                     SELECT "userId", COUNT(*) AS count, COUNT(*) filter(where "Lead".type = 'Qualified') as "qualifiedLeads"
                     FROM "Lead" WHERE "userId" = Us.id
                     GROUP BY "userId"
                    ) AS le ON true
                    WHERE Us."companyId" IN (
                    SELECT id FROM "Company" WHERE id = ${companyId}
                    )
`);

    };

    Company.countOtherCompanies = async (companyId, field, value) => {
        const res = await sequelize
            .query(
                `SELECT count(*) AS "count" FROM "Company"
                WHERE "${field}" = '${value}' AND id::VarChar NOT LIKE '${companyId}'`,
            );
        return parseInt(res[0][0].count);
    };


    Company.getCompanyInfo = async (slug) => {
        return await sequelize
            .query(`
                SELECT co.name, f.hash as "fileHash", f.name as "fileName", f."createdAt" as "fileCreatedAt"
                FROM "Company" as co 
                    LEFT JOIN LATERAL (
                        select * from "File"
                        where "entityName"='Company' and "entityId"=co.id
                    ) as f ON true
                where slug = '${slug}'
              `);
    };

    return Company;

};
