module.exports = (Scheduling, sequelize) => {

    Scheduling.associate = function(models) {
        Scheduling.belongsTo(models.CalendarAccount, { foreignKey: 'calendarAccountId' });
    };


    Scheduling.extendedShow = async(companyId, schedulingId) => {
        return sequelize.query(`
            SELECT sch.*, prop.id as "propertyId" FROM "Scheduling" as sch
            JOIN "Unit" as un ON un.id = sch."unitId"
            JOIN "Property" as prop ON prop.id = un."propertyId"         
            WHERE sch."userId" IN (
                SELECT id FROM "User" WHERE "companyId" = ${companyId}
            ) AND sch.id = ${schedulingId};
        `);
    };

    Scheduling.getCalendarInfo = async ([companyId, filter, limit, offset, order]) => {
        filter = JSON.parse(filter);

        return sequelize.query(`
            SELECT 
                sch.id, sch."userId", sch."unitId", sch."clientName", sch."clientEmail", 
                sch."clientPhone", sch.notes, sch."createdOnApp", lower(sch."dateOfShowing") AS start, 
                upper(sch."dateOfShowing") AS end, un."propertyId", 
                un.title AS "unitTitle", un.address AS "unitAddress", 
                un.unit AS "unitNumber", us.name AS "userName" 
            FROM "Scheduling" AS sch
            LEFT JOIN "Unit" AS un ON sch."unitId" = un.id
            JOIN "User" AS us ON sch."userId" = us.id
            WHERE sch."userId" IN (
            SELECT id FROM "User" WHERE "companyId" = ${companyId}
            ${filter.userId !== undefined && filter.userId !== null ? `AND sch."userId" = ${filter.userId}` : ''})
            ${filter.dateOfShowing !== undefined && filter.dateOfShowing !== null ? `AND sch."dateOfShowing" <@ '[${filter.dateOfShowing})'::tstzrange` : ''}
            ORDER BY sch."dateOfShowing" ${order} LIMIT ${limit} OFFSET ${offset}
        `);
    };

    Scheduling.countCalendarInfo = async ([companyId, filter]) => {
        filter = JSON.parse(filter);

        return sequelize.query(`
            SELECT COUNT(*)::INT AS "countCalendarInfo" FROM "Scheduling" AS sch
            WHERE sch."userId" IN (
            SELECT id FROM "User" WHERE "companyId" = ${companyId}
            ${filter.userId !== undefined && filter.userId !== null ? `AND sch."userId" = ${filter.userId}` : ''})
            ${filter.dateOfShowing !== undefined && filter.dateOfShowing !== null ? `AND sch."dateOfShowing" <@ '[${filter.dateOfShowing})'::tstzrange` : ''}
        `);
    };

    Scheduling.getAvailableTime = async (slug, minutes) => {

        return await sequelize.query(`SELECT NULLIF(now(), (
            SELECT sch."dateOfShowing"
            FROM "Scheduling" AS sch
            WHERE sch."userId" IN (
                SELECT id FROM "User" 
                WHERE "companyId" IN (
				    SELECT id FROM "Company" 
				    WHERE slug = '${slug}'
			    )
			)    
            AND sch."dateOfShowing" && tstzrange(now() + '${minutes - 30} minutes', now() + '${minutes} minutes', '[)')
            LIMIT 1))`);
    };

    Scheduling.getCalendarByInterval = async (companySlug, startDate, endDate) => {
        return sequelize.query(`
            SELECT 
                lower(sch."dateOfShowing") as start, 
                upper(sch."dateOfShowing") as end
            FROM "Scheduling" AS sch
            WHERE sch."userId" IN (
                SELECT id FROM "User" 
                WHERE "companyId" IN (
				    SELECT id FROM "Company" 
				    WHERE slug = '${companySlug}'
			    )
			)    
            AND sch."dateOfShowing" <@ '[${startDate},${endDate})'::tstzrange
        `);
    };


    Scheduling.getByLeadPhone = async (companyId, phone, filter) => {

        return sequelize.query(
            `SELECT sch.*, un.address AS "unitAddress", un.unit AS "unitNumber" FROM "Scheduling" AS sch
                LEFT JOIN "Unit" AS un ON sch."unitId" = un.id
                JOIN "User" AS us ON sch."userId" = us.id
                    WHERE sch."userId" IN (
                        SELECT id FROM "User" WHERE "companyId" = ${companyId}
                    ) AND sch."clientPhone" = '${phone}'
            ORDER BY "createdAt" ASC LIMIT ${filter.limit} OFFSET ${filter.offset};
            `);

    };


    Scheduling.countSchedulingsByLeadPhone = async (companyId, phone) => {
        return sequelize.query(`
            SELECT COUNT(*)::INT AS "countCalendarInfo" FROM "Scheduling" AS sch
            WHERE sch."userId" IN (
            SELECT id FROM "User" WHERE "companyId" = ${companyId} 
            ) AND sch."clientPhone" = '${phone}'
        `);
    };

    return Scheduling;
};
