module.exports = (Dictionary, sequelize) => {

    Dictionary.createAndPopulate = async (companyId, allowedData) => {
        return await sequelize
            .query(`
          INSERT INTO "Dictionary" ("companyId", amenities, utilities, appliances, "createdAt", "updatedAt")
          VALUES (
          ${companyId},
          '${JSON.stringify(allowedData.amenities)}',
          '${JSON.stringify(allowedData.utilities)}',
          '${JSON.stringify(allowedData.appliances)}',
          current_timestamp, current_timestamp
          )
          
          RETURNING *
          
      `);
    };

    Dictionary.checkItem = async (companyId, itemType, item) => {
        const res = await sequelize
            .query(`
          SELECT COUNT(*) FROM "Dictionary" where "companyId"=${companyId} "${itemType}"::jsonb @> '"${item}"'::jsonb
        `);

        return parseInt(res[0][0].count);
    };

    Dictionary.pushItems = async (companyId, itemType, item) => {
        return await sequelize
            .query(`
        UPDATE "Dictionary"
        SET "${itemType}" = "${itemType}" || '[${item.map(i => `"${i}"`)}]'::jsonb
        WHERE "companyId"=${companyId}
        `);
    };


    return Dictionary;

};
