module.exports = (Question, sequelize) => {

    Question.getQuestionsByCompany = async (slug) => {
        const questions = await sequelize
            .query(`select * from "Question"
	                    where "preQualificationId" in (
		                select "id" from "PreQualification"
		                where "userId" in (
			                select id from "User" 
			                where "companyId" in (
				                select id from "Company" where slug = '${slug}'
			                )
		                )
	                )`);
        return questions[0];
    };

    return Question;
};