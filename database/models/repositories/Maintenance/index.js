const filterToSql = require('../../../helpers/migrations/filter');

module.exports = (Maintenance, sequelize) => {
    Maintenance.getList = async (companyId, filter, order, limit, offset) => {
        return await sequelize.query(`
        SELECT Maint.*, Ve.name as "vendorName" FROM "Maintenance" as Maint
        LEFT JOIN LATERAL (
             SELECT id, name
             FROM "Vendor"
             WHERE Maint."vendorId" = "Vendor".id
        ) as Ve ON true
        WHERE Maint."leadId" IN (
            select id from "Lead" where "userId" IN (
                select id from "User" where "companyId" = ${companyId}
            )
        ) ${filterToSql(filter, 'Maint')}
        ORDER BY id ${order} LIMIT ${limit} OFFSET ${offset}`);
    };

    Maintenance.getCountList = async (companyId, filter) => {
        const res = await sequelize
            .query(`SELECT COUNT(*) FROM "Maintenance" as Maint
                    WHERE Maint."leadId" IN (
                        select id from "Lead" where "userId" IN (
                            select id from "User" where "companyId" = ${companyId}
                        )
                    ) ${filterToSql(filter, 'Maint')}`);

        return parseInt(res[0][0].count);
    };

    Maintenance.search = (query, [companyId, filter, limit = 'ALL', offset = 0, order = 'ASC']) => {
        return sequelize.query(`
            SELECT Maint.*, Ve.name as "vendorName" FROM "Maintenance" AS Maint
            LEFT JOIN LATERAL (
                SELECT id, name
                FROM "Vendor"
                WHERE Maint."vendorId" = "Vendor".id
            ) as Ve ON true
            WHERE Maint."leadId" IN (
                  select id from "Lead" where "userId" in (
                         select id from "User" where "companyId" = ${companyId}
                  )
            ) 
            AND 
                 (
                 Maint."tenantName" ILIKE '%${query}%' OR
                 (select name from "Vendor" where id = Maint."vendorId" ) ILIKE '%${query}%' OR
                 (select address from "Property" where id in (
                         select "propertyId" from "Vendor" where id = Maint."vendorId"
                 )
                 ) ILIKE '%${query}%'
                 )  
            ${filterToSql(filter, 'Maint')}
            ORDER BY Maint.id ${order} LIMIT ${limit} OFFSET ${offset}
        `);
    };

    Maintenance.countSearchResult = async (query, [companyId, filter]) => {
        const res = await sequelize
            .query(`SELECT COUNT(*) FROM "Maintenance" as Maint
                    WHERE Maint."leadId" IN (
                          select id from "Lead" where "userId" in (
                                 select id from "User" where "companyId" = ${companyId}
                          )
            ) 
            AND 
                 (
                 Maint."tenantName" ILIKE '%${query}%' OR
                 (select name from "Vendor" where id = Maint."vendorId" ) ILIKE '%${query}%' OR
                 (select address from "Property" where id in (
                         select "propertyId" from "Vendor" where id = Maint."vendorId"
                 )
                 ) ILIKE '%${query}%'
                 )  
            ${filterToSql(filter, 'Maint')}`);

        return parseInt(res[0][0].count);
    };

    Maintenance.extendedUpdate = async (companyId, body) => {
        const res = await sequelize.query(
            `UPDATE "Maintenance" as Maint
            SET status = '${body.status}',
                "vendorId" = Ve.id,
                "budget" = ${body.budget},
                "dateScheduled" = '${body.dateScheduled}',
                "dateCompleted" = '${body.dateCompleted}',
                estimate = ${body.estimate},
                cost = ${body.cost}
                FROM "Maintenance" as "fromMaint"
                 JOIN LATERAL (
                  SELECT id, "userId"
                  FROM "Vendor"
                  WHERE  "Vendor".id = ${body.vendorId} AND "Vendor"."userId" in (
                   SELECT id from "User" where "companyId" = ${companyId}
                  )
                  ) as Ve on true
            WHERE
            Maint.id = ${body.id} AND "fromMaint".id = ${body.id} AND
            Maint."leadId" in (
               select id from "Lead" where "userId" in (
                  select id from "User" where "companyId" = ${companyId}
               )
            )
`);
        return res[1].rowCount > 0;
    };

    Maintenance.extendedShow = async (companyId, maintenanceId) => {
        return await sequelize.query(
            `SELECT Maint.*,  Ven.email as "vendorEmail"
             FROM "Maintenance" as Maint
             JOIN LATERAL(
                select id, email
                from "Vendor"
                where "Vendor".id = Maint."vendorId"
            ) as Ven on true
            WHERE Maint.id = ${maintenanceId} AND
            Maint."leadId" in (
               select id from "Lead" where "Lead"."userId" in (
                  select id from "User" where "User"."companyId" = ${companyId}
               )
            )`);
    };
    Maintenance.getReport = async ([order, limit, offset, companyId, userId, propertyId, startDate, endDate]) => {
        return await sequelize
            .query(`
            SELECT Maint.*, Ve.name as "vendorName", Le."unitId" as unitid, Un.address as "address", Un.unit as "unit" FROM "Maintenance" as Maint
            LEFT JOIN LATERAL (
                 SELECT id, name
                 FROM "Vendor"
                 WHERE Maint."vendorId" = "Vendor".id
            ) as Ve ON true
            LEFT JOIN LATERAL (
                 SELECT id, "unitId"
                 FROM "Lead"
                 WHERE Maint."leadId" = "Lead".id
            ) as Le ON true
             LEFT JOIN LATERAL (
                 SELECT id, address,unit
                 FROM "Unit"
                 WHERE Le."unitId" = "Unit".id
            ) as Un ON true
            WHERE Maint."leadId" IN (
                select id from "Lead" where "unitId" IN (
                    select "id" from "Unit" where "userId" IN ( select id from "User" where "companyId" = ${companyId})
                    ${userId !== null && userId !== undefined ? `AND "Unit"."userId" = ${userId}` : ''}
                    ${propertyId !== null && propertyId !== undefined ? `AND "Unit"."propertyId" = ${propertyId}` : ''}
                    ${startDate !== null && startDate !== undefined ? `AND "Unit"."createdAt" >= '${startDate}'::date` : ''}
                    ${endDate !== null && endDate !== undefined ? `AND "Unit"."createdAt" <= '${endDate}'::date` : ''}
                    ) 
            ) 
                ORDER BY id ${order} LIMIT ${limit} OFFSET ${offset}`);

    };

    Maintenance.countReportsResult = async ([companyId, userId, propertyId, startDate, endDate]) => {
        const res = await sequelize
            .query(`
            SELECT COUNT(*) FROM "Maintenance" as Maint
            
            WHERE Maint."leadId" IN (
                select id from "Lead" where "unitId" IN (
                    select "id" from "Unit" where "userId" IN ( select id from "User" where "companyId" = ${companyId})
                    ${userId !== null && userId !== undefined ? `AND "Unit"."userId" = ${userId}` : ''}
                    ${propertyId !== null && propertyId !== undefined ? `AND "Unit"."propertyId" = ${propertyId}` : ''}
                    ${startDate !== null && startDate !== undefined ? `AND "Unit"."createdAt" >= '${startDate}'::date` : ''}
                    ${endDate !== null && endDate !== undefined ? `AND "Unit"."createdAt" <= '${endDate}'::date` : ''}
                    
                    ) 
            )`
            );

        return parseInt(res[0][0].count);
    };


    return Maintenance;
};
