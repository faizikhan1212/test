'use strict';
module.exports = (sequelize, DataTypes) => {

    /**
   * @swagger
   * definitions:
   *   Company:
   *     properties:
   *       id:
   *         type: integer
   *         format: int64
   *         description: Database identifier, unique
   *       name:
   *         type: string
   *         minLength: 2
   *         maxLength: 255
   *         description: Name of company
   *         required: true
   *       slug:
   *         type: string
   *         minLength: 2
   *         maxLength: 255
   *         description: slug of company, unique
   *       address:
   *         type: string
   *         minLength: 2
   *         maxLength: 255
   *         required: true
   *       email:
   *         type: string
   *         minLength: 2
   *         maxLength: 255
   *         required: false
   *       phones:
   *          type: array
   *          items:
   *            type: string
   *       createdAt:
   *         type: string
   *         format: dateTime
   *         description: Automatically generated
   *       updatedAt:
   *         type: string
   *         format: dateTime
   *         description: Automatically generated
   */

    const Company = sequelize.define('Company', {
        name: {
            type: DataTypes.STRING,
            unique: false,
            allowNull: true,
        },
        slug: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: true,
        },
        address: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        email: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        phones: {
            type: DataTypes.ARRAY(DataTypes.STRING),
            allowNull: true,
        },
    }, {
        freezeTableName: true,
    });

    return require('./repositories/Company')(Company, sequelize);

};
