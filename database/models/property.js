'use strict';
module.exports = (sequelize, DataTypes) => {

    /**
   * @swagger
   * definitions:
   *   Property:
   *     properties:
   *       id:
   *         type: integer
   *         format: int64
   *         description: Database identifier, unique
   *       userId:
   *         type: integer
   *         description: User identifier
   *       type:
   *         type: string
   *         required: true
   *         enum: [
   *             Single-Family House,
   *             Multi-Family House,
   *             Condo,
   *             Townhouse,
   *             Apartment
   *         ]
   *         description: Type of property
   *       propertyName:
   *          type: string
   *          required: true
   *          minLength: 2
   *          maxLength: 255
   *          description: Name of property
   *       numberUnits:
   *          type: number
   *          minLength: 1
   *          description: Number units in property
   *       leaseTerm:
   *          type: string
   *          required: true
   *          enum: [Month-to-month, 1 year, 6 months]
   *       amenities:
   *          type: array
   *          items:
   *              type: string
   *       utilities:
   *          type: array
   *          items:
   *              type: string
   *       address:
   *         required: true
   *         type: string
   *         minLength: 2
   *         maxLength: 255
   *       parking:
   *          required: true
   *          type: boolean
   *       parkingCost:
   *          required: false
   *          type: number
   *       parkingCovered:
   *          required: false
   *          type: boolean
   *       parkingSecured:
   *          required: false
   *          type: boolean
   *       petFriendly:
   *          required: true
   *          type: boolean
   *       petPolicy:
   *          required: false
   *          type: string
   *          minLength: 2
   *          maxLength: 5000
   *       applicationFee:
   *          required: true
   *          type: boolean
   *       appFeeNonRefundable:
   *          required: false
   *          type: boolean
   *       applicationFeeCost:
   *          required: false
   *          type: number
   *       createdAt:
   *         type: string
   *         format: dateTime
   *         description: Automatically generated
   *       updatedAt:
   *         type: string
   *         format: dateTime
   *         description: Automatically generated
   */

    const Property = sequelize.define('Property', {
        userId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'User',
                key: 'id',
                deferrable: sequelize.Deferrable.INITIALLY_IMMEDIATE,
            },
        },
        type: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        amenities: {
            type: DataTypes.ARRAY(DataTypes.STRING),
            allowNull: true,
        },
        utilities: {
            type: DataTypes.ARRAY(DataTypes.STRING),
            allowNull: true,
        },
        propertyName: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        numberUnits: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        leaseTerm: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        address: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        parking: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },
        parkingCost: {
            type: DataTypes.DECIMAL,
            allowNull: true,
        },
        parkingCovered: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },
        parkingSecured: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },
        petFriendly: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },
        petPolicy: {
            type: DataTypes.TEXT,
            allowNull: true,
        },
        applicationFee: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },
        appFeeNonRefundable: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },
        applicationFeeCost: {
            type: DataTypes.DECIMAL,
            allowNull: true,
        },
        noSmoking: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        }, url: {
            type: DataTypes.STRING,
            allowNull: true,
        }
    }, {
        freezeTableName: true,
    });

    return require('./repositories/Property')(Property, sequelize);

};
