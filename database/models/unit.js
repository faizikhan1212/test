'use strict';
module.exports = (sequelize, DataTypes) => {

    /**
   * @swagger
   * definitions:
   *   Unit:
   *     properties:
   *       id:
   *         type: integer
   *         format: int64
   *         description: Database identifier, unique
   *       userId:
   *         type: integer
   *         format: int64
   *         description: User identifier
   *       showingAgent:
   *         type: integer
   *         format: int64
   *         description: Showing agent identifier
   *       propertyId:
   *         type: integer
   *         format: int64
   *         description: Property identifier
   *       rent:
   *          required: true
   *          type: number
   *          minLength: 1
   *          description: Unit rent
   *       deposit:
   *          required: true
   *          type: number
   *          description: Unit deposit
   *       status:
   *          required: true
   *          type: string
   *          enum: [Vacant, Rented, Sold]
   *          description: Unit status
   *       title:
   *          required: true
   *          type: string
   *          minLength: 2
   *          maxLength: 255
   *          description: Unit title
   *       description:
   *          required: true
   *          type: string
   *          minLength: 10
   *          maxLength: 5000
   *          description: Unit description
   *       parking:
   *          required: true
   *          type: boolean
   *       parkingCost:
   *          required: false
   *          type: number
   *       parkingCovered:
   *          required: false
   *          type: boolean
   *       parkingSecured:
   *          required: false
   *          type: boolean
   *       petFriendly:
   *          required: true
   *          type: boolean
   *       petPolicy:
   *          required: false
   *          type: string
   *          minLength: 2
   *          maxLength: 5000
   *       applicationFee:
   *          required: true
   *          type: boolean
   *       appFeeNonRefundable:
   *          required: false
   *          type: boolean
   *       applicationFeeCost:
   *          required: false
   *          type: number
   *       unitShared:
   *          required: true
   *          type: boolean
   *       unitSharedPeople:
   *          required: false
   *          type: number
   *       sharedWashroomBath:
   *          required: true
   *          type: boolean
   *       sharedLaundry:
   *          required: true
   *          type: boolean
   *       bed:
   *          required: true
   *          type: number
   *          minLength: 0
   *          description: Count unit bed
   *       bath:
   *          required: true
   *          type: number
   *          minLength: 0
   *          description: Count unit bath
   *       sqFt:
   *          required: false
   *          type: number
   *          min: 0
   *       leaseTerm:
   *          required: true
   *          type: string
   *          enum: [Month-to-month, 1 year, 6 months]
   *       amenities:
   *          type: array
   *          items:
   *            type: string
   *       utilities:
   *          type: array
   *          items:
   *            type: string
   *       email:
   *          required: true
   *          type: string
   *          minLength: 2
   *          maxLength: 255
   *       phone:
   *          required: true
   *          type: string
   *          minLength: 2
   *          maxLength: 255
   *       unit:
   *          required: true
   *          type: string
   *          minLength: 1
   *          maxLength: 255
   *       address:
   *          required: true
   *          type: string
   *          minLength: 2
   *          maxLength: 255
   *       noSmoking:
   *          required: true
   *          type: boolean
   *       moveInDate:
   *          required: true
   *          type: date
   *       utilityBill:
   *          required: false
   *          type: number
   *       applicationProcess:
   *          required: false
   *          type: string
   *          minLength: 10
   *          maxLength: 5000
   *       appliances:
   *          required: false
   *          type: array
   *          items:
   *              type: string
   *       createdAt:
   *         type: string
   *         format: dateTime
   *         description: Automatically generated
   *       updatedAt:
   *         type: string
   *         format: dateTime
   *         description: Automatically generated
   */

    const Unit = sequelize.define('Unit', {
        userId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'User',
                key: 'id',
                deferrable: sequelize.Deferrable.INITIALLY_IMMEDIATE,
            },
        },
        showingAgent: {
            type: DataTypes.INTEGER,
            allowNull: true,
            references: {
                model: 'User',
                key: 'id',
                deferrable: sequelize.Deferrable.INITIALLY_IMMEDIATE,
            },
        },
        propertyId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'Property',
                key: 'id',
                deferrable: sequelize.Deferrable.INITIALLY_IMMEDIATE,
            },
        },
        rent: {
            type: DataTypes.DECIMAL,
            allowNull: false,
        },
        deposit: {
            type: DataTypes.DECIMAL,
            allowNull: true,
        },
        status: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        title: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        description: {
            type: DataTypes.TEXT,
            allowNull: true,
        },
        amenities: {
            type: DataTypes.ARRAY(DataTypes.STRING),
            allowNull: true,
        },
        utilities: {
            type: DataTypes.ARRAY(DataTypes.STRING),
            allowNull: true,
        },
        parking: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },
        parkingCost: {
            type: DataTypes.DECIMAL,
            allowNull: true,
        },
        parkingCovered: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },
        parkingSecured: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },
        petFriendly: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },
        petPolicy: {
            type: DataTypes.TEXT,
            allowNull: true,
        },
        bed: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        bath: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        sqFt: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        unitShared: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },
        unitSharedPeople: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        sharedWashroomBath: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },
        sharedLaundry: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },
        applicationFee: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },
        appFeeNonRefundable: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },
        applicationFeeCost: {
            type: DataTypes.DECIMAL,
            allowNull: true,
        },
        leaseTerm: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        email: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        phone: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        unit: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        address: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        noSmoking: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },
        moveInDate: {
            type: DataTypes.DATE,
            allowNull: false,
        },
        utilityBill: {
            type: DataTypes.DECIMAL,
            allowNull: true,
        },
        applicationProcess: {
            type: DataTypes.TEXT,
            allowNull: true,
        },
        appliances: {
            type: DataTypes.ARRAY(DataTypes.STRING),
            allowNull: true,
        },
    }, {
        freezeTableName: true,
    });

    return require('./repositories/Unit')(Unit, sequelize);

};
