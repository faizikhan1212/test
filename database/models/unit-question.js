'use strict';
module.exports = (sequelize, DataTypes) => {

    /**
   * @swagger
   * definitions:
   *   UnitQuestion:
   *     properties:
   *       id:
   *         type: integer
   *         format: int64
   *         description: Database identifier, unique
   *       unitPreQualificationId:
   *         type: Integer
   *         description: unitPreQualification identifier
   *       mandatory:
   *          required: true
   *          type: Boolean
   *       type:
   *         required: true
   *         type: string
   *         enum: [Text, Numeric, Yes/No]
   *         description: Type of question
   *       question:
   *          required: true
   *          type: string
   *          minLength: 2
   *          maxLength: 255
   *          description: Text of question
   *       answer:
   *          required: true
   *          type: string
   *          minLength: 2
   *          maxLength: 255
   *          description: Answer for question
   *       createdAt:
   *         type: string
   *         format: dateTime
   *         description: Automatically generated
   *       updatedAt:
   *         type: string
   *         format: dateTime
   *         description: Automatically generated
   */

    const UnitQuestion = sequelize.define(
        'UnitQuestion',
        {
            unitPreQualificationId: {
                type: DataTypes.INTEGER,
                allowNull: false,
                references: {
                    model: 'UnitPreQualification',
                    key: 'id',
                    deferrable: sequelize.Deferrable.INITIALLY_IMMEDIATE,
                },
            },
            mandatory: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
            },
            type: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            question: {
                type: DataTypes.TEXT,
                allowNull: false,
            },
            answer: {
                type: DataTypes.TEXT,
                allowNull: true,
            },
        },
        {
            freezeTableName: true,
        },
    );

    return require('./repositories/Unit-question')(UnitQuestion, sequelize);
};
