'use strict';
module.exports = (sequelize, DataTypes) => {

    /**
   * @swagger
   * definitions:
   *   Subscription:
   *     properties:
   *       id:
   *         type: integer
   *         format: int64
   *         description: Database identifier, unique
   *       userId:
   *         type: integer
   *         description: User identifier
   *       subscription:
   *         type: string
   *         minLength: 2
   *         maxLength: 255
   *         description: Subscription ID
   *         required: true
   *       createdAt:
   *         type: string
   *         format: dateTime
   *         description: Automatically generated
   *       updatedAt:
   *         type: string
   *         format: dateTime
   *         description: Automatically generated
   */

    return sequelize.define('Subscription', {
        userId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'User',
                key: 'id',
                deferrable: sequelize.Deferrable.INITIALLY_IMMEDIATE,
            },
        },
        subscription: {
            type: DataTypes.STRING,
            allowNull: false,
        },
    }, {
        freezeTableName: true,
    });
};
