'use strict';

/**
 * @swagger
 * definitions:
 *   Dictionary:
 *     properties:
 *       id:
 *         type: integer
 *         format: int64
 *         description: Database identifier, unique
 *       companyId:
 *         type: integer
 *         description: Company identifier
 *       amenities:
 *         type: jsonb
 *         required: false
 *       utilities:
 *         type: jsonb
 *         required: false
 *       appliances:
 *         type: jsonb
 *         required: false
 *       createdAt:
 *         type: string
 *         format: dateTime
 *         description: Automatically generated
 *       updatedAt:
 *         type: string
 *         format: dateTime
 *         description: Automatically generated
 */


module.exports = (sequelize, DataTypes) => {
    const Dictionary = sequelize.define('Dictionary', {
        companyId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'Company',
                key: 'id',
                deferrable: sequelize.Deferrable.INITIALLY_IMMEDIATE,
            },
        },
        amenities: {
            type: DataTypes.JSONB,
            allowNull: true,
        },
        utilities: {
            type: DataTypes.JSONB,
            allowNull: true,
        },
        appliances: {
            type: DataTypes.JSONB,
            allowNull: true,
        },
    }, {
        freezeTableName: true,
    });

    return require('./repositories/Dictionary')(Dictionary, sequelize);

};
