'use strict';
module.exports = (sequelize, DataTypes) => {

    /**
   * @swagger
   * definitions:
   *   User:
   *     properties:
   *       id:
   *         type: integer
   *         format: int64
   *         description: Database identifier, unique
   *       name:
   *         type: string
   *         minLength: 2
   *         maxLength: 255
   *         description: User name
   *       email:
   *         type: string
   *         description: User email
   *       removedEmail:
   *         type: string
   *         description: User's removed email
   *       phone:
   *         type: string
   *         minLength: 2
   *         maxLength: 255
   *         description: User phone
   *       ext:
   *         type: string
   *         minLength: 1
   *         maxLength: 10
   *         description: User phone ext
   *       status:
   *         type: string
   *         description: User status
   *         enum: [Verified, Invited, Deleted]
   *       confirm:
   *         type: boolean
   *         description: Confirm account by email
   *       role:
   *          type: string
   *          description: User role
   *          enum: [Owner, Member]
   *       billingPlan:
   *         type: string
   *         description: User billing Plan (only for Owner)
   *       billingExpire:
   *         type: string
   *         format: dateTime
   *         description: User billing Expire (only for Owner)
   *       createdAt:
   *         type: string
   *         format: dateTime
   *         description: Automatically generated
   *       updatedAt:
   *         type: string
   *         format: dateTime
   *         description: Automatically generated
   */

    const User = sequelize.define('User', {
        companyId: {
            type: DataTypes.INTEGER,
            allowNull: true,
            references: {
                model: 'Company',
                key: 'id',
                deferrable: sequelize.Deferrable.INITIALLY_IMMEDIATE,
            },
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        email: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false,
        },
        confirm: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        },
        status: {
            type: DataTypes.STRING,
            defaultValue: 'Active',
            allowNull: false,
        },
        removedEmail: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        role: {
            type: DataTypes.STRING,
            defaultValue: 'Owner',
            allowNull: false,
        },
        phone: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        ext: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        billingPlan: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        billingExpire: {
            type: DataTypes.DATE,
            allowNull: true,
        },
        subscription: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
            defaultValue: false,
        },
        maxUnits: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
    }, {
        freezeTableName: true,
    });

    return require('./repositories/User')(User, sequelize);
};
