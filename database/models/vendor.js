'use strict';

/**
 * @swagger
 * definitions:
 *   Vendor:
 *     properties:
 *       id:
 *         type: integer
 *         format: int64
 *         description: Database identifier, unique
 *       userId:
 *         type: integer
 *         required: true
 *         description: User identifier
 *       propertyId:
 *         type: integer
 *         required: true
 *         description: Property identifier
 *       name:
 *         required: true
 *         type: string
 *       email:
 *          required: true
 *          type: string
 *       phone:
 *         required: true
 *         type: string
 *       areasCovered:
 *          type: array
 *          items:
 *              type: string
 *       createdAt:
 *         type: string
 *         format: dateTime
 *         description: Automatically generated
 *       updatedAt:
 *         type: string
 *         format: dateTime
 *         description: Automatically generated
 */

module.exports = (sequelize, DataTypes) => {
    const Vendor = sequelize.define('Vendor', {
        userId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'User',
                key: 'id',
                deferrable: sequelize.Deferrable.INITIALLY_IMMEDIATE,
            },
        },
        propertyId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'Property',
                key: 'id',
                deferrable: sequelize.Deferrable.INITIALLY_IMMEDIATE,
            },
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        phone: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        areasCovered: {
            type: DataTypes.ARRAY(DataTypes.STRING),
            allowNull: false,
        },
    }, {
        freezeTableName: true,
    });

    return require('./repositories/Vendor')(Vendor, sequelize);
};
