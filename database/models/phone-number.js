'use strict';
module.exports = (sequelize, DataTypes) => {

    /**
   * @swagger
   * definitions:
   *   PhoneNumber:
   *     properties:
   *       id:
   *         type: integer
   *         format: int64
   *         description: Database identifier, unique
   *       userId:
   *         type: integer
   *         description: User identifier
   *       phoneNumber:
   *         type: string
   *         minLength: 2
   *         maxLength: 255
   *         description: Phone Number
   *         required: true
   *       createdAt:
   *         type: string
   *         format: dateTime
   *         description: Automatically generated
   *       updatedAt:
   *         type: string
   *         format: dateTime
   *         description: Automatically generated
   */

    return sequelize.define('PhoneNumber', {
        userId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'User',
                key: 'id',
                deferrable: sequelize.Deferrable.INITIALLY_IMMEDIATE,
            },
        },
        phoneNumber: {
            type: DataTypes.STRING,
            allowNull: false,
        },
    }, {
        freezeTableName: true,
    });
};