'use strict';
module.exports = (sequelize, DataTypes) => {

    /**
   * @swagger
   * definitions:
   *   Maintenance:
   *     properties:
   *       id:
   *         required: true
   *         type: integer
   *         format: int64
   *         description: Database identifier, unique
   *       leadId:
   *         required: true
   *         type: integer
   *         description: Unit identifier
   *       vendorId:
   *         type: integer
   *         description: Unit identifier
   *       tenantName:
   *         required: true
   *         type: string
   *       category:
   *          required: true
   *          type: string
   *          enum: [Heat, Water, Alarm, Security, Other]
   *       description:
   *         required: true
   *         type: string
   *       cost:
   *         type: integer
   *       budget:
   *         type: integer
   *       estimate:
   *         type: integer
   *       status:
   *          required: true
   *          type: string
   *          enum: [Heat, Water, Alarm, Security, Other]
   *       dateScheduled:
   *          type: string
   *          format: DateTime
   *       dateCompleted:
   *          type: string
   *          format: dateTime
   *       createdAt:
   *         type: string
   *         format: dateTime
   *         description: Automatically generate
   *       updatedAt:
   *         type: string
   *         format: dateTime
   *         description: Automatically generated
   */

    const Maintenance = sequelize.define('Maintenance', {
        leadId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'Lead',
                key: 'id',
                deferrable: sequelize.Deferrable.INITIALLY_IMMEDIATE,
            },
        },
        vendorId: {
            type: DataTypes.INTEGER,
            allowNull: true,
            references: {
                model: 'Vendor',
                key: 'id',
                deferrable: sequelize.Deferrable.INITIALLY_IMMEDIATE,
            },
        },
        tenantName: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        status: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 'Not Started',
        },
        category: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        description: {
            type: DataTypes.TEXT,
            allowNull: false,
        },
        budget: {
            type: DataTypes.DECIMAL,
            allowNull: true,
        },
        cost: {
            type: DataTypes.DECIMAL,
            allowNull: true,
        },
        estimate: {
            type: DataTypes.DECIMAL,
            allowNull: true,
        },
        dateScheduled: {
            type: DataTypes.DATE,
            allowNull: true,
        },
        dateCompleted: {
            type: DataTypes.DATE,
            allowNull: true,
        },
    }, {
        freezeTableName: true,
    });

    return require('./repositories/Maintenance')(Maintenance, sequelize);
};
