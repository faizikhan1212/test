'use strict';
module.exports = (sequelize, DataTypes) => {

    /**
   * @swagger
   * definitions:
   *   Availability:
   *     properties:
   *       id:
   *         type: integer
   *         format: int64
   *         description: Database identifier, unique
   *       userId:
   *         type: integer
   *         description: User identifier
   *       sundayFrom:
   *         type: string
   *         format: date
   *         required: true
   *       sundayTo:
   *          type: string
   *          format: date
   *          required: true
   *       mondayFrom:
   *         type: string
   *         format: date
   *         required: true
   *       mondayTo:
   *          type: string
   *          format: date
   *          required: true
   *       tuesdayFrom:
   *         type: string
   *         format: date
   *         required: true
   *       tuesdayTo:
   *          type: string
   *          format: date
   *          required: true
   *       wednesdayFrom:
   *          type: string
   *          format: date
   *          required: true
   *       wednesdayTo:
   *          type: string
   *          format: date
   *          required: true
   *       thursdayFrom:
   *          type: string
   *          format: date
   *          required: true
   *       thursdayTo:
   *          type: string
   *          format: date
   *          required: true
   *       fridayFrom:
   *          type: string
   *          format: date
   *          required: true
   *       fridayTo:
   *          type: string
   *          format: date
   *          required: true
   *       saturdayFrom:
   *          type: string
   *          format: date
   *          required: true
   *       saturdayTo:
   *          type: string
   *          format: date
   *          required: true
   *       limitProspects:
   *          type: integer
   *          min: 0
   *          required: true
   *       showingSlot:
   *         type: integer
   *         min: 0
   *         required: true
   *       otherProperties:
   *         type: boolean
   *         required: true
   *       stopAppointments:
   *         type: integer
   *         min: 0
   *         required: true
   *       maxShowings:
   *         type: integer
   *         min: 0
   *         required: true
   *       bufferTime:
   *         type: integer
   *         min: 0
   *         required: false
   *       createdAt:
   *         type: string
   *         format: dateTime
   *         description: Automatically generated
   *       updatedAt:
   *         type: string
   *         format: dateTime
   *         description: Automatically generated
   */

    return sequelize.define('Availability', {
        userId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'User',
                key: 'id',
                deferrable: sequelize.Deferrable.INITIALLY_IMMEDIATE,
            },
        },
        sundayFrom: {
            type: DataTypes.TIME,
            allowNull: true,
        },
        sundayTo: {
            type: DataTypes.TIME,
            allowNull: true,
        },
        mondayFrom: {
            type: DataTypes.TIME,
            allowNull: true,
        },
        mondayTo: {
            type: DataTypes.TIME,
            allowNull: true,
        },
        tuesdayFrom: {
            type: DataTypes.TIME,
            allowNull: true,
        },
        tuesdayTo: {
            type: DataTypes.TIME,
            allowNull: true,
        },
        wednesdayFrom: {
            type: DataTypes.TIME,
            allowNull: true,
        },
        wednesdayTo: {
            type: DataTypes.TIME,
            allowNull: true,
        },
        thursdayFrom: {
            type: DataTypes.TIME,
            allowNull: true,
        },
        thursdayTo: {
            type: DataTypes.TIME,
            allowNull: true,
        },
        fridayFrom: {
            type: DataTypes.TIME,
            allowNull: true,
        },
        fridayTo: {
            type: DataTypes.TIME,
            allowNull: true,
        },
        saturdayFrom: {
            type: DataTypes.TIME,
            allowNull: true,
        },
        saturdayTo: {
            type: DataTypes.TIME,
            allowNull: true,
        },
        limitProspects: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        showingSlot: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        otherProperties: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },
        stopAppointments: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        maxShowings: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        bufferTime: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
    }, {
        freezeTableName: true,
    });

};
