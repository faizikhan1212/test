'use strict';
module.exports = (sequelize, DataTypes) => {

    /**
   * @swagger
   * definitions:
   *   Scheduling:
   *     properties:
   *       id:
   *         type: integer
   *         format: int64
   *         description: Database identifier, unique
   *       userId:
   *         type: integer
   *         description: User identifier
   *       unitId:
   *         type: integer
   *         description: Unit identifier
   *       clientName:
   *         type: string
   *         minLength: 2
   *         maxLength: 255
   *         required: true
   *       clientEmail:
   *         type: string
   *         minLength: 2
   *         maxLength: 255
   *         required: true
   *       clientPhone:
   *         type: string
   *         minLength: 2
   *         maxLength: 255
   *         required: true
   *       dateOfShowing:
   *         type: array
   *         items:
   *              type: string
   *              format: dateTime
   *       notes:
   *          type: string
   *          minLength: 2
   *          maxLength: 255
   *       createdAt:
   *         type: string
   *         format: dateTime
   *         description: Automatically generated
   *       updatedAt:
   *         type: string
   *         format: dateTime
   *         description: Automatically generated
   */

    const Scheduling = sequelize.define('Scheduling', {
        userId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'User',
                key: 'id',
                deferrable: sequelize.Deferrable.INITIALLY_IMMEDIATE,
            },
        },
        calendarAccountId: {
            type: DataTypes.INTEGER,
            allowNull: true,
            references: {
                model: 'CalendarAccounts',
                key: 'id',
                deferrable: sequelize.Deferrable.INITIALLY_IMMEDIATE,
            },
        },
        unitId: {
            type: DataTypes.INTEGER,
            allowNull: true,
            references: {
                model: 'Unit',
                key: 'id',
                deferrable: sequelize.Deferrable.INITIALLY_IMMEDIATE,
            },
        },
        clientName: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        clientEmail: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        clientPhone: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        dateOfShowing: {
            type: DataTypes.RANGE(DataTypes.DATE),
            allowNull: false,
        },
        notes: {
            type: DataTypes.TEXT,
            allowNull: true,
        },
        createdOnApp: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 'lethub',
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
        },
        googleEventId: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        officeEventId: {
            type: DataTypes.STRING,
            allowNull: true,
        },
    }, {
        freezeTableName: true,
    });

    return require('./repositories/Scheduling')(Scheduling, sequelize);
};
