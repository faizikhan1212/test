'use strict';
module.exports = (sequelize, DataTypes) => {
    const CalendarAccount = sequelize.define('CalendarAccount', {
        userId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'User',
                key: 'id',
                deferrable: sequelize.Deferrable.INITIALLY_IMMEDIATE,
            },
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },
        type: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        accessToken: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        idToken: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        tokenType: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        refreshToken: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        scope: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        expiryDate: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        isTwoWaySync: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
        },
        alwaysSync: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: true,
        },
    }, {
        freezeTableName: false,
    });

    return require('./repositories/Calendar-account')(CalendarAccount, sequelize);
};
