'use strict';
module.exports = (sequelize, DataTypes) => {

    /**
   * @swagger
   * definitions:
   *   CBFile:
   *     properties:
   *       id:
   *         type: integer
   *         format: int64
   *         description: Database identifier, unique
   *       leadId:
   *         type: integer
   *         description: Lead identifier, unique
   *       entityName:
   *         type: string
   *         required: true
   *         enum: [Maintenance]
   *         description: Entity type
   *       entityId:
   *          type: integer
   *          required: true
   *          min: 1
   *          description: Entity id
   *       hash:
   *         type: string
   *         minLength: 1
   *       name:
   *         type: string
   *         minLength: 1
   *       type:
   *         type: string
   *         required: true
   *         enum: [Image, File]
   *         description: Entity type
   *       createdAt:
   *         type: string
   *         format: dateTime
   *         description: Automatically generated
   *       updatedAt:
   *         type: string
   *         format: dateTime
   *         description: Automatically generated
   *
   */

    const CBFile = sequelize.define(
        'CBFile',
        {
            leadId: {
                type: DataTypes.INTEGER,
                allowNull: false,
                references: {
                    model: 'Lead',
                    key: 'id',
                    deferrable: sequelize.Deferrable.INITIALLY_IMMEDIATE,
                },
            },
            entityName: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            entityId: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            name: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            type: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            hash: {
                type: DataTypes.STRING,
                unique: true,
                allowNull: false,
            },
        },
        {
            freezeTableName: true,
        });

    return require('./repositories/CBFile')(CBFile, sequelize);

};
