'use strict';
const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const basename = path.basename(__filename);
const { postgres, app } = require('../../config');
const db = {};

const Op = Sequelize.Op;
const operatorsAliases = {
    $notLike: Op.notLike,
};

const sequelize = new Sequelize(postgres.database, postgres.username, postgres.password, {
    dialect: postgres.dialect,
    host: postgres.host,
    port: postgres.port,
    pool: postgres.pool,
    operatorsAliases: operatorsAliases,
    query: {
        raw: true,
    },
    benchmark: app.node_env !== 'production',
});

fs.readdirSync(__dirname)
    .filter(file => {
        return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
    })
    .forEach(file => {
        const model = sequelize['import'](path.join(__dirname, file));
        db[model.name] = model;
    });


Object.keys(db).forEach(modelName => {
    if (db[modelName].associate) {
        db[modelName].associate(db);
    }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
