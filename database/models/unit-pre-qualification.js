'use strict';
module.exports = (sequelize, DataTypes) => {

    /**
   * @swagger
   * definitions:
   *   UnitPreQualification:
   *     properties:
   *       id:
   *         type: integer
   *         format: int64
   *         description: Database identifier, unique
   *       userId:
   *         type: integer
   *         description: User identifier
   *       unitId:
   *         type: integer
   *         description: Unit identifier
   *       pet:
   *         required: true
   *         type: boolean
   *       cat:
   *          required: true
   *          type: integer
   *       dog:
   *         required: true
   *         type: integer
   *       agree:
   *         required: true
   *         type: boolean
   *       createdAt:
   *         type: string
   *         format: DateTime
   *         description: Automatically generated
   *       updatedAt:
   *         type: string
   *         format: DateTime
   *         description: Automatically generated
   */

    const unitPreQualification = sequelize.define(
        'UnitPreQualification',
        {
            userId: {
                type: DataTypes.INTEGER,
                allowNull: false,
                references: {
                    model: 'User',
                    key: 'id',
                    deferrable: sequelize.Deferrable.INITIALLY_IMMEDIATE,
                },
            },
            unitId: {
                type: DataTypes.INTEGER,
                allowNull: false,
                references: {
                    model: 'Unit',
                    key: 'id',
                    deferrable: sequelize.Deferrable.INITIALLY_IMMEDIATE,
                },
            },
            pet: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
            },
            cat: {
                type: DataTypes.INTEGER,
                allowNull: true,
            },
            dog: {
                type: DataTypes.INTEGER,
                allowNull: true,
            },
            accept: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
            },
        },
        {
            freezeTableName: true,
        },
    );

    return require('./repositories/Unit-pre-qualification')(unitPreQualification, sequelize);

};
