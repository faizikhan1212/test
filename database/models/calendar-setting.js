'use strict';
module.exports = (sequelize, DataTypes) => {
    return sequelize.define(
        'CalendarSetting',
        {
            userId: {
                type: DataTypes.INTEGER,
                allowNull: false,
                references: {
                    model: 'User',
                    key: 'id',
                    deferrable: sequelize.Deferrable.INITIALLY_IMMEDIATE,
                },
            },
            isSync: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
            },
        },
        { freezeTableName: false },
    );

};
