'use strict';
module.exports = (sequelize, DataTypes) => {

    /**
   * @swagger
   * definitions:
   *   GeneralSettingEmail:
   *     properties:
   *       id:
   *         type: integer
   *         format: int64
   *         description: Database identifier, unique
   *       userId:
   *         type: integer
   *         description: User identifier
   *       sundayFrom:
   *         type: string
   *         format: date
   *         required: true
   *       sundayTo:
   *          type: string
   *          format: date
   *          required: true
   *       mondayFrom:
   *         type: string
   *         format: date
   *         required: true
   *       mondayTo:
   *          type: string
   *          format: date
   *          required: true
   *       tuesdayFrom:
   *         type: string
   *         format: date
   *         required: true
   *       tuesdayTo:
   *          type: string
   *          format: date
   *          required: true
   *       wednesdayFrom:
   *          type: string
   *          format: date
   *          required: true
   *       wednesdayTo:
   *          type: string
   *          format: date
   *          required: true
   *       thursdayFrom:
   *          type: string
   *          format: date
   *          required: true
   *       thursdayTo:
   *          type: string
   *          format: date
   *          required: true
   *       fridayFrom:
   *          type: string
   *          format: date
   *          required: true
   *       fridayTo:
   *          type: string
   *          format: date
   *          required: true
   *       saturdayFrom:
   *          type: string
   *          format: date
   *          required: true
   *       saturdayTo:
   *          type: string
   *          format: date
   *          required: true
   *       email:
   *          type: string
   *          min: 2
   *          max: 255
   *          required: true
   *       message:
   *         type: string
   *         min: 10
   *         max: 5000
   *         required: true
   *       forwardCall:
   *         type: boolean
   *         required: true
   *       forwardEmail:
   *         type: string
   *         min: 2
   *         max: 255
   *         required: false
   *       createdAt:
   *         type: string
   *         format: dateTime
   *         description: Automatically generated
   *       updatedAt:
   *         type: string
   *         format: dateTime
   *         description: Automatically generated
   */

    return sequelize.define('GeneralSettingEmail', {
        userId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'User',
                key: 'id',
                deferrable: sequelize.Deferrable.INITIALLY_IMMEDIATE,
            },
        },
        sundayFrom: {
            type: DataTypes.TIME,
            allowNull: true,
        },
        sundayTo: {
            type: DataTypes.TIME,
            allowNull: true,
        },
        mondayFrom: {
            type: DataTypes.TIME,
            allowNull: true,
        },
        mondayTo: {
            type: DataTypes.TIME,
            allowNull: true,
        },
        tuesdayFrom: {
            type: DataTypes.TIME,
            allowNull: true,
        },
        tuesdayTo: {
            type: DataTypes.TIME,
            allowNull: true,
        },
        wednesdayFrom: {
            type: DataTypes.TIME,
            allowNull: true,
        },
        wednesdayTo: {
            type: DataTypes.TIME,
            allowNull: true,
        },
        thursdayFrom: {
            type: DataTypes.TIME,
            allowNull: true,
        },
        thursdayTo: {
            type: DataTypes.TIME,
            allowNull: true,
        },
        fridayFrom: {
            type: DataTypes.TIME,
            allowNull: true,
        },
        fridayTo: {
            type: DataTypes.TIME,
            allowNull: true,
        },
        saturdayFrom: {
            type: DataTypes.TIME,
            allowNull: true,
        },
        saturdayTo: {
            type: DataTypes.TIME,
            allowNull: true,
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        message: {
            type: DataTypes.TEXT,
            allowNull: false,
        },
        forwardCall: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
            defaultValue: false,
        },
        forwardEmail: {
            type: DataTypes.STRING,
            allowNull: true,
        },
    }, {
        freezeTableName: true,
    });

};
