'use strict';
module.exports = (sequelize, DataTypes) => {

    /**
   * @swagger
   * definitions:
   *   File:
   *     properties:
   *       id:
   *         type: integer
   *         format: int64
   *         description: Database identifier, unique
   *       userId:
   *         type: integer
   *         description: User identifier
   *       entityName:
   *         type: string
   *         required: true
   *         enum: [property, unit, user, company]
   *         description: Entity type
   *       entityId:
   *          type: integer
   *          required: true
   *          min: 1
   *          description: Entity id
   *       hash:
   *         type: string
   *         minLength: 1
   *       name:
   *         type: string
   *         minLength: 1
   *       type:
   *         type: string
   *         required: true
   *         enum: [image, file]
   *         description: Entity type
   *       createdAt:
   *         type: string
   *         format: dateTime
   *         description: Automatically generated
   *       updatedAt:
   *         type: string
   *         format: dateTime
   *         description: Automatically generated
   *
   */

    return sequelize.define('File', {
        userId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'User',
                key: 'id',
                deferrable: sequelize.Deferrable.INITIALLY_IMMEDIATE,
            },
        },
        entityName: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        entityId: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        hash: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false,
        },
        type: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
    }, {
        freezeTableName: true,
    });

};
