'use strict';
module.exports = (sequelize, DataTypes) => {

    /**
   * @swagger
   * definitions:
   *   PreQualification:
   *     properties:
   *       id:
   *         type: integer
   *         format: int64
   *         description: Database identifier, unique
   *       userId:
   *         type: integer
   *         description: User identifier
   *       pet:
   *         required: true
   *         type: boolean
   *       cat:
   *          required: true
   *          type: integer
   *       dog:
   *         required: true
   *         type: integer
   *       agree:
   *         required: true
   *         type: boolean
   *       createdAt:
   *         type: string
   *         format: DateTime
   *         description: Automatically generated
   *       updatedAt:
   *         type: string
   *         format: DateTime
   *         description: Automatically generated
   */

    return sequelize.define('PreQualification', {
        userId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'User',
                key: 'id',
                deferrable: sequelize.Deferrable.INITIALLY_IMMEDIATE,
            },
        },
        pet: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
        },
        cat: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        dog: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        accept: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
        },
    }, {
        freezeTableName: true,
    });

};
