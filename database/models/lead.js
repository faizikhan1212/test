'use strict';

module.exports = (sequelize, DataTypes) => {

    /**
   * @swagger
   * definitions:
   *   Lead:
   *     properties:
   *       id:
   *         type: integer
   *         format: int64
   *         description: Database identifier, unique
   *       userId:
   *         type: integer
   *         description: User identifier
   *       unitId:
   *         type: integer
   *         description: Unit identifier
   *       name:
   *         type: string
   *         required: true
   *         minLength: 2
   *         maxLength: 255
   *         description: Name of lead
   *       status:
   *         type: string
   *         required: true
   *         enum: [Showing scheduled, Showing cancelled, Showing rescheduled, Viewing requested]
   *       creditScore:
   *         type: string
   *         required: true
   *         minLength: 2
   *         maxLength: 255
   *       phone:
   *         type: string
   *         required: true
   *         minLength: 2
   *         maxLength: 255
   *       email:
   *         type: string
   *         required: true
   *         minLength: 2
   *         maxLength: 255
   *       reason:
   *         type: string
   *         minLength: 2
   *         maxLength: 255
   *         description: Required field if there's disqualified type
   *       type:
   *         type: string
   *         required: true
   *         enum: [Qualified, Disqualified, Waitlist]
   *       createdAt:
   *         type: string
   *         format: dateTime
   *         description: Automatically generated
   *       updatedAt:
   *         type: string
   *         format: dateTime
   *         description: Automatically generated
   */

    const Lead = sequelize.define('Lead', {
        userId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'User',
                key: 'id',
                deferrable: sequelize.Deferrable.INITIALLY_IMMEDIATE,
            },
        },
        unitId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'Unit',
                key: 'id',
                deferrable: sequelize.Deferrable.INITIALLY_IMMEDIATE,
            },
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        status: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        creditScore: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        phone: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        type: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        reason: {
            type: DataTypes.STRING,
            allowNull: true,
        },
    }, {
        freezeTableName: true,
    });

    return require('./repositories/Lead')(Lead, sequelize);

};
