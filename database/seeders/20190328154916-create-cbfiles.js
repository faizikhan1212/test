'use strict';
const path = require('path');
const fs = require('fs');
const _ = require('lodash');
const { Maintenance, CBFile } = require('../models');
const uniqueArrayOfHash = require('../helpers/seeders/make-unique-array-of-hash');

module.exports = {
    up: async (queryInterface) => {
        const date = new Date(2019, 1, 1, 1, 1, 1, 1);

        let files = [];

        fs.readdirSync(path.resolve('storage/app/file/seeds/')).forEach(
            file => {
                files.push(file);
            },
        );

        const hashes = uniqueArrayOfHash(20 * 50000);
        let hashCounter = 0;

        const maintenance = await Maintenance.findAll({
            attributes: ['id', 'leadId'],
            where: {
                createdAt: date,
            },
        });

        let subMaintenance = [];
        for (let i = 0; i < Math.ceil(maintenance.length / 50000); i++) {
            subMaintenance[i] = maintenance.slice(i * 50000, i * 50000 + 50000);
        }


        const cbfilesChunk = (i, n = 0) =>
            _.times(50000, () => {
                const entityName = 'Maintenance';
                const file = _.sample(files);

                let leadId, entityId;

                const hash = hashes[hashCounter];
                const maintenance = subMaintenance[i][n];
                leadId = maintenance.leadId;
                entityId = maintenance.id;

                hashCounter++;
                n++;
                return {
                    leadId: leadId,
                    entityName: entityName,
                    entityId: entityId,
                    hash: hash,
                    name: file,
                    type: 'Image',
                    createdAt: date,
                    updatedAt: date,
                };
            });

        const mapSeries = async (amount) => {
            for (let i = 0; i < amount; i++) {
                await queryInterface.bulkInsert('CBFile', cbfilesChunk(i), {});
            }
        };

        return mapSeries(20);
    },

    down: () => {
        return CBFile.destroy({
            where: {
                createdAt: new Date(2019, 1, 1, 1, 1, 1, 1),
            },
            truncate: true,
            cascade: true,
        });
    },
};
