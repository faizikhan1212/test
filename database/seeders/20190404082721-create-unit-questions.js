'use strict';
const faker = require('faker');
const { UnitPreQualification, UnitQuestion } = require('../models');
const _ = require('lodash');

module.exports = {
    up: async (queryInterface) => {
        const date = new Date(2019, 1, 1, 1, 1, 1, 1);
        let i = 0;
        const unitprequalifications = await UnitPreQualification.findAll({
            attributes: ['id'],
            where: {
                createdAt: date,
            },
        });

        const availabilitiesChunk = () =>
            _.times(50000, () => {

                const unitPreQualificationId = unitprequalifications[i].id;
                const types = ['Numeric', 'Yes/No', 'Text'];
                const type = _.sample(types);

                const answer = () => {
                    switch (type) {
                        case 'Numeric': {
                            return _.random(100);
                        }
                        case 'Yes/No': {
                            return _.sample(['Yes', 'No']);
                        }
                        case 'Text': {
                            return faker.name.title();
                        }
                    }
                };

                i++;
                return {
                    unitPreQualificationId: unitPreQualificationId,
                    mandatory: _.sample([true, false]),
                    type: type,
                    question: faker.name.title(),
                    answer: answer(),
                    createdAt: date,
                    updatedAt: date,
                };
            });

        const mapSeries = async (amount) => {
            for (let i = 0; i < amount; i++) {
                await queryInterface.bulkInsert(
                    'UnitQuestion',
                    availabilitiesChunk(),
                    {},
                );
            }
        };

        return mapSeries(20);
    },

    down: () => {
        return UnitQuestion.destroy({
            where: {
                createdAt: new Date(2019, 1, 1, 1, 1, 1, 1),
            },
            truncate: true,
            cascade: true,
        });
    },
};
