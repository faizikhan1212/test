'use strict';
const faker = require('faker');
const _ = require('lodash');
const { Lead, Unit, Property, Note } = require('../models');

module.exports = {
    up: async (queryInterface) => {
        const date = new Date(2019, 1, 1, 1, 1, 1, 1);

        const leads = await Lead.findAll({
            attributes: ['id', 'userId'],
            where: {
                createdAt: date,
            },
        });

        let subLeads = [];
        for (let i = 0; i < Math.ceil(leads.length / 50000); i++) {
            subLeads[i] = leads.slice(i * 50000, i * 50000 + 50000);
        }


        const units = await Unit.findAll({
            attributes: ['id', 'userId'],
            where: {
                createdAt: date,
            },
        });

        let subUnits = [];
        for (let i = 0; i < Math.ceil(units.length / 50000); i++) {
            subUnits[i] = units.slice(i * 50000, i * 50000 + 50000);
        }

        const properties = await Property.findAll({
            attributes: ['id', 'userId'],
            where: {
                createdAt: date,
            },
        });

        let subProperties = [];
        for (let i = 0; i < Math.ceil(properties.length / 50000); i++) {
            subProperties[i] = properties.slice(i * 50000, i * 50000 + 50000);
        }

        const notesLeadChunk = (i, n = 0) =>
            _.times(50000, () => {
                const entityName = 'Lead';

                let userId, entityId;

                const lead = subLeads[i][n];
                userId = lead.userId;
                entityId = lead.id;

                n++;
                return {
                    userId: userId,
                    entityName: entityName,
                    entityId: entityId,
                    text: faker.name.title(),
                    createdAt: date,
                    updatedAt: date,
                };
            });

        const notesPropertyChunk = (i, n = 0) =>
            _.times(50000, () => {
                const entityName = 'Property';

                let userId, entityId;

                const property = subProperties[i][n];
                userId = property.userId;
                entityId = property.id;

                n++;
                return {
                    userId: userId,
                    entityName: entityName,
                    entityId: entityId,
                    text: faker.name.title(),
                    createdAt: date,
                    updatedAt: date,
                };
            });

        const notesUnitChunk = (i, n = 0) =>
            _.times(50000, () => {
                const entityName = 'Unit';

                let userId, entityId;

                const unit = subUnits[i][n];
                userId = unit.userId;
                entityId = unit.id;

                n++;
                return {
                    userId: userId,
                    entityName: entityName,
                    entityId: entityId,
                    text: faker.name.title(),
                    createdAt: date,
                    updatedAt: date,
                };
            });

        const mapSeries = async (amountLeadsChunk, amountPropertiesChunk, amountUnitsChunk) => {
            for (let i = 0; i < amountLeadsChunk; i++) {
                await queryInterface.bulkInsert('Note', notesLeadChunk(i), {});
            }
            for (let i = 0; i < amountPropertiesChunk; i++) {
                await queryInterface.bulkInsert('Note', notesPropertyChunk(i), {});
            }
            for (let i = 0; i < amountUnitsChunk; i++) {
                await queryInterface.bulkInsert('Note', notesUnitChunk(i), {});
            }
        };

        return mapSeries(20, 20, 100);
    },

    down: () => {
        return Note.destroy({
            where: {
                createdAt: new Date(2019, 1, 1, 1, 1, 1, 1),
            },
            truncate: true,
            cascade: true,
        });
    },
};
