'use strict';

const faker = require('faker');
const { Unit, Lead } = require('../models');
const _ = require('lodash');

module.exports = {
    up: async (queryInterface) => {
        const date = new Date(2019, 1, 1, 1, 1, 1, 1);

        const units = await Unit.findAll({
            attributes: ['id', 'userId'],
            where: {
                createdAt: date,
            },
            limit: 1000000,
            offset: 0,
        });

        const leadsChunk = () =>
            _.times(50000, () => {
                const unit = _.sample(units);

                const name =
          faker.name.title() +
          ' ' +
          faker.random.number(10000) +
          ' ' +
          faker.random.number(10000) +
          ' ' +
          faker.lorem.word();

                return {
                    userId: unit.userId,
                    unitId: unit.id,
                    name: name,
                    status: _.sample([
                        'Showing scheduled',
                        'Showing cancelled',
                        'Showing rescheduled',
                        'Viewing requested',
                    ]),
                    creditScore: faker.lorem.word(),
                    phone:
            faker.phone.phoneNumberFormat() +
            '-' +
            faker.random.number(100),
                    email: faker.internet.email(),
                    reason: faker.name.title(),
                    type: _.sample(['Qualified', 'Disqualified', 'Waitlist']),
                    createdAt: date,
                    updatedAt: date,
                };
            });

        const mapSeries = async (amount) => {
            for (let i = 0; i < amount; i++) {
                await queryInterface.bulkInsert('Lead', leadsChunk(), {});
            }
        };

        return mapSeries(20);
    },

    down: () => {
        return Lead.destroy({
            where: {
                createdAt: new Date(2019, 1, 1, 1, 1, 1, 1),
            },
            truncate: true,
            cascade: true,
        });
    },
};
