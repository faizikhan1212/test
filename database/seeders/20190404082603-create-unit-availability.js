'use strict';
const faker = require('faker');
const { Unit, UnitAvailability } = require('../models');
const _ = require('lodash');

module.exports = {
    up: async (queryInterface) => {
        const date = new Date(2019, 1, 1, 1, 1, 1, 1);
        let i = 0;

        const units = await Unit.findAll({
            attributes: ['id', 'userId'],
            where: {
                createdAt: date,
            },
            offset: 0,
            limit: 1000000,
        });

        const availabilitiesChunk = () =>
            _.times(50000, () => {

                const unitId = units[i].id;
                const userId = units[i].userId;

                i++;
                return {
                    userId: userId,
                    unitId: unitId,
                    sundayFrom: date,
                    sundayTo: date,
                    mondayFrom: date,
                    mondayTo: date,
                    tuesdayFrom: date,
                    tuesdayTo: date,
                    wednesdayFrom: date,
                    wednesdayTo: date,
                    thursdayFrom: date,
                    thursdayTo: date,
                    fridayFrom: date,
                    fridayTo: date,
                    saturdayFrom: date,
                    saturdayTo: date,
                    limitProspects: faker.random.number(100),
                    showingSlot: faker.random.number(100),
                    otherProperties: _.sample([true, false]),
                    stopAppointments: faker.random.number(100),
                    maxShowings: faker.random.number(100),
                    createdAt: date,
                    updatedAt: date,
                };
            });

        const mapSeries = async (amount) => {
            for (let i = 0; i < amount; i++) {
                await queryInterface.bulkInsert(
                    'UnitAvailability',
                    availabilitiesChunk(),
                    {},
                );
            }
        };

        return mapSeries(20);
    },

    down: () => {
        return UnitAvailability.destroy({
            where: {
                createdAt: new Date(2019, 1, 1, 1, 1, 1, 1),
            },
            truncate: true,
            cascade: true,
        });
    },
};
