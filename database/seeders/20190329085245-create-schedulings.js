'use strict';

const faker = require('faker');
const { Unit, Scheduling } = require('../models');
const _ = require('lodash');

module.exports = {
    up: async (queryInterface) => {
        const date = new Date(2019, 1, 1, 1, 1, 1, 1);

        const units = await Unit.findAll({
            attributes: ['id', 'userId'],
            where: {
                createdAt: date,
            },
            limit: 1000000,
            offset: 0,
        });

        const schedulingsChunk = () =>
            _.times(50000, () => {
                const unit = _.sample(units);

                const name = faker.name.title();

                return {
                    userId: unit.userId,
                    unitId: unit.id,
                    clientName: name,
                    clientEmail: faker.internet.email(),
                    clientPhone: faker.phone.phoneNumberFormat(),
                    dateOfShowing: '["2014-01-01 00:00:00+01","2015-01-01 00:00:00+01")',
                    createdAt: date,
                    updatedAt: date,
                };
            });

        const mapSeries = async (amount) => {
            for (let i = 0; i < amount; i++) {
                await queryInterface.bulkInsert('Scheduling', schedulingsChunk(), {});
            }
        };

        return mapSeries(20);
    },

    down: () => {
        return Scheduling.destroy({
            where: {
                createdAt: new Date(2019, 1, 1, 1, 1, 1, 1),
            },
            truncate: true,
            cascade: true,
        });
    },
};
