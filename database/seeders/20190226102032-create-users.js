'use strict';

const { User, Company } = require('../models');
const faker = require('faker');
const bcrypt = require('bcrypt-nodejs');
const _ = require('lodash');

module.exports = {
    up: async (queryInterface) => {
        const date = new Date(2019, 1, 1, 1, 1, 1, 1);
        const pass = bcrypt.hashSync('Aa12345678!!', bcrypt.genSaltSync());
        let i = 0;

        const companies = await Company.findAll({
            attributes: ['id'],
            where: {
                createdAt: date,
            },
        });

        const usersChunk = () =>
            _.times(50000, () => {
                const firstName =
          faker.name.firstName() + faker.random.number(10000);
                const lastName =
          faker.name.lastName() + faker.random.number(10000);
                const companyId = companies[i].id;

                i++;
                return {
                    companyId: companyId,
                    name: firstName + ' ' + lastName,
                    email:
            firstName.replace('\'', '') +
            '@' +
            lastName.replace('\'', '') +
            '.com',
                    confirm: true,
                    status: 'Active',
                    role: 'Owner',
                    phone: faker.phone.phoneNumberFormat(),
                    ext: faker.random.number(100),
                    password: pass,
                    createdAt: date,
                    updatedAt: date,
                    billingPlan: _.sample(['A', 'B', 'C']),
                    billingExpire: date,
                };
            });

        const mapSeries = async (amount) => {
            for (let i = 0; i < amount; i++) {
                await queryInterface.bulkInsert('User', usersChunk(), {});
            }
        };

        return mapSeries(10);
    },

    down: () => {
        return User.destroy({
            where: {
                createdAt: new Date(2019, 1, 1, 1, 1, 1, 1),
            },
            truncate: true,
            cascade: true,
        });
    },
};
