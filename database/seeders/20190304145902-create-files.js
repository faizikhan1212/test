'use strict';
const path = require('path');
const fs = require('fs');
const _ = require('lodash');
const { Unit, Property, File } = require('../models');
const uniqueArrayOfHash = require('../helpers/seeders/make-unique-array-of-hash');

module.exports = {
    up: async (queryInterface) => {
        const date = new Date(2019, 1, 1, 1, 1, 1, 1);

        let files = [];

        fs.readdirSync(path.resolve('storage/app/file/seeds/')).forEach(
            file => {
                files.push(file);
            },
        );

        const hashes = uniqueArrayOfHash(120 * 50000);
        let hashCounter = 0;

        const units = await Unit.findAll({
            attributes: ['id', 'userId'],
            where: {
                createdAt: date,
            },
        });

        let subUnits = [];
        for (let i = 0; i < Math.ceil(units.length / 50000); i++) {
            subUnits[i] = units.slice(i * 50000, i * 50000 + 50000);
        }

        const properties = await Property.findAll({
            attributes: ['id', 'userId'],
            where: {
                createdAt: date,
            },
        });

        let subProperties = [];
        for (let i = 0; i < Math.ceil(properties.length / 50000); i++) {
            subProperties[i] = properties.slice(i * 50000, i * 50000 + 50000);
        }

        const filesPropertyChunk = (i, n = 0) =>
            _.times(50000, () => {
                const entityName = 'Property';
                const file = _.sample(files);

                let userId, entityId;

                const hash = hashes[hashCounter];
                const property = subProperties[i][n];
                userId = property.userId;
                entityId = property.id;

                hashCounter++;
                n++;
                return {
                    userId: userId,
                    entityName: entityName,
                    entityId: entityId,
                    hash: hash,
                    name: file,
                    type: 'Image',
                    createdAt: date,
                    updatedAt: date,
                };
            });

        const filesUnitChunk = (i, n = 0) =>
            _.times(50000, () => {
                const entityName = 'Unit';
                const file = _.sample(files);

                let userId, entityId;

                const hash = hashes[hashCounter];
                const unit = subUnits[i][n];
                userId = unit.userId;
                entityId = unit.id;

                hashCounter++;
                n++;
                return {
                    userId: userId,
                    entityName: entityName,
                    entityId: entityId,
                    hash: hash,
                    name: file,
                    type: 'Image',
                    createdAt: date,
                    updatedAt: date,
                };
            });

        const mapSeries = async (amountPropertiesChunk, amountUnitsChunk) => {
            for (let i = 0; i < amountPropertiesChunk; i++) {
                await queryInterface.bulkInsert(
                    'File',
                    filesPropertyChunk(i),
                    {},
                );
            }
            for (let i = 0; i < amountUnitsChunk; i++) {
                await queryInterface.bulkInsert('File', filesUnitChunk(i), {});
            }
        };

        return mapSeries(20, 100);
    },

    down: () => {
        return File.destroy({
            where: {
                createdAt: new Date(2019, 1, 1, 1, 1, 1, 1),
            },
            truncate: true,
            cascade: true,
        });
    },
};
