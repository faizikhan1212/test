'use strict';
const { Unit, UnitPreQualification } = require('../models');
const _ = require('lodash');

module.exports = {
    up: async (queryInterface) => {
        const date = new Date(2019, 1, 1, 1, 1, 1, 1);
        let i = 0;
        const units = await Unit.findAll({
            attributes: ['id', 'userId'],
            where: {
                createdAt: date,
            },
            offset: 0,
            limit: 1000000,
        });

        const preQualificationsChunk = () =>
            _.times(50000, () => {

                const userId = units[i].userId;
                const unitId = units[i].id;
                const pet = _.sample([true, false]);

                i++;
                return {
                    userId: userId,
                    unitId: unitId,
                    pet: pet,
                    cat: pet ? _.random(23) : 0,
                    dog: pet ? _.random(23) : 0,
                    accept: _.sample([true, false]),
                    createdAt: date,
                    updatedAt: date,
                };
            });

        const mapSeries = async (amount) => {
            for (let i = 0; i < amount; i++) {
                await queryInterface.bulkInsert(
                    'UnitPreQualification',
                    preQualificationsChunk(),
                    {},
                );
            }
        };

        return mapSeries(20);
    },

    down: () => {
        return UnitPreQualification.destroy({
            where: {
                createdAt: new Date(2019, 1, 1, 1, 1, 1, 1),
            },
            truncate: true,
            cascade: true,
        });
    },
};
