'use strict';
const { Unit, Property } = require('../models');
const faker = require('faker');
const _ = require('lodash');

module.exports = {
    up: async (queryInterface) => {
        const date = new Date(2019, 1, 1, 1, 1, 1, 1);

        const properties = await Property.findAll({
            attributes: ['id', 'userId'],
            where: {
                createdAt: date,
            },
        });

        const amenitiesSet = [
            'Bike storage',
            'Patio or balcony',
            'In-Unit Washer & Dryer',
            'Shared Washer & Dryer',
            'Dishwasher in unit',
            'Air-conditioning',
            'Fridge',
            'Stove',
            'Storage',
            'Fitness Center',
            'Backyard',
            'Fireplace',
            'Party room',
            'Pool',
            'High-Tech Appliances',
            'Elevators',
            'Rooftop',
            'Hardwood Floors',
            'Business center',
            'Electric car charging stations',
            'Car sharing service',
            'Grilling/BBQ area',
            'Security guard',
            'Security cameras',
            'Gated access',
            'Recycling center',
        ];
        const utilitiesSet = [
            'Heat',
            'Hydro (Electricity)',
            'Hot Water',
            'Natural Gas',
            'Wireless Internet',
            'Cable TV',
            'Security/alarm system',
            'Trash collection',
        ];

        const unitsChunk = () =>
            _.times(50000, () => {
                const amenities = [...amenitiesSet];
                const utilities = [...utilitiesSet];

                _.times(_.random(1, amenitiesSet.length - 1), () => {
                    amenities.splice(
                        Math.floor(Math.random() * amenities.length),
                        1,
                    );
                });

                _.times(_.random(1, utilitiesSet.length - 1), () => {
                    utilities.splice(
                        Math.floor(Math.random() * utilities.length),
                        1,
                    );
                });

                const description =
          faker.name.title() +
          ' ' +
          faker.random.number(10000) +
          ' ' +
          faker.random.number(10000) +
          ' ' +
          faker.lorem.word();

                const property = _.sample(properties);

                return {
                    userId: property.userId,
                    propertyId: property.id,
                    rent: _.random(100),
                    deposit: _.random(1000000),
                    status: _.sample(['Vacant', 'Rented', 'Sold']),
                    title: faker.name.title(),
                    description: description,
                    parking: _.sample([true, false]),
                    petFriendly: _.sample([true, false]),
                    noSmoking: _.sample([true, false]),
                    bed: _.random(30),
                    bath: _.random(10),
                    leaseTerm: _.sample([
                        'Month-to-month',
                        '1 year',
                        '6 months',
                    ]),
                    amenities: amenities,
                    utilities: utilities,
                    email: faker.internet.email(),
                    phone:
            faker.phone.phoneNumberFormat() +
            '-' +
            faker.random.number(100),
                    unit: faker.lorem.word(),
                    address: faker.address.secondaryAddress(),
                    createdAt: date,
                    updatedAt: date,
                };
            });

        const mapSeries = async (amount) => {
            for (let i = 0; i < amount; i++) {
                await queryInterface.bulkInsert('Unit', unitsChunk(), {});
            }
        };

        return mapSeries(100);
    },

    down: () => {
        return Unit.destroy({
            where: {
                createdAt: new Date(2019, 1, 1, 1, 1, 1, 1),
            },
            truncate: true,
            cascade: true,
        });
    },
};
