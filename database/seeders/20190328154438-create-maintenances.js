'use strict';
const { Maintenance, Lead } = require('../models');
const faker = require('faker');
const _ = require('lodash');

module.exports = {
    up: async (queryInterface) => {
        const date = new Date(2019, 1, 1, 1, 1, 1, 1);

        const leads = await Lead.findAll({
            attributes: ['id'],
            where: {
                createdAt: date,
            },
        });

        const categories = [
            'Heat',
            'Hydro (Electricity)',
            'Hot Water',
            'Natural Gas',
            'Wireless Internet',
            'Cable TV',
            'Security/alarm system',
            'Trash collection',
        ];

        const maintenancesChunk = () =>
            _.times(50000, () => {

                const lead = _.sample(leads);

                return {
                    leadId: lead.id,
                    tenantName: faker.name.title(),
                    category: _.sample(categories),
                    description: faker.name.title(),
                    createdAt: date,
                    updatedAt: date,
                };
            });

        const mapSeries = async (amount) => {
            for (let i = 0; i < amount; i++) {
                await queryInterface.bulkInsert('Maintenance', maintenancesChunk(), {});
            }
        };

        return mapSeries(20);
    },

    down: () => {
        return Maintenance.destroy({
            where: {
                createdAt: new Date(2019, 1, 1, 1, 1, 1, 1),
            },
            truncate: true,
            cascade: true,
        });
    },
};
