'use strict';
const faker = require('faker');
const { PreQualification, Question } = require('../models');
const _ = require('lodash');

module.exports = {
    up: async (queryInterface) => {
        const date = new Date(2019, 1, 1, 1, 1, 1, 1);
        let i = 0;
        const prequalifications = await PreQualification.findAll({
            attributes: ['id'],
            where: {
                createdAt: date,
            },
        });

        const availabilitiesChunk = () =>
            _.times(50000, () => {

                const preQualificationId = prequalifications[i].id;
                const types = ['Numeric', 'Yes/No', 'Text'];
                const type = _.sample(types);

                const answer = () => {
                    switch (type) {
                        case 'Numeric': {
                            return _.random(100);
                        }
                        case 'Yes/No': {
                            return _.sample(['Yes', 'No']);
                        }
                        case 'Text': {
                            return faker.name.title();
                        }
                    }
                };

                i++;
                return {
                    preQualificationId: preQualificationId,
                    mandatory: _.sample([true, false]),
                    type: type,
                    question: faker.name.title(),
                    answer: answer(),
                    createdAt: date,
                    updatedAt: date,
                };
            });

        const mapSeries = async (amount) => {
            for (let i = 0; i < amount; i++) {
                await queryInterface.bulkInsert(
                    'Question',
                    availabilitiesChunk(),
                    {},
                );
            }
        };

        return mapSeries(10);
    },

    down: () => {
        return Question.destroy({
            where: {
                createdAt: new Date(2019, 1, 1, 1, 1, 1, 1),
            },
            truncate: true,
            cascade: true,
        });
    },
};
