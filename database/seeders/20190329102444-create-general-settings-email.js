'use strict';
const faker = require('faker');
const { Email, GeneralSettingEmail } = require('../models');
const _ = require('lodash');

module.exports = {
    up: async (queryInterface) => {
        const date = new Date(2019, 1, 1, 1, 1, 1, 1);
        let i = 0;
        const emails = await Email.findAll({
            attributes: ['email', 'userId'],
            where: {
                createdAt: date,
            },
        });

        const generalSettingsEmailChunk = () =>
            _.times(50000, () => {

                const userId = emails[i].userId;
                const email = emails[i].email;
                i++;
                return {
                    userId: userId,
                    sundayFrom: date,
                    sundayTo: date,
                    mondayFrom: date,
                    mondayTo: date,
                    tuesdayFrom: date,
                    tuesdayTo: date,
                    wednesdayFrom: date,
                    wednesdayTo: date,
                    thursdayFrom: date,
                    thursdayTo: date,
                    fridayFrom: date,
                    fridayTo: date,
                    saturdayFrom: date,
                    saturdayTo: date,
                    email: email,
                    message: faker.name.title(),
                    forwardCall: _.sample([true, false]),
                    forwardEmail: email,
                    createdAt: date,
                    updatedAt: date,
                };
            });

        const mapSeries = async (amount) => {
            for (let i = 0; i < amount; i++) {
                await queryInterface.bulkInsert(
                    'GeneralSettingEmail',
                    generalSettingsEmailChunk(),
                    {},
                );
            }
        };

        return mapSeries(10);
    },

    down: () => {
        return GeneralSettingEmail.destroy({
            where: {
                createdAt: new Date(2019, 1, 1, 1, 1, 1, 1),
            },
            truncate: true,
            cascade: true,
        });
    },
};
