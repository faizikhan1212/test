'use strict';
const faker = require('faker');
const { PhoneNumber, GeneralSettingPhone } = require('../models');
const _ = require('lodash');

module.exports = {
    up: async (queryInterface) => {
        const date = new Date(2019, 1, 1, 1, 1, 1, 1);
        let i = 0;
        const phones = await PhoneNumber.findAll({
            attributes: ['phoneNumber', 'userId'],
            where: {
                createdAt: date,
            },
        });

        const generalSettingsPhoneChunk = () =>
            _.times(50000, () => {

                const userId = phones[i].userId;
                const phoneNumber = phones[i].phoneNumber;
                i++;
                return {
                    userId: userId,
                    sundayFrom: date,
                    sundayTo: date,
                    mondayFrom: date,
                    mondayTo: date,
                    tuesdayFrom: date,
                    tuesdayTo: date,
                    wednesdayFrom: date,
                    wednesdayTo: date,
                    thursdayFrom: date,
                    thursdayTo: date,
                    fridayFrom: date,
                    fridayTo: date,
                    saturdayFrom: date,
                    saturdayTo: date,
                    phoneNumber: phoneNumber,
                    message: faker.name.title(),
                    forwardCall: _.sample([true, false]),
                    forwardPhoneNumber: phoneNumber,
                    createdAt: date,
                    updatedAt: date,
                };
            });

        const mapSeries = async (amount) => {
            for (let i = 0; i < amount; i++) {
                await queryInterface.bulkInsert(
                    'GeneralSettingPhone',
                    generalSettingsPhoneChunk(),
                    {},
                );
            }
        };

        return mapSeries(10);
    },

    down: () => {
        return GeneralSettingPhone.destroy({
            where: {
                createdAt: new Date(2019, 1, 1, 1, 1, 1, 1),
            },
            truncate: true,
            cascade: true,
        });
    },
};
