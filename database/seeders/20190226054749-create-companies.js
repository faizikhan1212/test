'use strict';

const _ = require('lodash');
const faker = require('faker');
const slug = require('slug');
const { Company } = require('../models');

module.exports = {
    up: async (queryInterface) => {
        const date = new Date(2019, 1, 1, 1, 1, 1, 1);

        const companiesChunk = () =>
            _.times(50000, () => {

                const name =
          faker.name.title() +
          ' ' +
          faker.random.number(10000) +
          ' ' +
          faker.random.number(10000) +
          ' ' +
          faker.lorem.word();

                return {
                    name: name,
                    slug: slug(name),
                    address: faker.lorem.sentence(),
                    createdAt: date,
                    updatedAt: date,
                };
            });

        const mapSeries = async (chunksAmount) => {
            for (let i = 0; i < chunksAmount; i++) {
                await queryInterface.bulkInsert(
                    'Company',
                    companiesChunk(),
                    {},
                );
            }
        };

        return mapSeries(10);
    },

    down: () => {
        return Company.destroy({
            where: {
                createdAt: new Date(2019, 1, 1, 1, 1, 1, 1),
            },
            truncate: true,
            cascade: true,
        });
    },
};
