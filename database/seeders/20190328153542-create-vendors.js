'use strict';
const { Vendor, Property } = require('../models');
const faker = require('faker');
const _ = require('lodash');

module.exports = {
    up: async (queryInterface) => {
        const date = new Date(2019, 1, 1, 1, 1, 1, 1);

        const properties = await Property.findAll({
            attributes: ['id', 'userId'],
            where: {
                createdAt: date,
            },
        });

        const areasCovered = [
            'Heat',
            'Hydro (Electricity)',
            'Hot Water',
            'Natural Gas',
            'Wireless Internet',
            'Cable TV',
            'Security/alarm system',
            'Trash collection',
        ];

        const vendorsChunk = () =>
            _.times(50000, () => {

                const property = _.sample(properties);
                const areas = [...areasCovered];

                _.times(_.random(1, areasCovered.length - 1), () => {
                    areas.splice(
                        Math.floor(Math.random() * areas.length),
                        1,
                    );
                });
                return {
                    userId: property.userId,
                    propertyId: property.id,
                    name: faker.name.title(),
                    email: faker.internet.email(),
                    phone: faker.phone.phoneNumberFormat(),
                    areasCovered: areas,
                    createdAt: date,
                    updatedAt: date,
                };
            });

        const mapSeries = async (amount) => {
            for (let i = 0; i < amount; i++) {
                await queryInterface.bulkInsert('Vendor', vendorsChunk(), {});
            }
        };

        return mapSeries(20);
    },

    down: () => {
        return Vendor.destroy({
            where: {
                createdAt: new Date(2019, 1, 1, 1, 1, 1, 1),
            },
            truncate: true,
            cascade: true,
        });
    },
};
