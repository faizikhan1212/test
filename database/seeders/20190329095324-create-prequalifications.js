'use strict';
const { User, PreQualification } = require('../models');
const _ = require('lodash');

module.exports = {
    up: async (queryInterface) => {
        const date = new Date(2019, 1, 1, 1, 1, 1, 1);
        let i = 0;
        const users = await User.findAll({
            attributes: ['id'],
            where: {
                createdAt: date,
            },
        });

        const preQualificationsChunk = () =>
            _.times(50000, () => {

                const userId = users[i].id;
                const pet = _.sample([true, false]);
                i++;
                return {
                    userId: userId,
                    pet: pet,
                    cat: pet ? _.random(23) : 0,
                    dog: pet ? _.random(23) : 0,
                    accept: _.sample([true, false]),
                    createdAt: date,
                    updatedAt: date,
                };
            });

        const mapSeries = async (amount) => {
            for (let i = 0; i < amount; i++) {
                await queryInterface.bulkInsert(
                    'PreQualification',
                    preQualificationsChunk(),
                    {},
                );
            }
        };

        return mapSeries(10);
    },

    down: () => {
        return PreQualification.destroy({
            where: {
                createdAt: new Date(2019, 1, 1, 1, 1, 1, 1),
            },
            truncate: true,
            cascade: true,
        });
    },
};
