'use strict';
const faker = require('faker');
const { User, PhoneNumber } = require('../models');
const _ = require('lodash');

module.exports = {
    up: async (queryInterface) => {
        const date = new Date(2019, 1, 1, 1, 1, 1, 1);
        let i = 0;
        const users = await User.findAll({
            attributes: ['id'],
            where: {
                createdAt: date,
            },
        });

        const phoneNumbersChunk = () =>
            _.times(50000, () => {

                const userId = users[i].id;

                i++;
                return {
                    userId: userId,
                    phoneNumber: faker.phone.phoneNumberFormat(),
                    createdAt: date,
                    updatedAt: date,
                };
            });

        const mapSeries = async (amount) => {
            for (let i = 0; i < amount; i++) {
                await queryInterface.bulkInsert(
                    'PhoneNumber',
                    phoneNumbersChunk(),
                    {},
                );
            }
        };

        return mapSeries(10);
    },

    down: () => {
        return PhoneNumber.destroy({
            where: {
                createdAt: new Date(2019, 1, 1, 1, 1, 1, 1),
            },
            truncate: true,
            cascade: true,
        });
    },
};
