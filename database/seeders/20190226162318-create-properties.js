'use strict';
const faker = require('faker');
const { User, Property } = require('../models');
const _ = require('lodash');

module.exports = {
    up: async (queryInterface) => {
        const date = new Date(2019, 1, 1, 1, 1, 1, 1);

        const users = await User.findAll({
            attributes: ['id'],
            where: {
                createdAt: date,
            },
        });

        const types = [
            'Single-Family House',
            'Multi-Family House',
            'Condo',
            'Townhouse',
            'Apartment',
        ];

        const amenitiesSet = [
            'Bike storage',
            'Patio or balcony',
            'In-Unit Washer & Dryer',
            'Shared Washer & Dryer',
            'Dishwasher in unit',
            'Air-conditioning',
            'Fridge',
            'Stove',
            'Storage',
            'Fitness Center',
            'Backyard',
            'Fireplace',
            'Party room',
            'Pool',
            'High-Tech Appliances',
            'Elevators',
            'Rooftop',
            'Hardwood Floors',
            'Business center',
            'Electric car charging stations',
            'Car sharing service',
            'Grilling/BBQ area',
            'Security guard',
            'Security cameras',
            'Gated access',
            'Recycling center',
        ];
        const utilitiesSet = [
            'Heat',
            'Hydro (Electricity)',
            'Hot Water',
            'Natural Gas',
            'Wireless Internet',
            'Cable TV',
            'Security/alarm system',
            'Trash collection',
        ];

        const leaseTerm = ['6 months', 'Month-to-month', '1 year'];

        const propertiesChunk = () =>
            _.times(50000, () => {
                const amenities = [...amenitiesSet];
                const utilities = [...utilitiesSet];

                _.times(_.random(1, amenitiesSet.length - 1), () => {
                    amenities.splice(
                        Math.floor(Math.random() * amenities.length),
                        1,
                    );
                });

                _.times(_.random(1, utilitiesSet.length - 1), () => {
                    utilities.splice(
                        Math.floor(Math.random() * utilities.length),
                        1,
                    );
                });

                const name =
          faker.name.title() +
          ' ' +
          faker.random.number(10000) +
          ' ' +
          faker.random.number(10000) +
          ' ' +
          faker.lorem.word();

                return {
                    userId: _.sample(users).id,
                    type: _.sample(types),
                    amenities: amenities,
                    utilities: utilities,
                    propertyName: name,
                    numberUnits: _.random(23),
                    leaseTerm: _.sample(leaseTerm),
                    address: faker.address.streetAddress(),
                    parking: _.sample([true, false]),
                    petFriendly: _.sample([true, false]),
                    noSmoking: _.sample([true, false]),
                    createdAt: date,
                    updatedAt: date,
                };
            });

        const mapSeries = async (amount) => {
            for (let i = 0; i < amount; i++) {
                await queryInterface.bulkInsert(
                    'Property',
                    propertiesChunk(),
                    {},
                );
            }
        };

        return mapSeries(20);
    },

    down: () => {
        return Property.destroy({
            where: {
                createdAt: new Date(2019, 1, 1, 1, 1, 1, 1),
            },
            truncate: true,
            cascade: true,
        });
    },
};
