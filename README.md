# Lethub Web

## Step-by-step
1. Run containers
2. Install Packages
3. Create environments
4. Run backend
5. Install relay app (local machine)
6. Create tunnel (local machine)
7. Using webhook is in the dialogflow

#### Docker & Postgres & Redis & Postgres Adminer & Zookeeper & Kafka       
*Run containers with logging*   
`docker-compose up`

*Run containers without logging*     
`docker-compose up -d`

*Stop containers*   
`docker-compose down`

#### Packages
*Install Backend*    
`yarn install`

#### Environments
*Copy .env.example to .env*    
`cp .env.example .env`  
`cp .docker-compose.example .docker-compose`

#### Backend
*Run yarn with nodemon (dev)*   
`yarn start`

*Run yarn without nodemon (dev)*   
`yarn dev`

*Run yarn (prod)*   
`yarn prod`

## Swagger
1. Set up in .env       
`SWAGGER_DOC=true`
2. Start / Restart app *(required restart if need)*     
`yarn start`
3. Open site with uri       
`/doc/v1`

## Create tunnel (local machine)

1. relay start  
`export RELAY_KEY=55d0d738-caa4-4ed6-b1e7-379ac008dd3f`     
`export RELAY_SECRET=jI3dOVR1szab`      
`relay connect -s d97lbipqlfesahm1gwd09n http://localhost:2019`

#### Relay app (local machine)
1. Install on mac           
`curl -sSL https://storage.googleapis.com/webhookrelay/downloads/relay-darwin-amd64 > relay && chmod +wx relay && sudo mv relay /usr/local/bin`
2. Install on windows           
`curl -LO https://storage.googleapis.com/webhookrelay/downloads/relay-windows-amd64.exe`
3. Install on linux     
`curl -sSL https://storage.googleapis.com/webhookrelay/downloads/relay-linux-amd64 > relay && chmod +wx relay && sudo mv relay /usr/local/bin`