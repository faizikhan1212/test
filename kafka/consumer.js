const { hostname } = require('os');
const { KafkaConsumer } = require('node-rdkafka');
const config = require('../config');
const unitFilterEmit = require('./consumes/unit-filter');
const leadCreation = require('./consumes/lead-creation');
const scheduleCreation = require('./consumes/schedule-creation');
const askQuestion = require('./consumes/ask-question');

const host = hostname();
const brokerList = config.kafka.broker_list;
const topicPrefix = config.kafka.topic_prefix;

const globalConfig = {
    'debug': 'all',
    'client.id': ['lethub-monolith', host].join('-'),
    'metadata.broker.list': brokerList,
    'group.id': host,
    'socket.keepalive.enable': true,
    'enable.auto.commit': false,
};
const topicConfig = {
    'auto.offset.reset': 'beginning',
};

const topics = [
    `${topicPrefix}-unit-filter`,
    `${topicPrefix}-lead-creation`,
    `${topicPrefix}-schedule-creation`,
    `${topicPrefix}-ask-question`,
];

const consumer = new KafkaConsumer(globalConfig, topicConfig);

consumer.on('event.log', (log) => {

});

consumer.on('event.error', (err) => {
    console.error('Error from consumer');
    console.error(err);
});

consumer.on('ready', (arg) => {
    console.log('consumer ready.' + JSON.stringify(arg));
    consumer.subscribe(topics);
    consumer.consume();
});

consumer.on('data', (message) => {
    const data = JSON.parse(message.value.toString());
    console.log(data);
    consumer.commit(message);

    switch (message.topic) {
        case `${topicPrefix}-unit-filter`: {
            unitFilterEmit(data);
            return false;
        }
        case `${topicPrefix}-lead-creation`: {
            leadCreation(data);
            return false;
        }
        case `${topicPrefix}-schedule-creation`: {
            scheduleCreation(data);
            return false;
        }
        case `${topicPrefix}-ask-question`: {
            askQuestion(data);
            return false;
        }
    }
});

consumer.connect();
