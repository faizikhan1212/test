const { Unit, Scheduling, Lead } = require('../../database/models');

module.exports = async (data) => {
    const msg = data.msg;

    if (msg !== undefined && msg !== null) {
        const unitId = parseInt(msg.unitId);
        const unit = await Unit.findByPk(unitId);
        const date = Date.parse(msg.text);

        if (!isNaN(date) && date > 0) {
            const moment = require('moment')(date);
            const format = 'YYYY-MM-DDTHH:mm:ssZZ';

            const from = moment.format(format);
            const to = moment.add(30, 'minutes').format(format);
            const phone = msg.phone.substr(-10);

            if (unit !== null) {
                const scheduling = await Scheduling.create({
                    userId: unit.userId,
                    unitId: unitId,
                    clientName: msg.username,
                    clientEmail: '',
                    clientPhone: phone,
                    dateOfShowing: [from, to],
                });

                if (scheduling !== null) {
                    await Lead.update({ status: 'Showing scheduled' }, {
                        where: {
                            unitId: unitId,
                            phone: phone,
                        },
                    });
                }
            }
        }
    }
};

