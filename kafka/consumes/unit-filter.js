const { Unit } = require('../../database/models');
const io = require('../../app/socket-io');

module.exports = async (data) => {
    const $units = Unit.getPublicListByParams(data.company, data.filters, [100, 0]);
    const $countUnits = Unit.countPublicListByParams(data.company, data.filters);

    const [
        [units],
        countUnits,
    ] = [
        await $units,
        await $countUnits,
    ];

    io.emit(data.conToken, { units: units, countUnits: countUnits });
};

