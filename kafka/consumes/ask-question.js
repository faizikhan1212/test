const { Unit, Property } = require('../../database/models');
const answerQuestion = require('../../kafka/produces/answer-question');
const { format } = require('date-fns');

module.exports = async (data) => {
    const { msg, context, socketId } = data;

    const unit = await Unit.findByPk(msg.unitId);
    const property = await Property.findByPk(unit.propertyId);

    if (unit !== null && property !== null) {
        switch (context.type) {
            case 'number':
            case 'string': {

                switch (context.category) {
                    case 'moveInDate': {
                        msg.text = context.template.replace('__template__', unit['rent'] !== null ? unit['rent'] : 0)
                            .replace('__template_second__', unit['deposit'] !== null ? unit['deposit'] : 0)
                            .replace('__template_third__', format(unit['moveInDate'], 'YYYY-MM-DD'));
                        break;
                    }

                    case 'renewLease': {
                        msg.text = context.template.replace('__template__', unit['leaseTerm']);
                        break;
                    }

                    case 'applicationProcess': {
                        const applicationProcess = unit['applicationProcess'];

                        if (applicationProcess !== null && applicationProcess.length > 0) {
                            msg.text = applicationProcess;
                            break;
                        }

                        msg.text = context.template;
                        break;
                    }

                    case 'leaseTermination':
                    case 'sublet':
                    case 'guest':
                    case 'decorating':
                    case 'maintenance':
                    case 'neighbourhood':
                    case 'sourceIncome': {
                        msg.text = context.template;
                        break;
                    }

                    case 'bed': {
                        msg.text = context.template.replace('__template__', unit['bed'])
                            .replace('__template_second__', property['type']);
                        break;
                    }

                    case 'moveIn': {
                        msg.text = context.template.replace('__template__', format(unit['moveInDate'], 'YYYY-MM-DD'));
                        break;
                    }

                    case 'utilityBill': {
                        const utilityBill = unit['utilityBill'];

                        if (utilityBill !== undefined && utilityBill !== null) {
                            const list = unit['utilities'].join(', ');

                            msg.text = context.template
                                .replace('__template__', utilityBill)
                                .replace('__template_second__', list.length > 0 ? list : 'no utilities included');
                            break;
                        }

                        msg.text = 'Utility bills vary based on usage so it’s hard to say accurately but you can discuss this during the tour 🙂';
                        break;
                    }

                    default: {
                        msg.text = context.template.replace('__template__', unit[context.category]);
                    }
                }

                break;
            }
            case 'enum': {

                if (context.query !== undefined) {

                    switch (context.category) {

                        case 'amenitiesCustom': {
                            const items = unit['amenities'];

                            if (items.length > 0) {
                                const list = items.join('|');

                                const regex = new RegExp(list
                                    .replace(' or ', '|')
                                    .replace(' & ', '|'),
                                ['i']);

                                if (regex.test(context.query)) {
                                    msg.text = context.template.replace('__template__', items.join(', '));
                                    break;
                                }
                            }

                            msg.text = `I'm sorry, no ${context.query} are included 😔`;
                            break;
                        }

                        case 'appliancesCustom': {
                            const items = unit['appliances'];

                            if (items.length > 0) {
                                const list = items.join('|');

                                const regex = new RegExp(list
                                    .replace(' or ', '|')
                                    .replace(' & ', '|'),
                                ['i']);

                                if (regex.test(context.query)) {
                                    msg.text = context.template.replace('__template__', list.join(', '));
                                    break;
                                }
                            }

                            msg.text = `I'm sorry, no ${context.query} are included 😔`;
                            break;
                        }

                    }

                    break;
                }

                const list = unit[context.category];

                if (list !== null && list.length > 0) {
                    msg.text = context.template.replace('__template__', list.join(', '));
                    break;
                }

                msg.text = `I'm sorry, no ${context.category} are included 😔`;
                break;
            }
            case 'yes-no': {
                const field = unit[context.category];

                if (field) {

                    switch (context.category) {
                        case 'unitShared': {
                            msg.text = context.templateYes.replace('__template__', unit['unitSharedPeople']);
                            break;
                        }

                        case 'sharedWashroomBath':
                        case 'sharedLaundry':
                        case 'noSmoking': {
                            msg.text = context.templateYes;
                            break;
                        }

                        case 'petFriendly':
                        case 'petPolicy': {
                            const petPolicy = unit['petPolicy'];

                            if ([null, 'null'].includes(petPolicy)) {
                                msg.text = context.templateNo.replace('__template__', msg.company);
                                break;
                            }

                            msg.text = context.templateYes.replace('__template__', petPolicy);
                            break;
                        }

                        case 'applicationFee': {
                            msg.text = context.templateYes
                                .replace('__template__', unit['applicationFeeCost'])
                                .replace('__template_second__', unit['applicationFeeNonRefundable'] !== true ? 'non-refundable' : 'refundable');
                            break;
                        }

                        case 'parking': {
                            msg.text = context.templateYes
                                .replace('__template__', unit['parkingCost'])
                                .replace('__template_second__', unit['parkingCovered'] === true ? 'covered' : '')
                                .replace('__template_third__', unit['parkingSecured'] === true ? 'secured' : '');
                            break;
                        }

                        default: {
                            msg.text = context.templateNo;
                        }
                    }

                    break;
                }

                switch (context.category) {
                    case 'unitShared':
                    case 'sharedLaundry':
                    case 'sharedWashroomBath':
                    case 'applicationFee':
                    case 'noSmoking':
                    case 'parking': {
                        msg.text = context.templateNo;
                        break;
                    }

                    case 'petFriendly':
                    case 'petPolicy': {
                        msg.text = context.templateNo.replace('__template__', msg.company);
                        break;
                    }

                    default: {
                        msg.text = context.templateYes;
                    }
                }

            }
        }

        return answerQuestion(msg, socketId);
    }

    return false;
};

