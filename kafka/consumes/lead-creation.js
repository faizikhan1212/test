const { Lead, Unit } = require('../../database/models');
const calendar = require('../../kafka/produces/calendar');
const leadDisqualified = require('../../kafka/produces/lead-disqualified');
const redis = require('../../app/services/redis');


module.exports = async (data) => {
    const QUALIFIED_TYPE = 'Qualified';
    const DISQUALIFIED_TYPE = 'Disqualified';

    const $unit = Unit.findByPk(data.unitId);
    const $countUnits = Unit.countPublicListByParams(data.company, data.filters, data.unitId);

    const [
        unit,
        countUnits,
    ] = [
        await $unit,
        await $countUnits,
    ];

    const phone = data.phone.substr(-10);

    if (unit !== null) {
        const leadType = countUnits > 0 ? QUALIFIED_TYPE : DISQUALIFIED_TYPE;


        let [[lead]] = await Lead.showByPhoneAndUserId(unit.userId, phone);

        if (!lead) {
            lead = await Lead.create({
                userId: unit.userId,
                unitId: data.unitId,
                type: leadType,
                name: data.name,
                status: data.status,
                creditScore: '',
                phone: phone,
                email: '',
                reason: data.reason,
            });
        }

        const id = (lead && lead.id) || (lead && lead.dataValues && lead.dataValues.id);

        // TODO: If you want you can create table in Postgres DB.
        redis.set(`lead-${id}`, JSON.stringify(data.filters));

        if (lead !== null) {

            switch (leadType) {
                case QUALIFIED_TYPE: {
                    calendar(data);
                    return false;
                }
                case DISQUALIFIED_TYPE: {
                    leadDisqualified(data);
                    return false;
                }
            }
        }
    }

    return false;
};

