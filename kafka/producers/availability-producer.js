const { Producer } = require('node-rdkafka');
const { hostname } = require('os');
const config = require('../../config');

const host = hostname();
const brokerList = config.kafka.broker_list;
const topicPrefix = config.kafka.topic_prefix;

const globalConfig = {
    'debug': 'all',
    'client.id': ['availability', host].join('-'),
    'metadata.broker.list': brokerList,
    'compression.codec': 'gzip',
    'retry.backoff.ms': 200,
    'message.send.max.retries': 10,
    'socket.keepalive.enable': true,
};
const topicConfig = {};

const producer = new Producer(globalConfig, topicConfig);

producer.topic = `${topicPrefix}-availability`;

producer.on('error', (error) => {
    console.error(error);
});

producer.on('ready', (arg) => {
    console.log('producer ready.' + JSON.stringify(arg));
});

producer.on('event.log', (log) => {

});

producer.on('disconnected', (arg) => {
    console.log('producer disconnected. ' + JSON.stringify(arg));
});

producer.connect();

module.exports = producer;