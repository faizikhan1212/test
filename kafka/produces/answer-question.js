const producer = require('../producers/answer-question');

module.exports = async (msg, socketId) => {
    const payload = Buffer.from(JSON.stringify({
        socketId: socketId,
        msg: msg,
    }));

    producer.produce(producer.topic, -1, payload);
};

