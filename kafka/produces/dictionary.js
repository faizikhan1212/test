const { Dictionary, Company } = require('../../database/models');
const producer = require('../producers/dictionary-producer');

module.exports = async (companyId) => {
    const company = await Company.findByPk(companyId);

    if (company !== null) {
        const payload = Buffer.from(JSON.stringify({
            company: company.slug,
            dictionary: await Dictionary.findOne({
                where: {
                    companyId: company.id,
                },
            }),
        }));

        producer.produce(producer.topic, -1, payload);
    }
};

