const { User, Availability, Company } = require('../../database/models');
const producer = require('../producers/availability-producer');

module.exports = async (userId) => {
    const user = await User.findByPk(userId);

    if (user !== null) {
        const company = await Company.findByPk(user.companyId);

        if (company !== null) {
            const payload = Buffer.from(JSON.stringify({
                company: company.slug,
                availability: await Availability.findOne({
                    where: {
                        userId: userId,
                    },
                }),
            }));

            producer.produce(producer.topic, -1, payload);
        }
    }
};

