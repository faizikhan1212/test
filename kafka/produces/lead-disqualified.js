const producer = require('../producers/lead-disqualified-producer');

module.exports = async (data) => {
    const payload = Buffer.from(JSON.stringify({
        socketId: data.socketId,
        msg: data.msg,
    }));

    producer.produce(producer.topic, -1, payload);
};

