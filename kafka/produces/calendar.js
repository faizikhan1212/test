const producer = require('../producers/calendar-producer');
const { Scheduling } = require('../../database/models');
const { addMonths, format } = require('date-fns');

module.exports = async (data) => {
    const msg = data.msg;

    const startDate = new Date();
    const endDate = addMonths(startDate, 1);
    const formatDate = 'YYYY-MM-DDTHH:mm:ssZZ';

    const scheduling = await Scheduling.getCalendarByInterval(msg.company, format(startDate, formatDate), format(endDate, formatDate));

    const payload = Buffer.from(JSON.stringify({
        socketId: data.socketId,
        msg: msg,
        calendar: scheduling[0],
    }));

    producer.produce(producer.topic, -1, payload);
};

