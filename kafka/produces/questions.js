const { Question, Company } = require('../../database/models');
const producer = require('../producers/questions-producer');

module.exports = async (companyId) => {
    const company = await Company.findByPk(companyId);

    if (company !== null) {
        const payload = Buffer.from(JSON.stringify({
            company: company.slug,
            questions: await Question.getQuestionsByCompany(company.slug),
        }));

        producer.produce(producer.topic, -1, payload);
    }
};

