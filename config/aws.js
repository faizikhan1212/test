const aws = {
    bucket: process.env.AWS_BUCKET,
    access_key_id: process.env.AWS_ACCESS_KEY_ID,
    secret_access_key: process.env.AWS_SECRET_ACCESS_KEY,
    region: process.env.AWS_REGION,
    endpoint: process.env.AWS_ENDPOINT,
    link_expiry: process.env.AWS_LINK_EXPIRY,
};

module.exports = aws;
