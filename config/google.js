const scopes = process.env.GOOGLE_APP_SCOPES;

const google = {
    client_id: process.env.GOOGLE_CLIENT_ID,
    project_id: process.env.GOOGLE_PROJECT_ID,
    auth_uri: 'https://accounts.google.com/o/oauth2/auth',
    token_uri: 'https://oauth2.googleapis.com/token',
    client_secret: process.env.GOOGLE_CLIENT_SECRET,
    redirect_uris: [`${process.env.GOOGLE_REDIRECT_URI}`, 'http://localhost'],
    scopes: scopes,
};

module.exports = google;
