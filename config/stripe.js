// const plans = {
//
//     'plan_EhfekUCWmB0wge': {
//         name: 'Individual',
//         features: {
//             max_units: 100,
//         }
//
//     },
//     'plan_EhfexOZ3wBqnOE': {
//         name: 'Team',
//         features: {
//             max_units: 300,
//         }
//     },
//     'plan_Ehfd01KtWOUZY1': {
//         name: 'Pro',
//         features: {
//             max_units: 500,
//         }
//     },
// };

const stripe = {
    publishable_key: process.env.STRIPE_PUBLISHABLE_KEY,
    secret_key: process.env.STRIPE_SECRET_KEY,
    product_billing_id: process.env.STRIPE_PRODUCT_BILLING_ID,
    trial_max_units: process.env.TRIAL_MAX_UNITS || 20
};




module.exports = stripe;
