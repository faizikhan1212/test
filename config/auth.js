const app = {
    google_client_id: process.env.GOOGLE_CLIENT_ID,
    google_client_secret: process.env.GOOGLE_CLIENT_SECRET,
    token_secret: process.env.TOKEN_SECRET,
    token_expiry: process.env.TOKEN_EXPIRY,
    refresh_token_expiry: process.env.REFRESH_TOKEN_EXPIRY,
    token_expiry_for_remember: process.env.TOKEN_EXPIRY_FOR_REMEMBER,
};

module.exports = app;
