const io = {
    port: process.env.SOCKET_IO_PORT,
};

module.exports = io;