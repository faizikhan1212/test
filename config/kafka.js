const kafka = {
    broker_list: process.env.KAFKA_BROKER_LIST,
    topic_prefix: process.env.KAFKA_TOPIC_PREFIX,
};

module.exports = kafka;