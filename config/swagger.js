const swagger = {
    swagger_doc: process.env.SWAGGER_DOC,
    swagger_host: process.env.SWAGGER_HOST,
    swagger_port: process.env.SWAGGER_PORT,
};

module.exports = swagger;
