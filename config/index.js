const dot = require('dotenv');
dot.config();

const auth = require('./auth');
const app = require('./app');
const postgres = require('./postgres');
const redis = require('./redis');
const sendgrid = require('./sendgrid');
const aws = require('./aws');
const swagger = require('./swagger');
const file = require('./file');
const stripe = require('./stripe');
const socketIO = require('./socket-io');
const kafka = require('./kafka');
const microsoft = require('./microsoft');
const google = require('./google');
const allowedData = require('./allowed-data');
const mailgun = require('./mailgun');

module.exports = {
    auth,
    app,
    postgres,
    redis,
    sendgrid,
    aws,
    mailgun,
    swagger,
    file,
    stripe,
    socketIO,
    kafka,
    microsoft,
    google,
    allowedData
};
