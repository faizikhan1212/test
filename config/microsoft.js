const microsoft = {
    client_id: process.env.MICROSOFT_CLIENT_ID,
    client_secret: process.env.MICROSOFT_CLIENT_SECRET,
    site: 'https://login.microsoftonline.com/common',
    authorizationPath: '/oauth2/v2.0/authorize',
    tokenPath: '/oauth2/v2.0/token',
    redirect_uris: [`${process.env.MICROSOFT_REDIRECT_URI}`, 'http://localhost'],
    scopes: process.env.MICROSOFT_APP_SCOPES,
};

module.exports = microsoft;
