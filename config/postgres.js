const postgres = {
    username: process.env.POSTGRES_USERNAME,
    database: process.env.POSTGRES_DATABASE,
    password: process.env.POSTGRES_PASSWORD,
    host: process.env.POSTGRES_HOST,
    port: process.env.POSTGRES_PORT,
    pool: {
        idle: process.env.POSTGRES_POOL_IDLE,
        min: process.env.POSTGRES_POOL_MIN,
        max: process.env.POSTGRES_POOL_MAX,
    },
    dialect: 'postgres',
};

module.exports = postgres;
