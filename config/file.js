const file = {
    file_store: process.env.FILE_STORE,
    local_path: `./storage/${process.env.FILE_LOCAL_PATH}`,
    file_size: process.env.FILE_SIZE,
    csv_size: process.env.CSV_SIZE,
};

module.exports = file;
